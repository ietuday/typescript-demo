var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = /** @class */ (function () {
    function Person(fn, ln, dateOfBirth) {
        this.firstName = fn;
        this.lastName = ln;
        this.dateOfBirth = dateOfBirth;
    }
    Object.defineProperty(Person.prototype, "fullName", {
        get: function () {
            return this.firstName + this.lastName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "age", {
        get: function () {
            return (new Date().getFullYear() - this.dateOfBirth.getFullYear());
        },
        enumerable: true,
        configurable: true
    });
    Person.prototype.greet = function (friendName) {
        return "Hello " + friendName + ", how are you doing?";
    };
    Person.prototype.selfIntrodution = function () {
        var age = (new Date().getFullYear() - this.dateOfBirth.getFullYear());
        return "Hello, I am " + this.firstName + " " + this.lastName + " and I am " + age + "yrs old";
    };
    return Person;
}());
var Employee = /** @class */ (function (_super) {
    __extends(Employee, _super);
    function Employee(fn, ln, dateOfBirth, deptName, sal) {
        var _this = _super.call(this, fn, ln, dateOfBirth) || this;
        _this._departmentName = deptName;
        _this._salary = sal;
        return _this;
    }
    //Method overriding
    Employee.prototype.selfIntrodution = function () {
        var s = _super.prototype.selfIntrodution.call(this);
        return s + " working in " + this._departmentName + " department";
    };
    Employee.prototype.incrementSalary = function (amount) {
        this._salary += amount;
    };
    Object.defineProperty(Employee.prototype, "salary", {
        get: function () {
            return this._salary;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Employee.prototype, "departmentName", {
        get: function () {
            return this._departmentName;
        },
        set: function (newValue) {
            if (newValue == "")
                throw "Department cannot be null";
            this._departmentName = newValue;
        },
        enumerable: true,
        configurable: true
    });
    return Employee;
}(Person));
