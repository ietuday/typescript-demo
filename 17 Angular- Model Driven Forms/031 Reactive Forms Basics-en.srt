1
00:00:18,550 --> 00:00:27,490
Active forms also referred as model driven forms or mere pepsin What exactly are template driven forms.

2
00:00:27,760 --> 00:00:34,580
And in that we have seen how we can have fun elements binded to the properties of the component and

3
00:00:34,580 --> 00:00:39,060
we'll have to explicitly do that through various binding attributes.

4
00:00:39,350 --> 00:00:44,900
But in this particular case we are going to achieve the same thing in very simple way by making use

5
00:00:44,900 --> 00:00:46,610
of the reactive forms.

6
00:00:46,790 --> 00:00:53,420
So topic wise old beltways mode whether it is template will form and reactive forms will give you same

7
00:00:53,420 --> 00:00:59,810
result but as reactive forms are considered to be little more simpler when compared to the template

8
00:01:00,170 --> 00:01:00,970
forms.

9
00:01:01,280 --> 00:01:09,140
So what are reactive forms Anglo reactive forms facilitate or reactive style of programming that flavors

10
00:01:09,230 --> 00:01:15,090
explicit management of data flowing between in on you a data model.

11
00:01:16,010 --> 00:01:20,940
Typically retrieved from the server at a UI oriented form mode.

12
00:01:21,020 --> 00:01:26,260
So basically what we are going to do now is for each team and form which we are going to have an edge

13
00:01:26,270 --> 00:01:35,990
team and template we are going to have a form model in the component and the form component is supposed

14
00:01:35,990 --> 00:01:42,980
to have properties which will represent the different elements in the extremal complete.

15
00:01:42,980 --> 00:01:49,340
So that is how we react to forms with the help of form component is what we are going to develop the

16
00:01:49,340 --> 00:01:56,360
active forms of full ease or for the ease of using the active portal testing and validation.

17
00:01:56,570 --> 00:02:02,930
Yes these three things are comparatively much simpler and easy to use in reactive forms when compared

18
00:02:02,930 --> 00:02:11,250
to template driven forms with reactive forms you can create a tree of angular form control objects in

19
00:02:11,250 --> 00:02:17,460
a component class and bind them to the native form control elements in the cardboard template.

20
00:02:17,490 --> 00:02:24,960
As I said earlier for every input element we are going to have an object in the Component class and

21
00:02:24,960 --> 00:02:33,600
those objects will be of type form control and you can create and manipulate form control or object

22
00:02:33,600 --> 00:02:36,060
directly in a component class.

23
00:02:36,510 --> 00:02:42,840
I would say that essentially there are four plus three classes which are very important.

24
00:02:42,990 --> 00:02:49,920
One is form control form group and form the form control track the value and value validity status of

25
00:02:50,010 --> 00:02:57,600
individual form control whereas one group is basically a collection of form controls.

26
00:02:57,600 --> 00:03:03,680
Basically we can say it's a dictionary of form controls and form and it tracks the value and validity

27
00:03:03,690 --> 00:03:08,770
state of numerically indexed area of abstract control instances.

28
00:03:08,790 --> 00:03:13,230
Of course the parent of all the three is abstract control.

29
00:03:13,230 --> 00:03:20,620
Actually we can put it like this abstract control is the parent inherited from that is a form control

30
00:03:20,670 --> 00:03:28,870
form group unplumbed added These are the main form plus us no ID Ideally I would explain this with an

31
00:03:28,870 --> 00:03:30,140
example.

32
00:03:30,850 --> 00:03:34,370
So I have a project a template project created.

33
00:03:34,770 --> 00:03:39,750
So here I'll begin with I was having it means exclude them for Westfall.

34
00:03:39,850 --> 00:03:43,740
So let's say I bought them in page.

35
00:03:43,930 --> 00:03:49,510
I'll call it as part of some form Nottage DMM.

36
00:03:49,960 --> 00:03:54,950
And in this part some form but Steimle we want to make use of this form template.

37
00:03:55,030 --> 00:04:01,700
So basically we have a contact we are able to you know validate in order the purpose of Nobili that

38
00:04:01,970 --> 00:04:03,090
we want to suppress.

39
00:04:03,110 --> 00:04:08,180
We want to stop the default Validus and supported by Steimle fight.

40
00:04:08,210 --> 00:04:14,350
We don't want to have it Steimle 5 validation because we are going to apply angular relegations then

41
00:04:14,350 --> 00:04:18,700
we are using the plus form group on form control.

42
00:04:18,700 --> 00:04:19,800
What are these for.

43
00:04:19,940 --> 00:04:23,930
Yeah neither of bootstrap c this.

44
00:04:24,070 --> 00:04:30,820
So what I'll do is I'll copy bootstrap bcsi is this from one of my earlier project in the contents

45
00:04:30,820 --> 00:04:31,960
folder here.

46
00:04:32,440 --> 00:04:37,260
And I would like to refer to that in that index not its K.M. fine drag and drop

47
00:04:41,960 --> 00:04:44,500
and we can actually begin with the rule gets lonely.

48
00:04:44,580 --> 00:04:48,310
Put dot dot.

49
00:04:48,400 --> 00:04:52,810
So that isn't because of the bootstrap style we are going to get a better look and feel.

50
00:04:53,290 --> 00:04:56,420
We are mentioning in type text as required.

51
00:04:56,440 --> 00:04:58,670
And of course I want to use this.

52
00:04:58,990 --> 00:05:04,410
Let's say for Name First Name and I would like to use this for last name.

53
00:05:07,050 --> 00:05:09,360
So right now this is a simple form.

54
00:05:09,360 --> 00:05:13,460
We don't even have parts and form and person both up or some form.

55
00:05:13,710 --> 00:05:17,550
These are supposed to lead up property in the component plus.

56
00:05:17,670 --> 00:05:29,810
So now it's time to add to the project component plus I get a glass script that is worth some board

57
00:05:30,050 --> 00:05:33,950
form board component board because

58
00:05:36,740 --> 00:05:38,290
some form competition.

59
00:05:38,300 --> 00:05:42,750
So he had what is that we should do we need to have a form component here.

60
00:05:43,890 --> 00:05:49,930
So very basic skeleton I copy from the handle but the rest of it I prefer typing at the initial part

61
00:05:54,120 --> 00:05:54,900
all of this

62
00:05:59,700 --> 00:06:04,710
well here you understand what this component and the component class which we are going to write here

63
00:06:05,390 --> 00:06:12,090
next because this has some form component the component plus which we are writing it wants to implement

64
00:06:12,150 --> 00:06:20,990
on in it to us it should provide implementation or it method different implementation of that.

65
00:06:21,020 --> 00:06:24,150
I'll delete that and then replace it with my custom implementation.

66
00:06:25,960 --> 00:06:32,700
And then we have good form group form control and validators like no we don't need validators I'll need

67
00:06:32,690 --> 00:06:33,710
it later.

68
00:06:33,730 --> 00:06:38,460
These are all in angler's last forms Mordieu.

69
00:06:38,500 --> 00:06:41,380
So what is that we are willing to do you here.

70
00:06:41,410 --> 00:06:46,630
I would like to have forced representation for force.

71
00:06:46,670 --> 00:06:53,120
I mean I call that as of type form control that is very important.

72
00:06:53,140 --> 00:06:55,440
The data type is form control.

73
00:06:55,670 --> 00:07:00,010
Likewise I would like to have a last name which is also a paper form.

74
00:07:00,440 --> 00:07:06,110
Not all the form elements we are going to group together and I'll call that as part of some form and

75
00:07:06,120 --> 00:07:10,300
that is of state form group.

76
00:07:10,420 --> 00:07:16,030
So we are going to have one form group and within that form group we could have many controls but how

77
00:07:16,030 --> 00:07:17,420
do we create them.

78
00:07:17,520 --> 00:07:29,290
Ideally let us have two methods here created form controls where I can say Dizdar first name is equal

79
00:07:29,300 --> 00:07:37,160
to new form control does not last name.

80
00:07:37,180 --> 00:07:40,570
Is it called new form control.

81
00:07:43,500 --> 00:07:46,770
And likewise I will create fall

82
00:07:49,440 --> 00:07:50,640
in create form.

83
00:07:50,820 --> 00:07:56,940
I'll say this God person form is the Perdo new form group.

84
00:07:57,540 --> 00:08:01,780
And what is form blue one group is supposed to be a dictionary.

85
00:08:01,800 --> 00:08:03,540
All right.

86
00:08:03,790 --> 00:08:04,520
Is.

87
00:08:04,700 --> 00:08:05,610
So yet.

88
00:08:06,060 --> 00:08:10,630
I would like to have a first name call of the value of this.

89
00:08:10,650 --> 00:08:12,720
This does not force me.

90
00:08:13,290 --> 00:08:25,170
Likewise come up last name call them this door last so form group now has two properties.

91
00:08:25,170 --> 00:08:27,590
First name and last name.

92
00:08:27,720 --> 00:08:28,970
So these two properties.

93
00:08:29,010 --> 00:08:34,450
I'm going to use it in the form so now I have a template here.

94
00:08:34,450 --> 00:08:39,170
Person form the file name is changed for some form.

95
00:08:39,610 --> 00:08:40,850
Dutton's DML.

96
00:08:41,140 --> 00:08:43,230
No good apart from that it's.

97
00:08:43,390 --> 00:08:45,220
And what is that we have to do now.

98
00:08:45,490 --> 00:08:48,140
No precisely we have to do linking.

99
00:08:48,340 --> 00:08:51,820
We have to link form controls to the form template.

100
00:08:51,850 --> 00:08:58,810
How do we link that first in the form that we are supposed to Croyde form group attribute.

101
00:08:59,470 --> 00:09:07,510
Our directive form go with binding to boresome Parcelforce that's what we want.

102
00:09:07,870 --> 00:09:11,820
Suppose some form is made here binding it to this.

103
00:09:12,070 --> 00:09:14,010
No this is in its last year.

104
00:09:14,290 --> 00:09:20,760
So first I created form controls.

105
00:09:20,790 --> 00:09:23,790
Of course it should be does not.

106
00:09:25,050 --> 00:09:28,300
And then I should call this dog.

107
00:09:28,570 --> 00:09:29,640
Create for

108
00:09:33,030 --> 00:09:36,040
aren't in it is calling these two methods.

109
00:09:36,360 --> 00:09:42,820
First we are creating all the form controls and then we are creating a form group form group is associated

110
00:09:42,820 --> 00:09:51,820
with that form and yet will simply mention form role name.

111
00:09:51,970 --> 00:09:53,320
What is this name.

112
00:09:53,320 --> 00:09:56,900
The name which we have used in the first name and last name

113
00:10:03,810 --> 00:10:11,140
last and this is supposed to be parsel for the same value so that we can see what is the current status

114
00:10:11,260 --> 00:10:13,000
as with the current value.

115
00:10:13,110 --> 00:10:19,000
The things status will indicate whether it is valid or invalid and valid invalid when we decided based

116
00:10:19,000 --> 00:10:21,380
on this required attribute.

117
00:10:21,820 --> 00:10:24,500
Because both are required that it does not provide it.

118
00:10:24,550 --> 00:10:29,570
It will give us invalid not something very important.

119
00:10:29,580 --> 00:10:40,220
We'll have to go to some component parts some form is the select support some form write in room component

120
00:10:40,550 --> 00:10:45,200
component or some form.

121
00:10:45,520 --> 00:11:01,620
And we know no need to include this in Mordieu So all in all what part for some form combo and

122
00:11:05,990 --> 00:11:09,250
not last for some form.

123
00:11:09,890 --> 00:11:14,010
And same thing with incommodious or some form.

124
00:11:14,010 --> 00:11:22,440
Come on now here in the template we have used form control name we have used form groups.

125
00:11:22,530 --> 00:11:28,580
There are these directives these directives are used in our big in

126
00:11:31,780 --> 00:11:33,680
the active forms.

127
00:11:33,720 --> 00:11:34,400
More news

128
00:11:38,090 --> 00:11:46,580
which isn't Doc's last I read slush funds.

129
00:11:47,010 --> 00:11:53,780
This react do far more than we have done in import and that model parents we are Remember importer's

130
00:11:53,780 --> 00:12:01,170
supposed to be for all the models whose directives we want to make use of in Haiti templates.

131
00:12:01,610 --> 00:12:08,610
So we have a directive in this more you that is a form control name and formable.

132
00:12:09,020 --> 00:12:13,010
Hopefully it's all completed larkspurs controller 5 and exiguous

133
00:12:22,940 --> 00:12:23,610
and dusting.

134
00:12:23,660 --> 00:12:28,790
We've got first name last name right now the first name last name are.

135
00:12:29,090 --> 00:12:32,830
And this data is invalid quite name and first name are not the word.

136
00:12:33,050 --> 00:12:33,880
So let's give a name.

137
00:12:33,890 --> 00:12:41,410
You see as I'm typing the value the value of the object Jingjing so long and then what I need or can

138
00:12:41,410 --> 00:12:44,800
I simply make use of this first name last name.

139
00:12:46,160 --> 00:12:50,520
But it says this first name last name or it hits DML template.

140
00:12:50,770 --> 00:13:00,860
But these are also part of form combining was named last and that state that is what we are printing

141
00:13:00,860 --> 00:13:03,310
actually on the screen.

142
00:13:03,680 --> 00:13:10,610
Now very importantly here if I want I can give a default value for the control whatever we want as the

143
00:13:10,610 --> 00:13:12,460
default value that we can get.

144
00:13:12,890 --> 00:13:17,520
So now you see the compering first name last name.

145
00:13:17,520 --> 00:13:23,530
Default Value will be provided there is some deep and Sony and also the values are in text box.

146
00:13:23,610 --> 00:13:24,420
Can we change that.

147
00:13:24,450 --> 00:13:33,160
Yes as we change that immediately it the object does this first name last me which we can use it for

148
00:13:33,170 --> 00:13:37,040
for the programming maybe passing that letter to someone and so on.

149
00:13:38,740 --> 00:13:39,630
So let me repeat.

150
00:13:39,670 --> 00:13:42,170
Everything will take a lot of work.

151
00:13:42,180 --> 00:13:46,670
First we have seen that we have got a reactive forms.

152
00:13:46,680 --> 00:13:48,900
Why are they called reactive forms.

153
00:13:49,000 --> 00:13:58,900
Because the state of the component is directly connected to the form elements that the form offers ease

154
00:13:58,900 --> 00:14:02,540
of using reactive pattern testing and validation.

155
00:14:02,540 --> 00:14:09,210
None of this we have covered so far and we will do it with reactive forms you can create it three of

156
00:14:09,260 --> 00:14:11,230
I love home control objects.

157
00:14:11,240 --> 00:14:17,550
Of course we have not created any tree right now but as we proceed we will see some examples that we

158
00:14:17,550 --> 00:14:25,550
can have if one grew up inside the group and each form group can have multiple phone controls and each

159
00:14:25,550 --> 00:14:27,950
of this form control object.

160
00:14:29,020 --> 00:14:34,750
Are bind to the native form control and then the template.

161
00:14:35,410 --> 00:14:43,270
You can create an manipulate form control object directly in the Component class we have four important

162
00:14:43,270 --> 00:14:50,470
classes to consider here abstract control form control form group and from any we have reduced form

163
00:14:50,470 --> 00:14:51,750
control and form groups.

164
00:14:51,910 --> 00:14:57,610
Of course abstract control we cannot make use of because it's just having common behavior which is like

165
00:14:58,120 --> 00:15:02,660
something like observable matter in case there are some changes happening.

166
00:15:02,860 --> 00:15:08,540
How do you react to those changes that you are going to have and that we will see an abstract control

167
00:15:09,310 --> 00:15:15,220
but otherwise we have seen from control the represents the value and validate the status of an individual

168
00:15:15,220 --> 00:15:16,590
form control.

169
00:15:16,750 --> 00:15:24,400
It corresponds to that Steimle form controls that as an input box or a selected form group is nothing

170
00:15:24,400 --> 00:15:30,740
but a group all or a dictionary or one controls identified by name.

171
00:15:30,820 --> 00:15:35,420
And these names are supposed to be used in designing the ex-teammate template.

172
00:15:36,580 --> 00:15:42,390
And then we have created a simple form first then move on to creating a form component

173
00:15:45,220 --> 00:15:52,650
supposed to be part of some form component.

174
00:15:54,210 --> 00:16:03,230
We are creating a group with a dictionary of form controls and eventually we are linking it.

175
00:16:03,520 --> 00:16:09,680
Here we are doing the data binding you just mentioned that's properties non-Iraq doing in the database.

176
00:16:10,230 --> 00:16:18,230
So this being the state here it can be a value or it can be status it can be as you are requirement

177
00:16:20,180 --> 00:16:22,910
it can actually do the job.

178
00:16:25,610 --> 00:16:27,440
So to get proper spacing

179
00:16:34,150 --> 00:16:42,650
and then we have got some form that is the selector of the person form component and very very important

180
00:16:42,680 --> 00:16:49,250
we act before Modu is what we are supposed to import because of acting for model only has the directive

181
00:16:49,760 --> 00:16:51,750
form control name and form.

182
00:16:51,810 --> 00:16:53,230
There's two directives on it.

183
00:16:53,390 --> 00:16:54,830
We are far more.

184
00:16:55,370 --> 00:17:01,840
And obviously we have to also include in declarations the company.

185
00:17:01,930 --> 00:17:07,250
So that's all in this next topic we are going to see how we can work with more form controls on the

186
00:17:07,450 --> 00:17:08,640
properties.

187
00:17:08,640 --> 00:17:10,180
This is only the basic.

188
00:17:10,330 --> 00:17:10,870
Thank you.

