interface IPoint {
    X: number;
    Y: number;
    Z?: number;
    DistanceFromOrigin(): number
    DistanceFromPoint(point: IPoint): number;
}

var pt1: IPoint = {
    X: 10,
    Y: 20,
    DistanceFromOrigin(): number {
        return Math.sqrt((this.X * this.X) + (this.Y * this.Y));
    },
    DistanceFromPoint(point: IPoint): number {
        return Math.sqrt(((point.X - this.X) * (point.X - this.X)) + ((point.Y - this.Y) * (point.Y - this.Y)));
    }
}

var pt2: IPoint = {
    X: 100,
    Y: 200,

    DistanceFromOrigin(): number {
        return Math.sqrt((this.X * this.X) + (this.Y * this.Y));
    },
    DistanceFromPoint(point: IPoint): number {
        return Math.sqrt(((point.X - this.X) * (point.X - this.X)) + ((point.Y - this.Y) * (point.Y - this.Y)));
    }
}

class Point implements IPoint {
    X: number;
    Y: number;
    constructor(x: number, y: number) {
        this.X = x;
        this.Y = y;
    }
    DistanceFromOrigin(): number {
        return Math.sqrt((this.X * this.X) + (this.Y * this.Y));
    };
    DistanceFromPoint(point: IPoint): number {
        return Math.sqrt(((point.X - this.X) * (point.X - this.X)) + ((point.Y - this.Y) * (point.Y - this.Y)));
    }
}


pt1 = new Point(10, 20);
pt2 = new Point(100, 200);

alert(pt1.DistanceFromOrigin())
alert(pt1.DistanceFromPoint(pt2))

interface IMath {
    (n1: number, n2: number): number;
}

let add: IMath = function (n1, n2) {
    return n1 + n2;
}

let sub: IMath = function (n1, n2) {
    return n1 - n2;
}

class Demo {
    static CalculateAndPrint(m: IMath) {
        alert(m(10, 20))
    }
}

//Demo.CalculateAndPrint(sub)

/* ------------------------------------*/
interface IPoint1 {
    x: number;
    y: number;
}

interface IPointsArray {
    [arr: number]: IPoint1;
    length: number;
    toString(): string;
}

let points: IPointsArray;
points = [{ x: 10, y: 15 }, { x: 10, y: 15 }]
alert(points.length + " " + points["length"]);
alert(points.toString()) 