1
00:00:16,920 --> 00:00:24,480
Pure and impure pipes so pipes can be categorized as either B or pipes and impure pipes pure pipes are

2
00:00:24,600 --> 00:00:25,400
immutable.

3
00:00:25,410 --> 00:00:27,310
That means not tangible.

4
00:00:27,780 --> 00:00:31,660
Whereas in beer pipes up mutable that means danger.

5
00:00:32,370 --> 00:00:41,680
So what is your pipe a beer pipe secured only when the input value or the input parameters are changed.

6
00:00:41,760 --> 00:00:49,210
That means only when the input Tyndall's the pure pipe is going to execute not after that with asked

7
00:00:49,740 --> 00:00:57,450
to run a pipe independent of the condition which is applied for your pipe we need to set up your property

8
00:00:57,480 --> 00:01:00,150
to False.

9
00:01:00,180 --> 00:01:03,040
So basically we have a Prime Directive right.

10
00:01:03,060 --> 00:01:10,680
That is pipo meter reader in which we are supposed to pass an object that will have no more additional

11
00:01:10,680 --> 00:01:18,510
property which will have to set it to false because in your pipe tractor all that indice performances.

12
00:01:18,990 --> 00:01:23,440
But what if they wanted the tenders will be reflected immediately.

13
00:01:24,000 --> 00:01:26,500
So let's see this with the proper example.

14
00:01:26,530 --> 00:01:32,630
So I'll go back to the same project and here I would like to add a very simple pipe for demo purpose.

15
00:01:32,970 --> 00:01:41,960
There is joined by so join doc by theists.

16
00:01:42,080 --> 00:01:57,690
So I would like to write in bold by Karl Moore by transform from David AngloGold

17
00:02:00,350 --> 00:02:02,170
then we should write a custom by.

18
00:02:02,300 --> 00:02:11,910
So by and custom by we are going to provide an object and that object will have of course Name calling

19
00:02:11,960 --> 00:02:12,610
doesn't join

20
00:02:16,510 --> 00:02:18,950
and I would like to provide an extra property.

21
00:02:18,960 --> 00:02:22,380
Hope that is beyond this time.

22
00:02:22,390 --> 00:02:27,920
I would like to first level a BE or so so now that the class

23
00:02:30,560 --> 00:02:35,070
joined by Anton pipe implements

24
00:02:45,550 --> 00:02:57,700
by transforming and here will provide a transform what is that we want to transform something.

25
00:02:58,070 --> 00:03:02,180
And of course we would like to give string.

26
00:03:02,180 --> 00:03:06,990
So basically we want to actually transform it and read a string.

27
00:03:07,010 --> 00:03:10,570
So here we can get it.

28
00:03:11,050 --> 00:03:19,790
And now we can see why you Kodokan what unethical staring at it or join.

29
00:03:20,060 --> 00:03:23,250
And we would like to separate by lexical.

30
00:03:23,660 --> 00:03:33,420
We want somebody to that's a simple transponding know what I would like to do is in my component

31
00:03:37,310 --> 00:03:40,780
I would like to double up all day.

32
00:03:40,790 --> 00:03:45,440
I mean let's say we haven't added in which we would provide some data.

33
00:03:46,010 --> 00:03:59,360
Let's say for someone like that person to answer on this array I would like to transform and display

34
00:04:01,980 --> 00:04:07,260
ultralow that will give you that thought.

35
00:04:07,980 --> 00:04:15,340
And let's see it is.

36
00:04:15,890 --> 00:04:18,760
I would like to transform using John

37
00:04:24,170 --> 00:04:30,960
but to demonstrate that teens in this what I would like to have your one input

38
00:04:34,360 --> 00:04:45,340
say by text and will simply give some reference text me

39
00:04:48,890 --> 00:04:52,890
let's have a bottom and I click on the button.

40
00:04:53,270 --> 00:04:56,550
I would like to execute one function with it.

41
00:04:56,600 --> 00:04:58,650
I like them.

42
00:04:59,580 --> 00:05:00,630
And do I like them.

43
00:05:00,650 --> 00:05:05,510
I would like to posit the value of the text box symbol.

44
00:05:05,540 --> 00:05:06,440
That's.

45
00:05:08,610 --> 00:05:09,570
And of course

46
00:05:12,760 --> 00:05:14,080
I don't mean text

47
00:05:25,970 --> 00:05:30,930
just to have some new lines at the bottom.

48
00:05:34,110 --> 00:05:37,100
Now we need to write this function at a time.

49
00:05:37,380 --> 00:05:40,380
I like them.

50
00:05:40,600 --> 00:05:51,290
Well you and I would like to hook into the value of string that value.

51
00:05:51,330 --> 00:05:57,270
I would like to say can do so or dot laws.

52
00:05:57,600 --> 00:05:58,100
That's.

53
00:06:05,630 --> 00:06:09,480
Dispiritedly use.

54
00:06:09,750 --> 00:06:20,080
So every time I click on the bottom I'm actively making changes to the and because it is beyond the

55
00:06:22,210 --> 00:06:23,650
Let us see what happens.

56
00:06:30,040 --> 00:06:36,440
It's just to it is not a lot but it doesn't in any area.

57
00:06:36,450 --> 00:06:37,230
And we must do it

58
00:06:40,200 --> 00:06:41,620
good enough.

59
00:06:42,060 --> 00:06:52,440
Running in debug mode.

60
00:06:52,540 --> 00:06:54,060
Something went wrong.

61
00:06:59,240 --> 00:07:01,440
At the end of the expression you don't board

62
00:07:04,540 --> 00:07:06,270
thing is wrong.

63
00:07:06,570 --> 00:07:07,410
OK.

64
00:07:07,820 --> 00:07:11,500
This call them struggling.

65
00:07:12,690 --> 00:07:14,120
So stop debugging.

66
00:07:14,160 --> 00:07:15,330
Run once again

67
00:07:25,230 --> 00:07:26,580
that is that

68
00:07:30,020 --> 00:07:31,540
by joining could not be fun.

69
00:07:31,580 --> 00:07:32,110
Yes.

70
00:07:32,150 --> 00:07:37,010
The mistake I've done is I've not included join by NPR.

71
00:07:37,700 --> 00:07:42,340
So unless I include in the declarations I cannot make use of it.

72
00:07:42,830 --> 00:07:48,090
So it is joined by its joined by

73
00:07:50,530 --> 00:07:57,280
from daubs last joined by my good will.

74
00:08:00,930 --> 00:08:10,000
So stop debugging continue working.

75
00:08:10,040 --> 00:08:13,530
So here you see right now it has only two elements.

76
00:08:13,580 --> 00:08:16,060
Now I type B-tree.

77
00:08:16,280 --> 00:08:19,600
It's not actually before.

78
00:08:19,990 --> 00:08:22,190
Data is getting out into the attic.

79
00:08:22,640 --> 00:08:32,070
But even though that area is tending the pipe is not producing the of the output that's what is the

80
00:08:32,070 --> 00:08:34,800
limitation of your pipe.

81
00:08:34,980 --> 00:08:41,390
If your pipe execute only when the input value or the input parameters are changed.

82
00:08:41,690 --> 00:08:44,290
So right no input but value is not changing.

83
00:08:44,760 --> 00:08:45,840
So what will it do now.

84
00:08:45,870 --> 00:08:48,930
We want this to change this change to reflect.

85
00:08:48,930 --> 00:08:58,230
So the only change will make in the whole example is make the pipe as simple Destin's as the false lifemate

86
00:08:58,290 --> 00:09:00,000
and does the same program

87
00:09:02,950 --> 00:09:05,530
stop debugging run once again.

88
00:09:12,420 --> 00:09:23,810
Here I have not been free and it will immediately reflect your before inflicting the five.

89
00:09:24,270 --> 00:09:28,200
It will keep.

90
00:09:28,370 --> 00:09:31,760
So that is basic difference between pure by impure.

91
00:09:32,090 --> 00:09:41,850
If you want though that transformed valuable refl. immediately then go for your faults.

92
00:09:42,080 --> 00:09:49,260
But remember the benefit of your pipe is it is faster and applied only to quiet whereas if your pipe

93
00:09:49,640 --> 00:09:54,380
is going to be slower because in background lot of wiring is happening kind of balling is happening

94
00:09:54,410 --> 00:09:55,400
in back them.

95
00:09:55,550 --> 00:10:02,210
That is a basic limitation of impure pay cut performance is bought when compared to pure pie because

96
00:10:02,530 --> 00:10:07,010
cantinas tracking of changes is happening here.

97
00:10:07,010 --> 00:10:15,860
People ask me a very frequent question what exactly is the meaning of when the input value changes when

98
00:10:15,860 --> 00:10:17,320
the input value is done.

99
00:10:17,390 --> 00:10:19,000
Well I'm not in the know.

100
00:10:19,130 --> 00:10:20,510
Actually no.

101
00:10:20,550 --> 00:10:26,750
Right now we are having the same rate in the West in which we are pushing the top of what the input

102
00:10:26,750 --> 00:10:29,690
routine just means the array itself changes.

103
00:10:29,690 --> 00:10:37,550
For example this thought you are now create a new method in which I have the be

104
00:10:41,220 --> 00:10:44,450
C and so on.

105
00:10:48,610 --> 00:10:49,900
I now understand this

106
00:10:55,850 --> 00:10:56,770
too.

107
00:10:57,440 --> 00:10:58,760
So are we changing it.

108
00:10:58,790 --> 00:11:03,700
Yes we are thinking that it will reflect not.

109
00:11:03,880 --> 00:11:05,750
Let's see if I just click on the button

110
00:11:15,230 --> 00:11:16,430
again.

111
00:11:16,890 --> 00:11:23,970
This is change in the input even though this B or the 10 is immediately reflected.

112
00:11:23,970 --> 00:11:30,360
That is what precisely the meaning of the input value or input parameters are changed.

113
00:11:31,250 --> 00:11:34,470
Only then the pipe will come into effect.

114
00:11:34,490 --> 00:11:41,600
Otherwise pipe is not mutable that means not changeable so that the precise meaning of it.

115
00:11:42,160 --> 00:11:42,760
So that's all.

116
00:11:42,760 --> 00:11:46,060
With this we can do the job done by.

117
00:11:46,210 --> 00:11:46,780
Thank you.

