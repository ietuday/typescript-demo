let varname: number = 0;
let boolVar: boolean = true;
let myName: string = "uday";
let message : string = "";

message = `Hello ${myName}, How are you`;

console.log(message);

message = "This is Line1\
           \nThis is Line2\
           \nThis is Line3"

console.log(message);

let list: number[] = [1,23,45,74,36];
let list2: Array<number> = [1,5,78,64,28];
let list3: Array<string> = ["1","2","4","65","89"];
let readList: ReadonlyArray<number> = list;
let list4: Array<any> = [1,"strin",[1,2,3]];
let x: [string, number] = ['hello', 10];
enum Color { Red, Blue, Green=5};
let c:Color = Color.Green;
let a: any = false;
// let s: string = <string> a;//Casting method 1
let s: string = a as string;//Casting method 2
let lst: Array<string> = ["A","B","C","D","E","F"];
for(let lis in lst){
     console.log("key",lis + " " + "Value",lst[lis]);
}

for(let lis of lst){
    console.log("value",lis);
}
console.log(s);
console.log(list);
console.log(list2);
console.log(list3);
console.log(readList);
console.log(c);
console.log(x);
console.log(list4);






