import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-for-track-by',
  templateUrl: './ng-for-track-by.component.html',
  styleUrls: ['./ng-for-track-by.component.scss']
})
export class NgForTrackByComponent implements OnInit {
  collection:Array<Object>  = [];
  constructor() {
    this.collection = [{id: 1}, {id: 2}, {id: 3}];
   }

  ngOnInit() {
  }

  getItems() {
    this.collection = this.getItemsFromServer();
  }

  getItemsFromServer() {
    return [{id: 1}, {id: 2}, {id: 3}, {id: 4}];
  }

  trackByFn(index, item) {
    console.log(item ? item.id : undefined);
    
    return item ? item.id : undefined

    // or 

    // return index; // or item.id
  }

  trackHero(index, hero) {
    console.log(hero);
    return hero ? hero.id : undefined;

}


}
