1
00:00:18,510 --> 00:00:24,680
Custom directives in the beginning of our course we have seen that there are three types of directors

2
00:00:25,460 --> 00:00:32,260
component directives structural directives and approval directives component is what we have been using

3
00:00:32,260 --> 00:00:33,940
from the very first example.

4
00:00:33,970 --> 00:00:34,710
All the time.

5
00:00:34,810 --> 00:00:41,030
And we understood pretty well that these are component based structural directives.

6
00:00:41,030 --> 00:00:44,780
Also we have to use Blake and Dean for India if and so on.

7
00:00:45,690 --> 00:00:51,780
Likewise we have also seen building acrobatic dips like in this pile in the glass which are basically

8
00:00:51,780 --> 00:00:58,950
responsible for changing the appearance or behavior of an element in this particular system.

9
00:00:58,990 --> 00:01:06,160
I'm going to beat how exactly we can make custom attribute data to instruct using the built in that

10
00:01:06,380 --> 00:01:09,330
we would not want to build a custom actable data.

11
00:01:10,420 --> 00:01:18,280
And the same custom attribute concept milfoil extended to build a custom data which can be used for

12
00:01:18,400 --> 00:01:26,240
validation so first let us see how we can build a custom attribute data to build a custom.

13
00:01:26,390 --> 00:01:29,400
And then we have to write a class.

14
00:01:29,780 --> 00:01:37,580
And very important that class is supposed to have a direct directive decorator and this directive decorator

15
00:01:37,640 --> 00:01:42,290
is basically used for specifying what will be the directives selector.

16
00:01:42,350 --> 00:01:45,620
That is the attribute name.

17
00:01:45,760 --> 00:01:52,660
And once we have done that using custom actual data do we continue to color the background the font

18
00:01:52,660 --> 00:01:54,310
size etc..

19
00:01:54,490 --> 00:01:56,500
That means we can transform.

20
00:01:56,600 --> 00:02:05,550
Mean we can not transform but we can change the different appearance of the word all means what for

21
00:02:05,550 --> 00:02:14,400
the host element we can apply using element Tref we can get the reference to the host element and once

22
00:02:14,400 --> 00:02:20,800
we get the element that is the host element we can place twice the content background and so on.

23
00:02:21,930 --> 00:02:28,250
Then the lower class is built in service that provides an obstruction for entry manipulation.

24
00:02:28,260 --> 00:02:33,480
So these two things is what we are going to make use of element of render.

25
00:02:33,780 --> 00:02:38,440
So we are going to develop an example that is custom protected by name.

26
00:02:38,430 --> 00:02:47,970
So a lot of what we want to do it does show a lot of real data is when it is provided for a particular

27
00:02:47,970 --> 00:02:48,740
task.

28
00:02:49,080 --> 00:02:56,040
We would like to do some kind of basic formatting for the text inside the stack and we when we click

29
00:02:56,040 --> 00:02:59,690
on it we would like to display certain messages.

30
00:02:59,700 --> 00:03:01,380
So that is what we would like to do.

31
00:03:01,380 --> 00:03:04,380
In short a lot of custom data.

32
00:03:04,800 --> 00:03:06,910
So what is the procedure for that.

33
00:03:06,930 --> 00:03:09,910
Let's first begin with I think a file by name.

34
00:03:10,050 --> 00:03:14,010
So a lot dot dot dot dot thius.

35
00:03:14,090 --> 00:03:15,900
So in my app for that.

36
00:03:16,020 --> 00:03:18,180
I'll add a new item

37
00:03:28,760 --> 00:03:30,900
added typescript file.

38
00:03:31,240 --> 00:03:41,960
Now in this we are going to include the basic skeleton or so this is a custom that a we are going to

39
00:03:41,960 --> 00:03:47,720
use element ref for reference to the host element.

40
00:03:47,820 --> 00:03:53,490
Somewhere I'll show you how we can make use of Horsley's not and rendering is very important because

41
00:03:53,690 --> 00:03:57,920
we have seen that it is going to be used for entry manipulation.

42
00:03:57,920 --> 00:03:59,270
Basically it's applying styles.

43
00:03:59,280 --> 00:04:03,010
And so and then direct them.

44
00:04:03,770 --> 00:04:05,350
This is the same thing here.

45
00:04:05,960 --> 00:04:07,580
And the selector.

46
00:04:07,700 --> 00:04:11,990
So that means we don't want to make use of short a lot.

47
00:04:12,230 --> 00:04:16,560
All the things which you specified here are going to be applied.

48
00:04:16,700 --> 00:04:24,360
We have to begin with writing a constructor in this class so that in the constructor we can inject the

49
00:04:24,360 --> 00:04:27,050
element reference element mythically

50
00:04:31,820 --> 00:04:36,550
element and we can inject the

51
00:04:41,420 --> 00:04:47,550
so these two are automatically injected into the custom directive by the angular frame book.

52
00:04:48,050 --> 00:04:48,540
Yes.

53
00:04:48,620 --> 00:04:49,730
How were they injected.

54
00:04:49,730 --> 00:04:55,400
We will see all that in the dependency index and how that dependent injection actually works.

55
00:04:55,400 --> 00:04:56,800
That's what we love to see.

56
00:04:58,170 --> 00:05:00,630
Ones we have the constructor executed.

57
00:05:00,830 --> 00:05:09,890
We will like to use this evil and render variable for tending the style of the contained element.

58
00:05:09,930 --> 00:05:11,750
How are we going to do that.

59
00:05:12,000 --> 00:05:20,240
For this we have got a master set elements time using which we can set this type of thing.

60
00:05:20,760 --> 00:05:27,080
So here we are supposed to specify the element reference type name and the style value.

61
00:05:27,510 --> 00:05:38,380
So let's say I would like to go in my example then the dot set elements time.

62
00:05:38,610 --> 00:05:43,240
And what we want to do for which element ealdorman native element.

63
00:05:43,260 --> 00:05:51,170
Basically the tag Conemaugh the style I would like to set this say cursor undergirds that I would like

64
00:05:51,170 --> 00:05:52,880
to get the point closer.

65
00:05:53,090 --> 00:05:53,800
So when

66
00:05:56,610 --> 00:05:58,540
value like this.

67
00:05:58,540 --> 00:06:01,140
Maybe we would like to change the color also.

68
00:06:01,150 --> 00:06:05,950
So in such case for the same element we would like to say do you.

69
00:06:06,040 --> 00:06:16,110
So this is one way of setting that data same thing can also be achieved by writing you'll need element

70
00:06:16,620 --> 00:06:17,320
dog.

71
00:06:17,380 --> 00:06:25,440
Let's just say we want to set and which style we want to set up and this value argument.

72
00:06:26,220 --> 00:06:34,770
So lead to same result using we can achieve our data building that element reference also we can achieve

73
00:06:34,770 --> 00:06:35,720
the same.

74
00:06:35,950 --> 00:06:47,000
The next requirement is more when I did so all or the text were so low that it is used that text when

75
00:06:47,000 --> 00:06:50,520
I take some sort of message.

76
00:06:51,050 --> 00:06:55,810
And at the same time it will change the color on one.

77
00:06:56,300 --> 00:07:03,940
So for tending the color on what we all want to handle the event most over so far that we have in the

78
00:07:04,000 --> 00:07:10,160
rendered something called lissom surrender or not.

79
00:07:10,160 --> 00:07:13,170
Let's listen to what your dog.

80
00:07:13,230 --> 00:07:14,750
Native element.

81
00:07:15,140 --> 00:07:16,220
Come on.

82
00:07:16,400 --> 00:07:23,390
We have to specify the name of the even the name here is mouse over the mouse.

83
00:07:23,430 --> 00:07:24,420
Or what.

84
00:07:24,490 --> 00:07:30,740
What is the function do exactly that function will have to give it so we can make anonymous function

85
00:07:30,740 --> 00:07:34,340
name and get a result that can provide the implementation.

86
00:07:34,340 --> 00:07:36,800
And what I would like to do here is simple.

87
00:07:37,190 --> 00:07:39,300
I would like to change the color red.

88
00:07:39,320 --> 00:07:45,280
So the same thing I would like to change the color from your book palette.

89
00:07:45,310 --> 00:07:52,890
Likewise I would like to also probably almost always increase the font size so don't need development

90
00:07:52,890 --> 00:08:02,830
or style or color is equal to two colors or the font size so font size is equal to maybe twenty four

91
00:08:02,830 --> 00:08:03,740
point four.

92
00:08:05,590 --> 00:08:15,660
Similarly I would like to handle another word on the native element that is most out on most out.

93
00:08:15,680 --> 00:08:17,610
I would like to restore the default values.

94
00:08:17,630 --> 00:08:22,750
That means I would like to get the color back to blue and maybe font size default.

95
00:08:22,850 --> 00:08:24,990
Say something like 14.

96
00:08:25,460 --> 00:08:37,610
And here we can get the default value of font size font size to 14 pixels on 14 points on the most important

97
00:08:37,610 --> 00:08:41,870
thing is when I click on the text we would like to display so also.

98
00:08:42,320 --> 00:08:50,810
So click on the text another that is like that you know like we would like to display here.

99
00:08:53,010 --> 00:08:54,140
Look right now.

100
00:08:54,150 --> 00:08:56,800
It is the constant Hello world.

101
00:09:00,830 --> 00:09:02,740
So basically what are we doing here.

102
00:09:02,990 --> 00:09:12,340
When we are using short of what we are going to have instance of this automatically created construct

103
00:09:12,400 --> 00:09:22,640
that is it element for us is injected also that an object is injected element of friends will give me

104
00:09:22,650 --> 00:09:27,380
the reference to the on which we are going to make use of this.

105
00:09:27,440 --> 00:09:35,660
So a lot that Ms mitigate attribute and rendered is an object using which we are able to attain the

106
00:09:35,720 --> 00:09:38,510
state.

107
00:09:38,570 --> 00:09:44,950
So we are seeing the state properties the default properties and we are also handling that three events.

108
00:09:44,960 --> 00:09:52,440
Most of what was up and click like this in combination we are supposed to do some kind of extremal of

109
00:09:52,510 --> 00:09:55,620
putting know what is next.

110
00:10:01,070 --> 00:10:07,710
Next is I would like to know make the appropriate changes to the more do more.

111
00:10:07,760 --> 00:10:13,210
We have to mention that that is so here.

112
00:10:13,210 --> 00:10:13,980
All right.

113
00:10:14,010 --> 00:10:17,450
So a lot that

114
00:10:20,830 --> 00:10:23,130
the same thing which we haven't yet.

115
00:10:23,820 --> 00:10:36,010
So a lot directed on this solar activity in which while Im bored so that duct tip from the last so dot

116
00:10:36,120 --> 00:10:44,660
dot dot dot because no one's been mentioned in declaration the corresponding attribute.

117
00:10:44,740 --> 00:10:46,540
Now we can use it anywhere.

118
00:10:47,350 --> 00:10:52,060
So I would like to go to component the road.

119
00:10:52,640 --> 00:11:03,290
And in conclusion I would probably provide here some fantastic inspan that we can read some text

120
00:11:06,100 --> 00:11:06,330
or

121
00:11:09,220 --> 00:11:13,020
and for this I'll provide the preview.

122
00:11:13,350 --> 00:11:20,140
So although that's very important and that's it.

123
00:11:20,190 --> 00:11:21,580
No let us run and see

124
00:11:25,450 --> 00:11:26,680
once we understand all

125
00:11:30,370 --> 00:11:31,200
the project

126
00:11:39,190 --> 00:11:40,250
around it.

127
00:11:45,040 --> 00:11:48,620
And here something went wrong.

128
00:11:50,620 --> 00:11:51,970
What is that.

129
00:11:51,970 --> 00:11:52,660
Afterworld

130
00:11:56,110 --> 00:11:56,730
not swing.

131
00:11:56,740 --> 00:11:57,210
OK.

132
00:11:57,220 --> 00:12:04,000
Again the output is coming from the gas.

133
00:12:04,100 --> 00:12:08,140
This is a fantastic click.

134
00:12:08,510 --> 00:12:09,560
And hello world.

135
00:12:11,240 --> 00:12:14,400
So expand that given the attribute.

136
00:12:15,170 --> 00:12:23,910
So here you should understand you don't need element is this Doug.

137
00:12:24,310 --> 00:12:28,920
So now you should have pretty well understood all this piece of gold.

138
00:12:29,020 --> 00:12:35,670
What is important is when I right on displaying it constant Can I replace this with.

139
00:12:35,790 --> 00:12:37,540
Dot Net development.

140
00:12:37,660 --> 00:12:45,790
Did this reports that spanned back and inside Pontac I have enough text or the text inside the span

141
00:12:45,830 --> 00:12:48,060
back so I know

142
00:12:55,570 --> 00:12:59,650
I'll change the browser type itself to that scene.

143
00:13:08,280 --> 00:13:09,220
Click.

144
00:13:09,240 --> 00:13:16,260
This is a it's the most in most out 9:49 this time.

145
00:13:16,300 --> 00:13:18,100
And so on and on.

146
00:13:18,370 --> 00:13:31,840
Also for your reference just look at we have rendered talk set elements style we have used we have got

147
00:13:31,840 --> 00:13:39,750
separate element classes so any attribute set element properties so many other things are also there.

148
00:13:40,480 --> 00:13:45,200
You can set the text inside that element and so on and on.

149
00:13:45,430 --> 00:13:48,870
So is very very very helpful.

150
00:13:48,970 --> 00:13:57,190
For me you through which we are able to change the behavior behavior as well as the appearance of the

151
00:13:57,190 --> 00:13:59,790
corresponding element.

152
00:13:59,800 --> 00:14:02,400
So this is a small example.

153
00:14:02,560 --> 00:14:10,240
It speaks a lot about how we can actually make use of an attribute for defining the behavior and the

154
00:14:10,240 --> 00:14:12,620
appearance of the task.

155
00:14:12,710 --> 00:14:13,600
That's all it is.

156
00:14:13,780 --> 00:14:14,380
Thank you.

