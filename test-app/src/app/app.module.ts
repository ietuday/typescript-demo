import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NgForTestComponent } from './ng-for-test/ng-for-test.component';
import { Heroes } from './heroes';
import { Hero } from './hero';
import { NgForTrackByComponent } from './ng-for-track-by/ng-for-track-by.component';
import { DataBindingDemoComponent } from './data-binding-demo/data-binding-demo.component';
import { PersonComponent } from './person/person.component';
import { SizerComponent } from './sizer/sizer.component';
import { StudentComponent } from './student/student.component';
import { DestructureArrayComponent } from './destructure-array/destructure-array.component';
import { CssStyleDemoComponent } from './css-style-demo/css-style-demo.component';
import { LifeCycleHooksDemoComponent } from './life-cycle-hooks-demo/life-cycle-hooks-demo.component';
import { AlertSucessComponent } from './alert-sucess/alert-sucess.component';
import { AlertFailureComponent } from './alert-failure/alert-failure.component';
import { TempleteDrivenFormComponent } from './templete-driven-form/templete-driven-form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { PipesDemoComponent } from './pipes-demo/pipes-demo.component';
import { PipeExampleComponent } from './pipe-example/pipe-example.component';
import { AgePipe } from './age.pipe';
import { SalaryPipe } from './salary.pipe';
import { ReverseStringPipe } from './reverse-string.pipe';
import { JoinPipe } from './join.pipe';
import { ShowAlertDirective } from './show-alert.directive';
import { JokeComponent } from './joke/joke.component';
import { JokeListComponent } from './joke-list/joke-list.component';
import { CardHoverDirective } from './card-hover.directive';
import { CustomValidatorComponent } from './custom-validator/custom-validator.component';
import { UpperCaseValidator } from './upper.validator';

@NgModule({
  declarations: [
    AppComponent,
    NgForTestComponent,
    Hero,
    Heroes,
    NgForTrackByComponent,
    DataBindingDemoComponent, 
    PersonComponent, 
    SizerComponent, 
    StudentComponent, 
    DestructureArrayComponent, 
    CssStyleDemoComponent, 
    LifeCycleHooksDemoComponent, 
    AlertSucessComponent, 
    AlertFailureComponent, 
    TempleteDrivenFormComponent, 
    ReactiveFormComponent, 
    PipesDemoComponent,
    PipeExampleComponent,
    AgePipe,
    SalaryPipe,
    ReverseStringPipe,
    JoinPipe,
    ShowAlertDirective,
    JokeComponent,
    JokeListComponent,
    CardHoverDirective,
    CustomValidatorComponent,
    UpperCaseValidator
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AlertSucessComponent, AlertFailureComponent]
})
export class AppModule { }
