import { Component } from '@angular/core';
import { Employee, EmployeeType } from './Employee';
import { PersonBasicInfo } from './PersonBasicInfo';
import { AlertSucessComponent } from './alert-sucess/alert-sucess.component';
import { AlertFailureComponent } from './alert-failure/alert-failure.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  alert = AlertSucessComponent;
  myTime: Date = new Date();
  fontSize: number = 10;
  employee: Array<Employee> = [
    new Employee(1, "E1", 10000, EmployeeType.Daily),
    new Employee(2, "E2", 11000, EmployeeType.Contract),
    new Employee(3, "E3", 13000, EmployeeType.Permanent)
  ];

  selectedEmployee: Employee = this.employee[1];

  evtNameHandler(studName: string) {
    console.log("Hello" + studName);
    
  }
  evtPersonalHandler(PersonBasicInfo: PersonBasicInfo) {
    console.log("person",PersonBasicInfo);
    
    console.log("Hello " + PersonBasicInfo.FName + " " + PersonBasicInfo.LName);
  }

  changeComponent(){
    if(this.alert == AlertFailureComponent)
        this.alert = AlertSucessComponent;
    else
        this.alert = AlertFailureComponent;
  }
}
