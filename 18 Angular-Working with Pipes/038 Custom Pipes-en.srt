1
00:00:16,790 --> 00:00:23,400
Custom pipes in this particular session I'm going to demonstrate how we can develop a custom pipe for

2
00:00:23,460 --> 00:00:24,310
our equipment.

3
00:00:24,360 --> 00:00:31,510
That means for our input we would like to write our own logic for transforming and producing some are.

4
00:00:32,340 --> 00:00:36,730
So for writing a custom by we need to write a glass.

5
00:00:36,870 --> 00:00:42,350
What is very important is it should be decorated with pipe maker.

6
00:00:42,380 --> 00:00:47,360
That means you should have a pipe attribute associated with it which is supposed to have the name of

7
00:00:47,360 --> 00:00:55,800
the by to the pipe plus it's supposed to implement the pipe transform interface.

8
00:00:56,640 --> 00:01:03,760
And accept an input value followed by an optional palomino and returns the transformed.

9
00:01:04,130 --> 00:01:11,520
So pipeline's pump is going to provide does it matter the name of the metric transform transform matter

10
00:01:11,520 --> 00:01:13,420
is what we need to operate.

11
00:01:13,590 --> 00:01:19,390
This model is going to have as part of the input which it is supposed to accept.

12
00:01:19,740 --> 00:01:26,580
And the second parameter maybe that may not be that that is the bottom for me parameters which we would

13
00:01:26,580 --> 00:01:27,780
like to process.

14
00:01:28,140 --> 00:01:36,350
And of course the return time is supposed to be transformed by the they really want it is no argument

15
00:01:36,350 --> 00:01:39,050
to transform into every part of it.

16
00:01:39,080 --> 00:01:43,600
Of course if there are three parameters we would like to pass they'll be three extrapyramidal.

17
00:01:43,640 --> 00:01:45,780
Other than the basic input.

18
00:01:46,310 --> 00:01:48,790
So let us see now how to write a custom pipe.

19
00:01:49,010 --> 00:01:54,500
I would like to write something called as I say it's like a glass

20
00:01:58,560 --> 00:02:01,700
installed by God.

21
00:02:01,850 --> 00:02:04,050
Yes it is and what I want.

22
00:02:04,050 --> 00:02:05,160
All right.

23
00:02:05,190 --> 00:02:12,940
So first import on top of the pipe and the pipe transformer pipe is actually the directive which we're

24
00:02:12,980 --> 00:02:20,660
going to make use of the metal the top and pipe transform is the interface.

25
00:02:20,700 --> 00:02:25,320
And both of these are in the core of it.

26
00:02:25,440 --> 00:02:26,400
And good luck.

27
00:02:27,390 --> 00:02:28,530
Let's go

28
00:02:32,830 --> 00:02:33,870
perfect.

29
00:02:34,160 --> 00:02:36,920
No export gloss.

30
00:02:37,000 --> 00:02:48,890
I would like to eat bike and it might be supposed to be implement by transform and BYB transform is

31
00:02:48,920 --> 00:02:49,710
supposed to

32
00:02:53,710 --> 00:02:55,130
transform matter.

33
00:02:55,350 --> 00:03:00,710
And I guess that transformer is supposed to have value and a variable number of arguments.

34
00:03:01,020 --> 00:03:09,010
But in case we do not want any palominos we can remove this and provide our own custom implementation.

35
00:03:09,030 --> 00:03:10,470
So what is that we want to do.

36
00:03:10,470 --> 00:03:15,610
We want to take as input a date each means we want to take some deep.

37
00:03:15,680 --> 00:03:16,630
So I give.

38
00:03:16,860 --> 00:03:22,400
But we want to produce or put some string so small change to the transformer or as per the requirement

39
00:03:22,400 --> 00:03:27,020
we'll have to make.

40
00:03:27,090 --> 00:03:34,490
Now of course that is because of somebody done that with a little bit of you know.

41
00:03:34,900 --> 00:03:42,200
So what is that we should do for transforming Let's say I wouldn't like to read it in yours.

42
00:03:42,530 --> 00:03:44,430
How do they do it in yours.

43
00:03:44,670 --> 00:03:50,920
First I should get let's get to the offset.

44
00:03:51,390 --> 00:03:56,030
So I knew the door get tight.

45
00:03:56,280 --> 00:04:10,590
So we'll get the offset from first Jarboe late 1970s 0 0 0 seconds and get the offset of one part D

46
00:04:10,980 --> 00:04:11,520
offset

47
00:04:15,370 --> 00:04:19,330
that is nothing but value dord get tight.

48
00:04:20,200 --> 00:04:21,360
Let's get the difference.

49
00:04:21,360 --> 00:04:23,430
Now we want a difference.

50
00:04:23,430 --> 00:04:26,190
What does that mean to date.

51
00:04:26,240 --> 00:04:36,330
Offset minus rather than the way it should be to the offset minus the bottom into the offset.

52
00:04:36,960 --> 00:04:39,780
So we got different in terms of

53
00:04:42,730 --> 00:04:51,970
milliseconds not this will have to convert into the day and rather year so to convert into day 1 day

54
00:04:52,040 --> 00:05:06,960
is how much one day is thousand milliseconds it has in to 60 seconds into 60 hours many minutes into

55
00:05:07,010 --> 00:05:08,560
24 hours.

56
00:05:09,520 --> 00:05:16,280
So that's what this is what the milliseconds seconds minutes and hours.

57
00:05:16,680 --> 00:05:20,280
Well this one day I would like to convert it to you.

58
00:05:20,660 --> 00:05:22,050
What is that.

59
00:05:22,220 --> 00:05:37,220
Yours is equal to pay the difference divided by one day in 365 assuming that 365 days we have in it

60
00:05:37,920 --> 00:05:40,640
you.

61
00:05:40,660 --> 00:05:46,090
So now I would like to read and hear yours.

62
00:05:46,180 --> 00:05:47,300
Oncogenes.

63
00:05:47,480 --> 00:05:49,400
Let's say the right and you ears.

64
00:05:49,440 --> 00:05:58,990
So it's a simple eight prime plus but because we are going to pro-white know our director what is that

65
00:05:59,750 --> 00:06:00,350
each

66
00:06:03,130 --> 00:06:04,370
pipe.

67
00:06:04,490 --> 00:06:13,700
And here we should get an object the name of the bike is supposed to be provided with ageplay that it

68
00:06:16,260 --> 00:06:17,900
of course this should been single

69
00:06:24,460 --> 00:06:26,300
name.

70
00:06:27,500 --> 00:06:29,440
So now what is the next hour.

71
00:06:29,570 --> 00:06:33,440
Because it is a bike we would like to use in that template.

72
00:06:33,650 --> 00:06:41,450
We have to force it include that in the galleries and that means you're right age by what where is it.

73
00:06:41,590 --> 00:06:43,780
It is an import.

74
00:06:46,230 --> 00:06:47,280
It by

75
00:06:50,650 --> 00:06:57,520
from door slides is not by now.

76
00:06:57,560 --> 00:07:01,600
By that it is what we can make use of.

77
00:07:01,620 --> 00:07:05,850
So I already have here a book by that date of birth.

78
00:07:05,890 --> 00:07:10,790
Right now let's add one more.

79
00:07:11,030 --> 00:07:13,840
I would like to put that in sort of dead.

80
00:07:14,180 --> 00:07:15,840
I would simply put it.

81
00:07:15,880 --> 00:07:17,230
That's it.

82
00:07:17,490 --> 00:07:22,200
So last word in the boat is supposed to show me the number of iOS

83
00:07:27,790 --> 00:07:28,160
0.

84
00:07:28,180 --> 00:07:29,600
Something went wrong.

85
00:07:33,490 --> 00:07:34,910
1.4 lines you know.

86
00:07:34,970 --> 00:07:37,070
OK let's do one thing.

87
00:07:37,120 --> 00:07:43,390
The problem is here we will do the mad dog.

88
00:07:45,280 --> 00:07:46,810
Is getting us to practice.

89
00:07:46,980 --> 00:07:57,740
We can work that into non-facts and not on this Zillo ears because I've deacon here also has guaranteed

90
00:07:57,740 --> 00:08:00,590
only to give some propertied

91
00:08:03,530 --> 00:08:04,620
to you.

92
00:08:04,980 --> 00:08:05,190
It's

93
00:08:08,950 --> 00:08:09,530
coded

94
00:08:12,610 --> 00:08:14,830
your legs to.

95
00:08:15,440 --> 00:08:19,850
Maw Mundt Gama'a some day.

96
00:08:20,210 --> 00:08:22,550
That's a lot.

97
00:08:25,380 --> 00:08:27,040
It came in 16 years

98
00:08:30,570 --> 00:08:33,810
it is 2017.

99
00:08:33,890 --> 00:08:35,270
This is supposed to be normal.

100
00:08:35,390 --> 00:08:39,910
Or let's take December.

101
00:08:40,110 --> 00:08:43,250
And now this will be 216.

102
00:08:43,580 --> 00:08:46,550
This should be something wrong in the logic.

103
00:08:46,550 --> 00:08:48,900
You can actually put a break point and then debug it

104
00:08:58,000 --> 00:08:58,860
all.

105
00:08:59,250 --> 00:09:00,570
Stop debugging.

106
00:09:02,010 --> 00:09:04,240
To Chrome and then debug.

107
00:09:10,780 --> 00:09:11,940
Buffering.

108
00:09:12,290 --> 00:09:15,630
Well done.

109
00:09:19,660 --> 00:09:20,560
Gringoire.

110
00:09:24,530 --> 00:09:25,260
Get years

111
00:09:48,250 --> 00:09:50,520
lumber done to them.

112
00:09:50,790 --> 00:09:51,990
17.

113
00:09:52,180 --> 00:09:53,520
That's the system that

114
00:10:04,340 --> 00:10:04,920
16.

115
00:10:04,940 --> 00:10:07,220
Maybe because we have taken the following

116
00:10:10,700 --> 00:10:16,490
that probably should be comfortable saying that said what I want to just there is something wrong in

117
00:10:16,490 --> 00:10:21,020
the formula but what is important is this function is getting executed.

118
00:10:21,230 --> 00:10:27,410
And for this input we are able to produce some output and that is what is getting displayed in the browser

119
00:10:27,650 --> 00:10:30,010
that is important at this point.

120
00:10:30,170 --> 00:10:38,270
Of course we love to Google and get that great formula for getting the values in ilk's the same custom

121
00:10:38,270 --> 00:10:44,800
pipe we can for the external in case we want to provide some palmatum what to do.

122
00:10:45,180 --> 00:10:49,800
As I said earlier for paramita here the extra parameter has to be Amant.

123
00:10:50,150 --> 00:10:57,870
So let us see if we want to have something like format with use of type String what we will do is once

124
00:10:57,870 --> 00:11:14,490
we get data of course if format is null that means a bar format is not given in such case or if format

125
00:11:14,610 --> 00:11:18,390
is equal to Y.

126
00:11:20,630 --> 00:11:23,220
But that can be a Perkis also.

127
00:11:23,220 --> 00:11:30,530
So let's say lower case in the running of the format it is uppercase white or lowercase y.

128
00:11:30,530 --> 00:11:39,960
In such case I will say your is equal to or I can simply write it down.

129
00:11:40,700 --> 00:11:48,580
And here only if we can concatenate with its.

130
00:11:48,650 --> 00:11:54,110
But what if the former is

131
00:11:56,710 --> 00:11:57,620
lowercase

132
00:12:00,670 --> 00:12:02,430
let's say mother.

133
00:12:02,540 --> 00:12:06,980
So you have in such case what is the formula.

134
00:12:07,560 --> 00:12:08,750
The same thing.

135
00:12:10,730 --> 00:12:15,370
But in truth 365 letters do it pretty well.

136
00:12:15,380 --> 00:12:24,320
Of course that is also wrong because some months some months are only 30 and 28 also for demo purposes.

137
00:12:24,550 --> 00:12:26,770
Let's accept that for a while.

138
00:12:26,960 --> 00:12:36,780
Alex I will say we won't do that in days only so days.

139
00:12:36,910 --> 00:12:38,520
And this should be.

140
00:12:38,850 --> 00:12:41,430
They want.

141
00:12:41,850 --> 00:12:51,110
So we are now able to take as input format with Desmonte into how can we really don't put that component

142
00:12:52,690 --> 00:13:02,220
ended up or it call them and give you a single code that's important.

143
00:13:02,460 --> 00:13:08,990
If I do not give anything it will produce output in you know an X

144
00:13:15,940 --> 00:13:17,710
OK in debug mode.

145
00:13:17,960 --> 00:13:19,730
We'll remove the break point

146
00:13:25,640 --> 00:13:26,930
if we don't run in debug mode.

147
00:13:26,940 --> 00:13:29,390
It's taken from guys you see we got it

148
00:13:34,670 --> 00:13:36,980
do not go by the values accuracy.

149
00:13:37,040 --> 00:13:38,120
That is not important.

150
00:13:38,160 --> 00:13:45,570
What is important is yet able to take as input a format and that we are able to process it and according

151
00:13:45,570 --> 00:13:53,310
to that format we are providing all and Disfarmer does nothing but oh Philetus that means at the time

152
00:13:53,310 --> 00:13:59,800
of applying though we are able to get in is.

153
00:14:00,420 --> 00:14:06,090
If you want one more Paromita Yes you can put your all on and then something and that something can

154
00:14:06,090 --> 00:14:08,420
be the next parliamentarian.

155
00:14:08,600 --> 00:14:12,320
You can keep adding about as many as you like.

156
00:14:12,350 --> 00:14:13,740
So this is how we do.

157
00:14:14,270 --> 00:14:27,120
Because Tom pipes in hand I have taken one more example I have salary my salary by employment by transport

158
00:14:28,530 --> 00:14:33,360
and transport matter here is going to get all employees.

159
00:14:33,780 --> 00:14:40,170
But we would like to display only those employees will salary is greater than a particular number and

160
00:14:40,230 --> 00:14:42,510
less that a particular number.

161
00:14:42,690 --> 00:14:46,450
That means we are displaying employees in the range of salary.

162
00:14:46,850 --> 00:14:53,370
So we are taking all employees thoughtfully done where salary is greater than results.

163
00:14:53,910 --> 00:15:01,980
So that is pretty simple and in usage we have to play like this type salary and starting salary that

164
00:15:01,980 --> 00:15:05,430
is greater than and less.

165
00:15:06,240 --> 00:15:13,290
Likewise there is one more example in the Hundal reversing a spring here also we have transformed starting

166
00:15:13,410 --> 00:15:15,810
and ending to reverse.

167
00:15:15,810 --> 00:15:22,270
So we are taking the substring slicing it and reversing soapies practice.

168
00:15:22,270 --> 00:15:28,300
All these examples as indeed handled multiple examples are there for practice but what is important

169
00:15:28,300 --> 00:15:35,660
is in all cases whether we take each pipe or we dig employee pipe or we take sidetrip I mean the worst

170
00:15:35,760 --> 00:15:44,360
string pipe in all cases the rules are see that is a pipe with a glass decorated with bite on it.

171
00:15:44,390 --> 00:15:54,260
Yes this is the pipe the pipe implements pipe transform and implements dimeter transform transform it

172
00:15:54,350 --> 00:16:00,920
accepts as input the value followed by an optional part of me and under-tone the transformable.

173
00:16:01,100 --> 00:16:01,640
Yes.

174
00:16:01,790 --> 00:16:04,840
First is that then orbital parameters maybe that.

175
00:16:05,090 --> 00:16:10,990
And finally the reason Rittenband they really wanted this argument to transport method for each farmer

176
00:16:11,030 --> 00:16:14,460
to pass to the right for every bite.

177
00:16:14,470 --> 00:16:17,910
I mean to me it is not mombo.

178
00:16:17,970 --> 00:16:21,150
I mean it is not argument mentioned for the transport method.

179
00:16:21,600 --> 00:16:25,130
So this is all we are going to implement custom bikes.

180
00:16:25,320 --> 00:16:25,690
Thank you.

