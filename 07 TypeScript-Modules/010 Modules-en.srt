1
00:00:16,810 --> 00:00:25,300
Modules in typescript all the code which we have written in all the different chapters so far was in

2
00:00:25,360 --> 00:00:26,680
a single file.

3
00:00:27,130 --> 00:00:34,080
We had only one frame and everything I was writing in that one single file but in real time projects

4
00:00:34,090 --> 00:00:36,390
it is not going to happen like that.

5
00:00:36,390 --> 00:00:42,250
We are really going to have lots and lots of typescript code and writing all of that into one single

6
00:00:42,250 --> 00:00:43,840
file is not feasible.

7
00:00:44,380 --> 00:00:50,530
We will want to split that into multiple files but how are these files then going to connect with each

8
00:00:50,530 --> 00:00:54,500
other what from one file will be shared with another file.

9
00:00:54,880 --> 00:00:59,830
All this is what we are going to look at under the chapter model snow.

10
00:01:00,490 --> 00:01:03,260
So what is it you see right now.

11
00:01:03,280 --> 00:01:11,380
Everything which we laid in in fighting would be a function would be available might be an interface

12
00:01:11,770 --> 00:01:16,930
might be a plastic coloration or maybe a data type declaration.

13
00:01:18,440 --> 00:01:20,270
All these are global.

14
00:01:20,330 --> 00:01:25,130
That means they're accessible everywhere in the same file as well as in other files.

15
00:01:25,130 --> 00:01:27,800
There is no restrict at all.

16
00:01:28,320 --> 00:01:35,360
But now what we're going to do is with the help of the rules we will restrict their scope to a given

17
00:01:35,370 --> 00:01:43,740
Mordieu only those tapes which we want to be used in other modules we will explicitly marketers explore

18
00:01:44,650 --> 00:01:51,730
and those which are not marked as export will not be used in other markets and pretty obvious in the

19
00:01:51,800 --> 00:01:54,520
other models where we want to use those types.

20
00:01:54,580 --> 00:01:56,760
We are going to market import.

21
00:01:57,430 --> 00:02:04,550
So one more you has to export tapes only then those tapes can be used in other markets.

22
00:02:04,880 --> 00:02:11,450
And in the other model to use those types we can make use of import only then we can use them other

23
00:02:11,460 --> 00:02:18,890
ways we can or to export and import of is what is going to happen in modules.

24
00:02:19,100 --> 00:02:26,170
Modules are declaring that the relationship between models are specified in terms of import and export

25
00:02:26,530 --> 00:02:31,210
and very importantly it is done at a file level at a fine level.

26
00:02:31,210 --> 00:02:32,060
It is done.

27
00:02:32,410 --> 00:02:38,410
So what we are going to do now is one model is going to have a rule by and then probably it will make

28
00:02:38,470 --> 00:02:40,870
itself dependent on other models.

29
00:02:41,410 --> 00:02:49,700
So we can say ahead of file containing top level important export is considered as important.

30
00:02:49,750 --> 00:02:55,290
That means as of now for simplicity let us assume that one file is one more thing.

31
00:02:57,480 --> 00:03:02,550
Everything exported from that will be imported in other motives.

32
00:03:02,610 --> 00:03:07,010
So what is that which is going to really make this work.

33
00:03:07,170 --> 00:03:15,000
Here we are going to have something called Mordieu LAUDER And that is responsible for locating and executing

34
00:03:15,330 --> 00:03:20,160
all of the dependencies of a model before executing it.

35
00:03:20,160 --> 00:03:25,250
See if I want to use it Mordieu basically the exported types from that model.

36
00:03:25,530 --> 00:03:30,310
Really obvious this model by itself also might have important some other things.

37
00:03:30,900 --> 00:03:39,700
So when I know more do all the dependencies also have to be loaded and then only to be eventually executed.

38
00:03:39,990 --> 00:03:46,150
So from where all these things are going to be taken care of yes we will not need something called more

39
00:03:46,150 --> 00:03:53,100
dual loader which will be responsible for doing all these things in typescript.

40
00:03:53,390 --> 00:04:00,720
Any file containing toplevel module or explored is considered as a more important note is the syntax

41
00:04:00,720 --> 00:04:05,060
for exporting the Mordieu for every type that we want to export.

42
00:04:05,060 --> 00:04:11,910
We have to use the word export or at the end of the find we will have to write export and followed by

43
00:04:11,910 --> 00:04:16,290
that in floor bracket that tight knit any one of these two things can be used.

44
00:04:16,320 --> 00:04:23,460
And I'll explain it with an example a little but more important than that in every project where we

45
00:04:23,460 --> 00:04:29,400
want to work with our people we will know how it is configured just fine.

46
00:04:29,840 --> 00:04:35,610
Now what is this U.S. contractor testified basically as the name implies a typescript configuration

47
00:04:35,610 --> 00:04:36,980
file.

48
00:04:37,170 --> 00:04:40,080
It is from this particular type configuration file.

49
00:04:40,080 --> 00:04:42,700
The compiler is going to read information.

50
00:04:42,720 --> 00:04:49,150
It requires no one else that important information is called empty.

51
00:04:49,890 --> 00:04:57,810
So basically we are using empty Mordieu for compiling all our gold target is easy and with good fight

52
00:04:58,750 --> 00:05:05,550
source for debugging should be generated Rimmel comments in the final output which is generated do not

53
00:05:05,550 --> 00:05:07,230
generate any ideas.

54
00:05:07,320 --> 00:05:14,010
If there is an error and if the tape is not specified do not implicitly take it as any type.

55
00:05:14,370 --> 00:05:22,520
So all these are like inputs for the compiler to compile it compile it from B as does of course.

56
00:05:22,520 --> 00:05:23,340
What is this.

57
00:05:23,480 --> 00:05:32,610
If there is a project exclude all folders and mode more do we have to export exclude those two folders.

58
00:05:32,910 --> 00:05:36,010
Right now we are not interested in that.

59
00:05:36,070 --> 00:05:41,210
So now there is one very important thing which will have to do in our project.

60
00:05:41,260 --> 00:05:46,120
So now we create a new project as in Visual Studio

61
00:05:49,830 --> 00:05:50,750
20:17

62
00:05:54,620 --> 00:05:55,370
this time.

63
00:05:55,370 --> 00:05:59,330
I'm not going to make use of a new website.

64
00:05:59,330 --> 00:06:07,100
I'm going to use a proper project now and in this project I'm going to take a dot net VB obligation.

65
00:06:07,210 --> 00:06:08,160
Not at all it does.

66
00:06:08,190 --> 00:06:09,260
More news.

67
00:06:09,310 --> 00:06:15,730
The app will save it in our standard folder.

68
00:06:15,930 --> 00:06:17,930
Good demos will do.

69
00:06:19,600 --> 00:06:20,760
Because browse and go

70
00:06:24,790 --> 00:06:32,420
click on hold get we can any type of project even maybe MT is good enough.

71
00:06:32,460 --> 00:06:36,430
OK so this is going to create an empty project.

72
00:06:36,490 --> 00:06:39,100
This time we are not using empty web site.

73
00:06:39,100 --> 00:06:45,200
We are using empty project so it is going to be probably going to build and generate a deal.

74
00:06:45,820 --> 00:06:51,320
Now in this project yes I would surely need extremely fine so that let me add first

75
00:06:55,210 --> 00:07:01,590
new item Steimle blades will simply call it a demon.

76
00:07:02,750 --> 00:07:08,690
And before I got script documents let me add now to this project.

77
00:07:12,930 --> 00:07:16,250
Our diverse group configuration is fine.

78
00:07:16,530 --> 00:07:18,700
You can also do that from here.

79
00:07:19,100 --> 00:07:28,310
Typescript does some configuration file find that must be daz conflict is an only Anything else is

80
00:07:28,310 --> 00:07:29,110
not allowed.

81
00:07:29,420 --> 00:07:31,100
So all these are default options.

82
00:07:31,100 --> 00:07:37,280
The only option I need to add here is that modules we want to support him anymore.

83
00:07:37,580 --> 00:07:42,690
We do have commandeers also which is used and so many other models are also used.

84
00:07:42,740 --> 00:07:44,980
We are good enough with MDA at this point of time.

85
00:07:46,130 --> 00:07:54,010
Another thing you'll have to do is this project will have to include a file called required days.

86
00:07:54,280 --> 00:08:01,290
So first the Google search would require DOT does

87
00:08:04,020 --> 00:08:04,340
don't

88
00:08:08,360 --> 00:08:14,800
record or does not or the latest release blah blah blah.

89
00:08:14,880 --> 00:08:17,670
Here you'll have the link and we can download

90
00:08:22,360 --> 00:08:24,660
required to his comments.

91
00:08:26,050 --> 00:08:36,380
We're getting the latest which will save us that saved in my models for my project for basically.

92
00:08:36,440 --> 00:08:43,480
And then here we're going to include that by points already included

93
00:08:47,220 --> 00:08:48,260
all of good

94
00:08:51,340 --> 00:08:54,680
no let us be explains

95
00:09:02,060 --> 00:09:02,900
pipes.

96
00:09:03,260 --> 00:09:09,540
Now let us do a small program that is kind of little bigger than what we have been doing it and in a

97
00:09:09,540 --> 00:09:10,770
more organized form.

98
00:09:10,890 --> 00:09:17,440
I point out as I want won't let us see what is going to comprise of interface.

99
00:09:17,490 --> 00:09:18,290
I point

100
00:09:21,250 --> 00:09:32,210
and this is going to be made up of two members x which is a number of white which is a number.

101
00:09:32,740 --> 00:09:36,870
And let's say we also have distance from origin

102
00:09:41,590 --> 00:09:46,330
the return type of this is also smallmouth Texas.

103
00:09:46,380 --> 00:09:50,000
I want this file to be printed as it more do.

104
00:09:50,590 --> 00:09:56,690
So here I'm going to know the key word export Moumin they are the key word export.

105
00:09:56,740 --> 00:10:05,400
This is what this file is going to become more by not this interface again just for that purpose I will

106
00:10:05,430 --> 00:10:14,050
write one more interface for something like I said just for the one purpose will not keep anything in

107
00:10:14,050 --> 00:10:19,330
this no I'm going to add another fine to the project.

108
00:10:22,660 --> 00:10:25,970
A good fight we'll go to that point.

109
00:10:26,000 --> 00:10:33,710
Geass what point on does should have a class by name point I need to implement.

110
00:10:33,780 --> 00:10:38,100
I want what is the point is not available at this point of time.

111
00:10:38,100 --> 00:10:38,960
Why.

112
00:10:39,300 --> 00:10:45,940
Because we do not have I want global i point is no part of Mordieu.

113
00:10:45,990 --> 00:10:54,790
So what we have to do and I point out as I didn't export your I'm going to do an import in from rocket.

114
00:10:55,080 --> 00:10:56,450
What is that we want to import.

115
00:10:56,490 --> 00:10:57,600
I want

116
00:11:00,990 --> 00:11:03,970
we'll call it from the finding.

117
00:11:04,080 --> 00:11:07,990
What is important is the filename should not be having.

118
00:11:08,020 --> 00:11:11,980
I mean we can do that great dot slots and just

119
00:11:15,250 --> 00:11:16,660
the file name we don't.

120
00:11:16,670 --> 00:11:19,080
Extension that is important.

121
00:11:19,080 --> 00:11:23,850
Now you can see we have a USB port call this point that we have not implemented it.

122
00:11:24,000 --> 00:11:32,250
Let's implement Charcot is that we didn't just put the cursor on point and say implement interface and

123
00:11:32,250 --> 00:11:35,330
we can provide some relevant implementation here.

124
00:11:35,910 --> 00:11:37,770
Let me take that from the handle.

125
00:11:38,070 --> 00:11:43,310
My daughter Widodo excluded minus three is good.

126
00:11:43,400 --> 00:11:47,580
Why is Chris so we have to face now.

127
00:11:47,700 --> 00:11:49,330
Basically two models.

128
00:11:49,780 --> 00:11:52,710
One is eye point and the other is fine.

129
00:11:52,960 --> 00:11:58,480
So these things we would like to use in another project that they did once

130
00:12:02,000 --> 00:12:10,310
all find latest contender for these files corresponding to is dispells have generated so right click

131
00:12:11,480 --> 00:12:19,190
open folder in file explorer and you see a pointer is where India's the quarter.

132
00:12:19,310 --> 00:12:22,980
All this is called before when I exit.

133
00:12:23,270 --> 00:12:25,240
What is that right in the water.

134
00:12:25,240 --> 00:12:36,830
It's steaming we have to note the empty volume through the quadrant is the immediate Mordieu And if

135
00:12:37,260 --> 00:12:41,020
you will have to provide one file at the start of the flight.

136
00:12:41,460 --> 00:12:47,850
So what I'll do is in my project only I think no one more states can file.

137
00:12:47,880 --> 00:12:52,310
I'll call it the main door yes don't get me in the ass.

138
00:12:52,310 --> 00:13:03,080
My whole idea is to have a variable of type I want and it should refer to an object of class Point which

139
00:13:03,080 --> 00:13:05,240
is happening which is not happening right now.

140
00:13:05,570 --> 00:13:10,670
I won't point would be wrong to you so unless I import them here.

141
00:13:10,700 --> 00:13:13,760
I cannot make use of those types.

142
00:13:13,760 --> 00:13:16,350
So import import.

143
00:13:16,850 --> 00:13:24,500
What is that I want to import Fosler to say I want from God Slax I point.

144
00:13:25,000 --> 00:13:28,250
Likewise I should say import white

145
00:13:33,640 --> 00:13:34,360
from

146
00:13:38,470 --> 00:13:40,170
dog's last point.

147
00:13:44,340 --> 00:13:51,820
No you say not that no OK we're getting aedile yet because I did not mention export.

148
00:13:52,440 --> 00:13:57,370
So export point and then we will be able to use it to.

149
00:13:57,420 --> 00:14:09,860
To the that we can of course said point X somebody find out why somebody and allowed begins a point

150
00:14:09,980 --> 00:14:13,250
toward distance from origin.

151
00:14:13,640 --> 00:14:16,620
So we have three major files on one point God.

152
00:14:16,980 --> 00:14:24,910
I point out the as one God does mean that ideas these are small acts and spoiled by

153
00:14:31,140 --> 00:14:41,800
no what should I read in the water and GM when people were as good with authority as required thought.

154
00:14:42,090 --> 00:14:48,770
So in the current directory only required or just.

155
00:14:49,390 --> 00:14:54,650
But what I mention the stock up did that we did.

156
00:14:54,730 --> 00:14:57,900
I mean what does that mean.

157
00:14:58,290 --> 00:15:00,890
D s that is important.

158
00:15:01,050 --> 00:15:05,180
We have to prove mean that does not mean that he is the compiled word.

159
00:15:05,580 --> 00:15:09,690
Despite all of this from the program.

160
00:15:11,060 --> 00:15:12,900
The stimulus is going to open

161
00:15:17,420 --> 00:15:19,880
and you think we got a lot.

162
00:15:19,880 --> 00:15:21,260
All.

163
00:15:22,540 --> 00:15:26,910
Yes ideally speaking we should make the at the start that

164
00:15:29,700 --> 00:15:35,260
now I go back to a point I removed this from here.

165
00:15:36,740 --> 00:15:38,330
Rebuild the whole project.

166
00:15:40,170 --> 00:15:41,230
And we're getting into

167
00:15:44,250 --> 00:15:48,080
we cannot import because it is not exported.

168
00:15:48,490 --> 00:15:51,270
So what is the alternative here you can say export.

169
00:15:51,270 --> 00:15:59,400
At the end of the file export I point out that that is the alternative syntax either women on Bobbo

170
00:15:59,500 --> 00:16:06,550
along with the pipe itself or at the last of the fight we can put all the export statements no secret

171
00:16:07,390 --> 00:16:12,460
ballot and we should see that there are no compilation errors.

172
00:16:12,460 --> 00:16:15,160
So that is alternatives impact's.

173
00:16:15,180 --> 00:16:16,920
So here we are exporting.

174
00:16:16,980 --> 00:16:19,310
And here we are importing.

175
00:16:19,710 --> 00:16:28,050
So this is how a model can be used for dividing the all into multiple files and with proper import and

176
00:16:28,050 --> 00:16:33,930
export we can explicitly mentioned what should be important and what should be exported.

177
00:16:33,930 --> 00:16:36,600
All this is pretty well covered in our example in the

178
00:16:47,550 --> 00:16:54,150
what is the extension on import export our domestic sin taxes on so that when we are doing an export

179
00:16:54,780 --> 00:16:58,890
we have the advantage of mentioning the alternate name.

180
00:16:58,890 --> 00:17:11,760
That means here I can get I export as I point as I point in office just in case we want to or does not

181
00:17:12,720 --> 00:17:17,950
mean I don't but we are trying to export it does I point interface.

182
00:17:18,380 --> 00:17:21,020
So in this kind of case what should be used here.

183
00:17:21,600 --> 00:17:29,490
I've tried what interface and pretty obviously yet I should also mention it as I went into office.

184
00:17:29,500 --> 00:17:37,450
So all this interface name is I point but it is exported as a last name not even a last name because

185
00:17:37,930 --> 00:17:41,760
also it is going to be identified with this name only not this thing.

186
00:17:42,310 --> 00:17:54,910
So it is this name which is exported outside Lakeway if needed we can also have an extension.

187
00:17:55,070 --> 00:17:56,090
Let me show you here.

188
00:17:56,110 --> 00:18:07,590
Last what we can do is put all the modules in one single file and exported so to the project there are

189
00:18:07,660 --> 00:18:10,020
no oil exports but these

190
00:18:14,020 --> 00:18:26,020
and a typescript on export dollar bills and in all it's called Doherty's I'll put all that export makes

191
00:18:30,560 --> 00:18:34,470
and what is the advantage of doing this really simple.

192
00:18:34,490 --> 00:18:37,020
I mean don't be us.

193
00:18:37,440 --> 00:18:40,130
Let me bring him his first point.

194
00:18:40,270 --> 00:18:42,410
I want to call it as I point only

195
00:18:45,800 --> 00:18:52,240
so you're there is I point I point every word at this point.

196
00:18:52,620 --> 00:19:01,520
All has start here in Maine in trough writing multiple statements like this.

197
00:19:01,810 --> 00:19:07,670
We can simply say I'm bored stop from

198
00:19:12,730 --> 00:19:15,190
Dodd's last all schools

199
00:19:20,910 --> 00:19:24,380
but we will have to give some name here.

200
00:19:24,780 --> 00:19:35,200
Let's say as might more do some the so all things are important as my model or what we have as my model.

201
00:19:35,240 --> 00:19:36,220
Advantages.

202
00:19:36,380 --> 00:19:43,130
Not we will receive more name got plastic model name dot interface not like that we will prequalified

203
00:19:44,180 --> 00:19:47,430
So multiple types can be exported.

204
00:19:50,110 --> 00:19:55,030
And all these can be imported at once sharp by providing a name.

205
00:19:55,060 --> 00:20:01,060
This is important in this particular case and because we have provided a name in this particular syntax

206
00:20:01,330 --> 00:20:10,010
we will have to quantify prequalified though by buying it more you need Disney exactly has to be used.

207
00:20:10,060 --> 00:20:18,300
So really very handy especially if there are multiple modules and if they have same name this is hope

208
00:20:18,310 --> 00:20:20,410
we can resolve the ambiguity.

209
00:20:20,410 --> 00:20:21,510
This is my model.

210
00:20:21,580 --> 00:20:30,040
Like that we might model in which also we might have probably point or point but in a different context.

211
00:20:30,130 --> 00:20:32,190
Not this point and not disappoint.

212
00:20:32,200 --> 00:20:39,710
Maybe some other declaration so name can be resolved pretty well by making use of this particular syntax.

213
00:20:40,310 --> 00:20:43,770
Now let us see something called re-export.

214
00:20:43,910 --> 00:20:45,900
Now what is this re-export.

215
00:20:45,940 --> 00:20:47,490
I'll take it to you.

216
00:20:47,770 --> 00:20:49,220
Here we got a point.

217
00:20:49,230 --> 00:20:52,580
Good ideas and what we are exporting is also.

218
00:20:53,110 --> 00:20:55,180
I point I point is the interface.

219
00:20:55,190 --> 00:21:00,330
Your point is only exported that we are using in point.

220
00:21:00,610 --> 00:21:09,250
So your point which is being imported and we export point of wherever I'm going to import the point

221
00:21:09,820 --> 00:21:11,440
I can only use point.

222
00:21:11,470 --> 00:21:13,750
I cannot use iPhone 4.

223
00:21:13,770 --> 00:21:20,950
I will have to use point here so that two different files two different modules but what I can do is

224
00:21:21,790 --> 00:21:25,280
in point I can see it export.

225
00:21:25,370 --> 00:21:30,440
I want so point is actually importing.

226
00:21:30,440 --> 00:21:38,050
I am also exporting what is the advantage of doing this that what does it mean.

227
00:21:38,230 --> 00:21:42,960
Let's go back to the old syntax.

228
00:21:42,990 --> 00:21:45,370
We don't have a name now.

229
00:21:45,850 --> 00:21:54,850
When I write mean I don't have to mention two different types here I mean two different times I don't

230
00:21:54,850 --> 00:22:02,080
have to means I can come this and yet Excel.

231
00:22:02,500 --> 00:22:09,590
I can write I want so from point where importing I point at point.

232
00:22:09,670 --> 00:22:11,680
But that's the basic meaning.

233
00:22:12,100 --> 00:22:14,080
So why is importing.

234
00:22:14,090 --> 00:22:16,770
I am also exporting.

235
00:22:16,880 --> 00:22:25,750
I along with point very very very interesting syntax so that one take in one single place we are able

236
00:22:25,770 --> 00:22:28,780
to control multiple types.

237
00:22:28,790 --> 00:22:33,390
Also one more interesting thing is in writing like this.

238
00:22:33,560 --> 00:22:34,920
I want to use both.

239
00:22:35,090 --> 00:22:37,220
I point and point.

240
00:22:37,220 --> 00:22:40,490
Qualified by some mean common name.

241
00:22:40,490 --> 00:22:50,940
So in that case I can say import start as lets say more and more from that last point.

242
00:22:53,090 --> 00:22:57,320
I don't need something like all oil exports.

243
00:22:57,350 --> 00:22:58,580
This is not me.

244
00:22:59,280 --> 00:23:01,240
So no one is going to get both of them.

245
00:23:01,250 --> 00:23:02,990
I can quantify it with seeming

246
00:23:09,170 --> 00:23:15,970
all of the things from point of being imported but it should be qualified.

247
00:23:15,980 --> 00:23:19,350
But more importantly this is an excellent index.

248
00:23:19,750 --> 00:23:27,820
But if I would if I was using some pretty fine I had to write in Woodstock as my model one from Dart's

249
00:23:27,820 --> 00:23:28,530
last.

250
00:23:28,950 --> 00:23:35,220
Point an import start as by model do like that we will have tried but not because of this common.

251
00:23:35,630 --> 00:23:39,650
We can take this and we don't even have to write a separate file called as all imports.

252
00:23:39,650 --> 00:23:41,620
That's also one not the quantity.

253
00:23:42,110 --> 00:23:47,580
So I would strongly recommend to use this particular syntax because we will have the qualifier.

254
00:23:47,630 --> 00:23:52,820
But if we are not mood to qualified you know that there is no ambiguity going to happen then happily

255
00:23:52,820 --> 00:23:55,070
I can make use of the syntax.

256
00:23:55,820 --> 00:23:59,480
Let's not look at the next big default example.

257
00:23:59,870 --> 00:24:07,580
But what is this default export point plus that means in one of the things that is exported from a file

258
00:24:07,970 --> 00:24:11,280
we can use the word default.

259
00:24:11,330 --> 00:24:19,550
For example in point we are exporting two things but here are the great export default.

260
00:24:19,650 --> 00:24:22,790
And what is the advantage of mentioning export default here.

261
00:24:25,340 --> 00:24:29,350
In Maine that is not point is not point.

262
00:24:29,450 --> 00:24:31,340
Basically it is not default.

263
00:24:31,700 --> 00:24:37,140
So they are putting this to default because it's a default class.

264
00:24:37,250 --> 00:24:39,540
It's a different type in a given more.

265
00:24:39,820 --> 00:24:45,290
So therefore the default type in a given model we can simply used this note on this.

266
00:24:45,290 --> 00:24:47,250
And we should still get the same result.

267
00:24:48,400 --> 00:24:49,050
Fantastic

268
00:24:52,040 --> 00:24:54,530
another syntax is here.

269
00:24:55,060 --> 00:25:01,520
At the time of using we can import and give some name from the file.

270
00:25:01,530 --> 00:25:02,770
That it.

271
00:25:02,890 --> 00:25:07,150
That means I will revert back to the old syntax where I use this

272
00:25:12,040 --> 00:25:27,040
it's not point it is diff one.

273
00:25:27,170 --> 00:25:31,010
So no advantage is what we get played like this.

274
00:25:35,740 --> 00:25:40,140
Defog So it's my choice exactly.

275
00:25:40,310 --> 00:25:44,480
I want to use qualifier al-Sayed qualified default if I want.

276
00:25:44,520 --> 00:25:51,410
I don't want to use use your default as give some name and that name can be used if you don't want to

277
00:25:51,410 --> 00:25:52,990
write in the same file.

278
00:25:56,750 --> 00:26:03,410
Let's say like this if you want to do I have a point also coming from my point and from point I would

279
00:26:03,410 --> 00:26:11,130
like to direct use default only so you don't even need to mention default as just this is you know so

280
00:26:11,130 --> 00:26:18,240
from here whatever it is or not taking again again we should not give brackets

281
00:26:25,160 --> 00:26:29,870
but automatically from this what is default that will be just.

282
00:26:29,940 --> 00:26:32,760
So in point is what is default.

283
00:26:33,040 --> 00:26:39,240
Point is default and that point will be default for Nimely not the big deal if you can do anything ABC

284
00:26:39,240 --> 00:26:42,800
x y z your choice whatever you like you can get over here.

285
00:26:43,530 --> 00:26:49,610
So this is how Also you can have the concept of default import and export.

286
00:26:50,160 --> 00:26:57,660
And coming to know the final thing we can have in boutique will do an export equal to Sendak's So we

287
00:26:57,660 --> 00:27:00,430
have two models others surely will come.

288
00:27:00,480 --> 00:27:08,630
And in general we have the concept of export object which contain all exports from Ahmadu exporting

289
00:27:08,630 --> 00:27:15,690
all those in that specifies a single object that is exported from the Mordieu that is imported with

290
00:27:15,690 --> 00:27:16,510
exports in.

291
00:27:16,530 --> 00:27:19,670
We will be able to specify only that particular type

292
00:27:24,270 --> 00:27:32,220
syntax if a single object is exported this can be classed into this namespace function or when importing

293
00:27:32,250 --> 00:27:39,640
we use export equal to typescript said We have exported using exporting at the time of importing we

294
00:27:39,640 --> 00:27:44,110
have to use import Mordieu is equal to the requirement.

295
00:27:46,490 --> 00:27:53,690
So like this export point equal the syntax is used not to import import.

296
00:27:53,730 --> 00:27:56,470
Point is equal don't require it.

297
00:27:57,000 --> 00:28:03,170
So this is also one of the Sendak's which can be used in a synchronous more dual definition in it's

298
00:28:03,170 --> 00:28:03,690
own choice.

299
00:28:03,690 --> 00:28:05,760
Actually we want to or we don't want to.

300
00:28:05,790 --> 00:28:11,300
This is also one of the export and import indexes.

301
00:28:11,720 --> 00:28:18,100
Finally we are going to see some concept of namespace that is actually the next topic.

302
00:28:18,230 --> 00:28:26,170
But before that only I want to warn you that when we are first moving to model based organization a

303
00:28:26,260 --> 00:28:34,310
common tendency is to prop up exports in an additional layer called namespace and that is not recommended.

304
00:28:34,310 --> 00:28:43,010
Think models have their own all and only exported declarations are visible from outside the model and

305
00:28:43,010 --> 00:28:48,470
because of this particular factor because modules itself have their own school which should not have

306
00:28:48,540 --> 00:28:56,100
namespace we don't have much scope for using namespace when we have the concept of Mordieu.

307
00:28:56,180 --> 00:29:00,210
Of course I'll come back to this when I'm talking about namespace in the next topic.

308
00:29:00,620 --> 00:29:03,760
But for now it's a very very important topic.

309
00:29:03,800 --> 00:29:11,410
From the typescript point of view as well as in when we are going to make use of this rather anywhere

310
00:29:11,420 --> 00:29:12,850
we want to make use of it.

311
00:29:13,040 --> 00:29:19,690
We want to use typescript library which is written and all that will be in different modules.

312
00:29:19,910 --> 00:29:25,700
So you should have a clear understanding of how to export about you and how to import a model and various

313
00:29:25,790 --> 00:29:27,300
syntax related stuff.

314
00:29:27,650 --> 00:29:28,230
Thank you.

