1
00:00:16,310 --> 00:00:22,350
Optional and default values for the parameters of the function when we're writing a function we can

2
00:00:22,350 --> 00:00:31,660
have a paramita which can be having a value which is distracted from another area or object.

3
00:00:32,060 --> 00:00:37,830
So in that kind of case how can we provide an optional artifact writing for the better meters.

4
00:00:37,920 --> 00:00:40,670
Let us see this with a proper example.

5
00:00:40,700 --> 00:00:50,400
So let me right now if function let's call it a school and here I'll get a permit of some one and let's

6
00:00:50,400 --> 00:00:55,760
see it is of type some object which has two members.

7
00:00:56,010 --> 00:00:58,260
One is string.

8
00:00:58,470 --> 00:01:03,280
And another is let's say number up which is the.

9
00:01:04,140 --> 00:01:12,200
And here I just want to write long first what I want to basically do here is the value of b and assign

10
00:01:12,240 --> 00:01:13,790
to another variable.

11
00:01:13,920 --> 00:01:15,380
So how can we do that.

12
00:01:15,430 --> 00:01:17,390
Left to go for object is proctoring.

13
00:01:17,720 --> 00:01:22,810
So in object this pattern we can simply write a column A B is equal to be one.

14
00:01:23,790 --> 00:01:34,200
And then I can show a lot the value of a and b for all this food what will I pass.

15
00:01:34,370 --> 00:01:36,770
I have to pass an object which has two properties.

16
00:01:36,780 --> 00:01:49,620
He looks at the Values string B undervalues And so I'm calling foo will evil A and B will be assigned

17
00:01:49,630 --> 00:01:55,380
to this n b which is eventually did in P one.

18
00:01:55,790 --> 00:02:02,720
So people object B wondered is it B and B is done that really are two variables and those will be displayed

19
00:02:02,720 --> 00:02:06,970
in the message box that is followed and 10 will be displayed.

20
00:02:06,970 --> 00:02:09,480
Good enough.

21
00:02:11,690 --> 00:02:18,580
What I want to do is let's say sometimes for some reason I will not be able to pass the value of a what

22
00:02:18,580 --> 00:02:20,660
if I cannot pass the value of it.

23
00:02:20,900 --> 00:02:26,050
Obviously it is going to be a compilation error because the required object is supposed to have two

24
00:02:26,050 --> 00:02:32,980
properties A and B is expecting values for both in B but to get rid of this error one thing we can do

25
00:02:32,980 --> 00:02:37,880
is simply make be as optional by putting your question mark.

26
00:02:37,970 --> 00:02:45,430
The paramita be now becomes optional not paramita precisely actually the member is not optional.

27
00:02:45,670 --> 00:02:48,240
So now you say there is absolutely no problem.

28
00:02:48,250 --> 00:02:56,270
So now we are providing value for me which is uppercase the so you need not even be assigned to it so

29
00:02:56,330 --> 00:02:57,630
it will be displayed here.

30
00:02:57,920 --> 00:02:59,660
But are we giving value for B.

31
00:02:59,690 --> 00:03:00,270
No.

32
00:03:00,440 --> 00:03:06,430
So in such cases this will be undefined so it will be assigned to this B which will be undefined.

33
00:03:06,440 --> 00:03:09,410
So what we're going to get as a result is it an undefined

34
00:03:12,590 --> 00:03:21,000
and undefined but in case we don't want undefined What is the solution we have multiple options or that

35
00:03:21,090 --> 00:03:22,540
one of the solution is here.

36
00:03:22,540 --> 00:03:28,180
So if I can say at the time of this proctoring I can say it is equal to a hundred.

37
00:03:28,430 --> 00:03:30,000
So how do you read this.

38
00:03:30,210 --> 00:03:35,810
If you want or B is undefined to be undervalued.

39
00:03:36,170 --> 00:03:42,570
But if we want but God b is not undefined then B will not be should be assigned to this b.

40
00:03:42,580 --> 00:03:44,380
Let's look at it.

41
00:03:44,440 --> 00:03:48,750
So we are going to get no one here and number at the same time.

42
00:03:49,490 --> 00:03:51,080
Let us get a copy of this

43
00:03:54,890 --> 00:03:55,790
one.

44
00:03:55,950 --> 00:04:02,120
Let us call it a and b are explicitly set of and you know let's say one for the first one.

45
00:04:02,120 --> 00:04:04,340
I'm going to get it in hundred.

46
00:04:04,590 --> 00:04:06,140
And the second one even and one

47
00:04:09,700 --> 00:04:16,360
like this we are able to provide our default value at the time of destructuring.

48
00:04:16,560 --> 00:04:21,420
That is the advantage at the time of this proctoring if the member is not present.

49
00:04:21,520 --> 00:04:23,450
What is the value we want to get.

50
00:04:23,470 --> 00:04:27,060
We are able to provide that.

51
00:04:27,430 --> 00:04:36,780
Now what you should know is just for the sake of readability we can use by and give your time say something

52
00:04:36,780 --> 00:04:40,740
like C is equal to this thing.

53
00:04:40,740 --> 00:04:47,130
This whole thing I cut from here put it here instead.

54
00:04:47,550 --> 00:04:48,750
I see.

55
00:04:49,400 --> 00:04:56,360
So I'm saying no one is of type C but what is C C is representing this.

56
00:04:56,380 --> 00:04:57,790
Dr..

57
00:04:57,960 --> 00:05:03,270
This is precisely like typedef from Stalybridge which we have in C programming language.

58
00:05:03,660 --> 00:05:05,800
Otherwise in simple language I can see it.

59
00:05:05,820 --> 00:05:12,430
This C is a placeholder for this but that's up.

60
00:05:12,470 --> 00:05:14,740
The result is going to be simple no changes.

61
00:05:16,830 --> 00:05:24,960
Now my next requirement is I want to be able to call the function fool like this.

62
00:05:24,960 --> 00:05:30,580
Also that means I should be able to cope without passing any data as an object.

63
00:05:30,950 --> 00:05:35,660
Well how will I do that but it is also absolutely disposable.

64
00:05:37,260 --> 00:05:39,360
When it is optional.

65
00:05:39,560 --> 00:05:44,480
In this case 500 it will be worth if the debt is not passed undefined.

66
00:05:44,540 --> 00:05:49,020
So if it is undefined if you want to you can provide a default value versus something like this.

67
00:05:49,920 --> 00:05:50,810
Immediately.

68
00:05:53,750 --> 00:05:55,600
So you can have something like this.

69
00:05:55,600 --> 00:05:56,390
Precisely.

70
00:05:58,890 --> 00:05:59,180
No.

71
00:05:59,230 --> 00:06:01,000
Let us expand this a little more.

72
00:06:01,300 --> 00:06:02,790
I want to read the same function.

73
00:06:02,790 --> 00:06:03,440
No.

74
00:06:03,880 --> 00:06:05,640
But in a little different way.

75
00:06:05,980 --> 00:06:07,650
Except it is for one.

76
00:06:07,900 --> 00:06:18,010
And instead of giving you what I would like to put this this proctoring A and B here would do for what

77
00:06:18,040 --> 00:06:18,620
you were in.

78
00:06:18,630 --> 00:06:26,240
We also want to specify what is that a flawed record for it.

79
00:06:26,270 --> 00:06:32,990
We can get a default value let's say district and for B we can a default value Clarissa's Z.

80
00:06:35,560 --> 00:06:37,090
So you don't have to actually do this.

81
00:06:37,080 --> 00:06:39,220
No not way.

82
00:06:39,310 --> 00:06:44,560
So we have got two variables that let it be bought have the default values

83
00:06:50,970 --> 00:06:54,260
in me and they have a default value here.

84
00:06:56,690 --> 00:06:59,140
And we are not moving in.

85
00:06:59,920 --> 00:07:07,980
So what I want to call for One can I simply put this yes.

86
00:07:09,380 --> 00:07:15,870
But trying to call for one with empty object will not be alone.

87
00:07:19,160 --> 00:07:24,070
It's not assignable to Parminter E-string be number.

88
00:07:24,090 --> 00:07:32,880
So actually this is what this is as good as writing is plan B number because of this before one is automatically

89
00:07:32,940 --> 00:07:34,700
type in type is in.

90
00:07:34,740 --> 00:07:41,470
So if we want to support this or do I should not provide default value for the year I should wait.

91
00:07:41,520 --> 00:07:45,620
So in that case I don't need this I can get rid of it.

92
00:07:46,300 --> 00:07:50,200
Rather not call equal to the.

93
00:07:50,250 --> 00:07:51,250
So what is happening.

94
00:07:51,320 --> 00:07:58,390
Like a real case Beaven was being assigned to.

95
00:08:01,000 --> 00:08:09,550
We drank or in an object all the destructor variables but because we do not have value of.

96
00:08:09,560 --> 00:08:13,300
Beyond that I can say automatically Beeville with Again as you

97
00:08:16,620 --> 00:08:21,280
likewise want to have some here is fine.

98
00:08:21,280 --> 00:08:21,920
Right.

99
00:08:21,990 --> 00:08:29,310
So what we have done now because it is mandatory we are supposed to give the can make make as optional

100
00:08:29,310 --> 00:08:30,620
in such case.

101
00:08:31,050 --> 00:08:33,860
Yes I can make it as optional by putting your

102
00:08:37,120 --> 00:08:46,700
equal to zero but by putting it equal to zero what we did emit it and beat us upstart.

103
00:08:46,730 --> 00:08:50,160
So this is how this is valid and why is this valid.

104
00:08:50,330 --> 00:08:54,030
Because if nothing is provided in such case also we will be digging down

105
00:08:56,830 --> 00:08:58,400
maybe the.

106
00:08:58,660 --> 00:09:06,330
That's what was in the quadrant so basically a little typical topic but what we have to understand is

107
00:09:06,330 --> 00:09:13,410
here at the time of this proctoring default values can be provided for the variables.

108
00:09:13,680 --> 00:09:19,410
So these are those two variables which are used as parameters and we know that for Parminter we can

109
00:09:19,410 --> 00:09:25,900
provide a default value and hence this is the default but by default value is there only for it I know

110
00:09:29,470 --> 00:09:32,320
but when default values provided for.

111
00:09:32,930 --> 00:09:38,860
We will not be able to call it with empty string because this would say isn't quite right not this means

112
00:09:38,860 --> 00:09:40,810
what is required.

113
00:09:42,030 --> 00:09:46,820
The parameter data type is in such a way that is required value and be optional.

114
00:09:47,160 --> 00:09:56,130
So what what to make Aylsworth obsolete so can we remove this because you already provided the run with

115
00:09:56,530 --> 00:09:57,570
this.

116
00:09:57,580 --> 00:10:03,130
No because if you remove this when you're calling without any object then problem will come.

117
00:10:03,130 --> 00:10:06,270
You see error will as there is expected.

118
00:10:06,310 --> 00:10:12,600
One argument but we have given Ziel so far that Parminter to have default value.

119
00:10:12,670 --> 00:10:16,630
We should write here equal to and give something here.

120
00:10:16,810 --> 00:10:19,530
Something like you come up on.

121
00:10:19,990 --> 00:10:21,970
I mean for you can give some value.

122
00:10:22,340 --> 00:10:25,690
And B you can make it up and it's your choice.

123
00:10:27,060 --> 00:10:32,800
So a little complicated but I would say you try various permutations and combinations and try to understand

124
00:10:32,800 --> 00:10:36,270
the simple copeck a few points.

125
00:10:38,610 --> 00:10:45,010
There's what we cover B is undefined so B is equal to 1 0 0 1 else.

126
00:10:45,040 --> 00:10:49,040
This whole object got be

127
00:10:54,000 --> 00:10:55,480
so good Paul objeck.

128
00:10:55,540 --> 00:11:05,020
Now in the variable of all object people object has every level of the whole object as well as properties

129
00:11:05,020 --> 00:11:10,200
in the event if B is undefined all object actually has two properties.

130
00:11:10,590 --> 00:11:15,090
Now take this I said Type C it's as good as copying and pasting it here.

131
00:11:16,270 --> 00:11:20,340
So we are saying it and B are two properties which are of this type.

132
00:11:21,310 --> 00:11:23,480
Basically they are different.

133
00:11:24,060 --> 00:11:30,150
If you want to get a default value so that without any parameters you want to call for it and B for

134
00:11:30,150 --> 00:11:34,500
the Parminter we have to give a different so equal to is used.

135
00:11:36,030 --> 00:11:36,680
For both.

136
00:11:36,700 --> 00:11:41,770
And we have to define our default value.

137
00:11:41,910 --> 00:11:47,590
And likewise here you see is said to industry but B not there.

138
00:11:47,910 --> 00:11:50,550
That's why B is compulsorily given value.

139
00:11:51,030 --> 00:11:57,570
And when you're calling a function compulsory we have to pass an object which acts as a property because

140
00:11:57,590 --> 00:11:58,990
it is required.

141
00:11:59,460 --> 00:12:06,340
You can pass empty also because this is optional and you can you cannot pass an object because it's

142
00:12:06,950 --> 00:12:07,740
required.

143
00:12:08,680 --> 00:12:10,320
So empty object cannot be passed.

144
00:12:10,370 --> 00:12:14,900
I mean no object is fine but nobody is passed automatically this value will be different.

145
00:12:15,170 --> 00:12:17,730
But when an object is passed this value will not be taken.

146
00:12:18,080 --> 00:12:21,750
But value for baby will be taken as zero.

147
00:12:22,370 --> 00:12:23,720
Simple language.

148
00:12:24,040 --> 00:12:31,330
If you are still confused remember this is the part of me whereas this is the default value for the

149
00:12:31,330 --> 00:12:31,860
problem.

150
00:12:32,090 --> 00:12:33,770
Very important.

151
00:12:33,770 --> 00:12:37,450
This is treat this as only one parameter.

152
00:12:37,570 --> 00:12:43,580
It has two variables that has one parameter and treat this as a default value for that Panamint.

153
00:12:43,940 --> 00:12:48,910
So when you're giving an object which is empty it is not going to accept because the default value will

154
00:12:49,220 --> 00:12:50,200
not be taken.

155
00:12:50,390 --> 00:12:51,430
But it really doesn't.

156
00:12:51,530 --> 00:12:58,970
This object where the value for is mandatory but when you are calling in B then automatically this value

157
00:12:58,970 --> 00:13:01,250
will be taken beyond the default value.

158
00:13:01,260 --> 00:13:03,040
It will be taken as implicitly.

159
00:13:03,380 --> 00:13:05,420
So this is really very confusing.

160
00:13:05,420 --> 00:13:10,460
You have to be careful when you're using this proctoring as part of me for the matter especially with

161
00:13:10,760 --> 00:13:13,600
default values and optional parameters.

162
00:13:14,240 --> 00:13:15,150
That's all in this.

163
00:13:15,350 --> 00:13:15,720
Thank you.

