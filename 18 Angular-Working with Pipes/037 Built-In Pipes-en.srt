1
00:00:16,840 --> 00:00:24,130
Working with pipes in Angola this particular feature is going to help both in formatting the old board

2
00:00:24,970 --> 00:00:28,210
and sometimes it will also do the transformation.

3
00:00:28,210 --> 00:00:34,630
So basically whenever we want the output to be transformed or formatted we can make use of the features

4
00:00:34,750 --> 00:00:36,090
called us by.

5
00:00:36,800 --> 00:00:44,260
Yes we have got some pipes or an eyeball that we can write our own custom byte we can pass parameters

6
00:00:44,260 --> 00:00:44,970
to the pipes.

7
00:00:44,980 --> 00:00:50,620
It can be either built in or custom pipes and we can have chaining of pipes.

8
00:00:50,710 --> 00:00:55,180
And finally we will also see here pure and impure pipes.

9
00:00:55,180 --> 00:01:04,000
So what is a pipe pipe takes as data top in pipes takes in data as input and transforms it to a desired

10
00:01:04,090 --> 00:01:04,910
output.

11
00:01:04,930 --> 00:01:10,070
That means for a given input that is supposed to produce some predicted output.

12
00:01:10,090 --> 00:01:11,620
So these are 8 pipes.

13
00:01:11,620 --> 00:01:20,540
What we have here did pipe uppercase pipe Lorqess by decimal currency by percentage paid decent by slice

14
00:01:20,560 --> 00:01:25,340
by all these people can make use of in different situations.

15
00:01:25,660 --> 00:01:28,070
Let me take a simple example first.

16
00:01:28,180 --> 00:01:33,970
I have created a new project called us pipes demo app and then pipes them all up they are caught up

17
00:01:33,970 --> 00:01:35,290
component.

18
00:01:35,530 --> 00:01:38,410
These are the four those but not here.

19
00:01:38,530 --> 00:01:40,730
I can see the value of name.

20
00:01:40,870 --> 00:01:46,830
I would like to convert it into uppercase and basically here it is mixed case but when it is displaying

21
00:01:46,840 --> 00:01:48,990
we want everything to be in uppercase.

22
00:01:49,180 --> 00:01:52,640
So we simply put a pipe and right here uppercase.

23
00:01:52,660 --> 00:01:56,810
So this is a pipe note on it and we are going to get it out.

24
00:01:56,810 --> 00:02:04,780
Put everything in upper case load you see and it's all in uppercase like that only we can also make

25
00:02:04,780 --> 00:02:06,760
use of logs.

26
00:02:06,850 --> 00:02:10,290
So these are simple pipes uppercase and lowercase.

27
00:02:10,540 --> 00:02:16,450
And these pipes actually do not take any parameters but there are other pipes which are going to take

28
00:02:16,450 --> 00:02:23,350
some parameters for example not to say in this class component class I'm going to have some variable

29
00:02:23,580 --> 00:02:25,210
say data about

30
00:02:30,760 --> 00:02:38,410
and this is of type date and the value for this is I'm giving some system whatever my might be or we

31
00:02:38,410 --> 00:02:45,050
can be very specific and put away the data also a whole will display this system that I'm going to provide

32
00:02:45,090 --> 00:02:52,310
inside that template latches on my door for separation and dead on board.

33
00:02:53,800 --> 00:02:57,660
And in the intercalation of days of work.

34
00:02:57,850 --> 00:03:01,390
So this is going to produce that before the book.

35
00:03:01,540 --> 00:03:11,070
I just it it so and so so so so and so is the default on this we can do some kind of formatting.

36
00:03:11,370 --> 00:03:21,600
So here we are now we will use a pipe for the date so I'll use it quite good then along with the pipe.

37
00:03:21,610 --> 00:03:29,230
We can also specify the format in which we want the date to be so we can see it date color whenever

38
00:03:29,230 --> 00:03:30,800
we want some paramita.

39
00:03:30,870 --> 00:03:39,850
We have to use color and in court we provide about let's say medium medium format like that we can have

40
00:03:39,970 --> 00:03:51,110
another pipe and Cidade color we want in small format small or it can be folded.

41
00:03:51,350 --> 00:03:53,670
It can be forwarded.

42
00:03:54,200 --> 00:03:59,460
So there are different formats not you fight on this.

43
00:03:59,620 --> 00:04:00,160
We'll see.

44
00:04:00,160 --> 00:04:03,460
Same old data but in different formats.

45
00:04:03,550 --> 00:04:05,970
Small medium for day

46
00:04:08,740 --> 00:04:09,900
something is wrong with

47
00:04:13,300 --> 00:04:15,950
you have to check what is a to full lead.

48
00:04:16,190 --> 00:04:18,380
So these are different formats for the

49
00:04:22,190 --> 00:04:23,030
lot on this

50
00:04:26,360 --> 00:04:33,100
music it's case sensitive physically there is something wrong here.

51
00:04:33,310 --> 00:04:36,780
It's repeating multiple times as am.

52
00:04:36,920 --> 00:04:37,610
OK.

53
00:04:37,940 --> 00:04:43,070
So it is supposed to be done what medium or short.

54
00:04:43,070 --> 00:04:43,760
It's not smart.

55
00:04:43,760 --> 00:04:45,490
It's basically short.

56
00:04:45,620 --> 00:04:56,400
So this should be sharp These values are very important medium shot dead long the medium dead shot dead.

57
00:04:56,580 --> 00:05:01,130
When we don't want pain we can make use of me and him dead shot dead when we don't want dead.

58
00:05:01,140 --> 00:05:04,330
But only that time that also we can make use of.

59
00:05:04,350 --> 00:05:06,830
So these are the various possibilities of that.

60
00:05:07,110 --> 00:05:13,780
What is important this is the actual variable that is going to be transformed into dead.

61
00:05:14,030 --> 00:05:17,890
But here because it is already dead there is no question of transformation.

62
00:05:18,120 --> 00:05:25,890
But we are able to do formatting with the help of this extra paramita so paramedics also play a very

63
00:05:25,890 --> 00:05:27,770
important role in byte.

64
00:05:27,930 --> 00:05:31,660
We have to understand how the parameters are being used.

65
00:05:31,860 --> 00:05:38,370
Then we have got a decimal by the simple pipe we are supposed to use number as the name of the bike

66
00:05:38,910 --> 00:05:42,730
and followed by that we are supposed to give colon and some digit in.

67
00:05:42,890 --> 00:05:47,320
And what is in for this ring which has the following format.

68
00:05:47,340 --> 00:05:50,740
That means it will be in the format of minimum digit.

69
00:05:50,850 --> 00:05:58,530
So 3 here is a minimum integer digit that is before decimal digits should be that it is not mentioned

70
00:05:58,620 --> 00:06:01,140
it is z one by default.

71
00:06:01,200 --> 00:06:12,340
Likewise often dot minimum fractionated how many digits are required of the dot minimum and maximum.

72
00:06:12,430 --> 00:06:14,550
Let's take with an example again.

73
00:06:14,790 --> 00:06:20,550
I have got some number maybe some of them.

74
00:06:20,650 --> 00:06:25,400
Let's say some something like number of items.

75
00:06:27,440 --> 00:06:28,570
And this is it.

76
00:06:28,610 --> 00:06:29,340
Number.

77
00:06:29,990 --> 00:06:32,460
And give some value to it.

78
00:06:32,490 --> 00:06:37,230
It's one two three four five six dot six five four three two one.

79
00:06:37,250 --> 00:06:40,410
I'm taking a long run so that I can display properly.

80
00:06:41,000 --> 00:06:47,580
So again I would be a top for suppresion and I'll give you a number of items.

81
00:06:50,220 --> 00:06:56,640
And we can get into position number of items.

82
00:06:56,710 --> 00:07:04,560
This is the default format that I can now do some kind of formatting.

83
00:07:04,770 --> 00:07:09,130
How will I know with by number.

84
00:07:09,220 --> 00:07:10,180
That's it.

85
00:07:10,560 --> 00:07:20,350
But I can give a call on of the number for them and I say I want only 4 digits Daut.

86
00:07:20,430 --> 00:07:25,560
I mean it should be all in single vote for Daut I want.

87
00:07:25,560 --> 00:07:33,580
Often the simple minimum four digits are three digits and maximum five digits.

88
00:07:33,760 --> 00:07:34,690
That's it.

89
00:07:34,800 --> 00:07:36,680
This is the restriction I'm putting.

90
00:07:36,900 --> 00:07:37,500
Same thing.

91
00:07:37,530 --> 00:07:46,980
I'm going to look for maybe here but then I'll say no we want minimum five digits.

92
00:07:47,020 --> 00:07:48,430
Or was it 90.

93
00:07:48,430 --> 00:07:54,340
It's actually we have only six digits but I'm giving it up more than six maybe eight.

94
00:07:54,370 --> 00:08:06,810
And often the symbol the want minimum maybe 8 and maximum I want is maybe 10 so like this I'm just pulling

95
00:08:06,830 --> 00:08:08,920
your two examples let's see the output.

96
00:08:08,960 --> 00:08:14,190
Now we will see that see if we got the actual value.

97
00:08:14,830 --> 00:08:17,390
But because of number.

98
00:08:17,420 --> 00:08:24,580
Does God formatter karma has come in between number formatting has been done and what is important and

99
00:08:24,580 --> 00:08:27,840
interesting is that before formatting is done

100
00:08:31,410 --> 00:08:39,140
number but 4.3 hyphen fine see for Dot three hyphen five.

101
00:08:39,300 --> 00:08:41,130
So minimum number of digits is four.

102
00:08:41,190 --> 00:08:43,610
But how many I have I've actually got six.

103
00:08:43,610 --> 00:08:48,000
So automatically all six are taken and then three

104
00:08:50,520 --> 00:08:55,850
three hyphen five minimum three maximum five that's all it is.

105
00:08:56,250 --> 00:08:59,580
But here you see I give aid.

106
00:08:59,640 --> 00:09:02,590
So that means before that we should have eight digits.

107
00:09:02,730 --> 00:09:09,510
So automatically it is proceeded with two zeros and often thought it is supposed to have minimum 8 digits.

108
00:09:09,690 --> 00:09:10,740
So I have six.

109
00:09:10,760 --> 00:09:19,050
And of course the maximum can be 10 only seven is not.

110
00:09:19,090 --> 00:09:25,600
I mean the format of broidered is greater than the actual number of digits automatically padding will

111
00:09:25,610 --> 00:09:27,810
be done with zeros.

112
00:09:27,810 --> 00:09:33,260
This is not the simple type of the decimal point.

113
00:09:33,270 --> 00:09:41,640
We have got currency right now currency five can be something like let's see if we have a solid salaries

114
00:09:41,880 --> 00:09:43,880
maybe some number.

115
00:09:44,630 --> 00:09:46,060
One two three.

116
00:09:46,080 --> 00:09:58,770
So same thing like decimal does that symbol will be out the currency symbol is going to be added.

117
00:09:59,460 --> 00:10:00,370
Salary.

118
00:10:00,660 --> 00:10:01,500
The default

119
00:10:05,570 --> 00:10:21,870
I do here by with currency default and then I'll give currency and we can do for the formatting.

120
00:10:23,750 --> 00:10:33,920
But call up and let's say I want four digits and often decimal I want two digits and maximum strategics

121
00:10:36,820 --> 00:10:39,400
legacy No.

122
00:10:39,500 --> 00:10:40,500
And the first one

123
00:10:44,640 --> 00:10:45,920
gold is missing.

124
00:10:51,110 --> 00:10:54,990
And the first one you see all of them have come correctly.

125
00:10:56,550 --> 00:10:57,350
What is it all

126
00:11:07,070 --> 00:11:12,460
Celtic currency in undetermined it did gold.

127
00:11:13,030 --> 00:11:17,900
Gold has not and properly some bad salary

128
00:11:20,470 --> 00:11:21,150
looks good

129
00:11:24,220 --> 00:11:25,940
it and once again press

130
00:11:30,420 --> 00:11:49,090
to.

131
00:11:49,450 --> 00:11:51,170
Something should be wrong in the syntax.

132
00:11:51,170 --> 00:11:55,920
Let's see the syntax and currency all currency code is important.

133
00:11:55,990 --> 00:11:57,350
I forgot this.

134
00:11:57,510 --> 00:12:06,340
We should give currency or symbols to display and then they need a currency called is ISO 4 to 1 7 currency

135
00:12:06,340 --> 00:12:08,830
code such as USD.

136
00:12:08,890 --> 00:12:10,150
You are like that.

137
00:12:10,150 --> 00:12:11,680
We'll have to give currency.

138
00:12:12,190 --> 00:12:14,200
Now this place symbol.

139
00:12:14,230 --> 00:12:17,100
That symbol should be displayed or not.

140
00:12:17,110 --> 00:12:19,890
So if you give blue it is going to display a symbol.

141
00:12:20,020 --> 00:12:24,210
If you're going to give false it is going to display use the likeness.

142
00:12:24,370 --> 00:12:29,310
And finally of course the digital info that means here we are supposed to be

143
00:12:32,280 --> 00:12:32,530
called

144
00:12:35,790 --> 00:12:40,760
next to use the color.

145
00:12:42,330 --> 00:12:44,460
And for that I mean c'mon this

146
00:12:50,210 --> 00:12:53,360
is still not rendering something is wrong.

147
00:12:53,870 --> 00:12:54,580
Well.

148
00:12:57,410 --> 00:13:00,340
Surprisingly not even showing any error.

149
00:13:04,210 --> 00:13:07,290
Let's run this in Firefox

150
00:13:17,320 --> 00:13:18,420
Chrome of course.

151
00:13:26,590 --> 00:13:28,500
Oh I forgot.

152
00:13:29,230 --> 00:13:31,520
It was deep and

153
00:13:42,710 --> 00:13:43,910
all rose

154
00:13:57,320 --> 00:14:00,450
but it is plain old.

155
00:14:00,470 --> 00:14:05,780
The problem is it's not equal.

156
00:14:05,840 --> 00:14:11,650
It was blown up anyway I'm Lexi though.

157
00:14:11,750 --> 00:14:12,360
Goodloe

158
00:14:16,430 --> 00:14:22,630
still giving us this is the billions so billions should be directly.

159
00:14:22,630 --> 00:14:23,790
True or False

160
00:14:29,420 --> 00:14:36,660
still not using any.

161
00:14:36,730 --> 00:14:42,890
To me it looks like seeing as you again tended to Microsoft at

162
00:14:50,510 --> 00:14:52,020
we have it yes.

163
00:14:52,050 --> 00:14:53,440
Perfect.

164
00:14:56,700 --> 00:15:03,960
So the first and important thing this is supposed to be the format you are going to see if I get I'm

165
00:15:03,960 --> 00:15:11,430
going to not get you to Euro symbol like waste.

166
00:15:11,610 --> 00:15:17,820
Right now Indian Dobies and we'll get to the symbol

167
00:15:21,070 --> 00:15:25,750
Likewise here if I give false.

168
00:15:25,750 --> 00:15:30,080
I'm going to get I end up not the symbol but the text I enter.

169
00:15:32,930 --> 00:15:36,530
And of course the last part is the formatting.

170
00:15:36,530 --> 00:15:39,750
The way we want we can actually provide the formatting.

171
00:15:39,830 --> 00:15:46,700
So let's say I want the output to be up to 5 digits maybe 6 digits.

172
00:15:47,300 --> 00:15:51,500
And of the symbol I want 3 and maximum five on five

173
00:15:54,190 --> 00:16:01,730
single code.

174
00:16:02,840 --> 00:16:07,020
And you see one leading zero has been added.

175
00:16:07,180 --> 00:16:15,530
But what if I have here of four five six What is maximum allowed is five.

176
00:16:15,820 --> 00:16:20,440
So up to this it has to take what do see in Obote the values.

177
00:16:20,470 --> 00:16:25,910
One two three four and then it actually shows six.

178
00:16:25,930 --> 00:16:28,360
That means it's automatically doing that on and off.

179
00:16:28,780 --> 00:16:32,940
If I change that six to maybe three it will just ignore it.

180
00:16:33,070 --> 00:16:41,180
Less than five it will ignore it and if it is by and more it will go on the.

181
00:16:41,190 --> 00:16:47,690
So this is how currency currency is working.

182
00:16:48,620 --> 00:16:51,340
Likewise we have got percentage by percentage point.

183
00:16:51,350 --> 00:16:57,650
We'll simply add the percentage symbol and nothing more than that.

184
00:16:58,190 --> 00:17:03,510
Let's say something maybe in my person the age

185
00:17:06,700 --> 00:17:13,510
again number is equal to let's say ninety nine point nine.

186
00:17:13,600 --> 00:17:16,730
A This is all I would like to get the right.

187
00:17:17,170 --> 00:17:22,660
So if I put your head top person daj

188
00:17:27,840 --> 00:17:35,740
my boss an 8 that's the default.

189
00:17:35,880 --> 00:17:38,060
And then we can further do formatting

190
00:17:42,060 --> 00:17:45,300
that is the currency is frozen.

191
00:17:45,350 --> 00:17:54,220
I mean so the bike name is present or you can go of course and followed by that some formatting maybe

192
00:17:55,240 --> 00:18:03,900
three door tell me I've done three and all of these

193
00:18:07,380 --> 00:18:10,030
it's going to just add percentage symbol

194
00:18:15,310 --> 00:18:16,020
3.

195
00:18:19,970 --> 00:18:23,030
it's not for my particular the same problem.

196
00:18:24,050 --> 00:18:29,690
For.

197
00:18:41,690 --> 00:18:45,520
Was that they call that

198
00:18:50,510 --> 00:18:51,230
and you see

199
00:18:55,720 --> 00:19:04,130
we call what is actually happening here is what value we give here it is automatically converting that

200
00:19:04,440 --> 00:19:07,170
percentage by multiplying by untracked.

201
00:19:07,660 --> 00:19:17,770
So if my requirement is 100 percent I would get point 1 0 or let's say for 1 to 3 it will multiply that

202
00:19:17,770 --> 00:19:18,710
by 100.

203
00:19:18,850 --> 00:19:21,000
And that we are going to get.

204
00:19:21,050 --> 00:19:25,020
So let's see you know we are going to get 12 percent.

205
00:19:25,300 --> 00:19:26,280
Yes.

206
00:19:26,860 --> 00:19:34,920
And here this is supposed to be in courts.

207
00:19:35,000 --> 00:19:43,160
So really good riddance and that too often works of course if the runway is long enough automatic formatting

208
00:19:43,160 --> 00:19:44,220
is going to be done.

209
00:19:44,570 --> 00:19:49,990
So remember at the time of giving to it it is going to multiply the value by 800.

210
00:19:50,390 --> 00:19:56,900
And also it is going to add the symbol parts of this thing multiply by a hundred and our symbol.

211
00:19:56,960 --> 00:20:02,180
That is precisely the behavior of percentage pipe like that.

212
00:20:02,180 --> 00:20:04,550
We have now got another pipe that is

213
00:20:07,890 --> 00:20:09,190
slaves fight.

214
00:20:09,360 --> 00:20:14,420
What is placed by it is basically for slicing and Eddie.

215
00:20:14,540 --> 00:20:24,570
So here again let's say we haven't added maybe some of the numbers it's a number.

216
00:20:25,420 --> 00:20:32,300
And the values are good in square brackets 1 2 3 4 5 6 7 8 9.

217
00:20:33,180 --> 00:20:39,240
So not if want to display this even like your first job.

218
00:20:39,840 --> 00:20:49,410
And then I would like to simply write numbers say these are all numbers so that it is going to give

219
00:20:49,430 --> 00:20:51,800
me the complete data

220
00:20:55,090 --> 00:21:00,390
but what is important is I want them to.

221
00:21:01,200 --> 00:21:03,210
So how do you get numbers.

222
00:21:03,300 --> 00:21:16,860
I'll give numbers which is my variable by bit but slice and say I want a slice from 3 that isn't

223
00:21:19,680 --> 00:21:23,300
numbers should be sliced from 0 1 to

224
00:21:26,580 --> 00:21:35,190
so you see four five six seven eight nine Why does one more record and it's so all the numbers will

225
00:21:35,190 --> 00:21:35,930
come here.

226
00:21:36,150 --> 00:21:46,600
But here we are getting the data from 4 5 4 3 2 0 1 2 3 5 index for its doctorate lightweight.

227
00:21:46,680 --> 00:21:55,130
If I want I can give this as a negative number that means but I'm lost will take my minus three to seven

228
00:21:55,160 --> 00:21:55,870
eight nine.

229
00:21:55,960 --> 00:21:59,360
Last three minus five.

230
00:21:59,940 --> 00:22:08,400
So we can actually decide by the left to right negative number right to left minus five last five digits.

231
00:22:08,400 --> 00:22:21,240
I mean last five minutes so like this if you fight it will start in the 6th politician 6 7 8 9 6.

232
00:22:22,640 --> 00:22:26,230
So Sly's is basically two print at it.

233
00:22:26,510 --> 00:22:31,590
And of course you can specify what more I want let's say start with two.

234
00:22:31,910 --> 00:22:34,570
I want to have only four digits.

235
00:22:34,720 --> 00:22:39,150
I don't want to go up to 4 to go for.

236
00:22:39,350 --> 00:22:50,650
So they don't want to an 0 1 2 3 0 1 2 and go for

237
00:22:54,730 --> 00:22:59,650
3 4.

238
00:23:00,120 --> 00:23:05,660
So like this we are able to print a bit of slice by.

239
00:23:06,020 --> 00:23:11,770
So getting back on top we have covered all these different by now what does this imply if at all we

240
00:23:11,770 --> 00:23:15,310
had a javascript object some object lets say

241
00:23:18,300 --> 00:23:25,920
some Kiraly their property one color but you probably need to call them by.

242
00:23:25,970 --> 00:23:29,680
We would like to see this in format at all.

243
00:23:30,030 --> 00:23:32,130
So here we can directly write

244
00:23:40,830 --> 00:23:43,580
the object.

245
00:23:44,210 --> 00:23:47,560
If I simply give all the let's see what happens.

246
00:23:47,860 --> 00:23:52,160
And if I give me the word does.

247
00:23:52,610 --> 00:23:54,130
Let's see what happens.

248
00:23:54,440 --> 00:23:56,870
You'll see this will display in string format

249
00:24:00,620 --> 00:24:10,010
this is object simple object but this is displaying to us that is an object in string father.

250
00:24:10,160 --> 00:24:17,550
So that's what this does and by percentage by currency by all these pipes have to be used appropriately

251
00:24:17,550 --> 00:24:19,610
in project.

252
00:24:19,650 --> 00:24:21,530
So we're going with an example in hand.

253
00:24:21,560 --> 00:24:25,040
I have written one class called an employee.

254
00:24:25,260 --> 00:24:30,870
I have different databases used here and we are creating a template component

255
00:24:33,430 --> 00:24:38,030
and see different types of steel at the time of displaying it.

256
00:24:38,030 --> 00:24:45,930
So when we are slicing when we are going through each rule we are saying we want only from 3 to 6 index

257
00:24:46,040 --> 00:24:47,230
three index 6.

258
00:24:47,240 --> 00:24:48,260
That's what we are saying.

259
00:24:48,470 --> 00:24:53,870
We don't want all likewise uppercase currency day person dead.

260
00:24:54,020 --> 00:24:57,250
The value will be converted into positive by multiplying two hundred.

261
00:24:57,260 --> 00:25:03,030
And then I think simple and last we are seeing implied as does Synoptic.

262
00:25:03,110 --> 00:25:08,700
So all the employees are here I would want you to do this example on your own from the handle.

263
00:25:08,990 --> 00:25:09,590
Thank you.

