import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-css-style-demo',
  templateUrl: './css-style-demo.component.html',
  styleUrls: ['./css-style-demo.component.scss']
})
export class CssStyleDemoComponent implements OnInit {
  isRedBorder:boolean = true;
  isYellowBackground:boolean = true;
  textColor:string = "red";
  backgroundColor:string = "yellow";

  constructor() { }

  ngOnInit() {
  }

  setClasses(){
    let classes = {
      saveable:true,
      modified:false,
      special:true
    }
    return classes;
  }

  setStyles(){
    let styles = {
      "font-style":'italic',
      "font-weight":'bold',
      "font-size":'32px'
    }
    return styles
  }

}
