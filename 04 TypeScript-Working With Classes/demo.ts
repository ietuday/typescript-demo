class Person {
    protected firstName: string
    protected lastName: string
    private readonly dateOfBirth: Date
    public get fullName(): string {
        return this.firstName + this.lastName;
    }
    public get age(): number {
        return (new Date().getFullYear() - this.dateOfBirth.getFullYear());
    }
    constructor(fn: string, ln: string, dateOfBirth: Date) {
        this.firstName = fn;
        this.lastName = ln;
        this.dateOfBirth = dateOfBirth;
    }
    greet(friendName: string): string {
        return "Hello " + friendName + ", how are you doing?";
    }
    selfIntrodution(): string {
        let age = (new Date().getFullYear() - this.dateOfBirth.getFullYear())
        return "Hello, I am " + this.firstName + " " + this.lastName + " and I am " + age + "yrs old";
    }
}
class Employee extends Person {
    private _departmentName: string
    private _salary: number
    constructor(fn: string, ln: string, dateOfBirth: Date, deptName: string, sal: number) {
        super(fn, ln, dateOfBirth);
        this._departmentName = deptName;
        this._salary = sal;
    }
    //Method overriding
    selfIntrodution(): string {
        var s: string = super.selfIntrodution()
        return s + " working in " + this._departmentName + " department";
    }
    public incrementSalary(amount: number) {
        this._salary += amount;
    }
    public get salary(): number {
        return this._salary;
    }
    public get departmentName(): string {
        return this._departmentName
    }
    public set departmentName(newValue: string) {
        if (newValue == "")
            throw "Department cannot be null";
        this._departmentName = newValue;
    }
}