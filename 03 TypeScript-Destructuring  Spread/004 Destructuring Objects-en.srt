1
00:00:16,060 --> 00:00:22,850
Object this pattern after we have seen how hard it can be this could turn into multiple variables.

2
00:00:22,870 --> 00:00:26,720
It's not time to look at how an object can be disrupted.

3
00:00:26,980 --> 00:00:32,430
That means an object we know has got properties in the form of key value pairs.

4
00:00:32,560 --> 00:00:38,220
So let's say I copied this object I'm using the existing project which I created in the previous session.

5
00:00:38,620 --> 00:00:49,780
So I have an object let's call it as or B and B has three properties A B and C A is of type string B

6
00:00:50,290 --> 00:00:54,270
is of type in a number and C is again of debt ceiling.

7
00:00:54,730 --> 00:01:00,140
So ABC three properties of an object from this three properties.

8
00:01:00,160 --> 00:01:05,260
I would like to extract data or let's say only for A and B.

9
00:01:05,260 --> 00:01:08,140
That means I want two variables by me it and B.

10
00:01:08,230 --> 00:01:12,360
So I'd say no I have to use curly braces.

11
00:01:12,640 --> 00:01:15,400
I really have used square brackets.

12
00:01:15,400 --> 00:01:17,730
Now we are using curly or floor brackets.

13
00:01:17,800 --> 00:01:22,180
So I'll say maybe I don't say equal do or B.

14
00:01:22,620 --> 00:01:28,090
And once I do that now I can see the value of it and the value of B.

15
00:01:28,090 --> 00:01:32,310
In other now what is this.

16
00:01:32,340 --> 00:01:38,950
These are two variables whose value will be purged from the properties of object.

17
00:01:39,180 --> 00:01:45,060
So the value of all we don't able to be as to the variable the value of all we not be will be as and

18
00:01:45,160 --> 00:01:46,280
the variable b.

19
00:01:46,770 --> 00:01:48,480
And let me show you the execution

20
00:01:51,440 --> 00:01:51,980
fool.

21
00:01:52,160 --> 00:02:02,680
I want to let's give you an 12 foot tall so object destructuring is what we have done.

22
00:02:02,990 --> 00:02:10,390
But what if they don't want to have the value of A and B as in B.

23
00:02:10,430 --> 00:02:13,040
I want to have different names for these two variables.

24
00:02:13,190 --> 00:02:15,000
So in such case what should we do.

25
00:02:15,020 --> 00:02:17,740
Simply put your color and give some of them in.

26
00:02:17,750 --> 00:02:27,340
Let's say I call that some other name B then the value will not be available now are not smart in small

27
00:02:27,360 --> 00:02:32,470
b but the very small up capital and capital B so that this will all be.

28
00:02:32,510 --> 00:02:34,250
It will be assigned to the variable.

29
00:02:34,380 --> 00:02:37,770
It will be dark B will be assigned to the variable b.

30
00:02:38,180 --> 00:02:43,940
So like that we now have two variables this is the properties of renaming.

31
00:02:43,980 --> 00:02:47,730
We can call it its properties renaming.

32
00:02:47,760 --> 00:02:55,680
Now what if the object has extra data and I'm only forwarding the parcel on the left hand side.

33
00:02:56,010 --> 00:03:05,250
So in such case let's say I'll haveour let flawed bracket give it the Conemaugh I can put a dot dot

34
00:03:05,250 --> 00:03:09,310
dot and the rest the same index which we learn actually in.

35
00:03:09,400 --> 00:03:10,520
I read this book.

36
00:03:10,970 --> 00:03:12,290
No one is going to happen.

37
00:03:13,610 --> 00:03:18,880
We are getting at is something that's already declared on top in the previous example.

38
00:03:18,980 --> 00:03:21,820
That's why we're up so let's call it a rest one.

39
00:03:22,610 --> 00:03:24,230
So no one is going to happen.

40
00:03:24,590 --> 00:03:26,760
We'll be taught it will be as to this.

41
00:03:26,780 --> 00:03:33,240
We all need or B or B or C will be assigned to rest one.

42
00:03:33,250 --> 00:03:36,390
So how do I get the data then something like this.

43
00:03:36,430 --> 00:03:45,240
Also a lot itself can which all the rest will not be easy and that's true under

44
00:03:56,090 --> 00:04:04,270
so from the object because of the property name we got it here and the rest of the properties have come

45
00:04:04,280 --> 00:04:06,080
into the other object.

46
00:04:06,180 --> 00:04:18,570
This practice reliable will fall to one and but for dwellin about was the BNZ of coming to the school

47
00:04:19,870 --> 00:04:29,320
but what is also possible is nested object destructuring What is this necessary object this platonic.

48
00:04:29,380 --> 00:04:30,830
Let's say we have got a function

49
00:04:37,400 --> 00:04:39,080
on let's forget about the function.

50
00:04:39,120 --> 00:04:44,400
We just have one object user at Nickleby and explain to you

51
00:04:49,760 --> 00:04:58,970
so I have an object called as you just got department name and favorite cricketer Fred quickly.

52
00:04:58,990 --> 00:05:01,580
Could he get that first and second two objects.

53
00:05:01,580 --> 00:05:01,880
C.

54
00:05:01,910 --> 00:05:04,260
This is also an object first.

55
00:05:04,330 --> 00:05:06,790
Again is an object which has a name.

56
00:05:07,190 --> 00:05:10,640
And second is another object which also has a name.

57
00:05:10,810 --> 00:05:13,210
Yeah if you want you can predict stardate also.

58
00:05:16,350 --> 00:05:17,850
Maybe something like comment

59
00:05:24,930 --> 00:05:27,230
in batsmanship

60
00:05:38,590 --> 00:05:41,310
excellent Kib we could get

61
00:05:44,270 --> 00:05:44,950
bored.

62
00:05:45,200 --> 00:05:47,930
So I have two objects.

63
00:05:49,420 --> 00:05:57,240
The third member favorite character and favorite character is a member of user what I'm trying to do

64
00:05:57,250 --> 00:06:03,300
now is from user I would like to only extract the favorite character part

65
00:06:11,250 --> 00:06:13,450
first and second directly already declared on top.

66
00:06:13,450 --> 00:06:14,980
That's my reasoning.

67
00:06:18,680 --> 00:06:20,420
So what is going to happen now.

68
00:06:23,560 --> 00:06:27,480
So this should be first and second only because first and second are the property names.

69
00:06:27,480 --> 00:06:41,330
Your favorite character so I'll put your colon first colon Second and this will be first and second.

70
00:06:41,330 --> 00:06:43,370
That's the proper syntax.

71
00:06:43,370 --> 00:06:48,700
So from user we are extracting favorite character.

72
00:06:48,970 --> 00:06:54,740
Use up dot favorite character is assigned to this object.

73
00:06:54,740 --> 00:06:56,960
That means what you use are not forever.

74
00:06:57,110 --> 00:07:01,010
This object is assigned to this but in this world.

75
00:07:01,100 --> 00:07:04,230
What is that we have members first and second.

76
00:07:04,690 --> 00:07:11,300
So we use the word dot dot dot for us it will be assigned to this first user dot.

77
00:07:11,320 --> 00:07:14,410
Favorite Character dot dot second will be assigned to this.

78
00:07:14,690 --> 00:07:18,550
So that was my first and second.

79
00:07:18,700 --> 00:07:26,060
So this word let's call it does not.

80
00:07:26,160 --> 00:07:29,470
You can get a name and you can also get comment

81
00:07:43,960 --> 00:07:47,400
on this.

82
00:07:47,470 --> 00:07:51,180
We should get first team excellent excellent.

83
00:07:51,990 --> 00:07:56,870
Excellent wicket keeper some come out.

84
00:07:56,900 --> 00:08:03,290
So what we did here we basically extracted the top from the nested object.

85
00:08:03,560 --> 00:08:05,480
An object is made of both.

86
00:08:05,700 --> 00:08:10,560
Another object is made up of other object from that be extracted.

87
00:08:10,610 --> 00:08:14,690
So first is a variable representing this object.

88
00:08:14,690 --> 00:08:24,140
Under this under this that is nesting of objects they remember I give the name or last names yet because

89
00:08:24,140 --> 00:08:27,830
first and second are already used on top as two different variables.

90
00:08:28,700 --> 00:08:35,750
Because of this I have to give them the property renaming we have done that next.

91
00:08:35,760 --> 00:08:39,720
Now we can look at what is mixed destructuring.

92
00:08:40,010 --> 00:08:45,460
So what we are seeing now is nested object is proctoring.

93
00:08:45,490 --> 00:08:54,010
Now we have to look at mixed this proctoring so I'll copy this user again or let that be the same object.

94
00:08:54,340 --> 00:08:55,240
But now

95
00:09:06,140 --> 00:09:08,800
so what is that we're doing now.

96
00:09:08,800 --> 00:09:18,510
Another favorite favorite kid that we have not got Havi those next property Nightengale and my favorite

97
00:09:19,550 --> 00:09:21,180
property hobbies.

98
00:09:21,500 --> 00:09:24,310
So user has hobbies.

99
00:09:24,490 --> 00:09:28,650
So what is going to happen no hobbies isn't any hobbies.

100
00:09:28,690 --> 00:09:35,420
Eddie with us favorite character is an object.

101
00:09:36,200 --> 00:09:37,640
So what am I doing here.

102
00:09:41,020 --> 00:09:43,140
User Daut favorite cricketer.

103
00:09:43,230 --> 00:09:52,240
For first will be a the fair one user dot favorite character Daut second will are Wasn't of EV-DO use

104
00:09:52,260 --> 00:09:59,510
of dot hobby's will be assigned to the ring which is playing cricket and playing chess.

105
00:09:59,560 --> 00:10:05,020
So hobby one will be playing cricket and hobby it will be playing just like that.

106
00:10:05,020 --> 00:10:08,770
All this will be printed on the console or you can print it in the

107
00:10:16,410 --> 00:10:19,160
team Dooney playing cricket.

108
00:10:19,290 --> 00:10:20,160
Playing chess.

109
00:10:20,160 --> 00:10:21,480
Hobbies.

110
00:10:21,480 --> 00:10:28,600
So here what we have done we have combination actually we have done mixed these proctoring we have done

111
00:10:28,730 --> 00:10:32,240
object destructuring and we have done at this point.

112
00:10:32,340 --> 00:10:35,060
Both are destructive in this particular context

113
00:10:38,120 --> 00:10:38,960
lets go back

114
00:10:43,700 --> 00:10:45,910
for off.

115
00:10:45,990 --> 00:10:52,570
We in the previous model we have seen far off with where we can get radically detached from the object.

116
00:10:52,970 --> 00:11:01,620
So what I'm trying to do is we have users which has departments and department name department name

117
00:11:01,710 --> 00:11:04,650
department name.

118
00:11:04,820 --> 00:11:13,960
So you're going to get users is a collection of objects which has a department that name has two properties.

119
00:11:13,960 --> 00:11:22,980
So we're going to extract one user at a time and assign to an end department barracked we can do this

120
00:11:23,310 --> 00:11:30,110
this way and all it does is how you Delaneys and get the shock that basically the whole purpose of this

121
00:11:30,110 --> 00:11:35,120
proctoring and spreaded is ultimately to do shortcut coding.

122
00:11:35,150 --> 00:11:41,000
You can always break this into a little elaborated version but so that coding can be done using this

123
00:11:42,550 --> 00:11:53,900
gold validities So let's quickly go through object this proctoring an object is to have an object whose

124
00:11:53,900 --> 00:11:57,050
properties will be extracted in two variables.

125
00:11:57,050 --> 00:12:01,010
So on the left hand side you should put proper names on the order of names.

126
00:12:01,040 --> 00:12:04,720
Doesn't matter the order of the variable names doesn't matter.

127
00:12:05,120 --> 00:12:10,690
But if it gives you want you can give different names to the variables inter-Korean call and syntax

128
00:12:10,700 --> 00:12:12,150
has to be used.

129
00:12:12,200 --> 00:12:19,940
They can be extracted partially ot the rest of the data can be parsed into another object with dot dot

130
00:12:19,940 --> 00:12:20,230
dot.

131
00:12:20,240 --> 00:12:26,760
Ellipsis should be used and destructor can be done through even nesting of objects.

132
00:12:28,740 --> 00:12:31,730
Mixed B-Spec where we help object as well.

133
00:12:31,770 --> 00:12:32,490
Eddie.

134
00:12:32,740 --> 00:12:37,320
Begin extract object we can extract any from under the bed involved.

135
00:12:37,920 --> 00:12:38,700
That's all in this.

136
00:12:38,820 --> 00:12:39,230
Thank you.

