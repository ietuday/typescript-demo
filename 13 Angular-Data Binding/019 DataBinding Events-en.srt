1
00:00:16,340 --> 00:00:20,390
In the previous session I have the property binding.

2
00:00:20,820 --> 00:00:28,350
That is one way of binding where we can use the interpellation syntax or we can assign a value to a

3
00:00:28,350 --> 00:00:35,240
property of a dorm element from property to property.

4
00:00:35,270 --> 00:00:37,560
You can also make use of the on syntax

5
00:00:40,250 --> 00:00:45,750
of the BIND syntax but in this particular section I'm going to cover even the binding.

6
00:00:45,920 --> 00:00:53,510
That means the reverse of what we have seen or we're any kind of change in the view in context of even

7
00:00:53,780 --> 00:01:01,040
any kind of even raised any of the actions on The View we would like to execute a mid-period in the

8
00:01:01,040 --> 00:01:02,430
Component class.

9
00:01:03,490 --> 00:01:05,340
That is what we would like to do here.

10
00:01:05,800 --> 00:01:11,410
Actions which result in the flow of data are from an element to the component.

11
00:01:11,410 --> 00:01:14,020
That's what is precisely the requirement.

12
00:01:14,020 --> 00:01:21,040
The only way to go about this user action is to listen to certain events or whatever those events keystroke

13
00:01:21,100 --> 00:01:22,670
events most humans.

14
00:01:22,810 --> 00:01:31,180
Given that even if you are doing well by so even binding is also one reason just that it is opposite

15
00:01:31,180 --> 00:01:34,050
of property by adding in property by name.

16
00:01:34,080 --> 00:01:40,750
It was taken from component and given to you but your actions of the view will be converted into matter

17
00:01:40,840 --> 00:01:42,570
in component.

18
00:01:42,580 --> 00:01:43,820
How is that done.

19
00:01:43,850 --> 00:01:48,290
Simply make use of the syntax with wrong brackets.

20
00:01:48,310 --> 00:01:51,460
So now I'm supposed to write a method.

21
00:01:51,520 --> 00:02:01,210
Let's say I'm supposed to write a metric in class we'll call it does button click.

22
00:02:01,440 --> 00:02:02,640
So we'll see it on

23
00:02:05,530 --> 00:02:06,760
some function I'm writing

24
00:02:10,020 --> 00:02:16,340
we can directly open close and write the function and say click

25
00:02:20,210 --> 00:02:23,560
now hold bind this button.

26
00:02:24,290 --> 00:02:26,130
Difference index or something.

27
00:02:26,450 --> 00:02:27,410
So I'll take your

28
00:02:34,370 --> 00:02:37,620
unforgotten we know that that isn't even called us.

29
00:02:38,090 --> 00:02:44,700
So in groundbreaker right click is equal to the function which we have done.

30
00:02:44,720 --> 00:02:46,220
Basically we have to call that function

31
00:02:54,850 --> 00:03:05,800
or we can also use the on syntax to can it on hyphen like and this is fine.

32
00:03:06,790 --> 00:03:07,890
Let's see no

33
00:03:11,660 --> 00:03:12,980
we have two buttons.

34
00:03:13,990 --> 00:03:15,420
Well something went wrong.

35
00:03:20,810 --> 00:03:21,660
For some

36
00:03:24,560 --> 00:03:26,210
Biscayan button up close to it.

37
00:03:26,240 --> 00:03:26,990
OK.

38
00:03:27,280 --> 00:03:36,930
Dr. This should be in the or else if I am using pedantic I should use the word improperly like this

39
00:03:36,930 --> 00:03:37,860
is essentially

40
00:03:42,020 --> 00:03:44,650
kill me and then

41
00:03:48,950 --> 00:03:50,760
this is very important.

42
00:03:50,930 --> 00:03:52,490
This is not a container.

43
00:03:52,680 --> 00:03:57,510
This is a container container that should be closed properly.

44
00:03:57,680 --> 00:03:59,490
We should not use the socket syntax.

45
00:04:03,760 --> 00:04:06,230
So two buttons appearance was bothersome.

46
00:04:06,250 --> 00:04:07,930
It's a big here.

47
00:04:08,380 --> 00:04:10,760
Click click.

48
00:04:10,990 --> 00:04:11,960
There's a lot of things

49
00:04:15,660 --> 00:04:16,150
of course.

50
00:04:16,170 --> 00:04:18,430
The second one behaves like a submit button also.

51
00:04:20,900 --> 00:04:22,790
When we click on it the form will get submitted.

52
00:04:22,790 --> 00:04:32,200
Right now we're not worried about all those things so they are a formidable bottom.

53
00:04:32,300 --> 00:04:38,270
In either case even just being in one case I have used wrong brackets and decks.

54
00:04:38,390 --> 00:04:46,870
In another case I've used on Hi-Point syntax like this we can have more examples created for this.

55
00:04:46,940 --> 00:04:49,520
I have downloaded two images.

56
00:04:49,610 --> 00:04:56,720
One is called the smiling covert and the other is the normal let me copy this into my application.

57
00:04:56,990 --> 00:05:00,720
So maybe here I can copy other images in the project.

58
00:05:00,730 --> 00:05:01,790
I'll take the folder

59
00:05:07,560 --> 00:05:13,730
images in images of copying this is copying from Explorer and pasted here.

60
00:05:15,340 --> 00:05:18,980
No we can't actually display these images.

61
00:05:19,490 --> 00:05:26,670
Let's say I am the sarcy is equal to first place.

62
00:05:27,060 --> 00:05:32,080
My normal thought into the extension of the file

63
00:05:35,380 --> 00:05:36,800
just a simple mistake.

64
00:05:38,220 --> 00:05:40,920
Lauren this requirement is.

65
00:05:40,940 --> 00:05:49,670
When I did the mouse cursor over that image I would like the image to change images mockup.

66
00:05:53,960 --> 00:05:55,040
Ressort smartphone.

67
00:05:55,070 --> 00:06:03,630
Yes I actually thought I gave incorrect but not the image is in for under images.

68
00:06:03,650 --> 00:06:10,860
And that's how we should run this.

69
00:06:10,900 --> 00:06:12,430
Now we have it.

70
00:06:12,990 --> 00:06:17,970
Now my requirement is when take the mouse cursor over this that image should change.

71
00:06:17,980 --> 00:06:21,540
So for this we will handle most events.

72
00:06:21,680 --> 00:06:23,550
So I have two.

73
00:06:23,670 --> 00:06:26,070
One is most all.

74
00:06:26,120 --> 00:06:40,200
And the other is most of those old so most all were equal to what's really going on.

75
00:06:40,360 --> 00:06:42,270
I'll have two functions here.

76
00:06:44,260 --> 00:06:46,870
On most all the

77
00:06:52,550 --> 00:06:58,220
and the other is on oh

78
00:07:03,750 --> 00:07:06,450
no.

79
00:07:06,570 --> 00:07:16,780
If I take this function and simply put it here like what is most old

80
00:07:21,180 --> 00:07:26,090
if I take the other one and put it there there is a big problem.

81
00:07:26,190 --> 00:07:27,320
What is the problem.

82
00:07:31,120 --> 00:07:34,980
Problem is here this is component.

83
00:07:35,140 --> 00:07:38,550
How am I going to get the reference to this image.

84
00:07:40,400 --> 00:07:44,290
I need the reference to that image object over here.

85
00:07:44,330 --> 00:07:50,060
How are we going to get in these kind of situations from here.

86
00:07:50,090 --> 00:07:52,230
We are going to posit an object.

87
00:07:52,330 --> 00:07:57,370
Let's say I'm going to pass the event object and same thing.

88
00:07:57,380 --> 00:08:02,370
I would like to do heroes of this event object we will get it here.

89
00:08:02,540 --> 00:08:04,050
Let's call it doesn't it.

90
00:08:04,340 --> 00:08:08,240
And it's of type and same here.

91
00:08:10,110 --> 00:08:16,040
Now the evil is a building object in this object.

92
00:08:16,170 --> 00:08:19,080
We're going to have this property called Target

93
00:08:23,990 --> 00:08:27,690
and Target is nothing but the corresponding human.

94
00:08:27,960 --> 00:08:37,890
So for that target I would like to know Tiant the SRT at which I would like to give it does my

95
00:08:40,650 --> 00:08:42,240
whole Wardog be.

96
00:08:43,320 --> 00:08:44,720
And this is that in.

97
00:08:45,040 --> 00:08:54,360
So slice images likewise no problem is I can always call or even that agree.

98
00:08:54,590 --> 00:08:56,010
I have to use this parameter.

99
00:08:56,180 --> 00:09:01,220
So if it is you know like why the same thing over here.

100
00:09:03,090 --> 00:09:05,920
You need target my normal

101
00:09:10,140 --> 00:09:11,090
Let's run this

102
00:09:17,740 --> 00:09:22,390
beautiful work we get a different take out.

103
00:09:22,430 --> 00:09:27,380
We get the artist at.

104
00:09:27,420 --> 00:09:30,380
So what is that we look at last May we are not learning.

105
00:09:30,400 --> 00:09:38,980
Maybe maybe we are learning how we can parse data to embed turd in the component object.

106
00:09:39,150 --> 00:09:42,980
That is how we are able to pass the event object precisely.

107
00:09:43,870 --> 00:09:49,860
Now mind you if you are a javascript programmer you may want to actually write something like this and

108
00:09:49,860 --> 00:09:51,010
pass it.

109
00:09:51,300 --> 00:09:57,110
You will probably write something like this but what is this here.

110
00:09:57,160 --> 00:10:00,670
You cannot make use of this data here.

111
00:10:01,580 --> 00:10:11,500
If I use this and let's say this will be an image in such case this will be imaged or something.

112
00:10:11,610 --> 00:10:13,220
This is not going to work for you.

113
00:10:18,910 --> 00:10:22,220
It doesn't change but it will not install

114
00:10:27,240 --> 00:10:28,810
is not working.

115
00:10:29,810 --> 00:10:36,080
So even the object has to be passed to get the reference to the element but on which the event has been

116
00:10:36,080 --> 00:10:39,630
placed and how do we get the reference to that element.

117
00:10:39,680 --> 00:10:47,200
Even non-target once we have human target all the DOM properties of that target object can be used over

118
00:10:47,200 --> 00:10:47,690
here.

119
00:10:49,080 --> 00:10:52,460
So let me restore this even more target.

120
00:10:52,940 --> 00:10:57,270
Even even PopTech also has a property called

121
00:11:00,510 --> 00:11:10,920
so I'll just put another variable let's call it as even type by default exempting and the value of that

122
00:11:10,960 --> 00:11:16,940
even I would like to said here remember because it is not a property of an object which will use this

123
00:11:16,950 --> 00:11:23,700
dot even like is equal to human DOT can be used.

124
00:11:23,940 --> 00:11:25,980
Same thing I will do here also.

125
00:11:28,910 --> 00:11:43,410
How good is Blender's even thought he had begun late even like all the so what the type of event we

126
00:11:43,410 --> 00:11:44,670
are trying to display.

127
00:11:45,070 --> 00:11:46,300
So on this no

128
00:11:50,970 --> 00:11:57,540
Mosel are also mozo.

129
00:11:57,660 --> 00:12:00,200
So what is the name of the event also can be used.

130
00:12:00,240 --> 00:12:01,460
That means what.

131
00:12:01,940 --> 00:12:04,730
Correct club both in the single function no.

132
00:12:04,740 --> 00:12:06,570
Yes we can do that.

133
00:12:06,620 --> 00:12:18,720
How are most of them have a single calling on Mole's

134
00:12:21,880 --> 00:12:23,490
and yet I'm going to get even.

135
00:12:23,510 --> 00:12:27,580
Does object.

136
00:12:28,160 --> 00:12:32,910
And what we do is we'll say is this dog

137
00:12:35,700 --> 00:12:46,990
even love this is it or even into the evening or dying is equal to most all our

138
00:12:50,720 --> 00:12:52,200
then is

139
00:12:56,130 --> 00:12:59,930
as I did this

140
00:13:03,880 --> 00:13:07,890
an advantageous same function we can use for both of them.

141
00:13:09,080 --> 00:13:14,260
On most most we don't have to change the name

142
00:13:17,810 --> 00:13:21,100
so re-usability to Parminter is what we are achieving.

143
00:13:21,110 --> 00:13:22,330
Something went wrong.

144
00:13:24,170 --> 00:13:27,800
As usual press F 12 to figure out what is wrong.

145
00:13:28,080 --> 00:13:31,460
Arnheim love cannot read the property that is ok.

146
00:13:31,640 --> 00:13:39,050
Fail to load resource or not for you and that is fine for you to take on that is

147
00:13:42,070 --> 00:13:43,500
all day of

148
00:13:46,620 --> 00:13:48,060
line number if one

149
00:13:50,980 --> 00:13:55,640
line number one oh it's a B W what you see.

150
00:13:55,660 --> 00:13:59,170
The beauty of a script is giving us a

151
00:14:02,480 --> 00:14:02,920
spooking

152
00:14:07,670 --> 00:14:11,540
by mistake single equal that becomes the assignment we want combatives.

153
00:14:11,870 --> 00:14:20,420
So even duct tape is mozo we are using does that is that 7:59 of having an extra Parkington re-usability

154
00:14:22,820 --> 00:14:29,490
or what is very important is the properties in the event object are going to vary based on the type

155
00:14:29,490 --> 00:14:32,570
of Dom human which we have to handle.

156
00:14:33,130 --> 00:14:38,560
Let's say for example I would like to have a text box.

157
00:14:38,570 --> 00:14:40,170
Another example I like.

158
00:14:40,490 --> 00:14:48,110
We have text box small say in pop music with no text for this text box.

159
00:14:48,110 --> 00:14:56,200
We would like to handle the keyboard events so there are only one key.

160
00:14:56,670 --> 00:15:10,600
So here you get up and get up to some function which I'm going to write on P and Basco it even object.

161
00:15:11,080 --> 00:15:16,420
So whenever I press and release this function is going to execute.

162
00:15:16,880 --> 00:15:19,330
So this function now I'm supposed to write here.

163
00:15:22,310 --> 00:15:24,980
You did not write an

164
00:15:28,370 --> 00:15:29,540
note here.

165
00:15:29,810 --> 00:15:31,130
We want to give it.

166
00:15:31,130 --> 00:15:35,600
We are basing Hogan we get that which is pressed.

167
00:15:36,180 --> 00:15:37,700
Let's do one thing.

168
00:15:38,010 --> 00:15:38,320
Your

169
00:15:41,990 --> 00:15:46,250
another property case which is by default implicitly.

170
00:15:46,780 --> 00:15:48,820
So today I prosequi.

171
00:15:48,850 --> 00:15:57,360
I would like to append to the piece so Geest less equal do you with the dot.

172
00:15:57,480 --> 00:16:02,410
We can make use of able to use it in context of on key.

173
00:16:02,680 --> 00:16:08,750
But we will not be able to use key in context of Mozilla and remember that

174
00:16:12,790 --> 00:16:16,090
this know this is

175
00:16:21,160 --> 00:16:22,060
keep rest

176
00:16:28,710 --> 00:16:29,130
at

177
00:16:34,540 --> 00:16:35,160
rotundus

178
00:16:40,040 --> 00:16:57,240
now I'm pressing the actually not when I press release because the key B C D E F backspins space line

179
00:17:05,750 --> 00:17:12,170
changing the value is not reflected that is because it is all one and we are only doing a painting here.

180
00:17:13,870 --> 00:17:22,200
But if you want to take the value of the text box itself then what you're going to go in that kind of

181
00:17:22,220 --> 00:17:26,640
is you'll say volume.

182
00:17:27,700 --> 00:17:37,490
And yet say does not run it will do you want to dot dot dot right.

183
00:17:38,430 --> 00:17:43,430
Then we're going to get the value of the X current value of the text box if you want to get press.

184
00:17:43,450 --> 00:17:46,170
You have to use GHI if you want element.

185
00:17:46,240 --> 00:17:48,150
You should use target and then the corresponding

186
00:18:05,600 --> 00:18:17,570
so I put it b c d backspace and see values changing and backspace I think it to keep list.

187
00:18:17,580 --> 00:18:24,980
So what is important is this event object is nothing but precisely the object in whatever.

188
00:18:25,020 --> 00:18:30,380
Even properties are that the same thing we can make use of all here also.

189
00:18:30,660 --> 00:18:37,200
And what is that you should know that even object properties are going to be based on the input element

190
00:18:37,270 --> 00:18:41,130
or based on the instrument element with which we are making use of

191
00:18:44,200 --> 00:18:44,610
all this.

192
00:18:44,620 --> 00:18:47,610
We have seen this with a different example.

193
00:18:47,830 --> 00:18:54,870
We have hot button click on click double click mozo word for Button we are doing all this same event

194
00:18:55,130 --> 00:19:03,340
you're passing the event object and I'm pretty sure the handler we will type even type mode or example

195
00:19:03,820 --> 00:19:04,730
or code.

196
00:19:04,960 --> 00:19:12,340
We are simply displaying the text of them the better example realtime image on key.

197
00:19:12,430 --> 00:19:20,360
We can use even target but one or we can simply use the property of the event object really depending

198
00:19:20,360 --> 00:19:28,410
on the type of bomb event target property or even reference to that element that reset the event.

199
00:19:28,550 --> 00:19:30,110
Very important target property.

200
00:19:31,040 --> 00:19:39,490
It refers to the input element or any other element which has raised the event passing event object

201
00:19:39,490 --> 00:19:41,490
is a dubious practice.

202
00:19:41,500 --> 00:19:45,550
Now what is the problem with that when we're passing event object.

203
00:19:45,820 --> 00:19:51,000
The matter will have complete control over the code.

204
00:19:51,370 --> 00:19:56,330
That means through an object it can do anything with the element.

205
00:19:56,380 --> 00:20:04,900
So basically I said through event object component is getting extra information which is more than required

206
00:20:05,530 --> 00:20:11,540
by the component we are passing the complete object.

207
00:20:11,540 --> 00:20:17,700
That means we are passing the complete control reference element reference so component can do anything

208
00:20:17,700 --> 00:20:19,450
on that particular element.

209
00:20:19,560 --> 00:20:24,000
And this may not be a preferred solution in many cases.

210
00:20:24,000 --> 00:20:28,970
So in those kind of cases we are going to make use of that template reference variable.

211
00:20:29,010 --> 00:20:32,990
Now what is this template reference available here.

212
00:20:32,990 --> 00:20:37,040
I know the same example of Ameet

213
00:20:40,380 --> 00:20:43,430
but the concept of completely different here.

214
00:20:58,400 --> 00:21:11,150
What is your you has and some ID know what is the advantage of highs and some ID know using this ID

215
00:21:11,290 --> 00:21:17,830
we can refer to this image that anywhere within the template.

216
00:21:18,060 --> 00:21:24,160
I repeat using the ID we will be able to refer to that image tag anywhere within the template thats

217
00:21:24,160 --> 00:21:25,710
been taught in malls or what.

218
00:21:25,770 --> 00:21:36,650
I dont have to pull a component function yet it sounds like an empty one door as RC is equal to but

219
00:21:42,320 --> 00:21:44,770
the bottom giving in single quotes.

220
00:21:45,210 --> 00:21:49,060
This is all likewise.

221
00:21:50,730 --> 00:21:53,610
This whole thing can be done for almost O'Toole's

222
00:22:00,130 --> 00:22:05,320
a lot of.

223
00:22:05,410 --> 00:22:07,820
So we have given a reference

224
00:22:11,170 --> 00:22:14,710
element thats called a template reference syntax

225
00:22:17,520 --> 00:22:23,910
fantastic of course if you've written like Robert with them they need that recycling problem.

226
00:22:27,880 --> 00:22:34,340
So it is recommended to use this index or we're passing event object for the only reason that we are

227
00:22:34,340 --> 00:22:39,950
passing even the object to the component we are completely exposing the input element to the component

228
00:22:40,640 --> 00:22:47,990
and component then we'll be able to change or get lots of information about the input element to the

229
00:22:47,990 --> 00:22:51,440
actual target because it's not actually getting the target reference.

230
00:22:51,440 --> 00:22:55,340
Everything about it is exposed and that is not recommended.

231
00:22:58,300 --> 00:23:03,240
So this will provide direct access to the equipment from within the template within that template.

232
00:23:03,240 --> 00:23:04,450
That's the key.

233
00:23:04,840 --> 00:23:10,880
I got use has be the one in my company and I can use it only within the template.

234
00:23:10,910 --> 00:23:17,030
That said So what is the value of the text box that you're trying to display here.

235
00:23:18,370 --> 00:23:20,050
But then there is a small console.

236
00:23:20,050 --> 00:23:22,050
Let me show you just the other examples.

237
00:23:25,110 --> 00:23:27,390
I typed up the same text

238
00:23:33,690 --> 00:23:35,330
be 61.

239
00:23:35,930 --> 00:23:37,280
I don't have this key or

240
00:23:41,170 --> 00:23:42,660
run this do.

241
00:23:42,750 --> 00:23:45,300
I'm going to change the value of the text box.

242
00:23:45,640 --> 00:23:47,750
It's not going to reflect any of it.

243
00:23:48,520 --> 00:23:53,440
But the moment I write here get up to zero

244
00:23:58,170 --> 00:24:05,820
zero is nothing but just some expression that said the expression now time the value is changing.

245
00:24:05,880 --> 00:24:06,210
See

246
00:24:12,810 --> 00:24:16,590
now why is it so this is a typical reason

247
00:24:19,310 --> 00:24:27,910
this won't work at all unless you combine the two and even remember no template reference is going to

248
00:24:27,910 --> 00:24:28,170
work.

249
00:24:28,220 --> 00:24:35,610
Only if there is at least one you can handle for that particular in that particular element.

250
00:24:35,750 --> 00:24:42,970
If you're not handling any even for this element that it is not going to angler does the binding and

251
00:24:42,970 --> 00:24:50,980
therefore the screen only if the app does something in response to the asynchronous events such as keystrokes

252
00:24:52,100 --> 00:24:53,600
very very important thing.

253
00:24:54,350 --> 00:25:00,230
So just putting a hash and then the ID and using the ID somewhere else is not enough for the corresponding

254
00:25:00,260 --> 00:25:01,480
element or bag.

255
00:25:01,580 --> 00:25:06,950
You're are also supposed to handle the event at least one event and in the event handler you do nothing

256
00:25:07,520 --> 00:25:09,020
like zero is actually nothing.

257
00:25:09,020 --> 00:25:15,290
We are not trying to do anything it's just like a null statement that this example goldmines given to

258
00:25:15,290 --> 00:25:20,650
us number zero Sawkins made possible statement.

259
00:25:21,020 --> 00:25:27,960
So what if we want to was the value of the text box the value of the text box I want to pass the component

260
00:25:28,080 --> 00:25:36,440
in such case what to what we really do is in those kind of cases here.

261
00:25:39,980 --> 00:25:44,230
Let's say Argentine's dysfunction itself.

262
00:25:45,420 --> 00:25:53,680
The sit on key and possible this on the one because on get the used and possible this be 61

263
00:25:57,000 --> 00:26:01,980
the 61 is the reference to is input only the value of that text box.

264
00:26:01,990 --> 00:26:09,760
I'm not passing on.

265
00:26:10,000 --> 00:26:27,250
Same thing on q one but what we're getting here is only the I can give of screen so here that it needs

266
00:26:30,800 --> 00:26:31,520
one.

267
00:26:32,010 --> 00:26:32,450
That's it.

268
00:26:32,450 --> 00:26:33,470
No target.

269
00:26:33,830 --> 00:26:39,800
So unlike in context of even we are getting the complete reference to target here we are only getting

270
00:26:39,800 --> 00:26:40,160
the right.

271
00:26:40,190 --> 00:26:41,910
That means only what is required.

272
00:26:41,910 --> 00:26:43,620
We are able to pass the matter.

273
00:26:43,800 --> 00:26:47,960
We are not passing everything to the matter by fiat on this

274
00:26:53,440 --> 00:26:53,970
the

275
00:27:05,300 --> 00:27:11,820
so that is also one thing which you should know when the template reference concept is used it's easier

276
00:27:11,820 --> 00:27:17,220
to get in input box with the template reference variable done to the event object.

277
00:27:17,220 --> 00:27:21,790
What we getting the input box by Darcie passing the reference.

278
00:27:21,790 --> 00:27:26,400
Not that this reference is not a good reference but the input box.

279
00:27:26,430 --> 00:27:27,700
So very handy syntax.

280
00:27:27,760 --> 00:27:35,940
We'll read more of your coding and finally you should also note that some facility of even filtering.

281
00:27:36,000 --> 00:27:37,930
What is this key even filtering.

282
00:27:38,150 --> 00:27:44,400
Right now the code is executing for every key type in the text box.

283
00:27:44,420 --> 00:27:49,000
I would like the code to execute only when enter is pressed.

284
00:27:49,040 --> 00:27:55,220
For example imagine the requirement is I would like to display the text in a lot so the whole message

285
00:27:55,220 --> 00:27:59,390
is going to become an object in the view.

286
00:27:59,390 --> 00:28:04,050
I would like to display an alert about that for whatever reason.

287
00:28:04,090 --> 00:28:14,810
So every time I press a key when new alert will come all day a lot has come I think a lot has come.

288
00:28:15,000 --> 00:28:15,830
I see.

289
00:28:15,870 --> 00:28:17,050
A lot has come.

290
00:28:17,070 --> 00:28:20,240
I want that alert to come only when I press enter.

291
00:28:20,850 --> 00:28:23,000
So what to do in that kind of case.

292
00:28:23,240 --> 00:28:30,640
We will use in such cases up Daut and get a dog.

293
00:28:30,680 --> 00:28:37,350
And it's like filtering it we filtering the keyboard even and saying only when enter is pressed on keep

294
00:28:37,450 --> 00:28:39,320
up.

295
00:28:39,440 --> 00:28:39,980
That means.

296
00:28:40,010 --> 00:28:41,950
And the key is released the display.

297
00:28:42,010 --> 00:28:45,550
So I take maybe CD press Enter key on the keyboard.

298
00:28:46,070 --> 00:28:52,000
You see only then the next box up you select this we will be able to do some kind of filtering.

299
00:28:52,000 --> 00:28:53,470
What are these options.

300
00:28:53,500 --> 00:28:54,120
All this.

301
00:28:54,170 --> 00:29:01,870
You'll have to load only from the documentation or additional options which are available such such

302
00:29:02,200 --> 00:29:08,650
such as you love to learn from the documentation going up even then and from there you can figure out

303
00:29:08,860 --> 00:29:13,000
what other events are what are the filters which are available to us.

304
00:29:13,450 --> 00:29:13,710
OK.

305
00:29:13,720 --> 00:29:16,090
Before I close one more observation here.

306
00:29:16,750 --> 00:29:18,040
When I run this program

307
00:29:20,840 --> 00:29:24,950
I type here in B C D I press enter.

308
00:29:24,950 --> 00:29:34,560
I get this good enough but if I type more text d undoubtedly the cursor will say you're not going to

309
00:29:34,560 --> 00:29:41,730
get anything but what my requirement is if I click outside or I enter it.

310
00:29:41,740 --> 00:29:45,720
I guess we want that to exit you.

311
00:29:45,740 --> 00:29:52,520
So that is where we will handle one more event even that is very popular remember block event is going

312
00:29:52,520 --> 00:29:57,430
to execute every time we lose the focus from the text box.

313
00:29:57,470 --> 00:29:58,940
So same thing we can do at

314
00:30:02,270 --> 00:30:06,620
not on this.

315
00:30:06,780 --> 00:30:12,730
I'm not going to type ABC and go

316
00:30:15,690 --> 00:30:16,610
and we stuck

317
00:30:20,410 --> 00:30:28,330
because we are stuck where you are stuck because of a lot we are losing the focus and we are again getting

318
00:30:28,330 --> 00:30:36,110
back to focus and because you're losing the focus again on globalising again we will a lot.

319
00:30:36,130 --> 00:30:39,450
So this is supposed to be better cheat.

320
00:30:39,640 --> 00:30:45,850
That means only when the value in the text boxes change too and we lose the focus that time the event

321
00:30:45,850 --> 00:30:47,240
is going to get raised.

322
00:30:48,420 --> 00:30:51,680
And it's working every hand.

323
00:30:51,860 --> 00:31:00,880
And so the I the gay rights type or the focus this will work and focus will go back

324
00:31:03,960 --> 00:31:05,120
some blood.

325
00:31:05,650 --> 00:31:12,330
Every time you lose the focus it is going to exiguous change only when you change the content and lose

326
00:31:12,330 --> 00:31:13,820
the focus it will execute.

327
00:31:13,830 --> 00:31:21,480
So bigger can create problems and generally will not create the problem so that no won't even be hunting.

328
00:31:22,020 --> 00:31:26,250
So yes in this particular session we got a lot of important things about the events.

329
00:31:26,370 --> 00:31:29,250
We have seen how we can actually handle the event.

330
00:31:29,370 --> 00:31:33,210
There are so many malls event there are so many don't even is available.

331
00:31:34,720 --> 00:31:42,640
Click double click event Keeble events cut copy paste events focus even blurry when scrolling and drag

332
00:31:43,050 --> 00:31:45,260
drag and drop are also supported here.

333
00:31:47,000 --> 00:31:48,570
And how do we do even mapping.

334
00:31:48,590 --> 00:31:58,210
We use either the wrong brackets index or we use the attribute on hyphen event name and subsequently

335
00:31:58,660 --> 00:32:04,500
we have seen how we can pass data to the event handler using event object.

336
00:32:04,570 --> 00:32:06,070
Even in the template.

337
00:32:06,220 --> 00:32:12,640
And here we can capture it in available even the object will have properties and these properties can

338
00:32:12,640 --> 00:32:20,300
vary based on the context in which particular event is based event object is not recommended.

339
00:32:20,300 --> 00:32:25,960
And that's where we have seen how we can make use of that template reference when we are using template

340
00:32:25,960 --> 00:32:26,350
reference.

341
00:32:26,350 --> 00:32:32,210
We have to ensure that at least one event is handled on that particular element.

342
00:32:32,260 --> 00:32:34,910
Otherwise template reference will not work.

343
00:32:35,110 --> 00:32:36,380
Binding will not happen.

344
00:32:37,490 --> 00:32:42,040
And finally we have seen how we can do even filtering.

345
00:32:42,410 --> 00:32:42,800
Thank you.

