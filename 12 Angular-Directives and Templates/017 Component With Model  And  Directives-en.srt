1
00:00:15,480 --> 00:00:23,100
Know in the previous season have shown you how we can make use of a component and the interpellation

2
00:00:23,100 --> 00:00:30,150
feature of component where the properties can be taken from the component class and embedded into unhedged

3
00:00:30,150 --> 00:00:31,570
email template.

4
00:00:31,950 --> 00:00:37,490
But many times in the project we are going to have the moral class.

5
00:00:37,700 --> 00:00:45,160
So Hogan imortal clause be used in say a component template and of course insert the component class

6
00:00:45,870 --> 00:00:48,440
so I'll continue with the same project.

7
00:00:48,570 --> 00:00:58,080
What I would like to do now is in the project under folder only I would like to add a new class a typescript

8
00:00:58,090 --> 00:01:00,970
class I'll call that a faculty

9
00:01:03,830 --> 00:01:08,410
or let's call it employing faculty as an employee.

10
00:01:08,410 --> 00:01:09,950
That doesn't say that I'm making.

11
00:01:10,130 --> 00:01:16,400
But right now I'm using employee and what is that an employee is made up of.

12
00:01:16,660 --> 00:01:25,910
First thing I would like to have employee classes exported Clauss employee an inside employee.

13
00:01:25,920 --> 00:01:30,370
I would like to have ID with just a number.

14
00:01:33,150 --> 00:01:39,650
Name with a string and maybe salary which is also.

15
00:01:40,230 --> 00:01:47,610
So a simple Employee class we have read on here and this employee class we would like to use it in the

16
00:01:47,610 --> 00:01:48,280
controller.

17
00:01:48,300 --> 00:01:53,140
I mean the component class also are for simplicity.

18
00:01:54,910 --> 00:01:56,180
A construct.

19
00:01:56,640 --> 00:02:03,960
So give your constructor and through this constructor I would like to initialize the

20
00:02:07,040 --> 00:02:07,670
name

21
00:02:19,780 --> 00:02:20,850
and salad

22
00:02:28,160 --> 00:02:33,080
does not only do I need this dog name is it.

23
00:02:33,110 --> 00:02:33,640
Well.

24
00:02:36,650 --> 00:02:38,920
Name this dog.

25
00:02:38,960 --> 00:02:43,090
Salary equal to salary.

26
00:02:43,370 --> 00:02:48,430
No this employee plus I would like to use it in company.

27
00:02:48,890 --> 00:02:53,540
So what is the first thing I love to do here you're in palm print on top.

28
00:02:53,570 --> 00:02:55,070
I should write import

29
00:02:58,610 --> 00:03:04,510
employee from DOT last employee.

30
00:03:04,880 --> 00:03:11,720
We are doing our script import a typescript import from Ahmadu.

31
00:03:11,890 --> 00:03:14,510
Next I would like to do it in my class.

32
00:03:14,560 --> 00:03:18,510
I would like to have internal faculty meeting.

33
00:03:18,540 --> 00:03:28,770
I would like to change that to an area of faculty's so I won't let a say have supporting faculties or

34
00:03:28,770 --> 00:03:35,210
supporting employees supporting employees at all.

35
00:03:35,210 --> 00:03:41,730
These are of tight employee added They've been taught.

36
00:03:41,810 --> 00:03:46,060
Some of it to not create an employee and initialize it.

37
00:03:46,410 --> 00:03:52,200
So I hope you agree that there is a square bracket inside square brackets for every employee object.

38
00:03:52,200 --> 00:04:07,220
I'm going to Croydon flog a new employee and here only we can ID name a lovely or employed one.

39
00:04:09,720 --> 00:04:14,210
Up some salary call Mo.

40
00:04:14,380 --> 00:04:16,550
I would like to add multiple employees

41
00:04:25,500 --> 00:04:42,760
so 10 to 20 for an find literacy and we have got employed in one two three four five

42
00:04:46,040 --> 00:04:47,430
and something a lot of them some the

43
00:04:57,310 --> 00:05:00,320
not these details I would like to display.

44
00:05:00,950 --> 00:05:04,360
Well maybe at the end of the section here.

45
00:05:07,070 --> 00:05:08,510
I would like to have one more.

46
00:05:08,510 --> 00:05:09,320
No that is

47
00:05:13,470 --> 00:05:14,900
associated employees.

48
00:05:14,910 --> 00:05:18,580
That's what I call it supporting employees.

49
00:05:18,900 --> 00:05:20,760
So the employees who are supporting

50
00:05:27,930 --> 00:05:36,340
the builders I see a dog and then I would like to provide a list of all employees you are supporting.

51
00:05:36,360 --> 00:05:47,740
And now I would like to present them in a table or for my music if you want to use people with the

52
00:05:52,960 --> 00:05:53,800
Didi's

53
00:05:57,650 --> 00:06:00,310
be a nice voice be heard.

54
00:06:00,350 --> 00:06:01,630
I would like to have any

55
00:06:09,510 --> 00:06:13,560
the name sounded.

56
00:06:14,220 --> 00:06:20,450
And then we should repeat the our section for every employee should we be 30 or section.

57
00:06:20,460 --> 00:06:21,410
That is a requirement.

58
00:06:21,410 --> 00:06:23,430
Now how to do that.

59
00:06:23,430 --> 00:06:30,370
For that I'm going to make use of stop and anti-foreign equal do.

60
00:06:31,050 --> 00:06:46,210
Let him be off selected supported employees that EFB of support for employees.

61
00:06:46,230 --> 00:06:49,470
So we are getting that employing me now.

62
00:06:49,530 --> 00:06:50,640
I would like to put up

63
00:06:53,620 --> 00:07:07,010
in some of your a d head if you have any really any theory and this should be what the police and of

64
00:07:07,250 --> 00:07:08,240
your MP are.

65
00:07:08,380 --> 00:07:08,790
I am

66
00:07:11,590 --> 00:07:22,180
in the position of the mp not named in the pledge of ENP not somebody.

67
00:07:22,550 --> 00:07:30,850
Property property is going to get rendered no with the post Hading and then for each employee one diong

68
00:07:34,290 --> 00:07:35,970
some compilation error.

69
00:07:36,540 --> 00:07:39,110
I missed Komal case.

70
00:07:39,550 --> 00:07:45,660
Yes Bill once again.

71
00:07:45,770 --> 00:07:54,420
No it has no control of Feiffer execution and we should have a table unless something went wrong.

72
00:07:54,840 --> 00:07:56,300
Yes something has gone wrong.

73
00:07:56,430 --> 00:07:58,380
Which is good for you.

74
00:07:58,560 --> 00:08:02,980
We are going to see what is an unexpected closing tag.

75
00:08:03,080 --> 00:08:14,740
Beyond the template is not properly structured problem is yet floor.

76
00:08:14,960 --> 00:08:15,760
Double quotes

77
00:08:19,720 --> 00:08:21,510
as we go to tape.

78
00:08:22,230 --> 00:08:24,660
Of course style you can provide borders and all that.

79
00:08:24,660 --> 00:08:33,980
That's fine but just one two three four five and the corresponding name cell long and for them see it

80
00:08:33,980 --> 00:08:39,470
is just a revision of whatever topics we have covered actually in the previous session.

81
00:08:39,680 --> 00:08:42,470
That is impractically demonstrating this.

82
00:08:42,570 --> 00:08:45,990
For if can be used in projects.

83
00:08:46,040 --> 00:08:50,930
So now I would like to add one more property here rather more.

84
00:08:51,290 --> 00:09:03,170
I export you know employed by employee types.

85
00:09:03,170 --> 00:09:07,490
We would like to have something like employees work temporary

86
00:09:11,180 --> 00:09:20,220
employees who are Coleman and and sometimes we would like to have some contract employees little longer

87
00:09:20,220 --> 00:09:21,170
duration.

88
00:09:21,540 --> 00:09:26,870
So let's say this isn't you know we have created allow for every employee.

89
00:09:26,930 --> 00:09:30,360
I would like to have a prop. no employee time.

90
00:09:31,010 --> 00:09:36,500
We call it as I call them employee.

91
00:09:36,980 --> 00:09:39,920
And of course we will also specify by

92
00:09:45,440 --> 00:09:47,740
long time employee that

93
00:09:51,330 --> 00:10:00,920
this does not equal to time.

94
00:10:01,000 --> 00:10:10,660
Now when we cleared off every employee we have to provide by common employee type Not that it is not

95
00:10:10,660 --> 00:10:16,010
listing employee data because on top we have not done an import of employee type.

96
00:10:16,420 --> 00:10:18,190
So we import employee type.

97
00:10:18,350 --> 00:10:27,160
No go down and give your employee they've got maybe contract for fuel.

98
00:10:27,170 --> 00:10:29,420
Let us give the contract

99
00:10:32,370 --> 00:10:36,270
maybe I'll give employee time.

100
00:10:36,330 --> 00:10:45,610
But then down somewhere I'll eat some random lines employee time Daut temperedly

101
00:10:51,640 --> 00:10:52,600
Noddy's values.

102
00:10:52,630 --> 00:11:01,040
I would like to display in the grilled that means here I should first add when you call them the head

103
00:11:02,610 --> 00:11:08,020
class type of them like.

104
00:11:08,620 --> 00:11:12,570
And then here I would like to provide Swades.

105
00:11:12,940 --> 00:11:17,560
I would say I have indeed inside the

106
00:11:21,720 --> 00:11:22,470
Keyspan

107
00:11:32,630 --> 00:11:39,480
for this but we can do a binding and fit.

108
00:11:39,660 --> 00:11:47,940
Do you P-Dog know what are the possible things here we should do.

109
00:11:47,940 --> 00:12:02,210
You know we spent time with Andy with case equal to the what is that what the value will be either 0

110
00:12:02,210 --> 00:12:05,880
1 or 2.

111
00:12:05,890 --> 00:12:09,060
So let's see here first zero is complete.

112
00:12:09,220 --> 00:12:15,280
So give 0 and give temporary

113
00:12:22,000 --> 00:12:40,840
and then multiple copies or one or two will get here is you fall in line LOL this is on fire.

114
00:12:41,370 --> 00:12:43,040
This is called

115
00:12:45,880 --> 00:12:50,390
Norton does and we should get the proper text.

116
00:12:51,110 --> 00:12:53,210
Something went wrong again.

117
00:12:53,930 --> 00:13:05,190
So BRASIER to parsing spanne may have closing another that losing us span is not done properly.

118
00:13:05,460 --> 00:13:08,720
Yeah does double cause is not required.

119
00:13:08,740 --> 00:13:15,310
That is a problem not on record.

120
00:13:20,980 --> 00:13:24,040
Not one more feature I want to add to this example.

121
00:13:24,370 --> 00:13:35,700
I would like to have one of these employees as selected Ambrois so say here is a property selected so

122
00:13:35,910 --> 00:13:48,270
all employee right now we can say this is of by employee and its value is supported does not support

123
00:13:48,330 --> 00:13:53,610
the employees of somebody let's say 3 randomly or given the number.

124
00:13:53,620 --> 00:13:56,600
In this example everything is constant.

125
00:13:56,620 --> 00:14:01,350
Eventually we are going to get all these values as dynamic but that is much needed.

126
00:14:02,050 --> 00:14:03,980
Right now everything is constant here.

127
00:14:05,300 --> 00:14:10,490
So does NOT support that implies Now how are we going to display the supported employee in the grid

128
00:14:11,150 --> 00:14:12,490
in that table.

129
00:14:13,010 --> 00:14:17,510
So let's say we have one more one more heads.

130
00:14:17,690 --> 00:14:19,590
Maybe we keep it empty.

131
00:14:20,450 --> 00:14:30,730
And here I would like to have one more to we had what I want to do in this city is this split up like

132
00:14:30,740 --> 00:14:32,430
this that's.

133
00:14:32,970 --> 00:14:35,380
But what is the condition for this plan.

134
00:14:35,430 --> 00:14:42,420
We don't want to put all the rules we want only for the rule which is marked as selected as stock.

135
00:14:43,080 --> 00:14:50,550
So that is where I would like to know use that and see if that active and if what is the value we should

136
00:14:50,550 --> 00:15:00,610
do if selected supporting employee is equal to EMV then only we won't this.

137
00:15:00,680 --> 00:15:03,880
I don't think otherwise we don't want that.

138
00:15:04,380 --> 00:15:08,110
Laura let's run this.

139
00:15:08,370 --> 00:15:12,430
And here you see we're getting the armor the symbol is not good enough.

140
00:15:13,580 --> 00:15:16,350
Oh something went wrong.

141
00:15:18,390 --> 00:15:20,270
One two three four five.

142
00:15:20,640 --> 00:15:24,300
Well they draw on this is to you the

143
00:15:27,320 --> 00:15:28,200
on this

144
00:15:38,210 --> 00:15:42,930
one two three four five selected

145
00:15:49,460 --> 00:15:52,080
and here this thing is wrong.

146
00:15:52,780 --> 00:15:53,450
OK.

147
00:15:53,720 --> 00:15:54,650
In other ways yes.

148
00:15:54,650 --> 00:15:58,740
Also we want B-D but we want that to be empty.

149
00:15:59,600 --> 00:16:09,890
So I hold out for that love to be late in this case elaborated template news that it's been empty.

150
00:16:18,400 --> 00:16:20,100
Well call us M8

151
00:16:23,510 --> 00:16:30,180
and be real on this and use it for the spot

152
00:16:33,720 --> 00:16:40,350
ans of course BMB MDO Let's take

153
00:16:43,110 --> 00:16:49,740
yes this is the rules and maybe we can have more better thing done with style and all that.

154
00:16:49,740 --> 00:16:53,130
But we haven't learned about styling at this point of time.

155
00:16:53,280 --> 00:16:59,250
We can probably give a different background color for this that we're going to enhance it later.

156
00:16:59,520 --> 00:17:06,840
But yes at this point of time we have a proper example in place where we are using more than any other

157
00:17:07,230 --> 00:17:14,920
employee we are using as more I can say the data is an object in traffic unusual properties.

158
00:17:15,000 --> 00:17:21,320
We all want to put it in the object like this and that is the real time projects are also done so if

159
00:17:21,330 --> 00:17:26,240
anything of course what I want to do is this template has really become very big.

160
00:17:26,340 --> 00:17:30,580
Now what I would I really want to do is cut all this template from here.

161
00:17:31,950 --> 00:17:35,570
And I would like to place it in a separate file.

162
00:17:36,170 --> 00:17:45,260
Let's say I call it us or dot Hatchie and paste it here.

163
00:17:47,080 --> 00:17:54,550
But how to use this code is not in the steamin in the component we have to change decorate a property

164
00:17:55,500 --> 00:18:06,270
in sort of template use template and simply give you or you are Mitty's dot slice horse dot extreme

165
00:18:06,350 --> 00:18:08,000
and that's it.

166
00:18:08,270 --> 00:18:13,370
So maybe Waterlow become very small that it only had one page note on this

167
00:18:20,400 --> 00:18:24,010
everything you say nothing has changed just that.

168
00:18:24,100 --> 00:18:28,530
So putting the complete template in the component file itself.

169
00:18:28,650 --> 00:18:31,780
We are taking the template from the Stephen.

170
00:18:31,950 --> 00:18:35,600
And of course here we are going to get the intelligence benefit also.

171
00:18:35,820 --> 00:18:40,150
We can actually do I need all of these benefits.

172
00:18:40,160 --> 00:18:41,260
We're going to get.

173
00:18:41,580 --> 00:18:45,790
So it is recommended to have a template when the instrument is large enough.

174
00:18:45,990 --> 00:18:47,740
Otherwise it is not required.

175
00:18:48,180 --> 00:18:51,540
So say for example are done in the hand or you can go through it later.

176
00:18:53,110 --> 00:19:01,450
And then we can have some information about template expression writing expressions I told you earlier

177
00:19:01,830 --> 00:19:04,360
in interpellation we can use expressions

178
00:19:09,360 --> 00:19:17,390
on neither different things we can use assignment new operator taining of expressions which to me call

179
00:19:17,390 --> 00:19:21,900
an order or is valid increment decrement operators can be used.

180
00:19:22,150 --> 00:19:25,090
And we do not have support for bitwise operators.

181
00:19:25,120 --> 00:19:25,990
There is no support.

182
00:19:25,990 --> 00:19:30,410
This is important point to remember those group expressions.

183
00:19:30,690 --> 00:19:35,570
All in all these are not supported are prohibited.

184
00:19:35,570 --> 00:19:37,280
All of these are prohibited.

185
00:19:37,280 --> 00:19:47,540
Other than this everything can be used in the expression plus minus multiplication division.

186
00:19:47,680 --> 00:19:53,640
But all of these are but not built like that because these are not supported.

187
00:19:53,640 --> 00:19:56,330
Semi-colon comma not supported.

188
00:19:56,360 --> 00:19:58,130
New is not supported.

189
00:19:58,990 --> 00:20:03,510
Assignment equal to not all of these are not supported.

190
00:20:03,530 --> 00:20:06,800
And second thing you should know is do new operators out there.

191
00:20:06,810 --> 00:20:09,960
One is by then what is this pipe operator.

192
00:20:09,960 --> 00:20:13,040
4 bytes is a topic by itself.

193
00:20:13,290 --> 00:20:19,200
But right now just to demonstrate that we don't we have a name I would like to display the name in uppercase.

194
00:20:19,380 --> 00:20:26,960
So just give here a body's life fate on this name is going to be displayed in the grid in all uppercase

195
00:20:28,020 --> 00:20:30,300
irrespective of the case in which it is stored.

196
00:20:30,480 --> 00:20:32,450
It's always displayed in uppercase.

197
00:20:32,580 --> 00:20:41,160
So that's what it is by laneways we have got all of this becomes handy especially when it is not for

198
00:20:41,160 --> 00:20:45,260
whatever reason imagine this is not not not able to enter.

199
00:20:45,360 --> 00:20:46,840
It will give an error.

200
00:20:46,890 --> 00:20:52,080
So in case we don't want another and we just want the expression to be ignored in such case we can use

201
00:20:52,080 --> 00:20:53,100
caution.

202
00:20:55,430 --> 00:20:56,910
So it will not break.

203
00:20:56,930 --> 00:21:02,300
It will not fail and it will not break and likely will lead to nothing.

204
00:21:02,630 --> 00:21:04,900
And then a few more things.

205
00:21:05,040 --> 00:21:11,840
The example which I just demonstrated start with a template for what a Star Wars system might see in

206
00:21:11,840 --> 00:21:12,470
syntax.

207
00:21:12,510 --> 00:21:24,330
I used spandex and I gave star in D if not this is a shortcut to indeed template that the binding and

208
00:21:24,330 --> 00:21:27,360
the binding is done to the property.

209
00:21:27,380 --> 00:21:30,630
So this is of value which is assigned to India.

210
00:21:30,650 --> 00:21:36,480
If I mean in writing like this we can as well write like this for if.

211
00:21:36,530 --> 00:21:37,750
Same with sweat

212
00:21:41,610 --> 00:21:42,080
for loop.

213
00:21:42,080 --> 00:21:45,480
Also we have the same thing in the template in the form.

214
00:21:45,510 --> 00:21:48,430
Let him be blah blah blah.

215
00:21:48,570 --> 00:21:50,130
So we have shortcuts available.

216
00:21:50,130 --> 00:21:56,460
Obviously we are going to use these shortcuts on the road or angle or programming but just for your

217
00:21:56,460 --> 00:22:00,040
knowledge these are all directives.

218
00:22:00,180 --> 00:22:02,980
And this is how the directive is supposed to be in the

219
00:22:07,250 --> 00:22:09,250
then we have got templates fight

220
00:22:12,120 --> 00:22:13,930
external instr. complete file.

221
00:22:13,980 --> 00:22:21,420
I already demonstrated that can make you love Steimle template fight in place of using Im pleased with

222
00:22:21,420 --> 00:22:23,090
this.

223
00:22:23,380 --> 00:22:27,130
Dodik unstable in the comp companied fight.

224
00:22:27,660 --> 00:22:28,720
That's all in this.

225
00:22:28,920 --> 00:22:29,490
Thank you.

