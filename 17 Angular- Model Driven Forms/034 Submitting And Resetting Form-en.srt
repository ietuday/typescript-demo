1
00:00:15,110 --> 00:00:20,810
The sifting and submitting the form basically in place we would like to solicit the forms paid.

2
00:00:20,920 --> 00:00:23,240
What of the form is submitted to settle.

3
00:00:23,700 --> 00:00:26,170
So what is the procedure for doing all this.

4
00:00:26,580 --> 00:00:29,600
What to do is first in our form.

5
00:00:30,380 --> 00:00:32,310
We are going to have a submit

6
00:00:36,660 --> 00:00:48,840
let's say Here I'll provide input type will submit class Button On button default

7
00:00:52,510 --> 00:01:00,590
and we can get values and will submit law in the company clause.

8
00:01:00,630 --> 00:01:05,310
I'm going to write up on submit function.

9
00:01:05,430 --> 00:01:07,060
That's what we want to do here.

10
00:01:08,800 --> 00:01:19,440
So let's say you're on submit and here I'm going to first check if the form can submit.

11
00:01:19,490 --> 00:01:28,380
So the form is valid or not and that's pretty easy here simply was this not for some form dollar value.

12
00:01:29,170 --> 00:01:33,740
We have a property involved in it and so on.

13
00:01:33,750 --> 00:01:35,170
So we can say it.

14
00:01:35,230 --> 00:01:37,860
Is and it goes on.

15
00:01:38,240 --> 00:01:46,740
What is that we will do what we'll do is you will have one property no message which is by default empty

16
00:01:47,520 --> 00:02:03,550
and this message I would like to set up this message form is it likewise else this message is invalid

17
00:02:05,720 --> 00:02:06,130
not

18
00:02:09,510 --> 00:02:18,390
this message I should display some event that is in the component on we would like to display the message

19
00:02:18,520 --> 00:02:32,960
of starting with Dave an incident that we would like to have the message that's it and maybe we can

20
00:02:32,960 --> 00:02:36,330
make this as your read

21
00:02:42,830 --> 00:02:44,970
so by default we will not have anything

22
00:02:49,980 --> 00:02:51,360
wrong with it.

23
00:02:51,960 --> 00:02:57,050
But when the form is invalid the form is not valid.

24
00:02:57,760 --> 00:02:59,890
So not if we to submit

25
00:03:02,660 --> 00:03:03,240
OK.

26
00:03:03,260 --> 00:03:05,940
Point is we have to do the mapping.

27
00:03:06,090 --> 00:03:16,060
We haven't done my thing all form on submit to on submit function in my

28
00:03:18,570 --> 00:03:21,300
component.

29
00:03:21,370 --> 00:03:24,890
We have to submit the same things and we are supposed to call it

30
00:03:32,870 --> 00:03:33,670
once again

31
00:03:37,350 --> 00:03:41,770
submit something is going wrong.

32
00:03:47,710 --> 00:03:50,010
On submit is not defined.

33
00:03:56,770 --> 00:04:04,830
Oh very important thing that are brackets for events we are supposed to point about at

34
00:04:10,660 --> 00:04:19,840
and it's not all submit it and submit to binding should be done in this

35
00:04:23,560 --> 00:04:30,250
form is valid we submit form is invalid.

36
00:04:35,520 --> 00:04:40,870
So pretty simple bind indeed submit and submit.

37
00:04:40,870 --> 00:04:45,360
We are able to simply take whether the form is valid or not.

38
00:04:46,090 --> 00:04:48,560
But we would like to give a different color.

39
00:04:48,940 --> 00:04:56,190
So you have four input type button that can make it a primary battle actually.

40
00:04:57,150 --> 00:04:58,270
So it looks blue

41
00:05:03,970 --> 00:05:07,540
that on me as well.

42
00:05:08,680 --> 00:05:15,040
Are Just added as an additional feature in case we want to pass some data here.

43
00:05:15,650 --> 00:05:17,290
We can do that.

44
00:05:17,300 --> 00:05:22,710
Let's say I was for some form don't you.

45
00:05:23,780 --> 00:05:30,290
Well just for some form who is going to be on deck and that form we can get it.

46
00:05:34,380 --> 00:05:38,240
And then yes the rest of it is all the same.

47
00:05:38,820 --> 00:05:54,890
This part we can keep it as it is and only for demo form door forced name concatenated that form or

48
00:05:57,230 --> 00:06:05,170
lastly So if form can be passed on how do we get the form what would the name be given here form group.

49
00:06:05,230 --> 00:06:08,350
God will get me the formula for its

50
00:06:13,390 --> 00:06:17,910
in a local ABC exabytes it was named last

51
00:06:22,030 --> 00:06:28,770
ideally speaking we want this bottom to be disabled if the form is not valid.

52
00:06:29,490 --> 00:06:31,700
So what best can be done here.

53
00:06:31,770 --> 00:06:46,010
We can give disabled is equal to the form name for some form Daut ballot or for some form got invited

54
00:06:46,540 --> 00:06:47,690
to invalid.

55
00:06:47,730 --> 00:06:54,630
Does anybody it comes to that said so submit but under the

56
00:06:58,380 --> 00:07:03,090
not working yes like no form is valid.

57
00:07:03,140 --> 00:07:04,910
Thats why it is in April thats good.

58
00:07:05,240 --> 00:07:16,510
Let me know once again I make it invalid first by removing the suits visible what becomes of that invalid

59
00:07:17,500 --> 00:07:22,390
so like this you can control the submit button from being submitted.

60
00:07:23,800 --> 00:07:32,810
See many times what happens is when we can submit we execute the call of on submit and we would like

61
00:07:32,810 --> 00:07:37,690
to reset the form they sent the form to its original state.

62
00:07:38,240 --> 00:07:49,090
So how do we submit and let's say here we submit it to sort of up and see changes.

63
00:07:49,090 --> 00:07:56,390
But now we would like to revisit the fall and that can be achieved by simply using this board for some

64
00:07:56,390 --> 00:07:56,860
form.

65
00:07:56,870 --> 00:08:07,200
Dodd has a function but he said that said that there is a default on idea that the

66
00:08:11,190 --> 00:08:16,450
let's say I've got something or maybe

67
00:08:19,030 --> 00:08:21,070
something we're going

68
00:08:24,250 --> 00:08:25,660
word in it.

69
00:08:25,780 --> 00:08:27,060
Oh semicolon.

70
00:08:28,580 --> 00:08:29,180
Submarine

71
00:08:31,960 --> 00:08:35,140
for me it.

72
00:08:35,160 --> 00:08:37,010
So again we should put that into

73
00:08:43,970 --> 00:08:47,000
something and it isn't.

74
00:08:47,210 --> 00:08:54,550
So we can always send a form by using form but Richard says that all the elements in that really good

75
00:08:54,650 --> 00:08:57,890
reason to.

76
00:08:57,970 --> 00:09:04,340
So this is very important basic features but very useful in projects whenever we are making a profit.

77
00:09:04,690 --> 00:09:05,620
That's all it is.

78
00:09:05,800 --> 00:09:06,130
Thank you.

