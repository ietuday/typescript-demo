1
00:00:16,730 --> 00:00:24,090
In the real season we have seen how we can have a simple style and class binding by just getting the

2
00:00:24,090 --> 00:00:32,190
name of those type of state properties to the appropriate balance and sending the cloth binding to the

3
00:00:32,190 --> 00:00:33,200
appropriate class.

4
00:00:33,200 --> 00:00:40,710
I mean we've been able to achieve what we want to but generally in project we are going to pre-poll

5
00:00:40,770 --> 00:00:44,350
using Indian style and indeed class attributes.

6
00:00:44,820 --> 00:00:51,510
Basically these two are directives which are used for binding multiple classes and multiple styles at

7
00:00:51,510 --> 00:00:52,370
the same time.

8
00:00:52,380 --> 00:00:54,660
That is the beauty of it.

9
00:00:54,660 --> 00:00:58,590
So let's see the same project.

10
00:00:58,590 --> 00:01:07,830
What we have created earlier I would be a top and I would want to write certain classes in each team

11
00:01:07,840 --> 00:01:13,690
in some additional classes I would like to write that is dot saveable

12
00:01:17,430 --> 00:01:20,630
fun staying in like

13
00:01:23,930 --> 00:01:25,610
not mortified.

14
00:01:30,040 --> 00:01:38,170
Fonte way more and I think one more as an example.

15
00:01:43,210 --> 00:01:53,970
There is font size will increase the size of the font size before because so three new styles style

16
00:01:54,010 --> 00:01:55,670
glasses I have written here.

17
00:01:55,690 --> 00:01:59,270
Saveable modified and special.

18
00:01:59,310 --> 00:02:01,240
How can we make use of these here.

19
00:02:02,530 --> 00:02:07,990
So whenever we want to play the style simply we can do like this actually right.

20
00:02:08,230 --> 00:02:17,750
You can see Dave glass as well and we can give multiple states that can do the same.

21
00:02:18,530 --> 00:02:20,700
I mean what those being used here.

22
00:02:25,370 --> 00:02:31,910
Space same modified.

23
00:02:37,590 --> 00:02:40,880
This is routine rule in programming.

24
00:02:40,920 --> 00:02:50,070
Our normal team and I would like to do know with the help of indeed stating or indeed glossily to say

25
00:02:50,250 --> 00:02:54,910
because these are class names I would like to achieve the same thing using indeed lost.

26
00:02:55,380 --> 00:02:57,780
So that is when I've been right there.

27
00:02:58,320 --> 00:03:06,080
And yet we get binding in the class and put this in square brackets because this binding is equal.

28
00:03:06,090 --> 00:03:14,090
So here we can make a call to some function which is going to read on multiple lines.

29
00:03:14,100 --> 00:03:18,330
So let's say we have a function class set classes.

30
00:03:18,330 --> 00:03:19,990
Now what is the set classes.

31
00:03:20,310 --> 00:03:23,930
It's a function which we write in the Component class.

32
00:03:24,280 --> 00:03:32,950
Note that classes will do classes is going to have a decent object.

33
00:03:33,690 --> 00:03:37,040
All classes is equal.

34
00:03:37,130 --> 00:03:38,650
In this case an object.

35
00:03:39,000 --> 00:03:41,900
What other class names have saveable

36
00:03:46,540 --> 00:03:58,620
the same order and I'm supposed to be true or false like was a comma modifier.

37
00:03:58,820 --> 00:04:15,080
I'm supposed to give two of those Mrs. waterfalls So basically does not end in exactly the desired object

38
00:04:15,080 --> 00:04:20,920
with those three properties to True or false is what we will be assigning to in the class.

39
00:04:21,140 --> 00:04:27,140
So yes we can see this is some sample text

40
00:04:30,620 --> 00:04:34,520
Knoller's want this just to maintain some gap between them.

41
00:04:34,560 --> 00:04:40,260
B or actually it's good does every.

42
00:04:44,430 --> 00:04:45,490
All sometimes

43
00:04:50,040 --> 00:04:54,080
not what you want this here is the last one.

44
00:04:54,470 --> 00:04:55,500
Two states are.

45
00:04:55,590 --> 00:04:56,930
Something wrong.

46
00:04:57,400 --> 00:04:58,870
Of course mine is expected.

47
00:04:58,960 --> 00:04:59,380
Yeah.

48
00:04:59,460 --> 00:05:04,380
It's not a semicolon comma just an object name calling you call all.

49
00:05:06,780 --> 00:05:09,600
And you see this is some sample text.

50
00:05:09,600 --> 00:05:11,090
It is not boring.

51
00:05:11,730 --> 00:05:12,740
Why does not work.

52
00:05:12,770 --> 00:05:16,700
Because you're given more importance.

53
00:05:16,740 --> 00:05:17,910
Change that to

54
00:05:20,830 --> 00:05:23,950
nodo.

55
00:05:24,130 --> 00:05:26,750
This is also more to.

56
00:05:26,790 --> 00:05:28,230
So how does it work.

57
00:05:28,730 --> 00:05:38,850
And these clocks should be assigned to an object and that object is supposed to have for classes.

58
00:05:38,860 --> 00:05:40,270
True or false.

59
00:05:40,470 --> 00:05:43,010
And what are the symbols modified special.

60
00:05:43,030 --> 00:05:45,790
You don't see it as classes that is important.

61
00:05:47,260 --> 00:05:52,850
The class is what we are supposed to make use of including bodies and objects.

62
00:05:52,900 --> 00:05:55,030
Again these class values be based on variable.

63
00:05:55,030 --> 00:05:55,870
Why not.

64
00:05:56,080 --> 00:06:03,150
You can actually have something like can see Squirtle to can modify

65
00:06:05,900 --> 00:06:07,070
ease modify

66
00:06:11,230 --> 00:06:15,770
because it will do likewise.

67
00:06:16,290 --> 00:06:18,540
Isn't that special.

68
00:06:18,900 --> 00:06:20,500
Is it true.

69
00:06:21,690 --> 00:06:22,280
Of course.

70
00:06:22,470 --> 00:06:31,600
I want to be properties so I'll cut from you the president or the component class.

71
00:06:31,620 --> 00:06:35,560
So how can we use those words in truth constant to false.

72
00:06:35,670 --> 00:06:44,040
We can actually I can say with all that here is bestiary is already there so we don't mean them so yet

73
00:06:44,150 --> 00:06:46,660
we can actually write this dot

74
00:06:59,650 --> 00:07:03,510
ears rather than say.

75
00:07:04,220 --> 00:07:07,660
There's not enough money for it here.

76
00:07:07,870 --> 00:07:13,440
This is special.

77
00:07:13,520 --> 00:07:21,570
Maybe more clarity you can get by putting them in different places.

78
00:07:21,650 --> 00:07:30,560
That's an asset classes is running a decent optic and decent object has property names as I see it as

79
00:07:30,570 --> 00:07:34,870
Glassman's values as true or false.

80
00:07:34,920 --> 00:07:37,260
So we know what we want to play in multiple classes.

81
00:07:37,260 --> 00:07:45,950
Policeman maybe may not be done in those kind of cases in de-clawed is more appropriate than using the

82
00:07:45,990 --> 00:07:47,150
whole cluster

83
00:07:50,810 --> 00:07:52,380
just rank controller type.

84
00:07:52,430 --> 00:07:54,410
And we should still get the same output

85
00:07:56,950 --> 00:07:58,950
table but it's not stable.

86
00:07:59,130 --> 00:08:00,230
It's not.

87
00:08:00,630 --> 00:08:07,550
Yeah because again it is false it's not a static like this.

88
00:08:07,870 --> 00:08:10,320
I can also have one more

89
00:08:23,150 --> 00:08:25,390
intro of Indeed gloss.

90
00:08:25,410 --> 00:08:25,830
All right.

91
00:08:25,850 --> 00:08:32,260
Indeed stion and Jarod's it said Stine's one more function.

92
00:08:32,320 --> 00:08:34,730
So does what I'll go be

93
00:08:37,560 --> 00:08:38,010
here.

94
00:08:38,040 --> 00:08:42,210
I have said styles in six.

95
00:08:42,240 --> 00:08:44,650
These are supposed to be what I.

96
00:08:44,700 --> 00:08:53,500
These are class names and those are not supposed to become stylings.

97
00:08:53,590 --> 00:08:58,670
For example fonde hyphen style

98
00:09:05,960 --> 00:09:13,150
name calling like ways for hyphen.

99
00:09:13,540 --> 00:09:14,020
Read

100
00:09:16,820 --> 00:09:18,950
foreign hyphen size

101
00:09:24,810 --> 00:09:28,170
but this is why this cannot be true or false.

102
00:09:28,280 --> 00:09:34,210
These values are supposed to be the value of this style and to do what we want.

103
00:09:34,210 --> 00:09:40,570
As of I'd say for example if can save is true I would like to have it done.

104
00:09:41,340 --> 00:09:44,390
So it taluk if Ganci is false.

105
00:09:44,490 --> 00:09:47,880
I would like to have Dorval so long.

106
00:09:49,350 --> 00:09:54,560
Likewise you are also in mourning for these blue to and what I would say bored.

107
00:09:55,500 --> 00:10:01,970
Else I would say loved like why is special.

108
00:10:02,340 --> 00:10:05,880
I would like to have font size as only for some

109
00:10:09,390 --> 00:10:14,640
else maybe 8 exults default.

110
00:10:14,830 --> 00:10:19,440
So remember in case of classes this is a to with a last name with value.

111
00:10:19,490 --> 00:10:22,680
True or false in case of.

112
00:10:23,330 --> 00:10:29,810
Just change styles that are supposed to be styes undervalues are supposed to be the proper value of

113
00:10:29,810 --> 00:10:35,790
this style as they give it in the in lines like this.

114
00:10:40,320 --> 00:10:45,560
So I've not seen text where is that coming that.

115
00:10:45,590 --> 00:10:48,290
That's because it's so low.

116
00:10:48,330 --> 00:10:56,000
Let me say IIS modified fonts but is modified is false.

117
00:10:56,010 --> 00:10:57,750
We are not going to get it just more.

118
00:10:57,780 --> 00:10:58,920
It will be normal text

119
00:11:02,140 --> 00:11:02,920
long text

120
00:11:05,880 --> 00:11:11,310
so this is all a decent object when you want to hold a script object literals would call it a different

121
00:11:11,330 --> 00:11:15,950
process Litella script object only with name and value pairs.

122
00:11:16,260 --> 00:11:22,690
Name of the property value of the property the same example is also given in.

123
00:11:22,700 --> 00:11:30,510
Note how you can use in the class that when you want to remove multiple Plus the SS classes and then

124
00:11:30,510 --> 00:11:31,650
do at the same time.

125
00:11:34,770 --> 00:11:35,570
Glasses

126
00:11:39,090 --> 00:11:46,120
in the state don't do.

127
00:11:47,740 --> 00:11:48,960
And here we would like.

128
00:11:48,980 --> 00:11:49,830
And this.

129
00:11:51,660 --> 00:11:53,940
Searchlight and we are setting the stakes

130
00:11:57,350 --> 00:11:59,840
so please look at it and practice the same.

131
00:11:59,870 --> 00:12:00,290
Thank you.

