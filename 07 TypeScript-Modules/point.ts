﻿import { IPoint } from "./IPoint"

export default class Point implements IPoint {
    x: number;
    y: number;
    distanceFromOrigin(): number {
        return Math.sqrt((this.x * this.x) + (this.y * this.y));
    }
}

export { IPoint }