1
00:00:16,720 --> 00:00:24,350
Setting up local development environment are they already in form in the previous session setting up

2
00:00:24,350 --> 00:00:30,200
the environment for Angle or is much complicated when compared to setting up the environment for teachers

3
00:00:31,030 --> 00:00:32,160
rather than an alert.

4
00:00:32,160 --> 00:00:38,930
Is it a matter of just copying one fine angle or got two years into the project in ruling that filing

5
00:00:39,030 --> 00:00:41,380
then beat and start using it.

6
00:00:42,160 --> 00:00:46,310
But in Angola for we'll have to follow a series of steps.

7
00:00:46,570 --> 00:00:55,080
First we will have to do is download more days and NPM what is also very important is to get proper

8
00:00:55,500 --> 00:00:56,600
development.

9
00:00:56,660 --> 00:01:03,860
We need a CD which is downloaded from QuickStart fightings the QuickStart files love to download.

10
00:01:03,950 --> 00:01:06,340
That will become the seed of your project.

11
00:01:06,650 --> 00:01:11,410
So that will be the minimal thing that you love to include in every got my project.

12
00:01:11,840 --> 00:01:14,880
Then we will have to use Visual Studio.

13
00:01:14,930 --> 00:01:21,710
Now I'm using visuals for you because I come from darknet background but anybody can make use of any

14
00:01:21,770 --> 00:01:24,910
typescript editor or angle editor.

15
00:01:24,920 --> 00:01:31,490
There are lots of editors which are free the lot of which are paid but I've found of all Visual Studio

16
00:01:31,550 --> 00:01:38,210
to be the best and simplest but of course Visual Studio of basic installation is not ready.

17
00:01:38,240 --> 00:01:40,720
We will have to lose some additional configuration.

18
00:01:40,730 --> 00:01:43,990
All that is what we are going to see in this particular session.

19
00:01:44,420 --> 00:01:48,310
So first and most important thing we will have to make use of.

20
00:01:48,360 --> 00:01:53,120
Nobody has and I'm not a package manager.

21
00:01:53,240 --> 00:02:00,560
These are actually not only used for Angola but these are used for many other modern development including

22
00:02:01,580 --> 00:02:04,490
the is on the server side.

23
00:02:04,500 --> 00:02:07,050
Not just programming and so on.

24
00:02:07,700 --> 00:02:17,810
So very importantly Nordie is worse than 4.6 point X or higher is needed and NPM also 3.0 X or higher

25
00:02:17,810 --> 00:02:22,260
is needed less and more than less than this will not work.

26
00:02:22,400 --> 00:02:27,330
And how can you define what is the current version you love to use load and minus.

27
00:02:27,320 --> 00:02:29,770
We just go to come on prompt.

28
00:02:29,840 --> 00:02:33,470
No Pinus we and you can see that wasn't like on my machine.

29
00:02:33,620 --> 00:02:35,810
It is 6 which is latest.

30
00:02:35,810 --> 00:02:45,540
Likewise I can type in B-M minus B on the Indian version which I have on my machine is three point one

31
00:02:45,540 --> 00:02:46,830
zero.

32
00:02:46,860 --> 00:02:52,540
All we need is three point X only we are good enough with that somebody will answer.

33
00:02:52,880 --> 00:02:55,440
But otherwise you will have to go to this particular you are

34
00:03:03,450 --> 00:03:07,990
and download the latest NPM installer based on your operating system.

35
00:03:08,380 --> 00:03:10,200
So let's say you should be using Windows.

36
00:03:10,360 --> 00:03:16,480
I really don't know 64 bit and use the same DLL don't know Ezekial the set.

37
00:03:16,500 --> 00:03:18,750
It's a simple setup process.

38
00:03:19,020 --> 00:03:21,350
Just keep clicking next next.

39
00:03:21,510 --> 00:03:28,430
And as you said in this once you have completed this the next important step is to download the QuickStart

40
00:03:28,430 --> 00:03:31,690
start seed.

41
00:03:32,130 --> 00:03:36,280
Basically this will become the seed of our project.

42
00:03:36,420 --> 00:03:38,480
So I'm going to download this now.

43
00:03:42,120 --> 00:03:45,800
From the get up download zip

44
00:03:51,820 --> 00:03:55,840
download zip you can download it.

45
00:03:55,850 --> 00:03:57,560
You have to save it all.

46
00:03:57,610 --> 00:04:04,080
I have already saved it in my folder because then Rick Starseed this is fine.

47
00:04:04,340 --> 00:04:09,100
Of course the next step would be to extract this file and you're going to get this pulled up.

48
00:04:09,140 --> 00:04:11,740
So this is what we love to do here.

49
00:04:12,110 --> 00:04:17,380
So first up we love to download and install and PM package manager.

50
00:04:17,480 --> 00:04:22,040
And of course no days of that that you have to download the seed

51
00:04:26,520 --> 00:04:29,460
for that particular folder.

52
00:04:33,230 --> 00:04:34,200
Come on rumped

53
00:04:42,640 --> 00:04:43,390
Starseed

54
00:04:46,940 --> 00:04:49,710
And here you already have certain files

55
00:04:53,520 --> 00:05:03,260
but these are only busy files after this will have to download complete no packet so what you do is

56
00:05:03,260 --> 00:05:06,720
preferably What do I have right now.

57
00:05:06,830 --> 00:05:11,830
I'll just take all this and I'll put it in another folder.

58
00:05:11,900 --> 00:05:14,070
Let's say Angola demos.

59
00:05:14,350 --> 00:05:20,370
I'll call it as New see that in this basting.

60
00:05:21,660 --> 00:05:23,780
So I know how to be called

61
00:05:27,280 --> 00:05:31,850
Anglo's that most eat and get this.

62
00:05:31,860 --> 00:05:36,080
No you're actually execute at command prompt NPM install.

63
00:05:36,080 --> 00:05:46,040
This is the command to execute and be installed and it's going to download a lot of files.

64
00:05:46,380 --> 00:05:48,310
Let me show you right now.

65
00:05:48,600 --> 00:05:50,120
See the original folder.

66
00:05:50,160 --> 00:06:03,750
See the QuickStart is hardly is 1:48 give me what you want is that the angle or folder is increasing

67
00:06:03,750 --> 00:06:08,350
inside as the content is downloading.

68
00:06:08,380 --> 00:06:14,140
There's a lot of files which are going to get downloaded or a bit of time left and just wait and watch.

69
00:06:14,170 --> 00:06:18,230
And also you're going to get certain warnings you can ignore at this point of time.

70
00:06:18,300 --> 00:06:20,720
You don't have to worry about those things.

71
00:06:20,790 --> 00:06:26,990
So as it is downloading and background I paused the video and would continue once it is completed.

72
00:06:27,480 --> 00:06:32,790
So just as a computer don't you see it a couple of minutes to complete the download to see what is the

73
00:06:32,790 --> 00:06:34,340
size of the seat.

74
00:06:34,830 --> 00:06:36,730
It is 104 M-B.

75
00:06:37,290 --> 00:06:43,740
It's almost hundred M-B of content will be downloaded which we are going to use for further development

76
00:06:44,310 --> 00:06:45,490
in the medium.

77
00:06:45,570 --> 00:06:53,240
We just don't know there is no more use which is going to have these angular libraries and stuff so

78
00:06:53,240 --> 00:06:58,410
a lot of libraries are that which we can't which we are going to make use of in our project come on

79
00:06:58,410 --> 00:07:05,990
like going to the combine our core library for it because we're going to use this for browser compatibility

80
00:07:05,990 --> 00:07:10,630
if we are going to use this for all we are going to use that's making it Steimle form.

81
00:07:10,880 --> 00:07:12,150
We're going to use this.

82
00:07:12,300 --> 00:07:17,610
So these are different libraries of Angola which are going to be used at the time of developing our

83
00:07:17,610 --> 00:07:18,920
product.

84
00:07:18,990 --> 00:07:23,400
So now that we have downloaded the seed for the

85
00:07:28,880 --> 00:07:35,810
just run here the next come on that is stop and stop and start

86
00:07:38,700 --> 00:07:46,070
is going to start your web browser and all the source files meetup that are automatically going to be

87
00:07:46,070 --> 00:07:46,590
compiled.

88
00:07:46,600 --> 00:07:51,480
You see the a c and then the first page is loaded.

89
00:07:53,300 --> 00:08:03,360
So what did or did that NPM has done and the start has done and being fought has forced noster typescript

90
00:08:03,360 --> 00:08:10,260
compiler do you see an index sarcy for it or whatever source code files are there.

91
00:08:10,500 --> 00:08:12,390
All those files are compiled

92
00:08:16,340 --> 00:08:27,850
NPM start executed here in the run minus the RC and it has started the likes or dislikes that would

93
00:08:27,920 --> 00:08:30,140
either know or just sort.

94
00:08:30,600 --> 00:08:37,600
I don't know just what is what we are using for getting the page executed.

95
00:08:37,640 --> 00:08:38,860
So look at the source

96
00:08:47,160 --> 00:08:47,390
my

97
00:08:50,760 --> 00:08:52,140
Hello angle.

98
00:08:52,320 --> 00:08:56,460
I'm going to explain more about the code which is auto generated and everything.

99
00:08:56,460 --> 00:08:58,370
When we take a bus to your project.

100
00:08:58,830 --> 00:09:02,720
But yes at this point of time what is that we have successfully done.

101
00:09:02,850 --> 00:09:06,010
We have created in B.M. package.

102
00:09:06,030 --> 00:09:15,890
I mean we don't know that the Indian packet and we started with just meters actually launched the angle

103
00:09:15,900 --> 00:09:16,600
of change.

104
00:09:16,620 --> 00:09:17,850
Hello Angola.

105
00:09:18,370 --> 00:09:24,020
Just to give you a gist of what has happened here you see there is a sort that in the sea.

106
00:09:24,410 --> 00:09:30,600
This sarcy folder has indexed not extreme and it is this index not only which has opened actually in

107
00:09:30,600 --> 00:09:31,170
the but also

108
00:09:34,020 --> 00:09:37,560
so on about

109
00:09:41,540 --> 00:09:42,260
there are to.

110
00:09:42,260 --> 00:09:43,500
What is that.

111
00:09:43,780 --> 00:09:51,410
Or number three told them so you can go to the browser.

112
00:09:51,620 --> 00:10:02,540
Give your local host and total you give your slice index Nottage DMM.

113
00:10:03,020 --> 00:10:05,040
We should still get the same content.

114
00:10:05,080 --> 00:10:13,870
So originally it is this file which is executing indexed not steamin in that Steimle is using the content

115
00:10:14,260 --> 00:10:15,940
from Apfel.

116
00:10:15,940 --> 00:10:21,170
These are all Angerer component you can see angle or component or just ideas.

117
00:10:21,400 --> 00:10:30,880
You have got a lot more this or that is model is also will be there another component angle or more.

118
00:10:30,930 --> 00:10:34,000
These two are very important points of course.

119
00:10:34,180 --> 00:10:40,560
MARTIN All these things we're going to see in the architecture let us now see how we can set up a studio

120
00:10:45,790 --> 00:10:51,210
not first and most important thing which version of story you are you using later.

121
00:10:51,250 --> 00:10:58,350
Currently when I'm recording this is Visual Studio 20:17 and most of the installations that I know are

122
00:10:58,410 --> 00:11:06,750
of studio 20:15 But in either case first and most important thing you'll have to do is verify what is

123
00:11:06,750 --> 00:11:13,740
the current version of angular or pipes that you're supporting.

124
00:11:13,780 --> 00:11:23,450
So go to the visual studio or help build a studio and you'll see typescript with them.

125
00:11:25,660 --> 00:11:33,430
2.20 not for angular devlopment typescript should be to point to a place that is a prerequisite.

126
00:11:33,430 --> 00:11:34,370
Mine is 2.3.

127
00:11:34,370 --> 00:11:38,860
So good enough but not coming here.

128
00:11:38,860 --> 00:11:44,140
You can go to Visual Studio and take it with these visuals to your 2015.

129
00:11:44,140 --> 00:11:47,260
Do you have a dead tree installed or not.

130
00:11:47,490 --> 00:11:55,570
For the angular development we need compulsory update tree whereas reasons to 20:17 comes everything

131
00:11:55,570 --> 00:11:58,230
with berichten you don't have to do anything.

132
00:11:58,240 --> 00:12:06,470
Also one more step that you will have to perform specially in Visual Studio 2015 is called Studio

133
00:12:11,550 --> 00:12:19,260
Tools Options you have to select projects and solutions

134
00:12:22,350 --> 00:12:35,630
package management external tools and here you have to insert that Dev and Varman is below normal modules.

135
00:12:36,110 --> 00:12:38,000
More models should be on top.

136
00:12:41,260 --> 00:12:50,520
But should we also uphold development environment the local Guiton should be overridden by the pop and

137
00:12:50,520 --> 00:12:51,910
part should be writing.

138
00:12:51,950 --> 00:12:55,030
No it's been so this ordnances be mundane.

139
00:12:55,410 --> 00:13:01,050
If you're thinking if you're using wizard school year 2015 you'll have to move this pop up because by

140
00:13:01,050 --> 00:13:05,920
default it can be done you love to move it up but in 2017 it is by default option.

141
00:13:06,090 --> 00:13:10,220
Like I can see it is two so this is not change

142
00:13:14,020 --> 00:13:16,210
that as wanting to have to take it off

143
00:13:19,880 --> 00:13:20,530
next.

144
00:13:20,540 --> 00:13:30,940
No to use Weasel's to we will have to double up an AP dot net application to file new project.

145
00:13:31,040 --> 00:13:36,990
Now you have the choice of either using is not meant for publication or is Dot Net code.

146
00:13:37,220 --> 00:13:39,720
VB applications anything is fine.

147
00:13:39,870 --> 00:13:42,100
I'm using is Bedacht not Beverly Kinsey's

148
00:13:49,650 --> 00:13:54,590
there's a mind forced them.

149
00:13:56,670 --> 00:14:07,310
Going to get the MTV application on a simple web form or MVC or you or any of these three is good enough.

150
00:14:07,710 --> 00:14:09,880
I'm taking MDI the application.

151
00:14:09,900 --> 00:14:20,910
Nothing is there in that into this MTV publication we are supposed to start the QuickStart famous the

152
00:14:21,010 --> 00:14:24,300
QuickStart finds we love to copy.

153
00:14:24,550 --> 00:14:29,600
So you start fights we don't from get up in the folder containing C S.

154
00:14:31,670 --> 00:14:38,820
Especially these four files have to be copied Sozzi the folder directly be conflict dot isn't back is

155
00:14:38,820 --> 00:14:45,410
not isn't the length that is these three files console's folder from the seed has to be copied.

156
00:14:46,210 --> 00:14:56,440
So I go back to the scene the original sin starts Starseed you're sarcy B is.

157
00:14:57,880 --> 00:15:07,790
Is linked by current copy this Control-C come back to studio and set up the project and paste it here.

158
00:15:09,540 --> 00:15:15,640
The next step is to right click on the packaged autism and series talkbacks.

159
00:15:15,910 --> 00:15:17,430
Now what is going to happen.

160
00:15:17,520 --> 00:15:20,910
Let me do that right click here and there is no packet.

161
00:15:21,050 --> 00:15:26,030
Now is going to download all the files which it has downloaded.

162
00:15:26,100 --> 00:15:27,640
You see when I did

163
00:15:30,930 --> 00:15:34,800
NPM Stark what does happen.

164
00:15:35,040 --> 00:15:37,470
The same thing is going to happen now.

165
00:15:37,590 --> 00:15:41,020
All the files are getting downloaded into my project.

166
00:15:41,340 --> 00:15:45,050
So suddenly you'll see that the size of the project has become big.

167
00:15:45,360 --> 00:15:47,780
Yeah obviously it will grow by 100.

168
00:15:47,850 --> 00:15:51,860
Meet insites and major files are on here.

169
00:15:51,940 --> 00:15:54,790
Our application is all here in app for.

170
00:15:55,240 --> 00:15:58,430
So is it more do that more.

171
00:15:58,440 --> 00:16:02,890
We've got component and this file you ignore it.

172
00:16:02,890 --> 00:16:09,090
You don't have to worry about Dot spectabilis but these two files of more do component.

173
00:16:09,340 --> 00:16:16,650
And of course mean that daz is very very important may not be as is the one which will do all that.

174
00:16:16,650 --> 00:16:23,000
The slide isn't indexed or it's daemon has a reference to attack.

175
00:16:23,110 --> 00:16:34,950
Might this might be actually mentioned in component components does a class this component which is

176
00:16:34,950 --> 00:16:36,560
used to upload.

177
00:16:36,840 --> 00:16:44,550
So how are all these things up Mordieu up component and Main.

178
00:16:44,760 --> 00:16:46,690
And of course indexed not axemen.

179
00:16:46,800 --> 00:16:52,640
How are all these things linked with each other is what we are supposed to understand and that we will

180
00:16:52,650 --> 00:16:56,650
do in the next chapter when we are going to discuss angular architecture.

181
00:16:57,890 --> 00:17:04,500
So right now it is the ongoing fights see the same directory structure will be created in our project.

182
00:17:04,510 --> 00:17:04,670
No

183
00:17:13,250 --> 00:17:21,580
you connect to the right click on the project open folder in file explorer and you see here also we

184
00:17:21,580 --> 00:17:32,260
have not underscored what was same respect to you and these are what is important is do not include

185
00:17:32,350 --> 00:17:35,470
nor underscore modules in the project.

186
00:17:35,800 --> 00:17:42,380
I repeat do not include nor underscore modules in the project for the project.

187
00:17:47,720 --> 00:17:52,920
It is for all these errors will go away.

188
00:17:53,110 --> 00:18:03,940
We will have to make certain teams as I've listed them here so what are those tedious first I have to

189
00:18:03,950 --> 00:18:12,890
go to Sochi next door to AGM in and change the base had to have to slice it sarcy.

190
00:18:13,300 --> 00:18:21,660
So next door to Yemen yet there is something called B's had to have.

191
00:18:21,700 --> 00:18:23,190
So where is my source.

192
00:18:23,230 --> 00:18:26,150
Right now my source file is in the folder.

193
00:18:26,170 --> 00:18:27,860
That's what we have to give it.

194
00:18:30,450 --> 00:18:34,740
Followed by that which is lost on those core modules.

195
00:18:34,740 --> 00:18:38,950
By default it will try to search for more underscore more sympathetic.

196
00:18:39,030 --> 00:18:44,010
That is something for the but it is not in this article.

197
00:18:44,240 --> 00:18:45,810
It is in the room folder.

198
00:18:45,980 --> 00:18:48,280
So you have to change it to.

199
00:18:48,710 --> 00:18:51,100
You're also you're also.

200
00:18:51,320 --> 00:18:53,240
So these directories will have to make just

201
00:18:57,440 --> 00:18:57,960
this

202
00:19:02,110 --> 00:19:11,380
and also enough to make a team system does not conflict nor does system as port conflict not does it

203
00:19:11,410 --> 00:19:14,560
also we have to padauk underscore Morty's so.

204
00:19:14,670 --> 00:19:15,340
Ms.

205
00:19:15,370 --> 00:19:16,250
Tippett Right.

206
00:19:16,270 --> 00:19:23,080
BOPP because when we click on a big package it's an unsafe install packages.

207
00:19:23,300 --> 00:19:30,130
It has restored the package just in the mood for the not in the current directory that is outsourcing.

208
00:19:30,210 --> 00:19:33,960
So this small team just to me not rebuild the project

209
00:19:36,820 --> 00:19:40,660
but can it automatically cut short the errors.

210
00:19:40,760 --> 00:19:49,210
Why do we actually get this there is some problem with top seed which we don't know actually it is making

211
00:19:49,210 --> 00:19:50,770
use of generics.

212
00:19:51,150 --> 00:19:54,030
Well hear what you have to do is go do your project.

213
00:19:54,300 --> 00:19:54,630
Yes.

214
00:19:54,630 --> 00:19:56,080
Confect got this.

215
00:19:56,400 --> 00:20:06,270
And here you have to give more strict generic takes and give the values to know it's trick Gennari text

216
00:20:06,270 --> 00:20:08,040
should be given as a rule.

217
00:20:08,160 --> 00:20:15,870
Once you get this and rebuild your project now this time you should not get any so this many required

218
00:20:16,000 --> 00:20:17,250
and may not be required.

219
00:20:17,290 --> 00:20:20,190
Depends on what CD you're downloading.

220
00:20:20,190 --> 00:20:23,470
So I don't see it as giving it a certainty it's it's not it up.

221
00:20:23,650 --> 00:20:25,520
Maybe it has something to do with Watson.

222
00:20:25,660 --> 00:20:33,790
So take care of my seat probably not up under typescript which I'm using my latest so that that could

223
00:20:33,790 --> 00:20:36,190
be the reason this error is coming.

224
00:20:36,940 --> 00:20:40,430
But if you have everything let's just even this requirement will not be good.

225
00:20:40,780 --> 00:20:45,850
So now I make index note it will start

226
00:20:48,830 --> 00:20:49,930
I launch it.

227
00:20:49,970 --> 00:20:56,160
I press control 5 and we are going to get the same but what we got before.

228
00:20:57,440 --> 00:21:05,270
Hello hello Angola and ensure that we do not get any better than the press afterwards.

229
00:21:06,170 --> 00:21:09,750
Yes we should not have any of this favorite icon you can ignore it.

230
00:21:09,760 --> 00:21:13,920
I don't have that file in my truck but otherwise we should not have any.

231
00:21:14,230 --> 00:21:19,480
If anything goes wrong and you're not getting halo angular to debug the problem you'll have to go to

232
00:21:19,480 --> 00:21:26,200
the debugging window of your browser that is press to 2L open this console and read the error message

233
00:21:26,260 --> 00:21:27,860
and figure out what could be wrong.

234
00:21:28,120 --> 00:21:34,360
So that is precisely the procedure for running the first application in the next session.

235
00:21:34,360 --> 00:21:39,760
I'm going to discuss about architecture and how we are going to make changes to this particular files

236
00:21:40,420 --> 00:21:43,420
to reflect the output in the browser accordingly.

237
00:21:43,630 --> 00:21:44,070
Thank you.

