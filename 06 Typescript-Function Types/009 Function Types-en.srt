1
00:00:17,110 --> 00:00:22,540
Let us know you get some advanced features of functions in which we are going to cover.

2
00:00:22,580 --> 00:00:24,110
And before you do.

3
00:00:24,560 --> 00:00:26,070
Let's do.

4
00:00:26,360 --> 00:00:34,790
Function overloading and very important topic function types you know only function we can have parameters

5
00:00:34,880 --> 00:00:36,670
as Oxnam.

6
00:00:36,860 --> 00:00:39,550
Let me demonstrate this whole thing with proper examples.

7
00:00:39,590 --> 00:00:41,070
So I'll create a new project

8
00:00:45,180 --> 00:00:48,620
file new website.

9
00:00:48,780 --> 00:00:50,360
If you don't need empty let's say

10
00:00:53,260 --> 00:00:54,660
follows them all

11
00:00:58,900 --> 00:00:59,320
to this.

12
00:00:59,320 --> 00:01:00,600
Let us know.

13
00:01:04,150 --> 00:01:05,290
Or could have to fight

14
00:01:08,360 --> 00:01:11,130
angler doesn't had a heads D.M. beats

15
00:01:13,880 --> 00:01:20,360
typescript Dragon Ball in its Diom.

16
00:01:20,470 --> 00:01:32,930
So let us write a simple function with the number b on both pro-white implementation.

17
00:01:32,950 --> 00:01:40,300
This is nothing special about what we can do is if I want to call Obviously this particular function

18
00:01:40,950 --> 00:01:41,750
I've used.

19
00:01:41,820 --> 00:01:45,220
I had some fabulous

20
00:01:50,400 --> 00:01:56,110
symbols and if I run this I'm going to get the result yesterday nothing special.

21
00:01:56,180 --> 00:02:00,480
Ordinary functions make more or less similar.

22
00:02:00,730 --> 00:02:01,340
Each

23
00:02:06,410 --> 00:02:07,290
no.

24
00:02:07,380 --> 00:02:13,740
My requirement is I would like to parse here maybe 30 but sometimes it may be that sometimes it may

25
00:02:13,740 --> 00:02:20,260
not be that that's all it is sometimes it is just that and sometimes it isn't.

26
00:02:20,460 --> 00:02:25,880
So in that kind of case I will make one more parameter here as optional Palamedes

27
00:02:30,200 --> 00:02:31,950
C is not optional.

28
00:02:32,270 --> 00:02:34,550
So value of c will be total.

29
00:02:34,650 --> 00:02:37,190
And yet I'm going to give Let's see.

30
00:02:37,470 --> 00:02:38,810
But then there is the problem.

31
00:02:38,880 --> 00:02:40,000
Or is that.

32
00:02:41,240 --> 00:02:42,340
Not in.

33
00:02:42,650 --> 00:02:43,930
Why not a number.

34
00:02:44,240 --> 00:02:48,400
Because when value is not passed it is given C is not given.

35
00:02:48,460 --> 00:02:52,590
C will be undefined and when C is undefined.

36
00:02:52,790 --> 00:03:00,470
Any expression with one of the variable as undefined is going to be caught not in the what to.

37
00:03:00,590 --> 00:03:10,220
If not see the value for C maybe C is equal to zero like that will have to give me parameter of the

38
00:03:10,240 --> 00:03:14,520
parameters look and off to make the parameters optional.

39
00:03:14,870 --> 00:03:18,650
We are now able to get the correct 30 and 60

40
00:03:21,380 --> 00:03:22,540
like that.

41
00:03:22,760 --> 00:03:26,680
Sometimes I can even write the function.

42
00:03:26,900 --> 00:03:34,310
Let's call it as I do now and instead of making this as optional I can provide directly a default value

43
00:03:34,310 --> 00:03:35,070
instance.

44
00:03:35,210 --> 00:03:42,320
In such case this problem will not be that you will have to do that again directly next.

45
00:03:42,390 --> 00:03:51,290
It must be the result within a top to.

46
00:03:51,400 --> 00:03:56,190
So either you can meet the parameters Obs by putting a question mark.

47
00:03:56,520 --> 00:04:00,580
In such case you'll have to explicitly take instead the value in the implementation.

48
00:04:00,900 --> 00:04:04,850
Or you can Dodik the said default value in which case also it is optional.

49
00:04:04,860 --> 00:04:11,190
It's your choice which when you want to make use of no one very important feature in typescript language

50
00:04:11,200 --> 00:04:17,300
is your default values always need not be for the last parameter.

51
00:04:17,790 --> 00:04:22,580
If required I can actually write dimeter let's say right at 3 in

52
00:04:25,470 --> 00:04:35,110
it maybe it is having before will be and see a lot of the default in such Guess what we are going to

53
00:04:37,950 --> 00:04:43,850
so when we are passing your value of is given value of being given what we see is not given

54
00:04:50,180 --> 00:04:52,570
sorry the between two and three.

55
00:04:52,930 --> 00:04:54,320
And we should get into it a lot.

56
00:04:54,340 --> 00:04:55,300
You're absolutely should get

57
00:04:58,360 --> 00:04:59,800
says not.

58
00:04:59,970 --> 00:05:01,740
So what to do in such kind of case.

59
00:05:02,620 --> 00:05:09,160
My requirement is I would like to pass before I would like it to be the default value is zero.

60
00:05:09,670 --> 00:05:14,770
So here we are not supposed to put undefined.

61
00:05:14,820 --> 00:05:19,610
So when we pass undefine it will take default value for.

62
00:05:19,920 --> 00:05:27,640
Then we are giving value for B and C to avoid this kind of problem only it is recommended not compulsory.

63
00:05:27,750 --> 00:05:32,820
It is recommended to put all optional default parameters at the end.

64
00:05:34,830 --> 00:05:38,090
So that so that you don't have to explicitly pass undefined.

65
00:05:38,140 --> 00:05:43,250
That is the only advantage that I can write.

66
00:05:43,280 --> 00:05:52,840
I mean that I should be able to call out with any number of arguments sometimes two sometimes three

67
00:05:52,840 --> 00:05:58,810
sometimes four like that I should be able to watch any number of arguments and so 40 or also should

68
00:05:58,810 --> 00:06:03,690
be correct come up 40 or 50 or so should be correct.

69
00:06:03,990 --> 00:06:06,770
Gomo 40 or 50.

70
00:06:07,480 --> 00:06:11,030
Anything should we take all these should be valid.

71
00:06:12,130 --> 00:06:13,690
How is that going to happen.

72
00:06:15,120 --> 00:06:17,990
So in such case it is art form.

73
00:06:18,450 --> 00:06:20,700
And also I would like to have for

74
00:06:24,820 --> 00:06:28,050
how to do this for this.

75
00:06:28,070 --> 00:06:42,840
You can actually simply put Piel dot dot dot numbers on what is just numbers but adding So numbers is

76
00:06:42,840 --> 00:06:46,430
not going to become an added all these numbers.

77
00:06:47,290 --> 00:06:59,030
So can we write your implementation when we go through for I is equal to zero all numbers.

78
00:06:59,240 --> 00:07:04,660
Then we can write some initialization of the

79
00:07:07,490 --> 00:07:07,940
X

80
00:07:11,790 --> 00:07:19,150
let X number Bideford does you know X plus equal

81
00:07:23,350 --> 00:07:37,720
number of and eventually that can be done x.

82
00:07:37,800 --> 00:07:39,750
I know we're going to get that isn't

83
00:07:47,240 --> 00:07:55,160
so when we want to pass a variable number of arguments to matter and use this syntax in the same syntax

84
00:07:55,550 --> 00:07:56,810
which we have used and spread

85
00:08:02,230 --> 00:08:07,660
of course of what undefined is not recommended because if undefined is passed operations on undefined

86
00:08:07,750 --> 00:08:09,450
will lead to not enough.

87
00:08:10,060 --> 00:08:17,930
But what you can do is here another way that you can call that Aleksei nimble Paul.

88
00:08:18,520 --> 00:08:25,700
So nobody is going to happen this value and then Denton rebias and to and and the rest of the world

89
00:08:25,990 --> 00:08:27,140
into notebooks.

90
00:08:28,120 --> 00:08:35,350
And for the same reason if ellipsis is used numbers like this is used it should be always the last parameters

91
00:08:35,350 --> 00:08:36,630
in the list.

92
00:08:36,730 --> 00:08:40,660
Remember the so called risk parameters index.

93
00:08:40,660 --> 00:08:42,270
Very important very interesting.

94
00:08:44,050 --> 00:08:48,510
Then we have also got support for function overloading.

95
00:08:48,520 --> 00:08:50,060
Now what does this function overloading

96
00:08:53,060 --> 00:08:54,000
optional parameters.

97
00:08:53,990 --> 00:09:00,770
We have covered either you can make the parameter as obstinate or provide a default value if it is optional

98
00:09:00,780 --> 00:09:02,740
we have to take care of it.

99
00:09:02,810 --> 00:09:07,190
You don't have to explicitly set of last parameter only as default.

100
00:09:07,460 --> 00:09:12,300
Are your parameters also can be mentioned as default but they should be given a value and then we have

101
00:09:12,600 --> 00:09:13,610
a I.

102
00:09:13,730 --> 00:09:19,340
No it is network overloading function overloading.

103
00:09:19,450 --> 00:09:26,830
So here what we have to do is when we want to write a function with variable number of parameters we

104
00:09:26,830 --> 00:09:28,680
can actually use a function.

105
00:09:28,720 --> 00:09:33,920
So it's not really the same function if different pattern different parameters.

106
00:09:34,330 --> 00:09:39,940
We can write a function and we don't get any datatype for the parameters.

107
00:09:39,940 --> 00:09:46,870
In such case it is going to take it as an unwritten and so we have given any.

108
00:09:47,310 --> 00:09:50,850
But in addition to simply keeping the function like this.

109
00:09:50,910 --> 00:10:01,460
So you're just copying all this just to see if some time and put it here.

110
00:10:01,500 --> 00:10:05,200
Let's see if I can comment on this.

111
00:10:05,210 --> 00:10:05,850
No

112
00:10:11,340 --> 00:10:16,140
so now here this is going to give compilation.

113
00:10:16,250 --> 00:10:18,770
The code will alone will give compilation error.

114
00:10:19,190 --> 00:10:26,910
Why why this give companies an error because we don't have matching prototype of parameters Boullier

115
00:10:27,040 --> 00:10:30,360
string is not that string string is the number number is.

116
00:10:30,380 --> 00:10:35,650
So this is number number up string string but boolean string is not there.

117
00:10:36,200 --> 00:10:43,250
So I said there is no direct support for function overloading but we provide signatures only signatures.

118
00:10:43,250 --> 00:10:46,310
And finally we can provide the function implementation.

119
00:10:46,520 --> 00:10:48,990
Implementation has to be provided separately.

120
00:10:49,220 --> 00:10:55,060
And what other functions we want to support in the declaration has to be provided once.

121
00:10:55,100 --> 00:10:59,440
So if you want to support boolean com a boolean by any chance you create one more copy.

122
00:10:59,450 --> 00:11:07,730
Or rather let's say I want to support let's say boolean and number then we have to write like this.

123
00:11:07,730 --> 00:11:09,640
Or let's say a boolean string.

124
00:11:09,760 --> 00:11:16,040
I don't let's not this also will be correct but it is my responsibility now to take care of properly

125
00:11:16,040 --> 00:11:20,690
implement the based on the value of x using type of.

126
00:11:20,870 --> 00:11:27,850
You have to figure out what is the type in X if X is NABO do this if X is string do this.

127
00:11:27,850 --> 00:11:34,990
If x is Boolean do this like that approach implementation I hope it's clear.

128
00:11:35,010 --> 00:11:39,340
Run the program Cody Eby Ruby.

129
00:11:39,380 --> 00:11:44,780
To me it works because automatically concatenation will be done because of the procedure when it is

130
00:11:44,780 --> 00:11:45,800
not what we are writing.

131
00:11:45,980 --> 00:11:50,220
When it is not IMO you are concatenating.

132
00:11:50,540 --> 00:11:53,240
So now this is also valid.

133
00:11:53,630 --> 00:12:02,730
Typical syntax for supporting function overloading feature.

134
00:12:02,740 --> 00:12:05,850
Then finally we have got something called a function.

135
00:12:05,860 --> 00:12:15,640
I use nor does this function by simulating a function which can at times to each of the paramita and

136
00:12:15,640 --> 00:12:21,560
then the function itself which is actually written like when every writing function we are providing

137
00:12:21,560 --> 00:12:26,430
types to each of the parameter to the function itself.

138
00:12:26,430 --> 00:12:29,330
The prototype which is the return type.

139
00:12:29,410 --> 00:12:36,640
That's what is known fact or typescript configured to return type by looking at the written statement

140
00:12:37,390 --> 00:12:39,850
so we can obscenely this off.

141
00:12:39,850 --> 00:12:47,650
In many cases so even if you do not provide a return type Still it is OK based on the value which we

142
00:12:47,650 --> 00:12:51,250
are returning automatically typescript will decide to return.

143
00:12:51,790 --> 00:12:57,890
So what is that information is made up of two parts the type of the arguments with the parameters and

144
00:12:58,000 --> 00:12:58,760
the return.

145
00:12:58,790 --> 00:13:01,530
This is what a function is made above.

146
00:13:01,690 --> 00:13:02,890
So what I don't have is

147
00:13:06,970 --> 00:13:19,070
Funk's types I have no let let's add subtract example in Dignam subtract.

148
00:13:20,160 --> 00:13:25,920
Call them I know provide a date such that I bracket.

149
00:13:25,930 --> 00:13:28,300
So for some novae up to date.

150
00:13:28,960 --> 00:13:32,680
Not this date or type i will do as a signature of a method.

151
00:13:32,740 --> 00:13:39,250
Let's say ex-school a number of white color number.

152
00:13:39,920 --> 00:13:44,340
So what is this x and y are two meters.

153
00:13:44,520 --> 00:13:51,120
And yet dimension place number 7.

154
00:13:51,490 --> 00:13:59,080
Or does this mean all this means that some good is going to read part of function which is going to

155
00:13:59,080 --> 00:14:03,930
have two parameters X and Y and return type number.

156
00:14:04,290 --> 00:14:09,770
So can the write function so we have to point some implementation No that's only a thing.

157
00:14:09,820 --> 00:14:10,980
This is only data.

158
00:14:11,230 --> 00:14:14,600
How do we initialize it for anything like this and we can ride.

159
00:14:14,710 --> 00:14:26,890
So it is call to function x y and implement this in here and if done X minus Y that's it.

160
00:14:26,890 --> 00:14:33,010
So what is the database of X because subbies already given the data your X will be treated as Mombo

161
00:14:34,730 --> 00:14:38,680
y will be treated as normal if you want to continue on.

162
00:14:38,840 --> 00:14:39,470
That's fine.

163
00:14:39,470 --> 00:14:41,100
Names do not matter.

164
00:14:44,320 --> 00:14:45,060
That's.

165
00:14:46,280 --> 00:14:54,200
Still it is normal if I take this to spring and spring or the material becomes unstring Yes if you want

166
00:14:54,210 --> 00:14:57,150
you can again explicitly mentioned type.

167
00:14:57,300 --> 00:14:58,150
There's nothing wrong

168
00:15:03,360 --> 00:15:07,300
but that's not quite so here what we have done.

169
00:15:07,390 --> 00:15:14,060
We have become functional as it type repeat function as it I with it.

170
00:15:14,160 --> 00:15:19,690
In this particular example it's a function with two barometers both of type in detail and return type

171
00:15:19,720 --> 00:15:22,600
indeed written date number not in number.

172
00:15:22,750 --> 00:15:25,880
Any double or indeed that everything is fine.

173
00:15:26,460 --> 00:15:32,290
And once we have the variable initialized we cannot call that function call.

174
00:15:32,320 --> 00:15:33,790
We can say a lot.

175
00:15:33,850 --> 00:15:40,060
So maybe 20 come out and we can do this.

176
00:15:40,070 --> 00:15:43,810
So let me come in this off and

177
00:15:48,840 --> 00:15:55,110
10 20 minutes to Mr. So functions can be used as that.

178
00:15:55,490 --> 00:16:02,820
So some important topics about function you can have optional parameters default values which need not

179
00:16:02,820 --> 00:16:04,500
be at the end.

180
00:16:04,710 --> 00:16:10,510
We can have a risk paramita we call this concept address parameters wherein we provide the parameter

181
00:16:10,580 --> 00:16:16,680
and at a nudist numbers I've taken as a real number but not the kind of area of string.

182
00:16:16,890 --> 00:16:27,310
And if you want hybrid or any and we can have an array of any any data type and then based on matter

183
00:16:27,320 --> 00:16:32,950
overloading features that we have deployed all the signatures and that pro-white implementation.

184
00:16:33,200 --> 00:16:39,530
But in the implementation we have to explicitly take care of the implementation based on the type type

185
00:16:39,530 --> 00:16:40,320
of the.

186
00:16:40,370 --> 00:16:49,120
That's why we use paypal and finally function types is the concept we have all these features are really

187
00:16:49,120 --> 00:16:56,020
frequently used in diets and so have very good understanding that we really can you show us more topic

188
00:16:56,350 --> 00:17:03,450
very important topic and to have your understanding I would request you to practice that is very important.

189
00:17:03,580 --> 00:17:03,910
Thank you.

