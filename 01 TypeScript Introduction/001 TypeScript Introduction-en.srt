1
00:00:16,620 --> 00:00:23,310
Typescript in this particular module I'm going to provide you a very high level introduction to what

2
00:00:23,310 --> 00:00:25,310
is typescript.

3
00:00:25,380 --> 00:00:26,740
Why did a toilet paper.

4
00:00:26,760 --> 00:00:29,330
It was because of the limitations of javascript.

5
00:00:29,480 --> 00:00:35,670
What limitations of DOS could we're eventually going to see certain important points about typescript

6
00:00:35,670 --> 00:00:38,290
and then the benefits of typescript.

7
00:00:38,430 --> 00:00:45,960
And finally we are going to set up our environment for writing papers code to write the first script

8
00:00:46,600 --> 00:00:49,280
bit and execute in the browser.

9
00:00:49,890 --> 00:00:53,930
So first thing first why did typescript evolve.

10
00:00:54,100 --> 00:01:00,570
So we already have a wonderful scripting language javascript and I'm pretty sure if you are looking

11
00:01:00,570 --> 00:01:08,550
at this particular video you already know javascript You will doubtless Kurupt for validation so far.

12
00:01:08,750 --> 00:01:14,290
We used a script for writing dynamic web pages.

13
00:01:14,310 --> 00:01:20,760
Notice we also use javascript for server side Nordiques programming so javascript or a period of time

14
00:01:20,820 --> 00:01:27,290
has become pretty powerful and is used on both plain sight as well as server side.

15
00:01:27,750 --> 00:01:35,290
But the fact is there are certain limitations of it and the most important of all is it is not safe.

16
00:01:35,590 --> 00:01:37,760
Or what does it mean it is not typesafe.

17
00:01:37,950 --> 00:01:46,500
Javascript does not have any data types so the result of expression at many times is not sure to be

18
00:01:46,500 --> 00:01:48,340
a perfect value.

19
00:01:48,360 --> 00:01:55,920
For example many Pless operator is used between two variables are the two variables going to be concatenated

20
00:01:56,470 --> 00:01:57,930
or they are going to be used.

21
00:01:57,930 --> 00:02:05,160
But adding the numbers in them is undecided until runtime when the value of the variables will be taking

22
00:02:05,160 --> 00:02:07,420
the priority.

23
00:02:07,440 --> 00:02:10,630
I mean most of the decision is going to be taken.

24
00:02:11,040 --> 00:02:15,480
So there are a lot of other cases where there are safety issues.

25
00:02:15,510 --> 00:02:19,510
That means because of no data types there are issues in programming.

26
00:02:20,220 --> 00:02:23,770
So that is one real big problem with javascript.

27
00:02:23,880 --> 00:02:29,750
Second it is too cumbersome when it becomes larger when you write large applications.

28
00:02:29,880 --> 00:02:36,570
It is going to become really very nasty very difficult to manage to properly separate them into proper

29
00:02:36,570 --> 00:02:37,610
just files.

30
00:02:37,740 --> 00:02:44,790
Lingos just files with each other so that one does file is properly used in the other disbalance on

31
00:02:44,790 --> 00:02:46,710
and on.

32
00:02:46,800 --> 00:02:52,880
Yes and also the other major problem which it hasn't is it is not a compiled language like CRC sharp

33
00:02:52,890 --> 00:02:54,890
or Java nowadays are compiled.

34
00:02:54,900 --> 00:02:56,940
It is not a compiled language.

35
00:02:57,000 --> 00:02:58,910
It is only the interpreter.

36
00:02:59,370 --> 00:03:08,160
So any kind of error if Did you will not know unless you run your program under javascript program on

37
00:03:08,460 --> 00:03:11,720
debugging Dallas Kurupt code is a kind of mess.

38
00:03:11,880 --> 00:03:18,960
You should be really a very good export to properly debug the error in a script as per my experience

39
00:03:18,970 --> 00:03:21,780
what I have noted is indels crap.

40
00:03:21,810 --> 00:03:28,290
If you have done some error unless you yourself know that it is error it is very difficult to figure

41
00:03:28,290 --> 00:03:30,160
out that problem and resolved.

42
00:03:30,800 --> 00:03:37,220
So with all these problems that there was a very good part for could to evolve.

43
00:03:37,740 --> 00:03:46,320
So yes by Anders Hazor both the founder of working in Microsoft has the is the person who was majorly

44
00:03:46,320 --> 00:03:48,620
contributed to was crap.

45
00:03:48,880 --> 00:03:55,360
It is an open source so contributed by many others as well from the industry.

46
00:03:55,360 --> 00:04:01,950
And what is very important thing to understand here is javascript is not a replacement of the typescript

47
00:04:01,950 --> 00:04:09,150
is not the placement of javascript and it doesn't even add any new feature of javascript.

48
00:04:09,330 --> 00:04:18,090
That means typescript is nothing but just the plain down script with types and one that's holding the

49
00:04:18,390 --> 00:04:19,700
typescript.

50
00:04:19,740 --> 00:04:24,070
So what is typescript type superset of Javascript.

51
00:04:24,870 --> 00:04:30,870
And whenever we are writing a script document it is absolutely not mandatory that we should and must

52
00:04:30,870 --> 00:04:32,130
have types.

53
00:04:32,160 --> 00:04:40,010
That means you can take an existing does by rename that file extension with Dot be as a byte script

54
00:04:40,470 --> 00:04:44,800
and you can compile it like any other types could find.

55
00:04:44,820 --> 00:04:53,190
That means every telescreen file is also a typescript fine but it takes time has to be compiled to javascript

56
00:04:53,910 --> 00:04:59,370
because in typescript we are going to have data types though they are optional but we can have data

57
00:04:59,430 --> 00:05:04,290
types but the compiled javascript are not going to have any debates.

58
00:05:04,410 --> 00:05:10,920
So what is important point here to remember is that here becomes like a source do you are going to write

59
00:05:11,040 --> 00:05:11,860
a script.

60
00:05:12,210 --> 00:05:16,630
The execution will not be typescript it is the compiled output of paper.

61
00:05:16,800 --> 00:05:19,490
That is javascript which is going to be executed.

62
00:05:20,270 --> 00:05:27,010
So your typescript does not replace javascript very very very important point.

63
00:05:27,150 --> 00:05:34,770
Typescript is only a superset of toms grip with additional feature that is data types and because of

64
00:05:34,770 --> 00:05:38,290
the data types lots of beautiful features are added to it.

65
00:05:38,340 --> 00:05:45,430
It becomes a little simple to manage many errors can be known at the time when we are trying to compile

66
00:05:45,430 --> 00:05:48,630
a typescript file in a script and so on.

67
00:05:48,630 --> 00:05:51,540
Or that one day just which we'll discuss later.

68
00:05:51,700 --> 00:05:57,770
So typescript generated script can reuse all the existing framework tools and libraries.

69
00:05:57,960 --> 00:06:04,440
So very well you have been using javascript Darick Dulles creep in all those places.

70
00:06:04,480 --> 00:06:13,780
Typescript generated script can be used whether it is angular full or more deals or reacting is or any

71
00:06:13,780 --> 00:06:17,080
other javascript frameworks which are popular today.

72
00:06:17,440 --> 00:06:19,450
All can make use of tapes Crebbin.

73
00:06:19,480 --> 00:06:21,650
Believe me trust me.

74
00:06:21,850 --> 00:06:27,230
Typescript to a major extent is replacing Dahlia's print directly we are not writing DOS script anymore

75
00:06:27,290 --> 00:06:34,750
but we would prefer writing tapes script and compiling that top down script VI because it is Object-Oriented

76
00:06:35,560 --> 00:06:38,950
so object orientation is the best of times.

77
00:06:39,460 --> 00:06:46,820
So in typescript you will find a lot of features which are like in Java or C-Sharp powerful languages.

78
00:06:47,110 --> 00:06:53,710
So if you want to learn Pape's grep basic knowledge of object orientation becomes essential.

79
00:06:55,180 --> 00:07:02,520
That is very very important and typescript is not just the only language which is generating a body

80
00:07:02,570 --> 00:07:03,060
is fine.

81
00:07:03,100 --> 00:07:10,480
There are many languages but somehow that script has become very popular among all these languages.

82
00:07:10,480 --> 00:07:15,970
So there is something called as your script office crap life script does or the other scripting languages

83
00:07:16,330 --> 00:07:19,640
which are dead which also generates bytes.

84
00:07:20,110 --> 00:07:26,360
But it's obviously because of the Microsoft backing and up bosome Anders is extraordinary.

85
00:07:26,380 --> 00:07:27,550
A brilliant post.

86
00:07:27,700 --> 00:07:33,980
The contribution he has made for typescript has made it really popular among the masses not just Microsoft.

87
00:07:34,210 --> 00:07:38,230
Even others are making use of typescript because it is not open source.

88
00:07:38,230 --> 00:07:42,700
It is no more native product of Microsoft.

89
00:07:43,450 --> 00:07:45,570
So what are some basic features of typescript.

90
00:07:45,580 --> 00:07:54,190
You have got Bible rotations compile time checking type inference by resource classes modules interfaces

91
00:07:54,490 --> 00:07:56,250
we have enumerated data types.

92
00:07:56,260 --> 00:08:00,530
There is some concept of optional parameters or different values but they probably do.

93
00:08:00,820 --> 00:08:07,150
Generics are also part of it namespace as can be did of 8 can be that all the other beautiful features

94
00:08:07,500 --> 00:08:15,540
which actually are the top of javascript make tapes more and more powerful coming to its history yes

95
00:08:15,920 --> 00:08:19,600
or is merely introduced in October 2012.

96
00:08:19,850 --> 00:08:23,750
Today we have got to appoint experts and I will not say 2.0.

97
00:08:23,840 --> 00:08:25,920
We already have two point six I believe.

98
00:08:25,940 --> 00:08:31,970
When I'm recording this particular video it's growing but boy is a pretty stable was in which people

99
00:08:31,970 --> 00:08:34,060
are using in the industry you know.

100
00:08:34,310 --> 00:08:36,620
So what are the benefits of typescript.

101
00:08:36,770 --> 00:08:44,980
It compiles the code and generates syntax errors if any like in C-sharp or Java you get syntax errors

102
00:08:45,440 --> 00:08:49,070
great script also generate syntax errors before you end it.

103
00:08:49,090 --> 00:08:51,850
Exit cubes rather tight script cannot execute.

104
00:08:52,060 --> 00:08:56,520
But before it generates javascript there will be compilation done.

105
00:08:56,560 --> 00:09:04,480
That means there is a paper script compiler whose all will be crap but when is the going to be generated

106
00:09:04,990 --> 00:09:07,940
only when there are no syntax errors.

107
00:09:07,960 --> 00:09:12,470
So yes this helps to highlight errors before the script gets executed.

108
00:09:12,940 --> 00:09:19,770
As I said earlier it is having all object oriented features and gap solution inheritance polymorphism.

109
00:09:19,780 --> 00:09:27,370
I can see all of that in this but because of the same reason it is reusable and easy to manage in large

110
00:09:27,400 --> 00:09:33,480
and complicated projects because the very basic code also we would write that class.

111
00:09:34,280 --> 00:09:40,480
Angular framework which is the very popular framework now is written in typescript and it is recommended

112
00:09:40,480 --> 00:09:48,500
that also use the same script as a language but writing angular pages any kind of AngloGold that you

113
00:09:48,500 --> 00:09:49,660
want to write it.

114
00:09:49,760 --> 00:09:58,520
It is recommended to use script over a script due the static typing done in typescript is more predictable

115
00:09:58,820 --> 00:10:01,270
and generally easier to debug.

116
00:10:01,280 --> 00:10:06,000
Basically intellisense is going to work if you are using any good editor.

117
00:10:06,080 --> 00:10:08,750
The lot of editors available for typescript.

118
00:10:09,080 --> 00:10:13,100
You have got visual studio by Microsoft.

119
00:10:13,130 --> 00:10:19,000
We also have eclipse and I think there are a lot of third party editors also available where we can

120
00:10:19,000 --> 00:10:19,960
write code.

121
00:10:20,180 --> 00:10:28,550
So yes intellisense is really the very beautiful feature which makes it very easy for one to write predictable

122
00:10:28,550 --> 00:10:35,840
code and obviously because of the same reason it is very easy to do but we can do debugging of typescript

123
00:10:36,110 --> 00:10:40,960
directly by putting point like we would do each of code debugging.

124
00:10:41,030 --> 00:10:46,580
You can do typescript called debugging with breakpoints and watch Windows or not those windows and so

125
00:10:46,640 --> 00:10:48,410
many other features.

126
00:10:49,340 --> 00:10:54,950
So the setup we will need either results to your 20:15 or results to your 20:17 that is what I am going

127
00:10:54,950 --> 00:10:57,200
to use throughout my course.

128
00:10:57,200 --> 00:11:02,150
I'm going to use the visual studio community in NATION which is absolutely free.

129
00:11:02,150 --> 00:11:10,230
You can also google visual studio 20:15 or the latest individuals on your 20:17 community edition download.

130
00:11:10,300 --> 00:11:13,520
So support this particular praise and then download that.

131
00:11:13,550 --> 00:11:15,770
Install it and we can.

132
00:11:15,770 --> 00:11:24,580
We are up and running so little if suppose you are planning to go for angular application development

133
00:11:24,670 --> 00:11:25,950
using typescript.

134
00:11:26,020 --> 00:11:32,710
You will need compulsory minimum typescript to point two words are higher of course so far that you

135
00:11:32,710 --> 00:11:37,030
have to download and install the latest version of typescript on your machine.

136
00:11:37,090 --> 00:11:43,960
So with this you are or if they are it is no more valid at the time of you watching this video.

137
00:11:43,990 --> 00:11:51,720
Simply google typescript for Visual Studio and it can take you to this link and you love to download

138
00:11:51,810 --> 00:11:53,310
and install it.

139
00:11:53,340 --> 00:11:53,900
That's.

140
00:11:53,970 --> 00:11:56,530
You should already have the tools for you on your machine.

141
00:11:56,730 --> 00:12:05,020
After that you have to install typescript SDK for Weasel's to deal with the latest wasn't use that it

142
00:12:05,120 --> 00:12:11,860
I suppose you've already got the note package manager on your machine you can start it's complete disposal

143
00:12:11,890 --> 00:12:17,840
by going to command prompt and putting and NPM in store minus details.

144
00:12:18,190 --> 00:12:23,750
But of course prior to this you should already have no package manager existing on your machine.

145
00:12:24,190 --> 00:12:29,980
So if not package manager is not there you can again go to Google search what package manager installer

146
00:12:30,360 --> 00:12:31,950
set up that for Windows machine.

147
00:12:31,960 --> 00:12:36,620
And after that run this suppose you've already done it.

148
00:12:36,610 --> 00:12:37,780
I couldn't find.

149
00:12:37,990 --> 00:12:41,490
And if you want to look command line completion you're going to like this.

150
00:12:41,510 --> 00:12:44,850
If it is only one file the Cynthia C. don't see

151
00:12:47,630 --> 00:12:54,850
is that b a c command is in bhakt then does DSC yet he will work from me.

152
00:12:54,890 --> 00:12:58,030
If not you'll have to search where the A C on your machine.

153
00:12:58,250 --> 00:13:05,120
And then from that directory to execute the DSC that he actually come on with the he is fine.

154
00:13:05,120 --> 00:13:11,470
Or if you own multiple files you can give multiple files with an option of specifying only bondages

155
00:13:11,560 --> 00:13:12,160
file.

156
00:13:12,470 --> 00:13:17,530
So you'll have multiple types of fights but in all we are going to get only one.

157
00:13:17,550 --> 00:13:23,200
Despite of course you're for one daz we are going to get the old one just fine.

158
00:13:24,380 --> 00:13:29,830
And what is this command if Suppose you write this that one day it is every time you make a change to

159
00:13:29,840 --> 00:13:37,030
this and save it automatically it is going to compile the output with minus what option.

160
00:13:37,040 --> 00:13:44,390
So just saving the file is sufficient biase will immediately compile and then the latest device.

161
00:13:44,420 --> 00:13:47,420
So we're going to of course make use of visual studio.

162
00:13:47,420 --> 00:13:54,530
As I said go do visual studio first verify if your results truly already has typescript are not good

163
00:13:54,680 --> 00:14:00,060
about and you'll see here we already should have Kyra should be lost.

164
00:14:00,290 --> 00:14:06,280
Typescript 2.3 is installed in my studio currently.

165
00:14:06,590 --> 00:14:09,190
Now we create a new project here.

166
00:14:09,460 --> 00:14:15,580
Any new website empty web site is good enough.

167
00:14:20,520 --> 00:14:23,540
My first demo

168
00:14:27,070 --> 00:14:32,360
some empty template will be created a web application is going to be created because we need to execute

169
00:14:32,360 --> 00:14:35,280
that Steimle file.

170
00:14:35,340 --> 00:14:37,110
I'm going to get it now.

171
00:14:39,400 --> 00:14:47,330
Or perhaps I could find makes it more tedious in this day.

172
00:14:47,420 --> 00:14:54,440
Yes we can write a file something like function the intellisense beautiful feature.

173
00:14:55,330 --> 00:14:56,290
Hello world.

174
00:14:56,290 --> 00:14:57,150
Or say hello

175
00:15:00,430 --> 00:15:09,660
we can be clear here a variable message coolants string is equal to some Hello.

176
00:15:09,690 --> 00:15:11,680
Let's say hello world.

177
00:15:11,680 --> 00:15:19,060
And this I would like to display the message.

178
00:15:19,940 --> 00:15:24,520
Semicolon is optional statement terminator like conduct like in Dallas.

179
00:15:24,830 --> 00:15:28,310
So this is just a function you only I can call the function

180
00:15:31,940 --> 00:15:38,000
but to execute this typescript we need to first have been filed and we did.

181
00:15:38,480 --> 00:15:40,700
And also we need to have this just fine.

182
00:15:40,740 --> 00:15:45,640
Included in Steimle bit small spelling mistake

183
00:15:50,000 --> 00:15:53,800
no let us add to the project

184
00:15:58,470 --> 00:16:09,740
deal will be the AGM and in order to extend will we see we select the BSA by drag and drop.

185
00:16:09,920 --> 00:16:13,140
And you'll notice that you are generated script.

186
00:16:13,430 --> 00:16:24,120
But it has replaced this with dis information and to me the modern daemon as the start page.

187
00:16:24,550 --> 00:16:26,330
And this control.

188
00:16:28,470 --> 00:16:31,320
Andruzzi Hello World types can.

189
00:16:31,380 --> 00:16:37,230
But executed through this the typescript was included.

190
00:16:37,340 --> 00:16:38,900
What was it a.

191
00:16:39,090 --> 00:16:40,740
Which was actually included.

192
00:16:41,080 --> 00:16:44,130
No you notice that in the same folder.

193
00:16:44,130 --> 00:16:48,570
Now we are going to have an open folder in file explorer.

194
00:16:48,770 --> 00:16:56,850
We are going to have a fight so we can see advantages by and let us compare the typescript and the genius

195
00:16:57,990 --> 00:17:00,760
will see almost everything the same just that.

196
00:17:00,760 --> 00:17:04,870
Here we have the bitartrate that is missing in case of genius.

197
00:17:05,220 --> 00:17:13,180
So this is the source daz the compiled output of that is that don't scream.

198
00:17:13,440 --> 00:17:15,940
Now this is one small example.

199
00:17:16,290 --> 00:17:18,360
So let's take one more example.

200
00:17:20,150 --> 00:17:25,250
Allowed one more fine Obregon probably did the same fighting with one more function.

201
00:17:25,260 --> 00:17:29,240
So I have no or I'll make changes to dysfunction itself.

202
00:17:29,760 --> 00:17:33,940
I would like to have forced me in spring.

203
00:17:34,310 --> 00:17:43,400
And lastly you see I'm declaring data types for them and I'll have methods to dismiss it.

204
00:17:43,400 --> 00:17:44,900
I would like to append

205
00:17:52,020 --> 00:17:57,220
hello first name.

206
00:17:58,140 --> 00:18:03,970
Last thing and other word is important.

207
00:18:03,980 --> 00:18:09,630
You see this is giving compilation error that is a beautiful thing yet why does giving compilation error.

208
00:18:09,640 --> 00:18:12,030
It is expected to have two arguments.

209
00:18:12,130 --> 00:18:17,750
We are not given one so give you let's say my name and the song

210
00:18:20,680 --> 00:18:25,500
so simple to get up if you have just I did but I did the paper here.

211
00:18:25,860 --> 00:18:33,560
Yup you can also see golden string with golden string.

212
00:18:33,560 --> 00:18:36,800
What is the meaning Reagen return a sprinkle.

213
00:18:37,010 --> 00:18:39,330
So don't let it message.

214
00:18:40,240 --> 00:18:41,540
So if needed.

215
00:18:42,240 --> 00:18:46,590
We can do this a lot from here and put a lot of

216
00:18:52,580 --> 00:18:53,750
Nora on the program.

217
00:18:53,990 --> 00:18:55,390
So what is the difference.

218
00:18:55,400 --> 00:19:01,700
We came from a simple function with no parameters to a function with parameters.

219
00:19:02,060 --> 00:19:07,340
Important thing is for every parameter we are mentioning the data type and we're also mentioning the

220
00:19:07,340 --> 00:19:10,670
return type so let the program know

221
00:19:14,240 --> 00:19:15,770
Hello Sunday morning.

222
00:19:16,120 --> 00:19:18,970
But again these values be taken from text boxes.

223
00:19:19,110 --> 00:19:20,880
Yes you can always do that.

224
00:19:21,240 --> 00:19:27,870
You just program in extremal the way you would program in any other normal fine like that only you have

225
00:19:27,870 --> 00:19:29,150
to do it.

226
00:19:29,760 --> 00:19:32,160
Now can we pull this whole thing in say the class.

227
00:19:32,250 --> 00:19:39,140
Yes we can as well put the whole function inside plus the instance of the class and use that instance

228
00:19:39,400 --> 00:19:42,040
for invoking a method in it.

229
00:19:42,420 --> 00:19:45,600
Let me give a very high level all of that.

230
00:19:45,630 --> 00:19:51,320
So I write the class know hello and insert the class.

231
00:19:51,360 --> 00:19:54,810
I would like to have the variables first name

232
00:19:58,900 --> 00:20:04,780
of type scoring last name which is also of paper string.

233
00:20:05,370 --> 00:20:10,420
I would like to say hello which is a function which I want to write.

234
00:20:10,440 --> 00:20:19,590
No equal to that function.

235
00:20:19,860 --> 00:20:24,610
Now I don't need parameters because we already have the first name and last name.

236
00:20:25,260 --> 00:20:34,930
And I simply say hello concatenate bit was named no force.

237
00:20:35,070 --> 00:20:37,730
Here we have to write this.

238
00:20:37,930 --> 00:20:41,910
Less space less.

239
00:20:42,090 --> 00:20:44,450
This last name.

240
00:20:44,890 --> 00:20:48,670
And here are the return type of string

241
00:20:51,940 --> 00:20:55,520
know more about object orientation I'm going to teach in detail later.

242
00:20:55,540 --> 00:20:57,650
But right now this is just an example.

243
00:20:57,970 --> 00:21:00,850
So now what can be done now we can write up

244
00:21:03,420 --> 00:21:15,750
instance of Hello negative hit call on hello new hello and then on head I can set first name

245
00:21:18,970 --> 00:21:21,170
on had a concept last name.

246
00:21:23,120 --> 00:21:28,250
I can then see a Ludd head dot say hello

247
00:21:31,820 --> 00:21:33,820
so like this we are now able to write

248
00:21:37,960 --> 00:21:39,130
object oriented code.

249
00:21:39,180 --> 00:21:42,720
And this is what most people are going to do of course.

250
00:21:42,720 --> 00:21:48,960
Again I'm repeating right now my target is not to demonstrate object orientation or any particular feature

251
00:21:49,020 --> 00:21:52,680
of each of typescript.

252
00:21:52,680 --> 00:21:56,440
I'm just trying to demonstrate how old can be done in paper.

253
00:21:56,850 --> 00:21:59,670
We're going to go each of these concepts in detail later.

254
00:21:59,780 --> 00:22:02,980
Let's run and we should again get to see him.

255
00:22:04,080 --> 00:22:05,100
OK.

256
00:22:05,750 --> 00:22:12,160
Now again let us look at the auto generated daz fine.

257
00:22:12,930 --> 00:22:14,970
And you see this lot

258
00:22:19,000 --> 00:22:24,520
are writing it plus in typescript in Dallas it does a functional

259
00:22:28,320 --> 00:22:30,730
we don't have to write this complicated function.

260
00:22:30,990 --> 00:22:38,790
We have written a simple class and that is why I'm saying typescript is going to go Poppel so in typescript

261
00:22:38,880 --> 00:22:44,490
we can write down the typescript we can also do debugging.

262
00:22:44,630 --> 00:22:46,820
Let's say for the break point here.

263
00:22:47,370 --> 00:22:54,200
Google GOOG will grow it automatically going to break the yes and now you can see the value of these

264
00:22:54,200 --> 00:22:55,110
variables.

265
00:22:56,210 --> 00:22:59,680
And use all the studio features step in.

266
00:22:59,670 --> 00:23:04,740
Most important for debugging the application.

267
00:23:04,780 --> 00:23:08,710
So this is how the first script program is.

268
00:23:08,840 --> 00:23:10,880
And it is object oriented.

269
00:23:11,230 --> 00:23:11,670
Thank you.

