1
00:00:16,890 --> 00:00:23,160
Resetting the phone generally what we do is put the form elements we will take that up.

2
00:00:23,490 --> 00:00:29,070
We will perform some action something like inserting the data into the database table by making a call

3
00:00:29,070 --> 00:00:35,160
to remote service using a tax or maybe updating the data into the database.

4
00:00:35,160 --> 00:00:41,630
Again making a call do remote attacks but often that the whole form has to be reset.

5
00:00:41,790 --> 00:00:46,110
So in Angola What are the challenges in resetting the form.

6
00:00:46,470 --> 00:00:51,210
Let's try to understand this for us who is in my head Steimle template.

7
00:00:51,210 --> 00:00:53,980
I'm not going to add a bottom.

8
00:00:54,140 --> 00:01:01,340
So I go to the human template that is employ that component or it's deman and let's say I'm not adding

9
00:01:01,400 --> 00:01:06,150
a new button but what is this what I'm going to do when I click on this button.

10
00:01:06,170 --> 00:01:11,580
It is calling new employee method which I would like to write in my component.

11
00:01:11,840 --> 00:01:13,320
So I go to complete.

12
00:01:13,580 --> 00:01:17,750
And here I see a new employee what should new employer do here.

13
00:01:17,960 --> 00:01:30,540
Actually here we should have code blue extract data from the MP component.

14
00:01:30,840 --> 00:01:36,000
I mean the audit using AJAX to make a deposit.

15
00:01:39,520 --> 00:01:47,580
Do some what part of this we should be set up for because that data is already consumed and updated

16
00:01:47,580 --> 00:01:48,580
on server.

17
00:01:49,440 --> 00:01:55,920
So let us only for demo purposes and say updated and often it is updated.

18
00:01:55,920 --> 00:02:00,330
We would like to reset reset what let's reset the Employee object.

19
00:02:00,330 --> 00:02:09,020
So we would like to create Employee object now with default data so Id Zero name empty salary zero.

20
00:02:09,060 --> 00:02:14,840
And that's a department also empty I said No let us run this.

21
00:02:14,880 --> 00:02:22,070
But even before I run I would like to provide a default name here some name which I remolded for many

22
00:02:22,070 --> 00:02:22,790
reasons.

23
00:02:24,580 --> 00:02:30,110
So I have to follow all the form elements are in pristine state.

24
00:02:30,350 --> 00:02:38,380
Just going to actually take the kill and you see form control untouched indeed pristine and de-valued

25
00:02:38,470 --> 00:02:44,300
all three are dead seamier that's pristine valid untouched pristine Voller.

26
00:02:44,550 --> 00:02:45,420
So what are we doing.

27
00:02:45,430 --> 00:02:50,660
We click on new employee all that it is going to get updated.

28
00:02:50,680 --> 00:02:51,490
Fantastic.

29
00:02:51,490 --> 00:02:54,450
All this happened.

30
00:02:54,610 --> 00:03:01,710
We don't have the text box also visible for the reason that it is still in pristine state only.

31
00:03:02,200 --> 00:03:03,880
But now I'm going to provide data.

32
00:03:03,910 --> 00:03:06,620
Let's say one two three whatever.

33
00:03:06,810 --> 00:03:17,270
The ABC and some salary and some department items from the list see the state of each one of them.

34
00:03:17,450 --> 00:03:21,660
It is dirty valid that it is dirty.

35
00:03:21,760 --> 00:03:23,370
It is valid untouched.

36
00:03:23,590 --> 00:03:26,030
That means the pristine is not there anymore.

37
00:03:26,960 --> 00:03:30,340
Dirty rallied that's afraid continue employing

38
00:03:33,620 --> 00:03:39,930
the existing one that's getting saved and not this time we're getting this this we don't want.

39
00:03:40,190 --> 00:03:47,840
This is what precisely we don't want because we have inserted why we are getting this because the state

40
00:03:47,840 --> 00:03:54,290
is not in the dirty and detached and indeed Invalides there and it is not pristine.

41
00:03:54,410 --> 00:03:59,680
White is not pristine because it is remembering the previous state precisely here.

42
00:04:01,040 --> 00:04:07,750
And obligacion again click on the new employee button and the form clears the required votes on the

43
00:04:07,750 --> 00:04:09,410
left of the input box.

44
00:04:09,580 --> 00:04:12,920
Which is good indicating invalid property name.

45
00:04:13,090 --> 00:04:16,190
That's understandable as these are required fields.

46
00:04:16,470 --> 00:04:20,190
The error messages are hidden because the form is pristine.

47
00:04:20,230 --> 00:04:28,310
We haven't changed anything but second time when we click on the new employee the application displays

48
00:04:28,310 --> 00:04:29,160
name is required.

49
00:04:29,180 --> 00:04:29,840
Error message.

50
00:04:29,840 --> 00:04:30,710
That's what is happening.

51
00:04:30,710 --> 00:04:34,740
Second time and we are doing an application again.

52
00:04:34,780 --> 00:04:38,070
We are getting this actually.

53
00:04:38,320 --> 00:04:41,180
Run the application again.

54
00:04:41,650 --> 00:04:42,450
Should be a

55
00:04:46,690 --> 00:04:54,100
it should be the application flustering application again we want we don't want error messages.

56
00:04:54,310 --> 00:05:01,630
When we create new empty imply why are we getting now inspecting the element in browser to retrieve

57
00:05:01,630 --> 00:05:06,190
that name text box is no longer pristine form.

58
00:05:06,190 --> 00:05:11,240
Remember that we entered a name before by clicking new employees.

59
00:05:14,020 --> 00:05:18,080
It remembers that by clicking on this new employee button it remember remembered.

60
00:05:18,300 --> 00:05:19,620
So what to do.

61
00:05:19,670 --> 00:05:20,770
Pretty simple.

62
00:05:20,980 --> 00:05:27,530
We have to simply do is right here.

63
00:05:29,020 --> 00:05:33,590
Semi-colon reportedly said the fall.

64
00:05:33,700 --> 00:05:36,110
Hope you get the reference to the form.

65
00:05:36,760 --> 00:05:41,180
So for the form I'll give has for name.

66
00:05:41,220 --> 00:05:50,130
There is something like MP form and I not simply use that empty form reset impy form.

67
00:05:50,230 --> 00:05:52,390
But that's it.

68
00:05:52,580 --> 00:06:01,890
This is very very important to see what is happening if I don't I'll run free speech first time I think

69
00:06:03,060 --> 00:06:04,320
I get this good.

70
00:06:04,440 --> 00:06:10,020
I give the date up.

71
00:06:13,380 --> 00:06:18,290
And again perfectly good to go there.

72
00:06:18,310 --> 00:06:25,360
Not all the data tends to focus on give the date up undo and we are getting the message.

73
00:06:25,420 --> 00:06:28,390
This is what is required behavior.

74
00:06:28,720 --> 00:06:34,770
So this is very important that we are resetting the phone often performing the action.

75
00:06:34,900 --> 00:06:40,940
The action might be of inserting the new employee into the database using AJAX call by invoking of a

76
00:06:40,940 --> 00:06:45,470
VPN server or so and so.

77
00:06:45,550 --> 00:06:49,250
So I just did this and this.

78
00:06:49,680 --> 00:06:51,140
Of course it didn't read right here.

79
00:06:51,140 --> 00:06:58,500
No I need it so that had Steimle validation can be suppressed as we are taking care of displaying our

80
00:06:58,500 --> 00:07:00,210
all error messages.

81
00:07:00,210 --> 00:07:07,610
We don't want it Steimle 5 to display its messages so the default behavior is suppressed here by I need

82
00:07:07,750 --> 00:07:11,480
it in the form that next

83
00:07:14,940 --> 00:07:16,240
submitting the form with.

84
00:07:16,270 --> 00:07:17,870
Indeed submit.

85
00:07:18,390 --> 00:07:30,010
Now in the form that we can actually provide he'll to a binding put in the Submit how to do that and

86
00:07:30,020 --> 00:07:40,910
we submit it and even actually for this I would like to write a letter now that is let us say in component.

87
00:07:40,940 --> 00:07:42,790
I'm going to pro-white on submit

88
00:07:46,360 --> 00:07:46,730
next.

89
00:07:46,730 --> 00:07:52,060
This is the requirement not all submit is a function of its ultralite in complement

90
00:07:56,110 --> 00:08:02,580
what what should they do in what I write here and submit the very first thing I would want to do is

91
00:08:03,090 --> 00:08:07,100
get the data from the form not to get the data from the phone.

92
00:08:07,150 --> 00:08:09,700
I need a reference to the form here.

93
00:08:10,480 --> 00:08:12,580
How do we get the reference references here.

94
00:08:12,590 --> 00:08:16,080
Actually this reference can be possible.

95
00:08:16,570 --> 00:08:24,290
Yes but when we posit that we will have to also get the reference by having an extra parameter here

96
00:08:25,030 --> 00:08:26,690
that is fop.

97
00:08:26,800 --> 00:08:27,640
You could call it that.

98
00:08:27,680 --> 00:08:38,290
And so once we have form can we get the data yes we get a lot disinform.

99
00:08:38,560 --> 00:08:42,890
And in that we have got mortal object.

100
00:08:43,210 --> 00:08:44,930
What is the model object.

101
00:08:46,000 --> 00:08:51,900
Morphologic we actually directly have it here itself so distraught ENP dot.

102
00:08:51,980 --> 00:08:56,920
If we can always do this but we don't want to make use of phone.

103
00:08:57,260 --> 00:09:04,000
So we have a property called a value which will actually refer to the form in that we have got a

104
00:09:08,320 --> 00:09:17,170
so like this form or Ronald or lessoning in human names ID a name.

105
00:09:17,450 --> 00:09:21,390
Let's see.

106
00:09:21,420 --> 00:09:24,500
So we have some data updated.

107
00:09:24,850 --> 00:09:29,120
Oh sorry I should pro-white it's a bit button in the phone tag.

108
00:09:29,310 --> 00:09:30,740
I didn't do that.

109
00:09:30,930 --> 00:09:37,140
So I thought we don't need this button because this button only is being replaced with submit.

110
00:09:37,140 --> 00:09:46,960
So I'll give it input or we can give button type is it will do submit or in bold type is a call to submit

111
00:09:47,500 --> 00:09:48,150
button.

112
00:09:52,290 --> 00:09:54,430
Submit name.

113
00:09:54,470 --> 00:09:59,260
You can get anything you like.

114
00:09:59,380 --> 00:10:02,590
And I would like to give your class

115
00:10:06,910 --> 00:10:08,700
BDM.

116
00:10:08,720 --> 00:10:11,550
I have one default.

117
00:10:11,810 --> 00:10:24,030
Good to see this title that is on this we'll call it a supplement.

118
00:10:24,190 --> 00:10:24,750
Something.

119
00:10:24,780 --> 00:10:26,180
You see click

120
00:10:29,700 --> 00:10:31,100
the code is not executing

121
00:10:37,790 --> 00:10:46,910
and submit all the on click the code to execute like Tony.

122
00:10:47,090 --> 00:10:53,600
I think we should reference full page is not referencing control.

123
00:10:53,640 --> 00:10:58,620
If submit it's not the exact wording.

124
00:11:01,890 --> 00:11:03,100
It's who's

125
00:11:06,570 --> 00:11:07,600
It's not loading

126
00:11:19,390 --> 00:11:26,880
to Neiman's my home

127
00:11:37,140 --> 00:11:38,170
for anything form.

128
00:11:38,200 --> 00:11:39,760
We don't have submit

129
00:11:42,870 --> 00:11:46,690
but it is distraint do I have written here on that

130
00:11:49,500 --> 00:11:50,480
indeed submit

131
00:11:55,020 --> 00:12:03,690
is that a problem.

132
00:12:04,070 --> 00:12:05,680
Let me do one thing.

133
00:12:06,090 --> 00:12:14,520
We place this book but this is not required or even if you keep it no problem.

134
00:12:16,290 --> 00:12:18,990
And here when the text submitted

135
00:12:22,170 --> 00:12:23,350
no let us play.

136
00:12:23,460 --> 00:12:26,030
Actually cute within all types of dolls.

137
00:12:27,750 --> 00:12:28,870
Because the form is something

138
00:12:32,160 --> 00:12:32,490
to

139
00:12:37,260 --> 00:12:41,490
cannot leave the property ID off undefined.

140
00:12:42,990 --> 00:12:48,540
So it come coming here cannot read the proper DJT

141
00:12:56,090 --> 00:12:59,610
form or it should be formed idea.

142
00:12:59,660 --> 00:13:07,970
Are you fond of her name Dadlani Lundy's

143
00:13:12,620 --> 00:13:25,580
of one and some of the forms is not listed in how to reset the phone for that like before I came up

144
00:13:25,580 --> 00:13:26,240
with this long

145
00:13:30,350 --> 00:13:32,020
Let's run this now.

146
00:13:34,310 --> 00:13:36,100
We had the same problem like before.

147
00:13:37,790 --> 00:13:40,730
This detailer say we make an object call and submit to say or what.

148
00:13:40,730 --> 00:13:44,470
An update it all good.

149
00:13:44,900 --> 00:13:48,240
I put away the value once again another employee diddles.

150
00:13:48,320 --> 00:13:48,770
As you

151
00:13:52,580 --> 00:13:55,800
now submit Quiggin we are getting this.

152
00:13:55,800 --> 00:14:03,800
We should not get this same story will have to reset the form state how to reset the form straight.

153
00:14:03,950 --> 00:14:12,680
Now that code I would like to write here and we can simply do form Daut reset because we have the form

154
00:14:12,690 --> 00:14:19,760
reference that is I want it now you can simply write phone doctress and non-arising on this

155
00:14:23,010 --> 00:14:23,760
submit.

156
00:14:23,830 --> 00:14:25,950
Go put some date up

157
00:14:31,580 --> 00:14:34,860
submit on it.

158
00:14:34,910 --> 00:14:38,850
Second time also what one was called.

159
00:14:39,070 --> 00:14:43,660
We are resetting the state of the phone so it becomes pristine again.

160
00:14:45,350 --> 00:14:47,820
It's all of it.

161
00:14:48,670 --> 00:14:49,390
So no

162
00:14:53,330 --> 00:14:55,170
this is one thing.

163
00:14:55,370 --> 00:15:00,590
Finally what we want to do is on it.

164
00:15:00,670 --> 00:15:07,610
Actually he said here we should have formed what I said that's missing in that note.

165
00:15:07,860 --> 00:15:15,100
And finally what we want to do is have a replacement of this form reference being passed as paramita.

166
00:15:15,970 --> 00:15:22,740
So in template driven We don't have access to the form model in the Comp..

167
00:15:23,190 --> 00:15:25,490
But how are we getting the border right now.

168
00:15:25,660 --> 00:15:29,890
We are getting it through the paramita and through Parminter we are trying to access the data.

169
00:15:30,360 --> 00:15:34,230
So form has ID its value for my name its value and saw.

170
00:15:34,650 --> 00:15:41,220
But what if I don't get that paramita in such case for good if I do not have the paramita.

171
00:15:41,220 --> 00:15:51,000
What we can do is whatever name we have given for the form we have given some name it the form by that

172
00:15:51,000 --> 00:15:51,800
name.

173
00:15:51,810 --> 00:16:01,360
We can declare a variable in the component class and associate with it of view child decorator util

174
00:16:01,370 --> 00:16:03,390
decorator we have deployed now.

175
00:16:03,770 --> 00:16:13,980
So I'm going to write in the view title that so when this beautiful liquidator and give a name here

176
00:16:14,700 --> 00:16:18,140
there's basically no reference to the form.

177
00:16:18,180 --> 00:16:20,510
So now what is I wanted here.

178
00:16:20,700 --> 00:16:22,550
We don't need this paramita.

179
00:16:22,920 --> 00:16:27,860
So I'll give this one and comment that right on submit

180
00:16:32,280 --> 00:16:32,770
without that

181
00:16:35,980 --> 00:16:37,860
this Lynley means as it does

182
00:16:40,890 --> 00:16:45,160
and I'll do ENP form.

183
00:16:45,240 --> 00:16:52,040
Don't listen to just different options I'm trying to show you.

184
00:16:52,040 --> 00:16:57,800
So now if I go back here I don't need this for we don't need that.

185
00:17:00,380 --> 00:17:04,000
Because by this name something went wrong.

186
00:17:06,450 --> 00:17:13,140
Does not because it's a reference not it's a member or member of this class.

187
00:17:13,450 --> 00:17:16,520
We have to use this dot if we form isn't

188
00:17:20,960 --> 00:17:22,470
on everything works.

189
00:17:31,360 --> 00:17:34,570
But once again it looks like

190
00:17:39,480 --> 00:17:46,730
submit when all the data are too clear including this which is not happening.

191
00:17:53,040 --> 00:17:55,450
But are we able to access the data in the phone.

192
00:17:55,490 --> 00:17:57,240
Let's play that at least.

193
00:17:58,800 --> 00:17:59,780
Got i.

194
00:18:00,000 --> 00:18:00,690
D.

195
00:18:02,640 --> 00:18:02,950
You

196
00:18:08,050 --> 00:18:13,260
this door the following board name Ali

197
00:18:16,120 --> 00:18:21,540
you know before is used tile which is good enough.

198
00:18:25,940 --> 00:18:27,250
OK.

199
00:18:27,670 --> 00:18:34,060
One small team will have to make when we are using empty form like this here.

200
00:18:34,470 --> 00:18:44,180
We love to provide indeed all that extra requirement with.

201
00:18:45,780 --> 00:18:48,540
We have to bind this to in the fall.

202
00:18:48,960 --> 00:18:52,170
No it look just that one more option.

203
00:18:52,230 --> 00:18:54,140
You don't have to use that option.

204
00:18:54,450 --> 00:18:55,290
Still not.

205
00:18:56,590 --> 00:18:58,410
Maybe it is not referenced

206
00:19:07,580 --> 00:19:10,860
that has not gone.

207
00:19:11,100 --> 00:19:13,010
Why did not come

208
00:19:22,360 --> 00:19:24,170
there should be some there.

209
00:19:24,320 --> 00:19:24,970
Yeah.

210
00:19:29,230 --> 00:19:31,150
Let's close on.

211
00:19:31,180 --> 00:19:34,940
Actually I was writing this and then this maybe some with

212
00:19:46,320 --> 00:19:48,860
what is wrong with 12

213
00:19:52,070 --> 00:19:54,820
cannot read the property value of undefined

214
00:20:03,370 --> 00:20:07,360
the form or you don't run

215
00:20:10,030 --> 00:20:11,500
and the form is going to

216
00:20:18,010 --> 00:20:20,780
let's put a break point here and see what is wrong.

217
00:20:29,290 --> 00:20:30,000
Briggs's

218
00:20:32,910 --> 00:20:42,940
and in some form it's ok controls and under control it will have itinerants.

219
00:20:43,540 --> 00:20:49,420
So your small change will have to make dot can stop debugging.

220
00:20:51,020 --> 00:20:58,640
Under him default full control stuff like 20.

221
00:21:01,460 --> 00:21:09,550
Kontos and phone directories looks Londis

222
00:21:13,120 --> 00:21:18,180
you don't have to do all these things I'm just showing you these are all possible alternatives.

223
00:21:18,550 --> 00:21:23,400
Perfect everything is reset all methods here.

224
00:21:23,950 --> 00:21:30,700
And as we start putting data that will start getting initialized.

225
00:21:30,930 --> 00:21:36,840
So you don't really have to do all these things but I wanted to actually show you here bouteille child

226
00:21:36,840 --> 00:21:38,450
significance.

227
00:21:38,520 --> 00:21:46,230
So if you have a template reference provided here for getting a reference to that template in a component

228
00:21:46,320 --> 00:21:48,220
we can make use of the word change.

229
00:21:48,780 --> 00:21:50,960
And the same idea should be used here.

230
00:21:52,490 --> 00:21:53,300
So that's all.

231
00:21:53,300 --> 00:22:01,200
So these are the ways we can reset the form so small correction in the note on this list should be as

232
00:22:01,250 --> 00:22:03,980
talk.

233
00:22:04,080 --> 00:22:09,380
Same here not controls dot.

234
00:22:09,510 --> 00:22:12,000
This should be dark form.

235
00:22:14,720 --> 00:22:16,920
So what is that we have covered in this chapter.

236
00:22:17,060 --> 00:22:20,210
We have seen how we can make use of an ordinary bottle.

237
00:22:20,600 --> 00:22:26,390
And when we click on the ordinary bottle in the hand that we would like to reset the model or resetting

238
00:22:26,390 --> 00:22:31,690
the bar does not reset of who answered the phone explicitly.

239
00:22:31,700 --> 00:22:34,410
We have to write in the phone data set.

240
00:22:34,880 --> 00:22:42,000
This is the case and we have an ordinary bottom but we have a submit button in all types of men.

241
00:22:42,400 --> 00:22:44,940
We are going to have the Submit click execute that

242
00:22:48,250 --> 00:22:54,820
to the submit the conform to the submit method we can pass the form reference that form reference we

243
00:22:54,820 --> 00:22:57,020
can use for extracting the deed.

244
00:23:00,220 --> 00:23:06,840
This is also one way of doing it from the model we get it but instead we can get it from the phone we

245
00:23:06,840 --> 00:23:08,170
reset the modem.

246
00:23:08,250 --> 00:23:15,360
And again we have to explicitly reset the phone and final topic of code is how the child can be used

247
00:23:15,780 --> 00:23:22,800
to get the reference to a all you typed in the form.

248
00:23:23,150 --> 00:23:31,950
But because here we are given in the form we have to use the phone that controls even I figured that

249
00:23:31,950 --> 00:23:34,210
would be bugging me.

250
00:23:34,440 --> 00:23:42,110
And then here we should say that this is important and that is thing in this particular example bouteille

251
00:23:42,120 --> 00:23:48,630
is important how we can have a template reference used in the component plus

252
00:23:53,530 --> 00:23:54,340
that's all.

253
00:23:54,490 --> 00:23:54,960
Thank you.

