function add1(x: number, y: number, z?: number): number {
    if (!z)
        z = 0;
    return x + y + z;
};
function add2(x: number, y: number, z: number = 0): number {
    return x + y + z;
};
alert(add1(10, 20))
alert(add2(10, 20))

function add(x: number, ...numbers: number[]): number {
    for (var i = 0; i < numbers.length; i++)
        x += numbers[i];
    return x;
};
alert(10)
alert(add(10, 20, 30))
alert(add(10, 20, 30, 40))
alert(add(10, 20, 30, 40, 50))

function add(x: string, y: string): string;
function add(x: number, y: number): number; //Function Implementation
function add(x, y): any {
    if (typeof x == "number")
        return x + y;
    else
        return x + " " + y;
};
alert(add(10, 20))
//valid
alert(add("A", "B")) //valid
//alert(add(true, "B")) //invalid