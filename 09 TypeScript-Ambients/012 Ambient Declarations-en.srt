1
00:00:17,240 --> 00:00:20,030
I'm yours all the time.

2
00:00:20,310 --> 00:00:25,680
We have seen how we can make use of typescript called in typescript.

3
00:00:25,870 --> 00:00:32,230
That means we can have some kind of functionality called it in typescript Some classes are done some

4
00:00:32,230 --> 00:00:39,220
interfaces are done some declarations are provided in the form of ENM maybe functions.

5
00:00:39,280 --> 00:00:45,290
All these things are declared in typescript and we are able to use them in typescript script but sometimes

6
00:00:45,290 --> 00:00:52,170
the requirement will be we will have some redeeming functionality in the form of javascript library.

7
00:00:52,380 --> 00:00:59,130
Some days when we are learning that we want to make use of those just by his in typescript for that

8
00:00:59,130 --> 00:01:04,860
is not a complex thing that is quite easy because even teletypes typescript is also going to be compiled

9
00:01:04,860 --> 00:01:06,940
as javascript only.

10
00:01:07,500 --> 00:01:09,730
But what is the requirement now.

11
00:01:09,900 --> 00:01:19,440
Precisely the requirement is when I write a script code in studio and I try to use javascript.

12
00:01:19,470 --> 00:01:22,020
The major problem can be indoors.

13
00:01:22,050 --> 00:01:29,220
There are no data types so the validation of the code will not be the compilation in context of data

14
00:01:29,220 --> 00:01:29,900
types.

15
00:01:29,940 --> 00:01:32,170
You will not get any kind of error.

16
00:01:32,610 --> 00:01:37,830
Also we will not get Intellisense because data types are missing when we put first thought we are not

17
00:01:37,830 --> 00:01:46,600
going to get Intellisense So these things are not my requirement that I have need to use javascript

18
00:01:46,740 --> 00:01:55,430
fine and I only use was a script for him in thanks Chris and in typescript when I'm using it I should

19
00:01:55,430 --> 00:01:58,740
have complete Intellisense benefit.

20
00:01:58,740 --> 00:02:02,690
And also we would like to do some kind of type checking.

21
00:02:02,720 --> 00:02:04,410
How can we achieve this.

22
00:02:04,730 --> 00:02:09,790
Yes the simple way to achieve this is for that javascript which is already there.

23
00:02:10,010 --> 00:02:13,110
We write all I'm being declaration.

24
00:02:13,140 --> 00:02:23,090
Fine we create a file with an extension dot dot because I did the same project so the compiler of typescript

25
00:02:23,450 --> 00:02:29,540
for the pool of dollars grep is going to make use of the declaration file for providing the Intellisense

26
00:02:30,470 --> 00:02:38,660
and also to compile based on the data types give errors when we are adding up string probably into something

27
00:02:38,660 --> 00:02:39,690
which is not a string.

28
00:02:39,690 --> 00:02:47,660
Say for example I'm trying to add two billion times or we are doing incompatible operations.

29
00:02:47,660 --> 00:02:52,790
Those kind of errors we are going to get even for the hours code at the time of compilers.

30
00:02:54,150 --> 00:02:58,020
So that is where we are going to make use of ambiant declaration.

31
00:02:58,020 --> 00:03:05,430
We can tell typescript that the code we are writing to use is existed at some other place using a declared

32
00:03:05,450 --> 00:03:05,830
keyboard.

33
00:03:05,850 --> 00:03:15,000
Yes the content in typescript is the content in dog yes is going to be in the form of declared We are

34
00:03:15,000 --> 00:03:18,600
only going to provide declaration without implementation.

35
00:03:18,600 --> 00:03:23,710
That means there will be a function declaration that it will not be implementation of the function clause

36
00:03:23,730 --> 00:03:30,450
declaration declaring the methodes declaring the data types variables which are there in that but there

37
00:03:30,450 --> 00:03:34,710
will not be implementation on the midterms so it is like prototyping.

38
00:03:34,740 --> 00:03:39,820
We are only prototyping the dollars that an underdog got.

39
00:03:39,850 --> 00:03:42,520
B is by.

40
00:03:42,770 --> 00:03:48,960
Actually we can have got Peter PIAs and then combined into body is fine.

41
00:03:49,430 --> 00:03:54,350
That means we don't need to maintain it simply got better despite what is recommended.

42
00:03:54,350 --> 00:03:59,960
It is recommended to have it not be that he has failed so greatly and the gold which we are writing

43
00:03:59,990 --> 00:04:01,740
in Dotti is fine.

44
00:04:01,790 --> 00:04:08,570
So this file will be compiled to a javascript fight and argue that this will be only used for compiler

45
00:04:09,920 --> 00:04:15,260
so let us see this with a proper example of I open Visual Studio

46
00:04:22,520 --> 00:04:24,470
and create a new project.

47
00:04:28,300 --> 00:04:29,970
Is the publication

48
00:04:32,820 --> 00:04:35,880
in the Malott OK.

49
00:04:37,220 --> 00:04:39,380
Empty the project is good enough

50
00:04:43,640 --> 00:04:45,200
in this project.

51
00:04:45,560 --> 00:04:52,920
I want to first have a javascript file or the script find not only the custom down script.

52
00:04:52,940 --> 00:05:00,700
If I remember the javascript can't find it can be your Angolans always good file can be all of the act

53
00:05:00,710 --> 00:05:09,470
is javascript file so it can be any kind of G20 javascript find is the complete script library so it

54
00:05:09,470 --> 00:05:12,240
can be any kind of existing.

55
00:05:12,280 --> 00:05:17,080
All is fine but to keep it simple and easy to understand.

56
00:05:17,120 --> 00:05:23,210
I'm adding an existing Della's could find that adding a new and simple DOS can be fine.

57
00:05:23,420 --> 00:05:26,410
I'll call it does my library not dis

58
00:05:28,970 --> 00:05:32,190
into my library don't just let me add this youngster.

59
00:05:32,270 --> 00:05:36,580
I mean this really midcourt.

60
00:05:36,700 --> 00:05:37,860
So what is this.

61
00:05:37,930 --> 00:05:40,160
What is an object.

62
00:05:40,160 --> 00:05:49,500
And it has one property called an employee one motorcar let's say Hindol employee is having employees

63
00:05:49,510 --> 00:05:56,410
actually the first a function returns an object with a name and suddenly.

64
00:05:56,520 --> 00:06:03,760
So the employee has got name and salary as do members.

65
00:06:03,950 --> 00:06:09,920
If you're not advanced javascript program what you write is look a little typical because you see function

66
00:06:09,950 --> 00:06:11,550
inside a function.

67
00:06:11,720 --> 00:06:12,700
Don't worry about it.

68
00:06:12,770 --> 00:06:15,180
That's not our purpose here.

69
00:06:15,200 --> 00:06:21,830
We want to have a declaration fine for this particular javascript so that we can use this particular

70
00:06:21,830 --> 00:06:28,440
javascript in invoking the functionality in other document.

71
00:06:28,880 --> 00:06:37,890
So first I'm going to add a typescript file where I want to use my script and here we will also have

72
00:06:38,000 --> 00:06:39,150
it Steimle page

73
00:06:48,870 --> 00:06:55,640
let's call it the pedestrian in the motor as it's Jaman.

74
00:06:55,660 --> 00:07:05,070
Obviously I want to use my library I'm followed by that in mind that in Maine does that is the compiled

75
00:07:05,100 --> 00:07:09,680
what of May not be as I would like to use the functionality from here.

76
00:07:10,090 --> 00:07:21,360
So can I for example in Maine Don't be as I would like to write is it called profile Daut say hello

77
00:07:23,100 --> 00:07:25,980
and give you some name.

78
00:07:26,420 --> 00:07:29,210
Let's say something better.

79
00:07:29,270 --> 00:07:30,110
The project

80
00:07:33,660 --> 00:07:37,450
problem with TS Is it doesn't even see various profiles.

81
00:07:37,590 --> 00:07:39,630
It is not able to identify a profile.

82
00:07:39,940 --> 00:07:40,230
Why.

83
00:07:40,230 --> 00:07:46,650
Because at the time of completion there is converting from B is to does mean that these two men are

84
00:07:46,650 --> 00:07:47,360
teachers.

85
00:07:47,550 --> 00:07:52,920
It is only going to compile may not be as it will not look at my library door just because there is

86
00:07:52,920 --> 00:07:53,510
modeling.

87
00:07:55,150 --> 00:07:57,150
So how are we going to achieve that link.

88
00:07:58,350 --> 00:08:01,620
That is where I'm going into the project now.

89
00:08:04,850 --> 00:08:11,460
A new file which I'll call it as my library got beat.

90
00:08:11,460 --> 00:08:20,070
Got these declarations on that I'm going to include that hobo in blue declarations that make up this.

91
00:08:20,100 --> 00:08:21,510
This is the declaration of

92
00:08:25,400 --> 00:08:37,860
the just fine declared Modu getting them more people to fight in model we have got an interface with

93
00:08:37,860 --> 00:08:45,330
just name and salary and a constructor which can be used for initialising name and salary returns.

94
00:08:45,370 --> 00:08:55,950
I employee that we are actually building an interface main ENP And so it is like this

95
00:08:59,820 --> 00:09:07,490
just one MP and say hello are part of one time whose instances were this profile.

96
00:09:07,850 --> 00:09:09,640
So that is what we are doing here.

97
00:09:09,800 --> 00:09:20,600
That day is amplified but mean which has two members so profile is available which is of type and profile

98
00:09:20,600 --> 00:09:25,360
not mean because it's part of a model you have to qualify by name yet

99
00:09:29,710 --> 00:09:32,700
blah blah blah blah.

100
00:09:32,710 --> 00:09:41,200
So no we don't go back to FORCE to say all this go back to mean that.

101
00:09:41,260 --> 00:09:41,640
Yes.

102
00:09:41,690 --> 00:09:47,300
And you see there is no way that is more because profiling is not automatically considered to be of

103
00:09:47,720 --> 00:09:50,910
type and profile not me.

104
00:09:51,050 --> 00:09:53,900
And in fact there isn't a lot better and very good.

105
00:09:54,200 --> 00:10:00,220
So this is the basic benefit all dog need not be asked by basically ambiant declarations.

106
00:10:00,230 --> 00:10:01,420
We call it.

107
00:10:01,520 --> 00:10:09,560
So I repeat once again because of ambiant declaration we are able to use an already existing javascript

108
00:10:09,650 --> 00:10:18,590
file in tight grip and what is the benefit because of the time because of the declaration don't or Despite

109
00:10:19,070 --> 00:10:24,280
or in typescript is going to compile by checking the types in.

110
00:10:24,720 --> 00:10:28,850
And also we are going to get the intelligence type here.

111
00:10:29,120 --> 00:10:30,700
Yes we should get intelligence

112
00:10:33,220 --> 00:10:35,230
we get that like that.

113
00:10:35,300 --> 00:10:43,730
Yeah you'll have let's say that MP called to him Dog.

114
00:10:43,850 --> 00:10:45,750
MP We can do that.

115
00:10:48,090 --> 00:10:49,000
But I guess

116
00:10:52,690 --> 00:11:04,540
on new Intellisense was not going to give name one Conemaugh some salad like this.

117
00:11:04,560 --> 00:11:07,360
MP got to see your name.

118
00:11:07,480 --> 00:11:09,510
Salary all this we are able to achieve.

119
00:11:10,360 --> 00:11:18,400
So this is precisely the benefit of using declaration time in our times project and very interesting

120
00:11:18,730 --> 00:11:28,160
specially whenever we want to make use of Laertes typescript from books like Dick where the art is.

121
00:11:28,300 --> 00:11:30,870
Nor does the quality is polyanthus.

122
00:11:30,880 --> 00:11:35,070
There are so many books which are existing only in typescript.

123
00:11:35,080 --> 00:11:40,760
There is no they are not written in the so that they are existing only in javascript.

124
00:11:40,760 --> 00:11:43,310
They are not written in typescript in such kind of case.

125
00:11:43,310 --> 00:11:50,210
If you want to write a script fine or follow on you're feeling comfortable writing code in typescript

126
00:11:50,240 --> 00:11:51,840
as you should have known by now.

127
00:11:52,070 --> 00:11:58,020
It is very easy to write code in typescript when compared to writing complicated javascript.

128
00:11:58,600 --> 00:12:01,680
Yes this particular feature is going to be very useful.

129
00:12:01,970 --> 00:12:02,470
Thank you.

