///<reference path="IPoint.ts"/>
var MyExamples;
(function (MyExamples) {
    var Point = /** @class */ (function () {
        function Point(x, y) {
            this.x = x;
            this.y = y;
        }
        Point.prototype.distanceFromOrigin = function () {
            return Math.sqrt((this.x * this.x) + (this.y * this.y));
        };
        return Point;
    }());
    MyExamples.Point = Point;
})(MyExamples || (MyExamples = {}));
