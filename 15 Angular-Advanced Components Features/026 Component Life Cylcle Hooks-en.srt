1
00:00:16,860 --> 00:00:23,940
Advanced competent tools advanced for the reason that we have already will be working with components

2
00:00:23,950 --> 00:00:31,260
in the past and either from the very first example we have seen how incompetent plays a very important

3
00:00:31,260 --> 00:00:35,650
role in building a UI for our Steimle output.

4
00:00:37,110 --> 00:00:43,770
In this particular session I'm going to have a quick revision of water components what is important

5
00:00:43,830 --> 00:00:50,970
we are going to look at is the lifecycle Hook's component lifecycle hooks at how we can have dynamic

6
00:00:50,970 --> 00:00:52,790
components created.

7
00:00:53,340 --> 00:01:02,640
So first let us have a quick revision of what our components see in Angola or for build a component

8
00:01:02,730 --> 00:01:09,350
we write a simple class and the class is then provided with the component decorator.

9
00:01:09,360 --> 00:01:11,180
Basically an attribute.

10
00:01:11,730 --> 00:01:18,930
And of course that attribute is going to have certain important properties and that the all of those

11
00:01:18,930 --> 00:01:28,500
properties that two very important properties which we have seen are the selector and the template template

12
00:01:28,500 --> 00:01:36,610
is going to persuade the hit steamin output by taking the data from the component class properties and

13
00:01:36,930 --> 00:01:42,930
that Stephen Colbert is going to get rendered at a position where the selector tag is mentioned in the

14
00:01:42,990 --> 00:01:46,700
other component or maybe in its tactical Steimle fine.

15
00:01:48,200 --> 00:01:57,620
So overall we have understood that components are the building blocks off and UI in an application component

16
00:01:57,650 --> 00:01:59,260
is a glass.

17
00:01:59,300 --> 00:02:01,900
Basically it is a directive actually.

18
00:02:02,060 --> 00:02:10,490
But that directive is a special kind of directive because it has a UI for it and that you are receiving

19
00:02:10,580 --> 00:02:17,670
is constructed through the complete component must belong to an angle or more do that.

20
00:02:17,770 --> 00:02:20,590
Indeed I'm not broken or script.

21
00:02:20,710 --> 00:02:26,350
And will add more and more about models we are going to discuss sometime later in our course.

22
00:02:26,420 --> 00:02:29,270
So every component must belong to an angle or model.

23
00:02:29,270 --> 00:02:37,290
In other words an angle or model can have multiple components and Howard is mention in the model.

24
00:02:37,300 --> 00:02:43,310
We are going to have a declaration field where we can provide a list of all components in that particular

25
00:02:43,310 --> 00:02:51,760
module only to select that that can be used in the head Steimle templates of various other components.

26
00:02:52,340 --> 00:02:59,300
In addition to the metadata configuration specified where the component decorator components can control

27
00:02:59,300 --> 00:03:06,860
and runtime behavior by implementing various lifecycle books with its lifecycle hooks are actually nothing

28
00:03:06,860 --> 00:03:13,730
but the events which are raised during the lifetime of a component where a developer would have an opportunity

29
00:03:13,730 --> 00:03:20,450
to do some kind of it is no code initialization or the initialization will want to see that little later.

30
00:03:20,460 --> 00:03:27,080
But yes a component we know is made up of Steimle template in which we are going to have intercalated

31
00:03:27,080 --> 00:03:28,630
properties.

32
00:03:28,850 --> 00:03:35,240
One way binding to be binding all that with the help of the component class which is going to have certain

33
00:03:35,450 --> 00:03:41,930
properties third and we have also seen that the component class can have events which are raised for

34
00:03:41,930 --> 00:03:49,580
the parent component and of course the template can make use of the styles from the styles within each

35
00:03:49,580 --> 00:03:50,810
component.

36
00:03:51,270 --> 00:03:53,800
So what is that a component is made up of three things.

37
00:03:53,850 --> 00:03:55,110
One is the import.

38
00:03:55,110 --> 00:03:59,980
On top we are going to import all the required classes.

39
00:04:00,060 --> 00:04:07,830
The most important is the component itself which is in Goulart library under all.

40
00:04:08,270 --> 00:04:14,960
Here we are also going to sometimes do an import of certain services and those services are going to

41
00:04:15,030 --> 00:04:17,520
inject it using the dependency index.

42
00:04:17,570 --> 00:04:20,240
And of course that's a topic to be covered later.

43
00:04:20,510 --> 00:04:27,320
Then we are going to have the component directed basically a component decorated with gunmetal the top

44
00:04:27,740 --> 00:04:35,720
of which the important properties are selector template you ordered studiedly ordered and minimal but

45
00:04:35,720 --> 00:04:37,090
complete list is here.

46
00:04:37,130 --> 00:04:43,930
You can have an emissions teens detection encapsulation in every component export as some of these we

47
00:04:43,930 --> 00:04:45,100
are already covered.

48
00:04:45,350 --> 00:04:48,390
Some of them we are going to cover a little later.

49
00:04:48,800 --> 00:04:53,510
But yes many of these are properties of the component decorator.

50
00:04:53,510 --> 00:04:59,400
We are already covered in the past template template you are old style inthe style you model.

51
00:04:59,510 --> 00:05:04,360
We have seen only some more we are going to cover in the coming sessions.

52
00:05:04,380 --> 00:05:07,660
And finally we are going to have a component class.

53
00:05:07,790 --> 00:05:09,370
What is a component class.

54
00:05:09,620 --> 00:05:14,810
It's the class that is going to define all the properties and methods which are going to be used in

55
00:05:14,810 --> 00:05:17,560
the template for generating the oral history.

56
00:05:17,610 --> 00:05:27,760
Or But as I said in this particular chapter I want to concentrate more on the lifecycle Hooke's component

57
00:05:27,760 --> 00:05:30,640
has a lifecycle managed by Anguilla.

58
00:05:30,700 --> 00:05:34,320
That means every component is created used and displayed.

59
00:05:34,330 --> 00:05:44,480
That's a simple lifecycle so Angola created renders it creates and then dirth children checks.

60
00:05:44,740 --> 00:05:51,540
It's when it's data about properties teemed destroys it before removing it from the door.

61
00:05:51,550 --> 00:05:54,340
All these things have to be taken care of by the angle.

62
00:05:54,850 --> 00:06:02,830
So a lot of what lifecycle hooks that brought visibility into these life movement and ability to act

63
00:06:02,920 --> 00:06:04,120
when they occurred.

64
00:06:04,480 --> 00:06:12,160
So basically it's an opportunity to the developer to embed to write some exec some code when these events

65
00:06:12,220 --> 00:06:13,740
are going to be erased.

66
00:06:14,530 --> 00:06:20,990
Our data has seemed sort of like circular hooks minus the hooks that have specified two component content

67
00:06:21,110 --> 00:06:22,850
and view.

68
00:06:23,510 --> 00:06:28,840
So whereas developers can tap into the movements of the lifecycle hooks to implement one or more lives

69
00:06:28,840 --> 00:06:31,470
they can hook interface in the pool library.

70
00:06:31,490 --> 00:06:35,650
Of course how we are going to implement even the proper example we really don't.

71
00:06:36,020 --> 00:06:42,420
But yes a component is created at that time the constructor is executed followed by that.

72
00:06:42,440 --> 00:06:49,740
These are the different other events which we are we can say are East Indian team just ended on it indeed

73
00:06:49,740 --> 00:06:52,180
do take on under that.

74
00:06:52,190 --> 00:07:01,800
Again the author can turn it off the context of The View in and of the utech indeed aren't destroyed.

75
00:07:02,270 --> 00:07:09,170
So here you have the summary of all of them and d'Antin This is called when an input binding value changes

76
00:07:09,950 --> 00:07:16,200
whenever any kind of binding value changes that and the Indy in this is going to get executed.

77
00:07:16,310 --> 00:07:26,030
So maybe we can use for kind of enabling of dead or disabling of certain elements on a page after the

78
00:07:26,030 --> 00:07:29,230
frost or changes.

79
00:07:29,420 --> 00:07:32,060
And all in it is going to get raised.

80
00:07:32,330 --> 00:07:42,150
So kind of initialises in we can do over here do take after every line of tainted action and so on.

81
00:07:42,170 --> 00:07:44,420
I would like you to read them whenever needed.

82
00:07:44,450 --> 00:07:46,180
Use them appropriately.

83
00:07:46,430 --> 00:07:50,620
But how are we going to handle them to handle them.

84
00:07:50,630 --> 00:07:56,200
We are supposed to do an import of all of these events.

85
00:07:56,200 --> 00:07:58,190
Basically they're not actually classes.

86
00:07:58,490 --> 00:08:00,550
All these classes are under the code

87
00:08:03,160 --> 00:08:04,970
demonstrate these events.

88
00:08:04,970 --> 00:08:08,510
Let me create a copy of my first application.

89
00:08:11,170 --> 00:08:21,380
Let's rename it live side unhooks demo.

90
00:08:22,750 --> 00:08:24,430
I know when Project like

91
00:08:27,580 --> 00:08:34,250
openness sarcy and we are compiling Doherty is the root component.

92
00:08:34,590 --> 00:08:38,060
Well what we will do is this itself we will make changes.

93
00:08:38,400 --> 00:08:41,160
So I would like to have all of these components

94
00:08:44,310 --> 00:08:44,980
to be used

95
00:08:48,800 --> 00:08:57,680
so come on an input we are not interested on in and onto and just on display Boucek all the events up

96
00:08:58,070 --> 00:09:04,670
here these are basically different lifecycle Hoag's and then the next thing we will do is

97
00:09:07,830 --> 00:09:14,610
just some text that is fine not to handle these events we have to implement them in the component class

98
00:09:15,860 --> 00:09:20,980
so we have to write implements and all these things have to be part of that.

99
00:09:21,190 --> 00:09:26,110
Now when we say implement pretty obvious we have to implement the corresponding methods also.

100
00:09:27,510 --> 00:09:30,470
So each one of them has corresponding method.

101
00:09:30,600 --> 00:09:38,480
All we have to implement so implement aren't in it like that.

102
00:09:39,640 --> 00:09:41,500
For each one of them we have to implement.

103
00:09:41,500 --> 00:09:46,050
So what I do is I copy from here only.

104
00:09:46,360 --> 00:09:47,620
All these attacks

105
00:09:55,750 --> 00:09:58,040
that isn't.

106
00:09:58,150 --> 00:10:08,120
So now the component class has implemented on it so on and on and on destroy.

107
00:10:08,260 --> 00:10:15,480
So indeed on this on destroying with it like that on gender's indeed on tenderers buddhic indeed to

108
00:10:15,480 --> 00:10:22,630
check for each one of them I'm putting up console when I think some to console.

109
00:10:22,730 --> 00:10:29,250
And now if I run this in the browser we are going to see the complete opposite in the debug window.

110
00:10:29,620 --> 00:10:34,850
So let's press control fight and execute this solution is not saved.

111
00:10:34,890 --> 00:10:35,700
OK.

112
00:10:37,100 --> 00:10:38,260
So right.

113
00:10:40,390 --> 00:10:41,420
That's fine.

114
00:10:43,420 --> 00:10:44,650
See if the solution

115
00:11:03,820 --> 00:11:17,660
life cycle hooks them up here is go let us go to the debugger to in Konsole we should see lot of open

116
00:11:21,260 --> 00:11:23,600
press and all that is inaudible.

117
00:11:23,800 --> 00:11:29,140
But here is the ending on it and we do take all the events got at least

118
00:11:37,240 --> 00:11:44,170
a very important thing you have to check it is indeed Boucek indeed often contented and indeed often

119
00:11:44,260 --> 00:11:50,350
you take these three events are going to get fired every time any kind of binding changes happen.

120
00:11:50,840 --> 00:11:54,950
So let me demonstrate this with a small change to the template.

121
00:11:55,070 --> 00:12:06,870
So yes I'm going to reform back some name doesn't matter inside the phone tag let us put in that type

122
00:12:07,250 --> 00:12:18,610
of text name that's mandatory some day one type Dick's name and then we will do indeed mortal binding

123
00:12:25,400 --> 00:12:33,010
is equal to bind it to the name property and finally close the phone back.

124
00:12:33,120 --> 00:12:39,980
But when we are using for and indeed more than the requirement is in model we have to actually import

125
00:12:40,050 --> 00:12:41,610
anymore.

126
00:12:42,260 --> 00:12:44,810
That means you have to import it form.

127
00:12:45,940 --> 00:12:49,770
But release forms forms and we're not caught.

128
00:12:49,880 --> 00:12:52,820
No not the forms.

129
00:12:52,840 --> 00:13:06,560
So you write your arms more do it from the read angle it slides false

130
00:13:13,640 --> 00:13:16,430
in.

131
00:13:16,710 --> 00:13:18,760
So now it's all set.

132
00:13:18,780 --> 00:13:19,920
Not under application

133
00:13:22,540 --> 00:13:23,860
one more time is open.

134
00:13:23,930 --> 00:13:32,070
And we got Hey look here are important thing is to see that even as C do they do of the content.

135
00:13:32,110 --> 00:13:36,010
Right now there's two sets right.

136
00:13:36,040 --> 00:13:39,950
So now I'm going to hop one more set here.

137
00:13:40,150 --> 00:13:45,760
The moment I modify this I give it a B C D.

138
00:13:45,760 --> 00:13:50,370
Every time I'm making changes these even are getting raised.

139
00:13:50,750 --> 00:13:59,000
So as the binding in this brilliant form as the binding is tending that means the data in any of the

140
00:13:59,000 --> 00:14:01,790
control binded is tending.

141
00:14:02,000 --> 00:14:11,080
We will have this after everything did action of that every take of component content and of that every

142
00:14:11,080 --> 00:14:14,480
set of complex deals.

143
00:14:14,540 --> 00:14:19,330
So every time it is perform these matters are going to execute and can be useful.

144
00:14:19,370 --> 00:14:22,430
Of course we are going to use them in subsequent topics right now.

145
00:14:22,460 --> 00:14:28,020
In this particular session I just want you to understand how we can provide the lifecycle hooks for

146
00:14:28,020 --> 00:14:36,320
that particular component by implementing the corresponding interfaces and the rest do matters.

147
00:14:36,680 --> 00:14:37,730
That's all in this.

148
00:14:37,880 --> 00:14:38,390
Thank you.

