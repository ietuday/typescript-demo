import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding-demo',
  templateUrl: './data-binding-demo.component.html',
  styleUrls: ['./data-binding-demo.component.scss']
})
export class DataBindingDemoComponent implements OnInit {
  fName:string = "Udayaditya";
  lName:string = "Singh";
  personName:string = "Udayaditya Singh";
  isModified: boolean = false;
  isHidden: boolean = false;
  evilTitle:string = 'Template <script>alert("evil never sleeps")</script> Syntax';
  eventType:string = "";
  keys:string = "";
  value:string = "";


  constructor() { }

  ngOnInit() {

  }

  onClick(){
    console.log("Inside Onclick");
    
  }

  onMouse(event:any){
    this.eventType = event.type;
    if(event.type == "mouseover"){
      
      event.target.src = '../../assets/2.svg';
    }else{
      event.target.src = '../../assets/1.jpg';
    }
  }

  onKey(event:any){
    console.log(event.key);
    this.keys += event.key;
    this.value = event.target.value;
  }

  onKey1(value:string){
    this.value = value;
    console.log("Value",this.value);
    
  }

}
