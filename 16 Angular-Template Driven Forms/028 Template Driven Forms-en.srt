1
00:00:16,790 --> 00:00:24,800
Template driven forms we all know pretty well that every page will beat when we want to deal with any

2
00:00:24,800 --> 00:00:32,520
kind of dynamic input we are going to have a similar form created Steimle form which can contain input

3
00:00:32,540 --> 00:00:36,420
type text Pasquotank Boggs you have done.

4
00:00:36,530 --> 00:00:40,300
But we said inmates and so on.

5
00:00:40,310 --> 00:00:46,510
So in this particular chapter we're going to consider a form as the base topic.

6
00:00:46,630 --> 00:00:48,590
Big lot of things on it.

7
00:00:49,500 --> 00:00:56,070
We're going to build a component which is going to represent the data for the form elements we're going

8
00:00:56,070 --> 00:01:04,660
to see how we can perform validations show the error messages show and hide messages using C is this.

9
00:01:04,890 --> 00:01:08,460
Also take care of the submit button and reset button.

10
00:01:08,880 --> 00:01:12,370
But first thing first it's the human fall.

11
00:01:12,480 --> 00:01:19,650
So you think it's time for we can build a simple Steimle file but it's Steimle 5 has extended that Steimle

12
00:01:19,650 --> 00:01:27,270
for phone features by providing additional attributes for different input elements like required Marineland

13
00:01:27,480 --> 00:01:33,750
Max lint and so on and these attributes are really very useful for doing validation.

14
00:01:33,750 --> 00:01:41,540
Also something which we all pretty well know is every fall does similar task across all application.

15
00:01:41,560 --> 00:01:46,330
These are the common things which we are going to do with the farm maintain the state of the data.

16
00:01:46,490 --> 00:01:51,810
I mean instead of the application in the fall we are going to track which part of the form is valid

17
00:01:51,840 --> 00:01:53,570
and which is invalid.

18
00:01:53,570 --> 00:01:58,600
We are going to display the appropriate error messages on how to make the phone valid.

19
00:01:58,890 --> 00:02:05,620
And of course most important handlers submit and reset buttons coming down to Angola.

20
00:02:05,820 --> 00:02:13,330
We should clearly understand that there are literally different strategies to handle the forms.

21
00:02:13,350 --> 00:02:20,030
One is template driven form which is very similar to that of how we deal with Anglo-Indians.

22
00:02:20,180 --> 00:02:27,660
One one forms hope that forms we deal with in Angola or this one the same thing we are going to do in

23
00:02:27,660 --> 00:02:33,730
template driven forms and the other it is completely new feature is a mortal driven form.

24
00:02:33,810 --> 00:02:36,780
We also refer it as reactive forms.

25
00:02:36,780 --> 00:02:39,760
We are going to go out reactive forms in the next chapter.

26
00:02:39,930 --> 00:02:43,810
But right now we are going to work with major league template driven.

27
00:02:44,180 --> 00:02:51,060
And this whole topic I would like to explain to us what to do we are going to construct a basic model

28
00:02:51,060 --> 00:02:57,200
class we are going to have a component created on top of it within the company it will provide a template

29
00:02:57,200 --> 00:03:04,560
a template and then in the fall we are going to have elements which we are going to do today by ending

30
00:03:04,920 --> 00:03:06,870
with the form elements.

31
00:03:07,080 --> 00:03:11,310
We are then providing services for the visual feedback.

32
00:03:11,490 --> 00:03:18,150
Handle error messages so unhide the messages submit the up front and what should happen when the form

33
00:03:18,150 --> 00:03:24,480
is submitted or the form should get reset carhop the phone details will be handled and eventually how

34
00:03:24,540 --> 00:03:26,790
we should take it off resetting of the phone.

35
00:03:27,030 --> 00:03:31,220
All these things we are going to cover one by one of course to begin with.

36
00:03:31,350 --> 00:03:33,870
I've created the empty project.

37
00:03:33,900 --> 00:03:42,510
I've taken the basic template named it as template driven forms and I would like to now add my own custom

38
00:03:42,510 --> 00:03:43,810
content.

39
00:03:43,860 --> 00:03:45,920
This is the seed project on top of it.

40
00:03:45,930 --> 00:03:51,170
We want to add now some types could fight that to begin with and stay employed.

41
00:03:51,220 --> 00:03:58,830
This is what I would like to add here and it's a simple class employed Altius it does for them but I'd

42
00:03:58,830 --> 00:04:04,470
enim salary department and all the forum members are trying to initialize to construct.

43
00:04:04,780 --> 00:04:07,670
How far this come from this mortal clutz.

44
00:04:07,850 --> 00:04:18,410
I would like to build a component and name that as employed dot com and dot the Is and what should be

45
00:04:18,410 --> 00:04:19,360
that in this.

46
00:04:19,510 --> 00:04:19,790
Yes.

47
00:04:19,790 --> 00:04:31,630
On top we should force the import component and various components in Adelaide and galop slice core.

48
00:04:31,730 --> 00:04:36,480
I would like to also import the Employee class which I'm going to use some of it in the.

49
00:04:37,220 --> 00:04:45,160
And that is in Dotts last employee then we are supposed to provide a decorator component.

50
00:04:46,120 --> 00:04:47,300
Inside the conference.

51
00:04:47,440 --> 00:04:51,470
They don't we will have the component properties.

52
00:04:51,470 --> 00:05:01,380
The first one will be legacy selector and yet the sector it does employee for simple followed by that

53
00:05:01,980 --> 00:05:04,040
I should have a template.

54
00:05:04,070 --> 00:05:11,100
What I do is pray for something like a template so that the complete form can be put in a separate file

55
00:05:11,640 --> 00:05:20,640
so I'll have employed dot com form and not hit steamin and then followed by this we are going to provide

56
00:05:20,700 --> 00:05:32,590
a class that is employee component employee component which will provide the exact properties so that

57
00:05:32,650 --> 00:05:35,040
the template form can be created.

58
00:05:35,080 --> 00:05:40,870
But before I go to the properties I would like to create it the amount paid by name employed or component

59
00:05:41,050 --> 00:05:42,020
esteeming.

60
00:05:42,430 --> 00:05:49,880
So right here are a DML document.

61
00:05:50,090 --> 00:05:56,200
And here we don't want all this because all that is already there in the index not in Steimle.

62
00:05:56,430 --> 00:06:01,470
We should simply add a form to this form that

63
00:06:04,530 --> 00:06:15,460
say we just give some container see basically the bootstrap container or provide some hitting on top

64
00:06:19,070 --> 00:06:19,910
employee

65
00:06:23,540 --> 00:06:32,420
for and then let us create a complete fall of form should have some name.

66
00:06:33,130 --> 00:06:34,750
Let's call it employee

67
00:06:37,880 --> 00:06:47,410
and within the form I would like to know how group basically I'm using the bootstrap div class form

68
00:06:48,280 --> 00:06:49,320
front group

69
00:06:52,420 --> 00:06:54,220
and within the group force.

70
00:06:54,220 --> 00:06:57,810
Let's take a label for.

71
00:06:58,090 --> 00:07:02,910
I'm going to have the name ID and all that.

72
00:07:03,250 --> 00:07:19,110
So first is form is for the next caller does ID text do like the and then immediately after that I'll

73
00:07:19,110 --> 00:07:21,250
give input.

74
00:07:24,470 --> 00:07:28,730
Text begin give class which is form control

75
00:07:32,430 --> 00:07:36,490
and Nene does you does the

76
00:07:39,310 --> 00:07:42,030
value we want to do binding.

77
00:07:42,160 --> 00:07:50,820
But before that let's just create ABC for this I should repeat this is for Neme

78
00:07:54,530 --> 00:07:59,290
this name also the college guys name followed by that salary

79
00:08:07,930 --> 00:08:14,710
and then we have got very important thing there is department in our department we would like to have

80
00:08:14,710 --> 00:08:23,690
it as a drop down list select is what we want to basically have yet in terms of so I'll give you a select

81
00:08:25,700 --> 00:08:27,850
select will have options and will have to fill in.

82
00:08:27,860 --> 00:08:33,950
We'll see what can be done right now I'm not doing any kind of binding of course for well-tended to

83
00:08:34,340 --> 00:08:39,470
salary urethral teams to name a name.

84
00:08:39,480 --> 00:08:47,480
I'll give that also as I did because for tag books based on ID not based on name

85
00:08:55,300 --> 00:09:07,150
let's say department.

86
00:09:07,280 --> 00:09:16,040
So we'll follow the theme can naming convention.

87
00:09:16,260 --> 00:09:22,120
Now it is a simple fall which we are going to get when we done this.

88
00:09:22,240 --> 00:09:24,280
How do we get this.

89
00:09:24,360 --> 00:09:30,580
What I do now is in my employ rule component I putting up

90
00:09:37,660 --> 00:09:39,190
that's it.

91
00:09:39,210 --> 00:09:41,780
So the whole thing is nothing but a form only.

92
00:09:41,940 --> 00:09:43,120
Let's run this now.

93
00:09:44,810 --> 00:09:48,390
And we should get in default which is doing right now nothing.

94
00:09:48,450 --> 00:09:50,450
Of course we added a new component.

95
00:09:50,700 --> 00:09:52,860
This new component to be reflected here.

96
00:09:53,130 --> 00:09:56,400
It will not work right not under declarations.

97
00:09:56,400 --> 00:10:04,680
I should give him the name of the new company that employed component and employee component is present

98
00:10:04,740 --> 00:10:05,190
in

99
00:10:09,730 --> 00:10:14,130
from door Sly's employee component

100
00:10:17,710 --> 00:10:21,630
all flight no one on this

101
00:10:26,000 --> 00:10:27,400
something should have gone wrong.

102
00:10:27,430 --> 00:10:34,720
Let's see as if to have fail to employ or Coquelin got it's the money of course like another part which

103
00:10:34,730 --> 00:10:39,840
I've given here is with respect to the current directory.

104
00:10:40,210 --> 00:10:47,070
So whenever we are giving respect to the current directory Yes we should add more modeling and let's

105
00:10:47,080 --> 00:10:49,180
say that is Mordialloc.

106
00:10:49,960 --> 00:10:58,450
So now it will look when the part here is relative to current We are all fantastic.

107
00:10:58,510 --> 00:11:00,180
So not this has to be binded.

108
00:11:00,190 --> 00:11:02,090
We have not binded this right now.

109
00:11:02,290 --> 00:11:03,210
We should binded

110
00:11:05,840 --> 00:11:17,300
one more thing which is problem of the concern is why am I not getting a proper see as we are not getting.

111
00:11:17,300 --> 00:11:26,130
When I say we not getting bootstrap code source OK the bootstraps this file is not included in the index

112
00:11:26,130 --> 00:11:28,300
but it's similar to that.

113
00:11:28,310 --> 00:11:32,470
So I would look in index started steamin on an index lotted.

114
00:11:32,580 --> 00:11:44,630
I would like to bootstrap dot see as we don't have it here so I'll copy this bootstrap content bootstrap

115
00:11:44,630 --> 00:11:46,020
Ghodsee is this.

116
00:11:46,260 --> 00:11:52,180
Let me copy this in the root directory and from here I'll drag dragon.

117
00:11:54,800 --> 00:12:00,250
Looks around once again and we should get the proper States ablate bootstrapped styles applied

118
00:12:04,100 --> 00:12:05,440
will still not applied.

119
00:12:06,410 --> 00:12:16,780
Look at the source sticking from the gas control five Yup we have it.

120
00:12:16,900 --> 00:12:19,220
It's Timofei was coming from the gas.

121
00:12:19,390 --> 00:12:23,960
So now we have the old bootstraps to see some serious he is applied.

122
00:12:24,520 --> 00:12:31,140
So now the next thing I would like to do is that there is still concerned with drop though.

123
00:12:31,150 --> 00:12:33,850
Same thing we do with the drop down also

124
00:12:37,770 --> 00:12:47,440
that these glass fall can go and come can qualify.

125
00:12:49,420 --> 00:12:58,000
Just as control of yes even drop down as well.

126
00:12:58,780 --> 00:12:59,290
All good.

127
00:12:59,290 --> 00:13:03,540
Let's close now let's start working with the binding aspect.

128
00:13:03,640 --> 00:13:07,110
So for this what we want to do is indirect component.

129
00:13:07,110 --> 00:13:12,880
We would like to instantiate the Employee class object.

130
00:13:13,540 --> 00:13:15,080
So I have employee

131
00:13:18,690 --> 00:13:28,590
eum be employed and we get the employee object here onto the constructor we can pass some ID some name

132
00:13:28,640 --> 00:13:32,750
I give my name some salary

133
00:13:36,710 --> 00:13:42,260
department right now I'll just give Department us training

134
00:13:45,860 --> 00:13:51,560
but we would like department to be a drop down list and if it has to be a dropdown list obviously it

135
00:13:51,560 --> 00:13:54,720
should be coming from an area of elements.

136
00:13:55,030 --> 00:13:58,260
But how are we going to provide that at a here.

137
00:13:58,730 --> 00:14:08,730
So let's call it departments is equal to I would like to have sales department Loveman department

138
00:14:11,160 --> 00:14:16,670
you'd like to have training department maybe or Cohn's department.

139
00:14:18,790 --> 00:14:23,760
Maybe our department human resources and so.

140
00:14:24,560 --> 00:14:29,280
So this department is what I would like to buy in my dropdown list.

141
00:14:29,840 --> 00:14:35,290
No because we have got this as the employee property here.

142
00:14:35,470 --> 00:14:42,490
How can the binded to the template Biegel for two rebinding simple to a binding is what we can make

143
00:14:42,490 --> 00:14:47,230
use of or here how we have value right now.

144
00:14:47,230 --> 00:14:51,680
Let's first begin with one rebinding and eventually you'll agree to do binding.

145
00:14:51,910 --> 00:14:54,250
So you'll get Impey not ID

146
00:14:56,980 --> 00:15:04,240
is upper case or lower case letters conform Lorqess only everything is Lorqess like that.

147
00:15:04,240 --> 00:15:14,770
This also in square bracket this also in square bracket you impede our name and our salary.

148
00:15:14,770 --> 00:15:19,970
Now comes the select select We have to provide is a drop down list.

149
00:15:20,110 --> 00:15:25,180
That means we have to make use of in default for the option tax.

150
00:15:25,180 --> 00:15:30,340
So this is the which I'm going to set the value here also

151
00:15:34,640 --> 00:15:35,560
value.

152
00:15:35,960 --> 00:15:42,140
Let's say this is binded to ENP dot Department.

153
00:15:42,610 --> 00:15:57,360
But here are other departments I'll give your obsolete stock in the full is equal do let department

154
00:15:57,450 --> 00:15:58,390
off.

155
00:15:58,600 --> 00:16:03,590
We have a property called deps in the company and that department is property.

156
00:16:03,880 --> 00:16:10,520
So one department at a time we are going to get that are blinded to the value property which is deep.

157
00:16:10,900 --> 00:16:16,670
And of course I want to display the text of the option up into police here.

158
00:16:16,720 --> 00:16:25,760
We just deal with the so called on is going to meet all the departments and it is binding to the current

159
00:16:25,760 --> 00:16:27,160
employee department.

160
00:16:28,400 --> 00:16:31,300
Option tax will be repeated because of the.

161
00:16:31,650 --> 00:16:37,080
Let's note on this are we going to get the default de-funded up from the employee.

162
00:16:39,010 --> 00:16:44,770
Both training has gone by default 40 one likes on the Tony and the.

163
00:16:44,840 --> 00:16:46,680
All of it.

164
00:16:46,760 --> 00:16:52,310
Now I would like to display here the current state of the employee how to get the current state of the

165
00:16:52,310 --> 00:17:01,430
employee forced an employee class employee component plus I would have one more property that said diagnostics

166
00:17:02,770 --> 00:17:11,020
and we'll call this good property get get diagnostics but if it is get we are supposed to provide a

167
00:17:11,020 --> 00:17:13,320
function return and some answer.

168
00:17:13,360 --> 00:17:21,960
What is that we ought to all this dot in but we will convert this to 10 to see that there's an uptick

169
00:17:22,360 --> 00:17:28,820
and that I would like to convert the string for that we have a relevant function called Diesendorf spring.

170
00:17:31,520 --> 00:17:36,000
So just an object which is here converted not to string

171
00:17:39,040 --> 00:17:45,530
get properties or bracket is required because it is a property not a field.

172
00:17:45,630 --> 00:17:49,440
What does diagnostics I would like to put it enough for.

173
00:17:49,870 --> 00:17:54,990
So are the form steamin fine.

174
00:17:56,860 --> 00:18:00,780
When it's up unlearnt say

175
00:18:05,590 --> 00:18:12,950
employee and give your diagnostics the property you don't have to completely committed because they

176
00:18:12,980 --> 00:18:14,790
do the property just don't it.

177
00:18:14,790 --> 00:18:15,690
It is good enough.

178
00:18:16,730 --> 00:18:25,380
Diagnostic the diagnostic or diagnostics agnostic that's called a diagnostically.

179
00:18:25,380 --> 00:18:26,590
Not on this.

180
00:18:26,590 --> 00:18:29,640
So down to the current state of the form that's displayed here.

181
00:18:29,820 --> 00:18:30,620
Perfect.

182
00:18:32,030 --> 00:18:38,580
But not if anti-India let's say from 1 to 1 to 3 it's not ending.

183
00:18:39,110 --> 00:18:40,830
It is not going to change here.

184
00:18:41,190 --> 00:18:44,900
Why it is not ending because it is one way binding.

185
00:18:44,900 --> 00:18:51,260
So we have to convert Nordman Fano to rebinding and we have learned our year in binding typed up for

186
00:18:51,270 --> 00:19:00,200
two rebinding we are supposed to make use of trough value we are supposed to change that to indeed mortal.

187
00:19:00,480 --> 00:19:03,240
And we are also pro-white but on brackets do.

188
00:19:03,270 --> 00:19:09,810
I think we want to so this and use every word in place of value.

189
00:19:16,390 --> 00:19:18,370
By changing this what is the benefit.

190
00:19:18,460 --> 00:19:19,880
Let's look at it now.

191
00:19:21,910 --> 00:19:24,160
That what it does if I didn't.

192
00:19:24,600 --> 00:19:26,230
The value of a property anyway.

193
00:19:26,250 --> 00:19:27,540
Something went wrong.

194
00:19:30,720 --> 00:19:38,550
Now when we are using in the model there is a constraint we have to include now.

195
00:19:39,140 --> 00:19:43,920
So forms a model has to include the come up forms model.

196
00:19:44,280 --> 00:19:46,430
And here I should have forms model

197
00:19:52,260 --> 00:19:54,060
and various forms Mordieu.

198
00:19:54,240 --> 00:19:58,610
It's in the red and glassless for

199
00:20:05,910 --> 00:20:06,580
forms

200
00:20:10,920 --> 00:20:11,570
no.

201
00:20:11,660 --> 00:20:15,460
1 Once again it should work when we are in India Wadham.

202
00:20:15,560 --> 00:20:17,800
It's very important that we go for it.

203
00:20:17,830 --> 00:20:19,010
So not one two three.

204
00:20:19,120 --> 00:20:34,000
It's tending your good good development that's not changing that's not binding at goals not changing.

205
00:20:34,270 --> 00:20:41,770
So what is wrong in this indie model is equal to old or department good enough.

206
00:20:43,250 --> 00:20:43,610
Ryan

207
00:20:47,090 --> 00:20:52,810
problem is name is not given a name is required not to be compulsory you should give name especially

208
00:20:52,820 --> 00:20:57,260
when you're going to be writing.

209
00:20:57,350 --> 00:20:58,910
I'm sure at this time it would work.

210
00:21:02,320 --> 00:21:09,430
Perfect Teens to sales Silsila a close second year and the value is terrific.

211
00:21:09,610 --> 00:21:11,730
So no it doesn't come to rebinding.

212
00:21:11,800 --> 00:21:13,760
So this is this type of form.

213
00:21:13,790 --> 00:21:20,440
It's simple form like this is an effort at template driven for what is that we have done.

214
00:21:20,440 --> 00:21:25,390
I just repeat quickly once again of course we are going to extend this in the next session.

215
00:21:25,840 --> 00:21:34,510
I did Employee class I'm going to treat this as my middle class to handle this moral class basically

216
00:21:34,510 --> 00:21:36,610
to perform operations.

217
00:21:36,880 --> 00:21:39,460
I've created and Steimle document.

218
00:21:39,460 --> 00:21:47,540
And in that statement we have got a form on form that has all this bind that we have or we are used

219
00:21:47,540 --> 00:21:50,360
to value but value was for one rebinding.

220
00:21:50,410 --> 00:21:52,020
Replace that with indeed more.

221
00:21:52,090 --> 00:21:55,000
And that is far too rebinding very important thing.

222
00:21:55,000 --> 00:22:00,960
Whenever we are using the model compulsory we should provide name attribute for the fall form element

223
00:22:01,740 --> 00:22:03,220
to.

224
00:22:03,550 --> 00:22:10,330
We have also written a component in the component we have got area of departments which we are going

225
00:22:10,330 --> 00:22:16,090
to use it for the drop down of department and we are creating employee object we can call this as our

226
00:22:16,090 --> 00:22:17,340
model it up.

227
00:22:17,500 --> 00:22:27,270
And also we are if I think the Employee object is in spring so that we can display in the Steimle What

228
00:22:27,350 --> 00:22:28,360
is the current state.

229
00:22:28,400 --> 00:22:32,350
And this is needed for knowing that the farm state is attending.

230
00:22:32,880 --> 00:22:34,200
A very basic thing is not.

231
00:22:34,250 --> 00:22:40,480
And of course for model has to be included and our custom component also has to be included for form

232
00:22:40,500 --> 00:22:42,820
is a model hence it should be imported.

233
00:22:43,010 --> 00:22:48,310
But as our uncloak component is just a component it's not a module.

234
00:22:48,430 --> 00:22:53,480
So we extend this same thing for that in the next season where we are going to see how validations can

235
00:22:53,480 --> 00:22:54,210
be pulled.

236
00:22:54,460 --> 00:22:55,230
That's all in this.

237
00:22:55,340 --> 00:22:55,970
Thank you.

