"use strict";
exports.__esModule = true;
var Point = /** @class */ (function () {
    function Point() {
    }
    Point.prototype.distanceFromOrigin = function () {
        return Math.sqrt((this.x * this.x) + (this.y * this.y));
    };
    return Point;
}());
exports["default"] = Point;
