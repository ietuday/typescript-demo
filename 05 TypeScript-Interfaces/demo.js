var pt1 = {
    X: 10,
    Y: 20,
    DistanceFromOrigin: function () {
        return Math.sqrt((this.X * this.X) + (this.Y * this.Y));
    },
    DistanceFromPoint: function (point) {
        return Math.sqrt(((point.X - this.X) * (point.X - this.X)) + ((point.Y - this.Y) * (point.Y - this.Y)));
    }
};
var pt2 = {
    X: 100,
    Y: 200,
    DistanceFromOrigin: function () {
        return Math.sqrt((this.X * this.X) + (this.Y * this.Y));
    },
    DistanceFromPoint: function (point) {
        return Math.sqrt(((point.X - this.X) * (point.X - this.X)) + ((point.Y - this.Y) * (point.Y - this.Y)));
    }
};
var Point = /** @class */ (function () {
    function Point(x, y) {
        this.X = x;
        this.Y = y;
    }
    Point.prototype.DistanceFromOrigin = function () {
        return Math.sqrt((this.X * this.X) + (this.Y * this.Y));
    };
    ;
    Point.prototype.DistanceFromPoint = function (point) {
        return Math.sqrt(((point.X - this.X) * (point.X - this.X)) + ((point.Y - this.Y) * (point.Y - this.Y)));
    };
    return Point;
}());
pt1 = new Point(10, 20);
pt2 = new Point(100, 200);
alert(pt1.DistanceFromOrigin());
alert(pt1.DistanceFromPoint(pt2));
var add = function (n1, n2) {
    return n1 + n2;
};
var sub = function (n1, n2) {
    return n1 - n2;
};
var Demo = /** @class */ (function () {
    function Demo() {
    }
    Demo.CalculateAndPrint = function (m) {
        alert(m(10, 20));
    };
    return Demo;
}());
var points;
points = [{ x: 10, y: 15 }, { x: 10, y: 15 }];
alert(points.length + " " + points["length"]);
alert(points.toString());
