1
00:00:16,890 --> 00:00:26,400
Style binding in a component display is a very important aspect of every development and the beautification

2
00:00:26,400 --> 00:00:30,950
aspect beautification of the painting is are done with the help of.

3
00:00:31,000 --> 00:00:36,490
See is this and we know that this stage can be applied in two ways.

4
00:00:36,490 --> 00:00:43,920
One way that the play the style attribute to attack or within a class and we apply the class.

5
00:00:43,930 --> 00:00:50,250
So in this particular session I'm going to discuss how the styles can be applied to all to talk.

6
00:00:50,460 --> 00:00:53,170
What are the bindings available in Angola.

7
00:00:53,180 --> 00:00:56,200
First time at last.

8
00:00:56,220 --> 00:01:01,610
So first and most important thing Angra applications are started with a regular season.

9
00:01:01,630 --> 00:01:04,810
That means state classes are not different.

10
00:01:04,930 --> 00:01:08,840
Those times you have to be in the same state.

11
00:01:08,840 --> 00:01:12,980
We're going to make use of it and also the rules are the same.

12
00:01:13,060 --> 00:01:16,150
The selectors are same in case you are doing media queries.

13
00:01:16,150 --> 00:01:21,590
Those are the same if you are using bootstrap those classes are bootstrap can be used here.

14
00:01:21,820 --> 00:01:23,230
It's all up to you.

15
00:01:24,040 --> 00:01:32,200
But how do we apply now we can sit in late stage with style and class with class by two different aspects

16
00:01:32,890 --> 00:01:39,340
stifling syntax resembles property painting the way we bind property exactly like that.

17
00:01:39,340 --> 00:01:44,960
We are going to buy staples instead of an element property between brackets.

18
00:01:45,010 --> 00:01:49,320
We start with the prefix stating we have to use the prefix Stayton

19
00:01:52,420 --> 00:01:57,220
followed by a dot and then the name of the C S is state property.

20
00:01:57,220 --> 00:02:03,820
For example Styne dot style Eiffel property for this type of property will be named with proper name

21
00:02:03,820 --> 00:02:05,610
of this type.

22
00:02:05,640 --> 00:02:08,190
So here we have an example.

23
00:02:08,240 --> 00:02:13,880
So what I do is first create a copy of my exist.

24
00:02:14,150 --> 00:02:20,840
My inforced angle and then move up critical in this.

25
00:02:21,590 --> 00:02:35,690
Let's call it as stylin binding in combo and gabled of all open the wanting.

26
00:02:36,040 --> 00:02:39,490
Before we do anything if the solution also

27
00:02:43,120 --> 00:02:50,630
we'll call it style binding in combination.

28
00:02:50,700 --> 00:02:58,990
So you see now we have the old guard and the first application what we have in a.

29
00:02:59,340 --> 00:03:00,330
All that uncleaned

30
00:03:02,970 --> 00:03:06,040
nothing is to.

31
00:03:06,600 --> 00:03:08,430
So here what I would like to do now is

32
00:03:16,240 --> 00:03:28,040
add some docs lets see the whole movie Sextine we are going to give it time is it will say something

33
00:03:28,050 --> 00:03:29,670
like that.

34
00:03:32,690 --> 00:03:33,350
This is

35
00:03:37,600 --> 00:03:47,410
like that I would do stylistical do something like background galop

36
00:03:50,610 --> 00:03:55,040
background hyphen color color or maybe

37
00:03:58,980 --> 00:04:08,600
this is text with your little background.

38
00:04:08,920 --> 00:04:18,090
Sometimes we will do a combination of the color and red semi-colon background color.

39
00:04:18,130 --> 00:04:25,290
All of this is text with red color as you look back.

40
00:04:26,000 --> 00:04:27,350
Let's see those of this

41
00:04:41,150 --> 00:04:44,690
red color black text.

42
00:04:44,690 --> 00:04:48,510
A little background background with text.

43
00:04:48,680 --> 00:04:56,610
The same thing I would like to do with conditioning based on certain component properties.

44
00:04:56,760 --> 00:05:06,410
So for example I would like to have here one property called that is red.

45
00:05:06,720 --> 00:05:08,580
What shall we do with Israel.

46
00:05:08,770 --> 00:05:13,760
It can be either true or false.

47
00:05:14,110 --> 00:05:21,340
So either red is a property no let's put a lower case here in terms of giving it constant.

48
00:05:21,340 --> 00:05:22,060
Same thing.

49
00:05:22,060 --> 00:05:24,110
I would not like to throw stones.

50
00:05:24,120 --> 00:05:28,420
I mean to binding I would like to achieve the diamond binding.

51
00:05:28,420 --> 00:05:33,110
The difference is here we have to give square bracket and gives time.

52
00:05:33,200 --> 00:05:34,460
Dog.

53
00:05:37,420 --> 00:05:38,610
On this.

54
00:05:38,670 --> 00:05:41,360
We get assigned what value we want.

55
00:05:41,820 --> 00:05:43,650
Or let's call it doesn't text call

56
00:05:46,520 --> 00:05:47,860
and does.

57
00:05:48,150 --> 00:05:49,810
I give it to us.

58
00:05:54,990 --> 00:05:56,540
So what do I get.

59
00:05:58,730 --> 00:06:04,630
One here let's say for example like you've read so read is going to be assigned here and hence it will

60
00:06:04,640 --> 00:06:07,610
be the same thing here.

61
00:06:07,920 --> 00:06:14,430
I will not give Styne daughter back.

62
00:06:14,510 --> 00:06:18,040
Hi hello equal.

63
00:06:18,040 --> 00:06:21,100
Do background color.

64
00:06:24,910 --> 00:06:25,260
What does

65
00:06:30,410 --> 00:06:31,270
another property

66
00:06:35,910 --> 00:06:42,610
maybe.

67
00:06:42,670 --> 00:06:46,180
Same thing here style.

68
00:06:51,040 --> 00:06:51,480
Equals

69
00:06:54,330 --> 00:06:56,250
text color.

70
00:07:00,120 --> 00:07:01,890
Style background color.

71
00:07:06,520 --> 00:07:06,680
Is

72
00:07:13,910 --> 00:07:15,040
background color.

73
00:07:17,060 --> 00:07:19,830
So we are mixing two styles of color.

74
00:07:19,980 --> 00:07:21,550
Style not background color.

75
00:07:25,130 --> 00:07:26,600
Rwandas and you'll see

76
00:07:30,650 --> 00:07:32,170
something that's gone wrong.

77
00:07:32,170 --> 00:07:34,590
I'm not sure what.

78
00:07:34,620 --> 00:07:36,150
Let it run once again.

79
00:07:43,730 --> 00:07:45,300
Background color is not working.

80
00:07:45,430 --> 00:07:49,690
So some spelling mistake may be background color.

81
00:07:51,990 --> 00:07:58,220
Is it on both before and after the top.

82
00:07:58,250 --> 00:07:59,400
We should get the same effect.

83
00:07:59,450 --> 00:08:04,210
Same thing so can we do this conditionally also.

84
00:08:04,570 --> 00:08:12,430
Yes we can always send only text column only bathroom color but then here instead of putting a color

85
00:08:12,430 --> 00:08:19,870
like this we will have to put an expression which will lead to do this or else do this.

86
00:08:19,940 --> 00:08:23,790
You will have to let me this with an example.

87
00:08:24,050 --> 00:08:29,650
Rather take it from the handle as it is and see it all day.

88
00:08:29,660 --> 00:08:40,850
This is it.

89
00:08:41,040 --> 00:08:41,530
This

90
00:08:44,620 --> 00:08:47,990
so we have no something wrong.

91
00:08:48,440 --> 00:08:55,030
We have not two properties is a special can say these two properties.

92
00:08:55,050 --> 00:08:55,830
I have no.

93
00:08:56,150 --> 00:09:08,760
So I have to declare these two properties no easy special let's say like and can see like no and he

94
00:09:08,870 --> 00:09:11,030
just into focus.

95
00:09:11,120 --> 00:09:17,650
So if gansey is full we are going to get Grigolo disable Gollop for the background

96
00:09:21,720 --> 00:09:22,250
gansey

97
00:09:24,890 --> 00:09:34,880
because small is Beseler we are using three if each person is false.

98
00:09:35,640 --> 00:09:43,750
We are using 50 the important thing to note here is we are putting your dot iam and Dot person at it.

99
00:09:43,810 --> 00:09:51,560
So the value which is provided here is three of the value that is provided here is 150 percentage.

100
00:09:51,720 --> 00:10:01,030
Lets take this to be X so falsa is not because we are not going to mention the pixel.

101
00:10:01,030 --> 00:10:02,620
This is bigger than 150 pixel

102
00:10:06,890 --> 00:10:07,390
design.

103
00:10:07,430 --> 00:10:09,400
We have to present that look as desirable.

104
00:10:09,400 --> 00:10:10,020
That said

105
00:10:13,020 --> 00:10:17,190
So this is also a very important thing when we are mentioning those.

106
00:10:17,460 --> 00:10:23,300
If there is a unit of measurement for this that unit should be as and as Dr. addition here.

107
00:10:23,970 --> 00:10:32,420
So what is the syntax style Daut style property and obscenely daughter unit of measurement.

108
00:10:32,490 --> 00:10:38,640
In some cases it may be that some said it may not be.

109
00:10:38,720 --> 00:10:40,280
So this is what we are going to play.

110
00:10:40,280 --> 00:10:46,720
Stein No let us see how we can make use of the glass window we like glass.

111
00:10:47,120 --> 00:10:48,960
What is the glass half full.

112
00:10:49,030 --> 00:10:54,980
I prefer writing that in the head section of the bit where we read where its head.

113
00:10:55,190 --> 00:11:02,340
We know that my app is going to be used in index not its Tiemann so we can actually go in index got

114
00:11:02,480 --> 00:11:13,040
extremal and in a head on the head we can specify here the stone and whatever style we give here we

115
00:11:13,040 --> 00:11:14,860
can make use of that.

116
00:11:15,380 --> 00:11:20,020
So we were not let's say Yellow back yellow border

117
00:11:24,250 --> 00:11:33,930
and we get something like border color solid border maybe two pixels and we would like to have yellow

118
00:11:33,970 --> 00:11:37,150
but more like that.

119
00:11:37,150 --> 00:11:43,660
I would like to have class by name red background Visy background

120
00:11:46,830 --> 00:11:49,090
and give me a background color.

121
00:11:53,400 --> 00:11:59,250
Lets say like some states with some classes is what I wanted.

122
00:11:59,250 --> 00:12:05,710
That said two classes eyes glittered yellow bottom and red back.

123
00:12:06,120 --> 00:12:10,110
You can put more properties if you want.

124
00:12:10,140 --> 00:12:11,800
There is absolutely no problem.

125
00:12:13,390 --> 00:12:16,270
I hope to make use of these classes.

126
00:12:16,300 --> 00:12:17,430
Use these classes.

127
00:12:17,440 --> 00:12:22,160
We will not have to go back to what we want to use it here.

128
00:12:23,180 --> 00:12:25,150
So now I get your heads up

129
00:12:29,780 --> 00:12:36,550
in heads up what do we write Let's say I want to have simple again Dudack only.

130
00:12:36,920 --> 00:12:42,830
But this time the property is supposed to be glass window binding.

131
00:12:43,070 --> 00:12:44,840
We simply like blocks and they will do.

132
00:12:44,850 --> 00:12:58,540
And in the class or let's say a red background and here I'm going to get this is a red background text

133
00:13:01,220 --> 00:13:10,410
and then I'll give you her do I want to the binding How are we going to do my thing for class

134
00:13:13,320 --> 00:13:17,580
is equal to just write the plumbing.

135
00:13:17,580 --> 00:13:20,260
How do I do that.

136
00:13:20,370 --> 00:13:21,360
My last name is

137
00:13:24,700 --> 00:13:30,110
back I don't remember this in single code because that is a value right now.

138
00:13:30,190 --> 00:13:38,700
It's not a medium if you want variable you can actually declare that variable in the next example.

139
00:13:38,780 --> 00:13:44,930
So your this is read back don't.

140
00:13:45,710 --> 00:13:49,420
I'm just trying to show you in a different and different ways.

141
00:13:49,420 --> 00:13:50,710
Now if I to use variable

142
00:13:54,550 --> 00:13:55,050
Semakin

143
00:13:58,140 --> 00:13:59,030
what is on.

144
00:13:59,160 --> 00:14:00,460
I didn't find that.

145
00:14:03,400 --> 00:14:12,460
And maybe the manual was me when read back the class thing.

146
00:14:12,980 --> 00:14:16,600
So it is a class name that isn't available.

147
00:14:16,750 --> 00:14:27,490
Unbuildable we are binding the class one reminding the Likewise what if you want to apply two classes

148
00:14:33,470 --> 00:14:43,620
if you want to apply two classes in his let's say back don't our board do we want again.

149
00:14:44,170 --> 00:14:49,780
So you know we will have background and border as one with the will between two classes a red background

150
00:14:51,070 --> 00:14:53,240
and yellow border.

151
00:14:53,590 --> 00:14:59,490
But what we can make use of and note on this.

152
00:14:59,520 --> 00:15:06,000
You should see all that which we are applying while nothing is a.

153
00:15:08,760 --> 00:15:13,460
Why problem is the head Steimle file is not updated actually.

154
00:15:13,860 --> 00:15:17,210
If you look at the source in the header section we don't have state

155
00:15:20,170 --> 00:15:23,340
there is because the external pages are generally cast.

156
00:15:23,360 --> 00:15:33,570
So what you do press control and F5 to get the latest was when you see we have a button for the Fortenbacher

157
00:15:33,570 --> 00:15:40,470
results with it back only those with for the 4st we have really given back to.

158
00:15:40,560 --> 00:15:44,280
So you can this is the normal routine usage.

159
00:15:44,770 --> 00:15:53,200
But if you're doing binding you have to ensure that this is of value off the blossoming if you want

160
00:15:53,200 --> 00:15:57,910
to bind it to a variable the debatable variable should be declared in the component.

161
00:15:57,940 --> 00:16:02,160
And we should use it for the value of that variable.

162
00:16:03,250 --> 00:16:06,400
That will become the value of the classic.

163
00:16:06,940 --> 00:16:08,680
So this is the basic syntax.

164
00:16:08,680 --> 00:16:12,100
Very close to what we have in Rubinius demon.

165
00:16:12,100 --> 00:16:15,830
We also have the ability to play styles in little more than one street.

166
00:16:15,970 --> 00:16:17,730
And let's see that in the next session.

167
00:16:17,980 --> 00:16:18,430
Thank you.

