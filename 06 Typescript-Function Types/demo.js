function add1(x, y, z) {
    if (!z)
        z = 0;
    return x + y + z;
}
;
function add2(x, y, z) {
    if (z === void 0) { z = 0; }
    return x + y + z;
}
;
alert(add1(10, 20));
alert(add2(10, 20));
function add(x) {
    var numbers = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        numbers[_i - 1] = arguments[_i];
    }
    for (var i = 0; i < numbers.length; i++)
        x += numbers[i];
    return x;
}
;
alert(10);
alert(add(10, 20, 30));
alert(add(10, 20, 30, 40));
alert(add(10, 20, 30, 40, 50));
function add(x, y) {
    if (typeof x == "number")
        return x + y;
    else
        return x + " " + y;
}
;
alert(add(10, 20));
//valid
alert(add("A", "B")); //valid
//alert(add(true, "B")) //invalid
