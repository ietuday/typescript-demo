1
00:00:16,480 --> 00:00:20,110
In the first model we have seen what is angular.

2
00:00:20,310 --> 00:00:21,660
What are the features of it.

3
00:00:21,660 --> 00:00:24,200
What are the advantages of it.

4
00:00:24,360 --> 00:00:31,180
And also we have seen how we can set up the development environment for building an application.

5
00:00:31,380 --> 00:00:37,000
What is the role of the seed project which we need to download from top and in second.

6
00:00:37,050 --> 00:00:44,250
I have explained to you a very high level overview of the various building blocks which are required

7
00:00:44,250 --> 00:00:52,900
for building an angular application or know it is high time we get into more details about understanding

8
00:00:53,140 --> 00:01:00,940
how we can customize the seed project which is provided to us which we have added to the project how

9
00:01:00,940 --> 00:01:04,200
we can add our own custom components.

10
00:01:04,510 --> 00:01:08,980
We can make changes to the templates and so on and on.

11
00:01:08,990 --> 00:01:11,080
So his first and most important thing.

12
00:01:11,090 --> 00:01:13,340
What is a directive.

13
00:01:13,580 --> 00:01:21,100
Directives are classes that can change the behavior or appearance of a component by making use of C

14
00:01:21,150 --> 00:01:22,130
SS classes.

15
00:01:22,150 --> 00:01:29,490
C says styles and events very very important directives are by themselves classes.

16
00:01:29,630 --> 00:01:38,380
We are going to use those classes for changing the behavior of the way the output is going to be rendered.

17
00:01:38,390 --> 00:01:40,260
How is that going to happen.

18
00:01:40,280 --> 00:01:47,870
We may have certain classes we may have certain states CSSA classes since as styles we have directives

19
00:01:47,870 --> 00:01:57,530
like if day for directive and saw directives are further categorized as competent data structure and

20
00:01:57,520 --> 00:02:06,160
directives and attribute directives right now we are more interested in obviously component data and

21
00:02:06,250 --> 00:02:09,730
also we are going to look at the structural directives.

22
00:02:09,910 --> 00:02:13,070
At least some of them.

23
00:02:13,290 --> 00:02:14,430
What is the difference.

24
00:02:14,490 --> 00:02:16,070
Component is the core.

25
00:02:16,380 --> 00:02:20,550
Every module is going to have one component.

26
00:02:21,180 --> 00:02:29,440
The content of the component is going to be presented to the user as a view so gone by itself is one

27
00:02:29,440 --> 00:02:31,400
special type of character.

28
00:02:34,230 --> 00:02:37,200
Then we have got structural data structure.

29
00:02:37,230 --> 00:02:47,300
They are responsible for actually changing the layout of the piece the object model is manipulated to

30
00:02:47,340 --> 00:02:49,260
this structural directive.

31
00:02:50,180 --> 00:02:57,700
And for example we want to have something repeat that we want to have something conditional.

32
00:02:58,140 --> 00:03:04,290
So those kind of things are going to achieve using structural directives so that in the structure of

33
00:03:04,290 --> 00:03:10,830
the view what are the examples and different indeed if these are structural directives then we are also

34
00:03:10,830 --> 00:03:17,520
going to have attributes with change that appearance or behavior of the element.

35
00:03:17,820 --> 00:03:19,820
So the that style of tributes.

36
00:03:20,040 --> 00:03:23,460
Basically this is something which we want to cover later.

37
00:03:23,910 --> 00:03:30,330
So the first and most important component properties and interpellation let us understand what is this

38
00:03:30,330 --> 00:03:41,590
component properties and in politian No let me create a new project I'm going to create a new project.

39
00:03:41,930 --> 00:03:47,490
Copy certain files from the downloaded seed which we have seen in the first model.

40
00:03:52,030 --> 00:03:55,940
I'll create a new dot net of application.

41
00:03:55,960 --> 00:04:00,870
I told you or here you can also create a dot net or obligation.

42
00:04:00,880 --> 00:04:03,130
It's your choice.

43
00:04:03,130 --> 00:04:10,600
I'm taking a simple is a lot of obligation I call it does component's

44
00:04:13,940 --> 00:04:14,540
Demel

45
00:04:19,680 --> 00:04:23,610
click on OK we want it to be an empty project.

46
00:04:23,610 --> 00:04:26,650
We don't want to use the form MVC or anything like that.

47
00:04:26,700 --> 00:04:34,080
A simple dot net empty project and into this empty project we are going to add the content from our

48
00:04:34,260 --> 00:04:38,770
QuickStart seed which we don't know what are those things.

49
00:04:38,820 --> 00:04:39,450
Sarcy

50
00:04:42,310 --> 00:04:51,130
biggest config in fact is not just in the US link but isn't this forefronts only we need we don't need

51
00:04:51,130 --> 00:04:53,190
to rest this files.

52
00:04:53,200 --> 00:04:58,590
I'm going to copy into solution of a studio project.

53
00:04:58,840 --> 00:05:07,840
Now right click on package D'Argenton series packages these two packages is going to download all the

54
00:05:07,840 --> 00:05:16,330
sample project all the libraries and libraries which are required for executing the project.

55
00:05:16,360 --> 00:05:18,290
So that's what you can actually see in our

56
00:05:25,110 --> 00:05:30,380
download and you can see in the symbol it's ongoing and it takes couple of time a couple of minutes

57
00:05:30,710 --> 00:05:33,630
and you can ignore all these warnings that are generated.

58
00:05:33,700 --> 00:05:37,620
They are not relevant to us.

59
00:05:37,900 --> 00:05:41,060
Yes it does computer downloading all the files.

60
00:05:41,240 --> 00:05:43,190
No build the project.

61
00:05:43,260 --> 00:05:45,180
There's tons that you might get an error.

62
00:05:47,170 --> 00:05:47,910
Yes.

63
00:05:48,220 --> 00:05:54,910
What do you love to do is go to another source who will fight this conflict or some and enough to make

64
00:05:55,020 --> 00:05:59,860
changes laughter our new property that is

65
00:06:03,110 --> 00:06:04,250
alos strict

66
00:06:11,060 --> 00:06:13,320
no strict checks.

67
00:06:13,490 --> 00:06:19,600
This book contains continue to make is in index rodded Steimle

68
00:06:23,800 --> 00:06:28,550
we have got here base our code is on in-sourcing folder.

69
00:06:28,610 --> 00:06:32,630
So we'll have to add that for all these changes.

70
00:06:33,540 --> 00:06:39,180
Don't know on that score modules folder is also generated in the room.

71
00:06:39,640 --> 00:06:43,490
That's why the room is supposed to be given you just that slice.

72
00:06:43,510 --> 00:06:48,130
And finally one more thing you have to make is in system there is no

73
00:06:52,720 --> 00:06:58,310
yes your system does not conflict don't does just give here also.

74
00:06:58,770 --> 00:07:01,860
So all these things are actually used as a part of bootstrapping.

75
00:07:01,860 --> 00:07:08,700
You don't have to do anything that actually otherwise in these files you can just close them I'm closing

76
00:07:08,700 --> 00:07:09,520
everything.

77
00:07:09,900 --> 00:07:11,680
Now I will the source.

78
00:07:12,210 --> 00:07:14,440
Major we are going to begin with.

79
00:07:14,670 --> 00:07:19,550
I model the rule model which has a component of component.

80
00:07:19,560 --> 00:07:23,590
So right now the component has only one property angle up.

81
00:07:23,970 --> 00:07:25,260
And here we have got one.

82
00:07:25,260 --> 00:07:26,080
Me.

83
00:07:26,580 --> 00:07:35,310
So a year I want to have actually set two properties so it does first name next in my name I'll pull

84
00:07:35,340 --> 00:07:36,220
there.

85
00:07:36,780 --> 00:07:38,490
And lastly

86
00:07:41,410 --> 00:07:47,750
and what I support here these two values I want to be part of the achievement.

87
00:07:48,300 --> 00:07:49,500
So I'm going to put you.

88
00:07:49,530 --> 00:07:50,430
Hello.

89
00:07:52,200 --> 00:07:59,960
We would like to have the first name space and double square brackets.

90
00:07:59,960 --> 00:08:05,720
That's called Last spring interpellation So that that's what we want to do here actually.

91
00:08:06,850 --> 00:08:11,020
Put your last name build the project.

92
00:08:11,030 --> 00:08:13,420
We should not get any error.

93
00:08:13,820 --> 00:08:16,040
It should be all error free zero.

94
00:08:16,160 --> 00:08:19,260
We should get perfect.

95
00:08:19,410 --> 00:08:20,990
No you can execute it.

96
00:08:21,240 --> 00:08:27,170
You can if you want to do debugging you can execute in debug mode if not simply say control a file and

97
00:08:27,170 --> 00:08:32,500
that actually execute without debugging in the browser output will be displayed to you.

98
00:08:32,610 --> 00:08:34,400
Hello Sundeep Sawney.

99
00:08:34,650 --> 00:08:44,670
Of course we have to set a sarcy in which we have good index not petchem Helo's.

100
00:08:44,860 --> 00:08:48,020
So ideally what I would recommend this to me.

101
00:08:48,040 --> 00:08:55,660
This as the start of set up start beach so the next time you don't have to take the water press control

102
00:08:55,980 --> 00:08:57,920
on the keyboard directly gifs.

103
00:08:57,960 --> 00:08:59,420
Hello Sundeep Sony

104
00:09:03,980 --> 00:09:12,080
buffer and index actually execute no at the time of interpellation.

105
00:09:12,210 --> 00:09:18,270
Remember we always have the option of even using an expression.

106
00:09:18,420 --> 00:09:25,520
For example I can write some off 10 and 20 e's.

107
00:09:25,560 --> 00:09:38,340
I can pull till then less guarantee that it is going to display here at the break time.

108
00:09:40,560 --> 00:09:46,410
It is also no problem that one will break it.

109
00:09:46,730 --> 00:09:55,000
Brant is totally.

110
00:09:55,060 --> 00:09:58,370
This also is actually not like that because we have espaa

111
00:10:02,560 --> 00:10:03,400
So like this.

112
00:10:03,490 --> 00:10:10,930
You can keep adding properties here and for these properties whatever value you can assign.

113
00:10:11,000 --> 00:10:12,910
Right now these are static values.

114
00:10:13,160 --> 00:10:20,120
Later we are going to see that these values also can be dynamic and these values we want to make use

115
00:10:20,120 --> 00:10:21,000
of in the.

116
00:10:21,060 --> 00:10:23,730
It's different here.

117
00:10:23,900 --> 00:10:26,300
We can also have something more.

118
00:10:26,300 --> 00:10:28,520
Maybe I'll put a p doc or a span.

119
00:10:30,220 --> 00:10:31,030
Type.

120
00:10:31,510 --> 00:10:44,930
I would like to display that position so we can have new date and no let's run this

121
00:10:50,660 --> 00:10:51,920
the golden era.

122
00:10:52,270 --> 00:10:53,780
I wonder that too.

123
00:10:54,250 --> 00:10:59,910
So here it did not give any error here it did not give any error for what reason.

124
00:11:00,190 --> 00:11:03,490
Because this is Steimle and this is not come by.

125
00:11:03,500 --> 00:11:05,050
This is not typescript.

126
00:11:05,080 --> 00:11:06,840
This is not dangler.

127
00:11:06,880 --> 00:11:15,640
This is only steamin all everything is playing and you can actually see the error when you go to the

128
00:11:15,640 --> 00:11:18,890
debugger as well and you can see the error here.

129
00:11:19,860 --> 00:11:21,400
Unexpected.

130
00:11:22,830 --> 00:11:24,250
Very different.

131
00:11:24,840 --> 00:11:29,880
So what we should actually do your only we will have some date

132
00:11:33,090 --> 00:11:34,300
other properties.

133
00:11:34,640 --> 00:11:38,110
Or let's say some of our time or my time

134
00:11:40,960 --> 00:11:41,710
for my time.

135
00:11:41,710 --> 00:11:53,400
I'll provide the valuable new date and maybe we can convert to the law by installing something and this

136
00:11:53,420 --> 00:11:53,910
might.

137
00:11:53,980 --> 00:12:04,520
Time is ideally supposed to be used at all properties of gamboling should be initialized and used for

138
00:12:04,580 --> 00:12:06,660
interpellation with the template.

139
00:12:06,660 --> 00:12:08,210
Now you're going to get the proper

140
00:12:14,770 --> 00:12:15,650
likewise.

141
00:12:15,940 --> 00:12:18,500
We can have some boolean variable here.

142
00:12:19,120 --> 00:12:25,360
This is an sometimes I want to decide what the time should be show or not.

143
00:12:25,440 --> 00:12:31,670
Dynamically we want to take a decision so I will declare another variable.

144
00:12:31,870 --> 00:12:36,110
Let's say it's show time the value of it right now.

145
00:12:37,570 --> 00:12:41,470
William is equal to.

146
00:12:41,920 --> 00:12:43,260
Maybe false.

147
00:12:43,260 --> 00:12:47,190
Remember all these are automatically taken to be of type string.

148
00:12:47,190 --> 00:12:47,760
Why.

149
00:12:47,910 --> 00:12:49,510
Because they are informed.

150
00:12:49,680 --> 00:12:54,020
We have learned that Clinton is going to they are in full

151
00:12:56,870 --> 00:12:57,310
orders.

152
00:12:57,320 --> 00:13:04,390
You're actually supposed to give it all unstring all unstring Kallen.

153
00:13:04,450 --> 00:13:07,880
David and so

154
00:13:12,420 --> 00:13:17,100
golden string because they are already given debt to local.

155
00:13:19,040 --> 00:13:20,410
So long what is next.

156
00:13:20,570 --> 00:13:26,870
Based on this short term value I would like to display this or highlight this dynamically we would like

157
00:13:26,870 --> 00:13:28,000
to do that.

158
00:13:28,130 --> 00:13:35,870
For this we have got a ready made and if Derek we have got Andy in there.

159
00:13:36,420 --> 00:13:44,060
So this directive you should use like this start in the IF is equal to the variable name.

160
00:13:44,940 --> 00:13:53,910
So just come here and get start and see if I would say so.

161
00:13:53,960 --> 00:13:54,360
Tyler

162
00:13:57,540 --> 00:13:59,300
So short time is.

163
00:13:59,460 --> 00:14:04,750
This dog is going to be displayed if not that he's going to get invisible

164
00:14:08,110 --> 00:14:14,770
indeed if it's a directive structural character that is important you'll see it is false.

165
00:14:14,850 --> 00:14:19,990
Time is not getting this big tent this time will be displayed no

166
00:14:25,960 --> 00:14:26,940
puffing.

167
00:14:27,280 --> 00:14:32,650
So Andy if that you know we have got a difference in taxes for Andy.

168
00:14:32,690 --> 00:14:36,870
Andy if can also be used with the else condition.

169
00:14:37,270 --> 00:14:42,620
But when we have it this condition we're actually supposed to write so employee.

170
00:14:43,030 --> 00:14:51,780
And for that we are supposed to make use of in deep template that we are going to write something like

171
00:14:51,780 --> 00:14:59,640
an indie template and the hyphen template.

172
00:14:59,890 --> 00:15:09,230
And we should use some ID policy that has some Let us create a practical situation.

173
00:15:09,230 --> 00:15:10,690
Imagine business calls me

174
00:15:13,840 --> 00:15:18,630
and this will be the and we don't need.

175
00:15:18,940 --> 00:15:24,840
So yes we can say of course call of course name

176
00:15:27,890 --> 00:15:29,420
we don't need.

177
00:15:30,370 --> 00:15:33,470
So on top of that swing of course you're going to.

178
00:15:33,470 --> 00:15:34,610
So the timing is

179
00:15:37,480 --> 00:15:39,540
this will be called Access or pay

180
00:15:43,770 --> 00:15:45,360
and then we don't want.

181
00:15:45,360 --> 00:15:48,390
I can believe that that was just an example.

182
00:15:49,770 --> 00:15:52,450
Looks if we're going to have one property called that can be

183
00:15:56,430 --> 00:15:57,240
say.

184
00:15:57,630 --> 00:16:02,270
So I'm just trying to get my name.

185
00:16:02,420 --> 00:16:09,910
So your course name timings and I would like to know if I can answer

186
00:16:14,740 --> 00:16:21,380
broidery in the deck on inside the debt.

187
00:16:21,440 --> 00:16:31,920
Frankel the call on here is it d name

188
00:16:34,790 --> 00:16:39,190
occluding of that that's all very important.

189
00:16:39,280 --> 00:16:44,970
You have to ensure that everything is properly vetted for all that is pretty important.

190
00:16:48,150 --> 00:16:57,410
Now I would like to have your variable show faculty or can anybody teach.

191
00:16:57,650 --> 00:17:03,730
So sometimes what happens is any faculty can teach not only one particular faculty.

192
00:17:03,780 --> 00:17:11,300
So in that kind of case what should be the No this thing I would want it to be conditional I didn't

193
00:17:11,430 --> 00:17:14,080
know that you know

194
00:17:17,820 --> 00:17:21,720
there is one variable specialization

195
00:17:25,950 --> 00:17:34,230
William no specialization is to the goals can be taken up by anybody.

196
00:17:34,270 --> 00:17:35,010
So you know.

197
00:17:35,020 --> 00:17:36,150
All right.

198
00:17:36,640 --> 00:17:42,160
Pete I'd start with Andy if it's equal to

199
00:17:46,250 --> 00:17:50,350
no more specialized corpsmen call it

200
00:17:57,250 --> 00:17:59,070
for the specialized courses.

201
00:17:59,320 --> 00:18:05,130
Then he should explicitly display the faculty right.

202
00:18:05,160 --> 00:18:07,400
So so time is time spent

203
00:18:11,200 --> 00:18:18,170
faculty name specialist I can name and specialist on this and see here we have some merit.

204
00:18:18,200 --> 00:18:19,950
Let us ignore for now

205
00:18:27,360 --> 00:18:29,870
you're not getting the faculty name displayed.

206
00:18:30,030 --> 00:18:30,300
Why.

207
00:18:30,300 --> 00:18:34,720
Because this is all something is wrong.

208
00:18:34,810 --> 00:18:36,580
What is all your.

209
00:18:36,810 --> 00:18:37,760
It should be equal

210
00:18:40,940 --> 00:18:41,800
or equal

211
00:18:47,660 --> 00:18:49,210
and we got Pacak.

212
00:18:49,310 --> 00:18:55,340
Obviously if I say it is not a specialized course then I would like to display something that any faculty

213
00:18:55,340 --> 00:18:56,540
can beat.

214
00:18:56,900 --> 00:19:02,570
Right now we'll get nothing no faculty that is what I want to mention in that place and the faculty

215
00:19:02,570 --> 00:19:03,570
can be.

216
00:19:03,920 --> 00:19:07,100
That is where we can make use of that as bhakt.

217
00:19:07,160 --> 00:19:10,330
So I'll give you an indeed template.

218
00:19:11,530 --> 00:19:15,980
Heiss that's what Id.

219
00:19:16,280 --> 00:19:20,210
Let's call it doesn't meet any faculty.

220
00:19:21,430 --> 00:19:22,330
That I live in.

221
00:19:22,350 --> 00:19:33,020
We have given to the complete and in that say I'm saying faculty because it is more specialized.

222
00:19:33,190 --> 00:19:36,900
I would like to mention any faculty can beat

223
00:19:41,200 --> 00:19:45,170
in the closet.

224
00:19:45,270 --> 00:19:46,860
Now what is the requirement.

225
00:19:47,040 --> 00:19:52,090
When this condition is false we people like to make use of that as part.

226
00:19:52,110 --> 00:20:03,510
So put your semicolon give as is and then give the template the employee and the faculty

227
00:20:06,670 --> 00:20:15,460
So if this condition is true this is going to be displayed if the condition is false then this does

228
00:20:15,720 --> 00:20:18,620
this template that this text is going to be displayed.

229
00:20:21,040 --> 00:20:22,270
Now let's run it.

230
00:20:25,580 --> 00:20:27,520
Any I can digging deep.

231
00:20:27,770 --> 00:20:28,520
Why.

232
00:20:28,730 --> 00:20:33,530
Because what DNS faults specialized.

233
00:20:33,740 --> 00:20:42,870
So we have got if if else know if required we can have fun.

234
00:20:42,890 --> 00:20:49,970
Then also one can be like I can get one more complete answer.

235
00:20:50,000 --> 00:20:50,810
Faculty

236
00:20:53,610 --> 00:20:58,320
faculty name and this I'll remove from here.

237
00:20:58,320 --> 00:21:00,200
So what is my requirement requirement.

238
00:21:00,280 --> 00:21:03,690
If this condition is true then I should use

239
00:21:07,230 --> 00:21:11,580
specific then I should use specific DMP

240
00:21:15,720 --> 00:21:18,510
specific faculty.

241
00:21:19,830 --> 00:21:23,180
As any faculty.

242
00:21:23,310 --> 00:21:29,440
So if then else it's like the difference in taxes.

243
00:21:29,460 --> 00:21:34,070
Actually So as part of your requirement you have to display any one of them.

244
00:21:34,130 --> 00:21:37,000
Something went wrong as well.

245
00:21:41,070 --> 00:21:43,680
This only failed attack on a lawyer.

246
00:21:43,900 --> 00:21:53,390
So what is wrong if then else everything looks good template indeed.

247
00:21:53,430 --> 00:21:56,660
OK I did not close the template you'll see.

248
00:21:56,670 --> 00:21:58,600
This is really dangerous.

249
00:21:58,720 --> 00:22:04,900
We have to close the template back open the template lose the complete back number.

250
00:22:04,940 --> 00:22:06,960
There's no Intellisense here because of that.

251
00:22:06,960 --> 00:22:12,480
Only all these problems are coming any faculty complete.

252
00:22:13,180 --> 00:22:22,430
If we're really in control at 5 you should not get an effect of what you can do.

253
00:22:22,450 --> 00:22:23,770
In either case.

254
00:22:23,770 --> 00:22:25,930
All you have one faculty named

255
00:22:31,360 --> 00:22:31,980
on this

256
00:22:38,600 --> 00:22:42,600
something so there is hope.

257
00:22:42,650 --> 00:22:48,230
Indeed if with different syntaxes can be used all this argument in the handle.

258
00:22:48,250 --> 00:22:56,920
You are of course simple indeed if indeed if with any spot and if with then and else so is your choice.

259
00:22:56,920 --> 00:22:59,400
What syntax you want to make use of.

260
00:22:59,440 --> 00:23:05,320
Based on your requirement and of course we have also seen your out and do templates can be used for

261
00:23:05,330 --> 00:23:10,260
embedding content into the fact based on the condition.

262
00:23:10,270 --> 00:23:15,740
Likewise we have also got in this village the hotel indispensables.

263
00:23:15,750 --> 00:23:22,960
Now I can have your one more property called duration.

264
00:23:23,000 --> 00:23:26,140
The situation is tedious now.

265
00:23:26,250 --> 00:23:30,560
I would like to display the duration.

266
00:23:30,910 --> 00:23:32,270
Will that be.

267
00:23:32,850 --> 00:23:36,940
So I'll put here and meet Dr.

268
00:23:40,740 --> 00:23:43,830
duration followed by this.

269
00:23:43,840 --> 00:23:45,740
I would like to put us back back

270
00:23:50,780 --> 00:23:55,180
Alvis date of closing properly

271
00:23:58,250 --> 00:24:00,560
is not a lot of problems come.

272
00:24:00,780 --> 00:24:03,020
Now what should be done.

273
00:24:03,670 --> 00:24:04,280
Inspan.

274
00:24:04,290 --> 00:24:11,800
I would like to provide swinge so give no square bracket.

275
00:24:12,040 --> 00:24:13,550
And with it.

276
00:24:13,550 --> 00:24:15,400
Remember we are using square bracket

277
00:24:18,290 --> 00:24:20,880
that is actually binding syntax.

278
00:24:20,930 --> 00:24:25,970
So we are binding in this but the value of the duration.

279
00:24:26,000 --> 00:24:31,170
This is binding Sendak's we are going to see more in detail in the binding.

280
00:24:31,460 --> 00:24:39,860
But right now the value of duration is going to be assigned to this with no inside interest which we

281
00:24:39,860 --> 00:24:43,580
are going to have so it gives us multiple cases.

282
00:24:43,850 --> 00:24:55,630
So let's say we have got one case well I would like to use now a data start and does it gays as it were.

283
00:24:55,760 --> 00:25:00,040
But the duration is one I would like to display here.

284
00:25:00,050 --> 00:25:00,710
One of the

285
00:25:05,960 --> 00:25:13,230
Likewise I would have different ones on in one of the case I would like to get the default.

286
00:25:13,800 --> 00:25:22,230
So here in this case will be pleased with the default of an East Indies with default executed

287
00:25:28,090 --> 00:25:30,510
when none of these values match.

288
00:25:30,720 --> 00:25:35,460
So we are assuming the munition is supposed to be either 1 day or 7 days.

289
00:25:35,730 --> 00:25:39,960
Or let's say 13 is like that the result is supposed to.

290
00:25:40,020 --> 00:25:43,450
If it is one day you will want me to do this.

291
00:25:43,470 --> 00:25:45,450
I'll give that one month.

292
00:25:46,050 --> 00:25:50,090
So the duration is either one day or one week or one month.

293
00:25:50,370 --> 00:25:55,720
It's based on the value of duration which is binding to and which.

294
00:25:55,740 --> 00:25:56,830
So in this way.

295
00:25:56,890 --> 00:26:00,340
Is equal to 1 this text will be rendered in this way.

296
00:26:00,540 --> 00:26:04,730
If we are to seven this text will be rendered in this which is equal to 30.

297
00:26:04,730 --> 00:26:10,110
This makes me wonder but any other value of this which this text is going to be.

298
00:26:10,110 --> 00:26:16,260
And so right now I've given for dualism 30 toes and this we just told.

299
00:26:16,490 --> 00:26:18,920
So we are supposed to get a good one month

300
00:26:24,480 --> 00:26:25,620
let us place some of the

301
00:26:28,670 --> 00:26:35,200
something like maybe 30 to according to our logic that is wrong.

302
00:26:35,410 --> 00:26:37,320
So please correctible.

303
00:26:38,310 --> 00:26:40,690
So the switch.

304
00:26:41,020 --> 00:26:47,580
Likewise we can also make use of food for in the fall when they were told to repeat something.

305
00:26:47,620 --> 00:26:49,440
We can make use of indiv.

306
00:26:49,650 --> 00:26:58,020
So let us say we are now going to have one more variable here something called the related courses is

307
00:26:58,230 --> 00:26:58,990
an the

308
00:27:02,800 --> 00:27:07,260
area of springs and I'll give him something like

309
00:27:11,860 --> 00:27:16,990
javascript maybe extremal and so on.

310
00:27:17,450 --> 00:27:20,830
No all these related courses we would like to display here.

311
00:27:31,940 --> 00:27:33,660
Followed by that sum it up.

312
00:27:33,920 --> 00:27:37,470
And then we shall have all the related causes.

313
00:27:37,480 --> 00:27:39,690
So there are multiple courses.

314
00:27:39,700 --> 00:27:41,460
This is an area of string.

315
00:27:41,770 --> 00:27:49,450
So we have to go to an area of string one at a time and get these values spread and very importantly

316
00:27:49,540 --> 00:27:57,210
I would like to probably also for some bullwhips So here I'll get an order list.

317
00:28:01,360 --> 00:28:06,440
Inside an ordered list we have to repeat list all the time.

318
00:28:06,850 --> 00:28:11,020
But all Dagget supposed to be used for each course.

319
00:28:11,170 --> 00:28:12,050
So for oil

320
00:28:16,500 --> 00:28:18,760
we want to get form.

321
00:28:19,230 --> 00:28:26,130
So do we stop indeed for that active Zic will do.

322
00:28:26,130 --> 00:28:38,990
In other words let us or later Goertz or related cause us

323
00:28:42,020 --> 00:28:43,580
and here we can.

324
00:28:43,960 --> 00:28:45,010
That is as

325
00:28:47,630 --> 00:28:48,130
was

326
00:28:52,940 --> 00:28:54,870
so are we getting all the related courses.

327
00:28:54,890 --> 00:28:55,460
Let's see

328
00:29:01,310 --> 00:29:02,320
know nothing is coming.

329
00:29:02,320 --> 00:29:04,090
So something went wrong.

330
00:29:04,410 --> 00:29:07,780
If it forced to figured out no real

331
00:29:12,110 --> 00:29:14,090
should be some spelling mistake.

332
00:29:14,210 --> 00:29:23,230
Of course those copy and all his music related courses not on once again.

333
00:29:28,180 --> 00:29:28,770
We got.

334
00:29:28,960 --> 00:29:33,640
But why are they coming all in straight line.

335
00:29:33,710 --> 00:29:37,640
This actually should be given for the parent that is.

336
00:29:39,920 --> 00:29:41,420
Oil is supposed to be repeated

337
00:29:47,040 --> 00:29:52,680
No what is wrong with a dirty mistake.

338
00:29:52,680 --> 00:29:58,210
This should be like and yes it should be limited.

339
00:29:58,350 --> 00:30:00,610
So we're supposed to be repeated.

340
00:30:00,660 --> 00:30:04,000
We should give it for Elaine.

341
00:30:04,870 --> 00:30:09,570
No run this under you will list item and I should be dead.

342
00:30:11,000 --> 00:30:12,620
Well for the first one it is getting

343
00:30:15,860 --> 00:30:22,000
what could be wrong here related courses it could cause us to bring it all year.

344
00:30:22,100 --> 00:30:23,350
That is a mistake.

345
00:30:23,900 --> 00:30:24,980
This should be one spring.

346
00:30:24,980 --> 00:30:26,480
This is a great spring.

347
00:30:26,490 --> 00:30:31,530
Should be simplistic by default otherwise all three will be treated as one single string and that's

348
00:30:31,530 --> 00:30:31,800
fine.

349
00:30:31,850 --> 00:30:35,180
We were getting the errors but now you see it should work.

350
00:30:38,180 --> 00:30:41,400
Script does its treatment in a bullet form.

351
00:30:42,030 --> 00:30:44,870
So we know what we want to eat a particular bag.

352
00:30:45,960 --> 00:30:51,120
For that we will have to make use of anti-foreigner now.

353
00:30:51,380 --> 00:30:56,960
Right now indeed for is giving me one element at a time from this it.

354
00:30:57,380 --> 00:31:00,640
But what if I want to know that index.

355
00:31:00,650 --> 00:31:02,940
I would like to do some kind of numbering also.

356
00:31:02,960 --> 00:31:08,770
Imagine so in such case you can put the semi-colon and give your lead.

357
00:31:09,080 --> 00:31:10,680
I or I indeed.

358
00:31:10,700 --> 00:31:12,680
Is it called top index.

359
00:31:12,680 --> 00:31:14,690
This index is a key word no.

360
00:31:15,160 --> 00:31:19,830
And this I know we can make use of this for example.

361
00:31:19,830 --> 00:31:20,580
I like get

362
00:31:26,180 --> 00:31:27,820
eye indeed.

363
00:31:28,180 --> 00:31:29,420
Typhon.

364
00:31:29,790 --> 00:31:31,570
So now we're going to get 0 1

365
00:31:35,900 --> 00:31:39,430
what you want to change it and give your last one.

366
00:31:39,580 --> 00:31:40,680
That's fine.

367
00:31:40,800 --> 00:31:41,410
You can do that.

368
00:31:41,410 --> 00:31:45,660
So you're going to get one two or three one two three.

369
00:31:46,410 --> 00:31:55,320
So if required you can get index and based on the index you can perform the action or if required you

370
00:31:55,320 --> 00:31:58,500
can get one element at a time from the end.

371
00:31:58,980 --> 00:32:04,720
So it is your choice what exactly you want to OP.

372
00:32:04,740 --> 00:32:07,010
Here we are both actually.

373
00:32:07,620 --> 00:32:13,200
So this is most of the time this is the syntax which is just one element at a time from the area we

374
00:32:13,200 --> 00:32:17,760
get and that is why we try to show you.

375
00:32:17,850 --> 00:32:18,590
So that's what it is.

376
00:32:18,590 --> 00:32:23,790
Indeed if Sendak's study in the first index

377
00:32:26,460 --> 00:32:30,550
here you see the example a little different than the handle in the handle.

378
00:32:30,570 --> 00:32:34,560
I'm just trying to come up except for the first one.

379
00:32:35,060 --> 00:32:43,080
If index is not equal to zero then we can go for a second one like that between to get.

380
00:32:43,270 --> 00:32:47,140
Except for the forestland.

381
00:32:47,190 --> 00:32:56,580
So yes with this we know how are understanding of how it's Steimle template can be interpreted in the

382
00:32:56,570 --> 00:33:00,060
position through a property of a competent class.

383
00:33:00,230 --> 00:33:03,030
And we have also seen some built in directives.

384
00:33:03,080 --> 00:33:09,110
One of them is indeed in this village and in the fall.

385
00:33:09,780 --> 00:33:14,260
We'll continue with more classes and further discussion in next week.

386
00:33:14,360 --> 00:33:14,820
Thank you.

