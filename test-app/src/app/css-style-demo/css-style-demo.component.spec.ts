import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CssStyleDemoComponent } from './css-style-demo.component';

describe('CssStyleDemoComponent', () => {
  let component: CssStyleDemoComponent;
  let fixture: ComponentFixture<CssStyleDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CssStyleDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CssStyleDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
