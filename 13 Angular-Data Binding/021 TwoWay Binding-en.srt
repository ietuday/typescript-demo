1
00:00:16,290 --> 00:00:22,470
In the previous sessions I've got one property binding and one be even binding.

2
00:00:23,280 --> 00:00:31,480
But both of these studies did that will go either from view to the component or component to the view.

3
00:00:32,070 --> 00:00:36,760
But there are many cases where we would like to have to be binding.

4
00:00:36,900 --> 00:00:43,060
That means we make changes in component we want that to reflect in view and we make changes.

5
00:00:43,140 --> 00:00:48,530
Do we want that to reflect in component C like the project which we have.

6
00:00:49,070 --> 00:00:56,460
If I run this project every bit within the value in the text box it would probably reflect somebody

7
00:00:56,460 --> 00:00:56,880
else

8
00:01:01,610 --> 00:01:04,340
but didn't hear is only going to reflect

9
00:01:07,480 --> 00:01:15,510
not here maybe 10 here you'll see is not reflecting any of it but that is my requirement if maintained

10
00:01:15,610 --> 00:01:19,950
here it should reflect in some other place rather.

11
00:01:20,040 --> 00:01:27,150
The one thing I agree and it's a great example altogether let's say I want to it but some

12
00:01:31,120 --> 00:01:37,960
component might not this is a component I would like to have no important component.

13
00:01:37,990 --> 00:01:41,020
So first I'm going to gloss Borson

14
00:01:43,940 --> 00:01:49,300
Borson clause is supposed to have oh I'm sorry this is C-sharp.

15
00:01:49,460 --> 00:01:53,750
We don't want to teach our games.

16
00:01:53,900 --> 00:01:55,800
Great gig.

17
00:01:56,660 --> 00:01:56,940
David

18
00:01:59,690 --> 00:02:03,910
percent plus is supposed to help Glasper some.

19
00:02:04,010 --> 00:02:14,200
Is that a person would be made up of first name and last name so I'll only say constructed public first

20
00:02:14,200 --> 00:02:24,600
name of string last name that also say public the of string

21
00:02:27,400 --> 00:02:30,620
and we want the glass to be exported.

22
00:02:31,390 --> 00:02:37,510
So a simple class with two properties first name and last name but initialized constructor.

23
00:02:37,610 --> 00:02:41,080
No I'm adding to the project.

24
00:02:41,420 --> 00:02:46,130
Another day is going to fight with al-Qadi does person not

25
00:02:49,030 --> 00:02:56,440
component got to use parts and or component or D s or what is the requirement here.

26
00:02:57,690 --> 00:03:02,710
Here I would like to have very similar to what we have in context of a component.

27
00:03:02,880 --> 00:03:07,140
That means we would like to have all this and just take the skeleton from here.

28
00:03:09,200 --> 00:03:10,960
I'm pretty sure Freud will do that

29
00:03:16,950 --> 00:03:21,930
and followed by that we will have what some component

30
00:03:24,870 --> 00:03:31,840
but what is important is dishcloths should be exported.

31
00:03:32,010 --> 00:03:32,850
So let's do that

32
00:03:43,290 --> 00:03:54,590
and here I would like to have in person component an object of type person.

33
00:03:54,980 --> 00:03:58,130
So how do we get that here we should sit for some

34
00:04:04,720 --> 00:04:06,160
so long I can write here.

35
00:04:06,160 --> 00:04:18,250
So is it worth it to new person or maybe the first name is ABC and the last name x y z.

36
00:04:22,240 --> 00:04:28,300
So no this person is what I would like to binded two different elements which are going to be there

37
00:04:28,330 --> 00:04:30,050
in the template.

38
00:04:30,970 --> 00:04:33,810
So in template Let us assume we have good form DAQ

39
00:04:37,840 --> 00:04:46,120
an inside phone tag but when you first name and give your input say as text

40
00:04:48,560 --> 00:04:53,810
value and give it as data we want to do.

41
00:04:53,980 --> 00:04:55,270
So how we do generally.

42
00:04:55,510 --> 00:04:59,950
Right now we know the Sendak's value is equal to.

43
00:05:00,190 --> 00:05:03,540
I would say for some dark forced the

44
00:05:13,080 --> 00:05:17,910
like that only I'll say no last name would do the same

45
00:05:20,630 --> 00:05:21,880
this will last me

46
00:05:24,690 --> 00:05:27,750
and this does but back

47
00:05:30,930 --> 00:05:32,930
so select that I've given this person.

48
00:05:33,390 --> 00:05:41,220
So in selected person how are they going to embed this content in a statement that we already know that

49
00:05:41,360 --> 00:05:45,410
I can not be complete.

50
00:05:46,020 --> 00:05:48,990
So here only in the root component

51
00:05:53,680 --> 00:05:56,620
we would all need entertainment right.

52
00:05:56,850 --> 00:05:59,720
So some of this forum I can close it

53
00:06:04,760 --> 00:06:07,090
and offer that up.

54
00:06:10,810 --> 00:06:12,250
For some details.

55
00:06:15,540 --> 00:06:19,840
And here I would like to include that content of some component.

56
00:06:20,070 --> 00:06:25,580
So that is where we would like take Select up some.

57
00:06:25,870 --> 00:06:32,830
So in this particular location in our component the content from here is going to be put up this content

58
00:06:33,040 --> 00:06:34,160
be replaced that

59
00:06:39,050 --> 00:06:41,280
just creating some empty spaces at the end.

60
00:06:42,330 --> 00:06:49,660
On the debate at this point of time but we will see as what is the value of the property.

61
00:06:49,660 --> 00:06:53,590
Those will be reflected in the component.

62
00:06:53,700 --> 00:06:55,760
No we have gotten error.

63
00:06:55,820 --> 00:06:59,410
There is no compilation error but we got an error at runtime.

64
00:06:59,420 --> 00:07:00,820
What is the problem.

65
00:07:00,830 --> 00:07:04,070
Press F to begin can read here.

66
00:07:05,640 --> 00:07:08,660
This is please ignore it for not for ignore it.

67
00:07:08,820 --> 00:07:13,880
If the person is angular component then verify that this part of this Mordieu.

68
00:07:13,920 --> 00:07:15,370
This is important.

69
00:07:15,690 --> 00:07:19,250
So now we have created a new person component right.

70
00:07:19,380 --> 00:07:24,700
This person components should be declared to and Goulart that it is a component.

71
00:07:24,720 --> 00:07:26,360
How are we going to do this.

72
00:07:26,610 --> 00:07:30,220
For this we will go to app more.

73
00:07:30,230 --> 00:07:33,030
You will not be as an under declaration.

74
00:07:33,030 --> 00:07:35,890
We are going to add some component.

75
00:07:36,100 --> 00:07:43,260
What response in component has declared that we will have to include your recent import for some component

76
00:07:44,130 --> 00:07:57,830
from DOT slots or some dot com and not be as note on this one Zygi every new componentry I remember

77
00:07:57,840 --> 00:08:04,780
we are actually supposed to include it in the board of now most important thing.

78
00:08:04,890 --> 00:08:11,400
What I would like to do is in person component only year I would like to put hello followed by that

79
00:08:11,460 --> 00:08:19,830
I would like to display the person's name so here can I do data mining expression for Sunday or first

80
00:08:19,830 --> 00:08:24,600
name it first and last name

81
00:08:30,030 --> 00:08:31,480
let's take.

82
00:08:31,880 --> 00:08:35,990
We are going to get hello NBC and x y z.

83
00:08:36,230 --> 00:08:38,710
But what is important is if it ends here.

84
00:08:38,870 --> 00:08:40,300
It's not ending here.

85
00:08:40,490 --> 00:08:43,010
If it ends here it's not ending yet.

86
00:08:43,130 --> 00:08:46,200
So how do we ensure that it changes.

87
00:08:46,220 --> 00:08:49,790
That is where we would like to do Bouverie by mining right now.

88
00:08:49,790 --> 00:08:55,400
What they want is the current value of first name and last name underparts an object is getting displayed

89
00:08:55,400 --> 00:08:57,630
in these two elements.

90
00:08:57,740 --> 00:09:00,710
Because this is one way of finding this is property finding.

91
00:09:00,950 --> 00:09:07,700
How do we make it to Ebrahimi for that we will have to go for it in bracket give something like input

92
00:09:09,370 --> 00:09:16,590
and we have to provide a value that means what is going to change.

93
00:09:16,720 --> 00:09:25,470
We want to take no person dog for me is equal to what is the value we want to use.

94
00:09:25,600 --> 00:09:32,980
We want to take the value of the current X-Box Hogan we get the current X Box dollar event don't target

95
00:09:33,250 --> 00:09:37,740
don't value dollar even our target is the current x box it's value.

96
00:09:37,750 --> 00:09:39,900
We won't do it.

97
00:09:40,030 --> 00:09:47,140
So whenever the value in the text box changes input even it's going to be raised in response to that

98
00:09:47,590 --> 00:09:48,250
event.

99
00:09:48,450 --> 00:09:57,100
What is the current object that the current text box its value we are signing up for us and something

100
00:09:57,100 --> 00:10:04,330
similar we will have to do also for the last minute but we lost them into buildings.

101
00:10:06,120 --> 00:10:10,290
Not on the same program once again this time what we are going to notice is

102
00:10:15,360 --> 00:10:17,390
let me just bring this up.

103
00:10:18,940 --> 00:10:19,510
So here

104
00:10:23,800 --> 00:10:26,640
I would like to add some dummy breaks

105
00:10:29,970 --> 00:10:31,640
not exit items on the second

106
00:10:34,560 --> 00:10:35,290
better.

107
00:10:35,630 --> 00:10:37,460
So we have got.

108
00:10:37,800 --> 00:10:44,660
I put here recently and you see immediately extending yet and its also changing gears.

109
00:10:44,710 --> 00:10:54,680
So this has to be seen in property off and component will reflect the input element and teams in the

110
00:10:54,770 --> 00:11:01,980
input element really reflecting the component the logic in the component the property of the component

111
00:11:01,990 --> 00:11:02,840
is changing.

112
00:11:03,230 --> 00:11:04,910
But we will have to do it manually.

113
00:11:05,090 --> 00:11:11,550
So this is one way and this is the other way that the reason is the reason it is for the rest who will

114
00:11:11,590 --> 00:11:12,420
buy in.

115
00:11:12,780 --> 00:11:18,610
And I believe you remember even the object has come from the previous session where we have seen event.

116
00:11:19,130 --> 00:11:25,080
It is associated with the input agreement which has raised the event and gives us certain information.

117
00:11:25,100 --> 00:11:30,320
Most of the time that information is specific to the element but Target is guaranteed that in every

118
00:11:30,320 --> 00:11:34,670
event object we would always have target which is nothing but a reference to the input element that

119
00:11:34,910 --> 00:11:36,460
raised the event.

120
00:11:36,610 --> 00:11:38,010
You can be I wont even object.

121
00:11:38,030 --> 00:11:39,740
Yes we have seen that also.

122
00:11:39,850 --> 00:11:41,970
We can always have a reference here.

123
00:11:43,400 --> 00:11:55,460
Yes let's say first name and then we can make use of this first name dot rather we can always do this

124
00:12:02,290 --> 00:12:06,230
working.

125
00:12:06,230 --> 00:12:09,740
So this is precisely what we call it guys rebinding

126
00:12:14,920 --> 00:12:16,610
rebinding with empty modem.

127
00:12:16,610 --> 00:12:21,070
Let's look at this not what I call what is this one.

128
00:12:21,210 --> 00:12:24,000
We have our binding with Indeed more than.

129
00:12:24,150 --> 00:12:31,650
This is not the best way of avoiding to rebinding where we operate one way and the other way.

130
00:12:32,170 --> 00:12:36,480
Instead the same thing can be done with little more ease.

131
00:12:36,630 --> 00:12:39,810
We can make use of something called that Indian model.

132
00:12:40,320 --> 00:12:46,500
So Angola of course is special to rebinding syntax and that is this particular syntax

133
00:12:50,640 --> 00:12:57,240
the syntax combines the brackets of property binding with the parent that is of the even binding combination.

134
00:12:57,240 --> 00:13:02,730
Is that actually property binding you the square bracket and even binding you just called me but on

135
00:13:02,730 --> 00:13:10,080
brackets the parent does is regular's parenthesis so that syntax is easy to demonstrate when element

136
00:13:10,080 --> 00:13:17,910
has a stable property called X and a corresponding event named X teams.

137
00:13:17,910 --> 00:13:19,330
This is important.

138
00:13:19,560 --> 00:13:24,690
The syntax will work only if that element has two things.

139
00:13:24,690 --> 00:13:31,740
One is property by name x x can be anything ABC X Y and Z what elements will have a property by name

140
00:13:31,770 --> 00:13:37,540
x and then compulsory should have an even by name X Team.

141
00:13:37,830 --> 00:13:43,690
So instead of writing two things one with square brackets x is equal to property and then later on but

142
00:13:43,710 --> 00:13:50,490
I can extend to the core the same expression or statement we can combine it into one single thing so

143
00:13:50,590 --> 00:13:57,600
anymore do is one thing which is very common or commonly used in this kind of situation so anymore is

144
00:13:57,630 --> 00:14:03,410
it that either directive and it's a part of that model that forms Mordieu.

145
00:14:03,660 --> 00:14:10,390
So even though we have input elements in text in a passport in so many other different type elements

146
00:14:11,170 --> 00:14:17,390
in those kind of cases whenever we want to program little at once we will always need these forms.

147
00:14:17,390 --> 00:14:25,200
Mordieu So we will have to know in forms Mordieu before we can make use of the Indian model directive

148
00:14:25,510 --> 00:14:36,210
for Bouvier binding so Holdaway actually include forms modem for that we will have to go to that model

149
00:14:36,250 --> 00:14:38,200
dot because this is the model.

150
00:14:38,660 --> 00:14:40,550
Right now there is only one browser model.

151
00:14:40,550 --> 00:14:43,080
Here are two art forms more.

152
00:14:44,270 --> 00:14:54,870
But the response module class that we will have to give here we have to import forms module from the

153
00:14:57,910 --> 00:15:08,360
red and large slice forms and lower class forms is supposed to have forms Mordieu and only offer we

154
00:15:08,360 --> 00:15:09,600
make use of this forms.

155
00:15:09,610 --> 00:15:18,610
But you will then be able to use that indeed more syntax anti-motorist index.

156
00:15:18,650 --> 00:15:20,370
So I wouldn't know how.

157
00:15:20,370 --> 00:15:28,370
Let me say one more form only for the whole purpose and here I don't want to use all this all this

158
00:15:32,590 --> 00:15:34,190
and I'll make it very simple.

159
00:15:36,210 --> 00:15:44,850
I lose the moral syntax of youth and the moral syntax and the model and indeed more that we should write

160
00:15:44,880 --> 00:15:47,770
in square brackets.

161
00:15:48,850 --> 00:15:49,840
But does this

162
00:15:56,920 --> 00:15:59,500
is equal what is that we should when we do.

163
00:15:59,710 --> 00:16:06,460
I want to buy to let us say person dot first name.

164
00:16:06,730 --> 00:16:13,340
This is what I want to binded same thing I'm going to do even for this.

165
00:16:13,640 --> 00:16:20,070
And the other person's last name no let's run this program.

166
00:16:23,040 --> 00:16:25,670
And we'll have all forms at the end.

167
00:16:26,630 --> 00:16:31,280
First one is all syntax second.

168
00:16:31,320 --> 00:16:32,340
The news index

169
00:16:34,990 --> 00:16:36,210
something is going wrong.

170
00:16:36,220 --> 00:16:38,390
I don't think there is anything wrong here.

171
00:16:38,400 --> 00:16:46,020
Your with want that either the name must be set or the form control must have standalone.

172
00:16:46,040 --> 00:16:46,680
OK.

173
00:16:47,180 --> 00:16:50,420
The requirement is for any model to walk.

174
00:16:50,420 --> 00:16:53,600
It says the requirement is it should have a name attribute.

175
00:16:53,610 --> 00:17:05,760
Does your family not on it.

176
00:17:05,820 --> 00:17:07,380
This is still not working.

177
00:17:11,850 --> 00:17:14,000
For the first name it is working.

178
00:17:14,130 --> 00:17:16,160
The last thing which is not working.

179
00:17:18,630 --> 00:17:20,140
Lose the focus it's talking.

180
00:17:20,140 --> 00:17:21,990
OK let's do one thing.

181
00:17:22,120 --> 00:17:24,910
Let us come in this off.

182
00:17:25,020 --> 00:17:28,160
You're trying to bind everything to the same property.

183
00:17:28,230 --> 00:17:30,090
Maybe that is causing some confusion.

184
00:17:34,060 --> 00:17:34,560
Oh sorry.

185
00:17:34,570 --> 00:17:42,890
This is not a comment I want.

186
00:17:43,310 --> 00:17:50,950
Let's delete this.

187
00:17:50,980 --> 00:17:52,810
I remember one more problem is

188
00:17:56,180 --> 00:17:57,930
you have to actually give name.

189
00:18:04,340 --> 00:18:06,360
If you don't give a name it will not work.

190
00:18:08,930 --> 00:18:15,560
Especially when we are using the modem we know we are using the T-Mobile it is compulsory to provide

191
00:18:15,560 --> 00:18:15,970
name.

192
00:18:16,040 --> 00:18:24,820
Remember this a name is always given the name an element is actually useless when you see it and we

193
00:18:24,820 --> 00:18:28,140
see x y z z x y z.

194
00:18:29,440 --> 00:18:33,460
So very simple and straightforward syntax.

195
00:18:33,620 --> 00:18:34,760
We don't need this

196
00:18:38,320 --> 00:18:52,210
immortal syntax and by particular property of immortal object which is in a property in the component.

197
00:18:52,260 --> 00:18:54,160
So this is what I've done actually.

198
00:18:55,370 --> 00:18:56,430
Input element.

199
00:18:56,500 --> 00:19:03,110
Oh you're also there is error we should have known without naming a lot.

200
00:19:03,110 --> 00:19:04,390
We'll get into it we give

201
00:19:12,690 --> 00:19:19,680
and then you've been arguing for some gloss that a good person parsnip or some last name or two properties

202
00:19:20,100 --> 00:19:22,020
of the component which we are finding it

203
00:19:25,640 --> 00:19:34,830
the details are specific to each kind of element and therefore all that to valks for specific form elements

204
00:19:34,890 --> 00:19:35,700
only.

205
00:19:35,730 --> 00:19:42,600
That means it is not going to work for all the elements in the form where would is sensible then only

206
00:19:42,600 --> 00:19:49,530
we can probably make and often you know it does more important text box part passport check box day.

207
00:19:49,640 --> 00:19:57,210
These are all controls we have implemented the interface control value Access and so in the model will

208
00:19:57,210 --> 00:20:02,640
work only for those controls which are implemented control value access.

209
00:20:02,850 --> 00:20:05,200
And what is the purpose of this.

210
00:20:05,200 --> 00:20:10,290
It is the one which is going to print the control and the native element

211
00:20:14,080 --> 00:20:16,270
but right now what is the problem.

212
00:20:16,420 --> 00:20:24,520
When we are using in the model we are getting the value from our standard for me and we are also setting

213
00:20:24,520 --> 00:20:26,910
the value in parts and or cost them on me.

214
00:20:26,920 --> 00:20:32,560
I mean last name in this case we are getting the value from person not last name and we're also setting

215
00:20:32,560 --> 00:20:34,960
the value in parts and the plastic.

216
00:20:35,340 --> 00:20:37,590
The single propertyless binding.

217
00:20:37,900 --> 00:20:44,650
But what if in case you want to do something more as I said earlier in that kind of case you're supposed

218
00:20:44,650 --> 00:20:46,890
to use this index.

219
00:20:46,900 --> 00:20:49,990
Remember I told you earlier and you can be persuaded.

220
00:20:50,010 --> 00:20:56,660
I mean the double brackets and tax can be used only if the property is there by name x and then and

221
00:20:56,670 --> 00:21:02,620
then even by name exchange that's all.

222
00:21:02,920 --> 00:21:05,670
So now we're going to do something like that.

223
00:21:05,860 --> 00:21:11,760
What we will do say this I'll change it here I'll give any more.

224
00:21:12,170 --> 00:21:13,990
We don't want brackets.

225
00:21:14,020 --> 00:21:18,140
So although to the contrary of course and that last name will that reflecting in the model.

226
00:21:18,190 --> 00:21:19,240
Yes.

227
00:21:19,540 --> 00:21:23,830
But next I'm going to write for the other do if we want to make it.

228
00:21:24,020 --> 00:21:30,640
So year I'll stay in the model T and then X and exchange.

229
00:21:30,720 --> 00:21:31,870
That's how it is.

230
00:21:31,900 --> 00:21:39,990
And this Schulin on Bracken's equal to what we want to be we will say

231
00:21:44,120 --> 00:21:51,260
we want to convert let's say person not last name to uppercase of value which is typed in the text box.

232
00:21:51,320 --> 00:21:59,150
So actually speaking it is like this by default this syntax is equivalent to writing something like

233
00:21:59,150 --> 00:22:08,300
this is equal to only DOLLERY and know don't even remember them more than what is the value of this

234
00:22:08,300 --> 00:22:16,020
text box no dollar even is no more going to refer to the input element dollar event because of this.

235
00:22:16,010 --> 00:22:25,760
And the moral here is not going to the effort to day by itself so that it was only to the last minute.

236
00:22:25,850 --> 00:22:29,230
So all of this syntax is this.

237
00:22:29,360 --> 00:22:35,060
So in sort of writing an elaborated form like this really providing the shortcut like this.

238
00:22:35,120 --> 00:22:42,770
So when you think the syntax is going to be really useful when you want to get something and do some

239
00:22:42,770 --> 00:22:49,940
processing and then as an idiom or when you want to do some processing on this and as only to the property

240
00:22:50,570 --> 00:22:57,780
for example you or I would like to tend the case imagine what they wanted the value of the text box.

241
00:22:57,860 --> 00:23:03,150
I want to convert that to uppercase and as to the last thing that's what I want to do.

242
00:23:03,520 --> 00:23:05,650
So let's say change

243
00:23:08,800 --> 00:23:16,980
parties and what it means to uppercase it is supposed to be of function in the comprend class.

244
00:23:17,240 --> 00:23:18,070
So you.

245
00:23:19,360 --> 00:23:33,470
In to guess what the value and return type the string values of papes string or they don't want you

246
00:23:33,470 --> 00:23:36,980
not to Orkus

247
00:23:41,510 --> 00:23:43,890
know let's run this and you will see the difference.

248
00:23:47,540 --> 00:23:58,320
You'll see at the moment they type it does become uppercase the last name is on in uppercase no caps

249
00:23:58,320 --> 00:24:01,100
lock upper case.

250
00:24:01,320 --> 00:24:05,000
I switch off caps lock about keys.

251
00:24:05,140 --> 00:24:11,820
So in either case it is uppercase only so in those kind of cases where we want to do something extra

252
00:24:12,690 --> 00:24:20,580
that is when precisely we would like to separate them but otherwise a singleton syntax is WREFORD

253
00:24:26,660 --> 00:24:29,600
c of this is this

254
00:24:34,170 --> 00:24:38,670
and more syntax can be said to do one property only.

255
00:24:38,680 --> 00:24:45,230
And if you want to do something more or something different then in such cases compulsory you have to

256
00:24:45,230 --> 00:24:46,510
use the syntax on the

257
00:24:50,340 --> 00:24:52,070
very very important.

258
00:24:52,310 --> 00:24:58,170
Yet you see I'm not writing I'm just calling it function an index function via setting the value of

259
00:24:58,170 --> 00:24:59,800
the property even though it is fine.

260
00:25:01,910 --> 00:25:04,470
So this is the binding is done.

261
00:25:04,490 --> 00:25:10,740
One is the simple to rebinding where we take the value of the input element and set the value for it.

262
00:25:10,900 --> 00:25:15,500
Take the input event and in response to that even people on the action.

263
00:25:15,770 --> 00:25:18,780
But this syntax is not going to always work.

264
00:25:19,100 --> 00:25:21,850
We want to have better syntax and more simple one.

265
00:25:21,860 --> 00:25:30,420
That is indeed modal syntax but for it to include first form support you want to be an important opportunity

266
00:25:30,430 --> 00:25:31,420
to do more.

267
00:25:31,820 --> 00:25:39,100
And often that they can simply make use of any moral character Windows Forms.

268
00:25:39,140 --> 00:25:44,270
You cannot make use of more than that active because the corresponding classes are available in the

269
00:25:44,270 --> 00:25:50,350
form's module library only.

270
00:25:50,480 --> 00:25:51,540
That's all in this.

271
00:25:51,680 --> 00:25:52,050
Thank you.

