1
00:00:16,430 --> 00:00:24,900
In the previous session I've created a new MT project a seed project I did with an Employee class which

2
00:00:24,900 --> 00:00:33,320
is the model I did imply component and then for the employee component we have all corresponding hits

3
00:00:33,330 --> 00:00:40,680
Steimle template and that template has got different in Patrolmen's we initially bind them to you.

4
00:00:40,710 --> 00:00:43,030
And finally we did do it binding.

5
00:00:43,170 --> 00:00:45,460
This is what we completed in the last person.

6
00:00:45,840 --> 00:00:51,450
So let us see how we can track changes the detail is in the form.

7
00:00:52,060 --> 00:00:56,810
And as the data is stinting we would like to perform certain validations.

8
00:00:56,910 --> 00:01:05,080
So yes if it would eat up we are going to have certain styles no.

9
00:01:05,660 --> 00:01:13,770
So indeed that is the style cloth and the duty the style cloth and the reality the style cloth woven

10
00:01:13,800 --> 00:01:15,220
are these glasses.

11
00:01:16,140 --> 00:01:24,060
Indeed that is true when the control has been busy that if you have visited the control indeed that

12
00:01:24,090 --> 00:01:26,630
cloth will be there for that particular attack.

13
00:01:27,120 --> 00:01:32,870
But that indeed that is false.

14
00:01:32,900 --> 00:01:34,810
If the control is not with it.

15
00:01:35,230 --> 00:01:41,680
I mean it is untouched it is visited it becomes touched it is not with it that it is untouched like

16
00:01:41,680 --> 00:01:44,340
that control value has changed.

17
00:01:44,340 --> 00:01:50,520
Then it is indeed dirty but if the control really was not teens then it is indeed pristine.

18
00:01:50,530 --> 00:01:55,170
Likewise if the validation is taken is that validation rules are there.

19
00:01:55,480 --> 00:01:58,250
If the control is valid it is indeed valid.

20
00:01:58,300 --> 00:02:01,110
And if it is not it is indeed invalid.

21
00:02:01,120 --> 00:02:09,940
So these are all SS classes which are going to be existing for the controls in the fall.

22
00:02:09,940 --> 00:02:12,370
Let me demonstrate this with an example.

23
00:02:12,730 --> 00:02:18,340
What I would like to do is first I already have a demon file.

24
00:02:18,670 --> 00:02:28,090
Let's take name and here I'm going to give now required and also let us say we provide a of what the

25
00:02:28,090 --> 00:02:28,980
pattern is.

26
00:02:29,050 --> 00:02:36,970
It should start with either case equals it or uppercase A to Z and then other ways also works along

27
00:02:36,970 --> 00:02:45,600
with 0 2 9 and at least there should be one or more characters that the pattern we are given right now.

28
00:02:45,640 --> 00:02:46,630
If I run this

29
00:02:51,800 --> 00:02:55,160
if I change this the state has changed Octon.

30
00:02:55,460 --> 00:02:57,020
But how do I see that state.

31
00:02:57,080 --> 00:02:59,490
I want to see that state for that.

32
00:02:59,510 --> 00:03:02,390
What we'll do is only

33
00:03:09,230 --> 00:03:14,060
We'll get now we need to reference to this particular element.

34
00:03:14,110 --> 00:03:25,120
The best thing to do is go for reference ENP name here and then just put the name dot the last name

35
00:03:29,490 --> 00:03:34,850
your name becomes the reference to the reference we are printing the class name here.

36
00:03:37,110 --> 00:03:42,010
C is this class knowledgeable on this.

37
00:03:42,730 --> 00:03:44,380
Yes.

38
00:03:44,380 --> 00:03:53,630
So right now the state is going to be form control in the untouched and pristine and indeed invalid

39
00:03:54,400 --> 00:04:02,560
why invalid because there is space and we are not allowed space C No because of remote space it became

40
00:04:02,560 --> 00:04:06,270
indeed dirty.

41
00:04:06,350 --> 00:04:08,020
It is indeed dirty.

42
00:04:08,160 --> 00:04:19,580
It is valid and it is just it is not valid and and so because we are not given spacier we would like

43
00:04:19,580 --> 00:04:23,370
to allow space on patent.

44
00:04:23,530 --> 00:04:26,890
We should give space.

45
00:04:26,970 --> 00:04:27,850
Let's see no

46
00:04:31,000 --> 00:04:32,010
because space is a.

47
00:04:32,020 --> 00:04:38,390
No it is indeed valid indeed untouched and deep pristine.

48
00:04:38,510 --> 00:04:41,060
So this is all styles of play.

49
00:04:41,190 --> 00:04:47,290
What we can do is based on these times we can write certain attributes.

50
00:04:47,300 --> 00:04:53,800
I mean we can write some see it as if it is valid what to do if it is invalid what to do.

51
00:04:53,840 --> 00:05:06,980
And so so when it is indeed valid I would like to provide a green color here and then it is not valid.

52
00:05:06,990 --> 00:05:10,180
I would like to provide something like a regular day.

53
00:05:10,880 --> 00:05:12,320
So I hope to do that.

54
00:05:14,030 --> 00:05:15,410
So lets do one thing.

55
00:05:18,790 --> 00:05:25,720
Maybe the root or ideally the standard practice followed is we create a folder

56
00:05:28,640 --> 00:05:34,280
called us does content and are

57
00:05:37,300 --> 00:05:38,200
a sci fi

58
00:05:44,350 --> 00:05:51,070
let's call it just forms don't see it as I would like to provide the answers.

59
00:05:51,250 --> 00:05:56,970
But what is the C S's daughter in the valley.

60
00:05:59,780 --> 00:06:08,470
When it is Violet I would like to give us time somebody we want right there is left border to border

61
00:06:08,650 --> 00:06:19,340
left let's say some take less so five pixel solid and maybe green color like that.

62
00:06:19,340 --> 00:06:22,690
I'll give no dot.

63
00:06:26,170 --> 00:06:26,820
Vollard

64
00:06:29,430 --> 00:06:34,270
not for what he invented.

65
00:06:37,120 --> 00:06:39,220
Let's not give this first and see what happens.

66
00:06:39,290 --> 00:06:50,380
So you know we won't bother left same five pixel solid red color

67
00:06:56,950 --> 00:07:02,170
now we need to include this in Ed's team will find a good team and find

68
00:07:06,470 --> 00:07:10,990
and we include those.

69
00:07:11,090 --> 00:07:12,040
SEE IS THIS

70
00:07:19,350 --> 00:07:20,370
activities in.

71
00:07:20,440 --> 00:07:25,490
So you don't even need dot dot hoops last contend dot dot is.

72
00:07:25,520 --> 00:07:26,810
Now let's see what is happening.

73
00:07:30,290 --> 00:07:31,970
By default it is valid.

74
00:07:32,180 --> 00:07:37,150
So we should get green green and all green borders are coming.

75
00:07:37,670 --> 00:07:46,330
This is because even foreign tech has to stay even fought for forwards so this will be valid.

76
00:07:46,990 --> 00:07:55,000
So what I do know is I'll put a restriction I don't want for fall other than fall we want wherever it

77
00:07:55,000 --> 00:07:59,530
is mentioned as it required specifically for required.

78
00:07:59,530 --> 00:08:00,740
We want to help.

79
00:08:00,760 --> 00:08:03,170
So is the condition yes.

80
00:08:06,110 --> 00:08:12,690
It is quite.

81
00:08:16,940 --> 00:08:17,630
Fantastic

82
00:08:22,210 --> 00:08:25,880
same required attribute we can actually give for the other elements also.

83
00:08:25,890 --> 00:08:28,470
So that consistently we can get it everywhere.

84
00:08:28,870 --> 00:08:30,000
So IDs required.

85
00:08:30,040 --> 00:08:31,020
Yes.

86
00:08:31,030 --> 00:08:38,740
So we are required salary's required and this is also required not on this

87
00:08:43,200 --> 00:08:49,380
fantastic remove this ok.

88
00:08:49,680 --> 00:08:50,620
Same problem.

89
00:08:51,780 --> 00:08:55,260
As soon as they remove the bottom is coming right up.

90
00:08:55,290 --> 00:08:56,550
That is for foam.

91
00:08:56,790 --> 00:09:01,040
But here we are not kidding about what to do.

92
00:09:01,050 --> 00:09:11,670
So first what we do is we don't want this stay for for supportive Coolum not for.

93
00:09:12,130 --> 00:09:13,360
I mean other than for

94
00:09:17,300 --> 00:09:18,130
one on this

95
00:09:24,000 --> 00:09:24,810
fantastic

96
00:09:35,600 --> 00:09:36,590
know what is next

97
00:09:39,420 --> 00:09:48,530
let me explain once again what I did here in de-evolved wherever we have required to be and if there

98
00:09:48,530 --> 00:09:54,090
is a class in de-valued then we are putting this stuff on.

99
00:09:54,170 --> 00:10:03,300
Similarly when the class is indeed invalid except for a form we are going to operate a regular state.

100
00:10:03,530 --> 00:10:06,660
That's what is done here.

101
00:10:06,770 --> 00:10:08,770
All these are pretty well covered here.

102
00:10:16,880 --> 00:10:17,580
Next thing is.

103
00:10:17,760 --> 00:10:21,990
So I'll hide validation error messages.

104
00:10:21,990 --> 00:10:26,610
See if we can leverage in the invalid class to levy a helpful message.

105
00:10:27,090 --> 00:10:28,890
Indeed valid indeed invalid.

106
00:10:29,010 --> 00:10:30,730
We have these classes right.

107
00:10:30,870 --> 00:10:41,060
We can use this word willing to help put my seat not just think what we have to do is this template

108
00:10:41,060 --> 00:10:42,970
reference which we have used.

109
00:10:43,190 --> 00:10:45,530
We can simply assent to it anymore.

110
00:10:46,250 --> 00:10:47,590
What is that one.

111
00:10:48,110 --> 00:10:57,720
In such case when we don't have to write Daut formed or control is not the name we can get access of

112
00:10:57,780 --> 00:11:07,380
instance of Endemol access of instance of the model by using a local reference template variable like

113
00:11:07,380 --> 00:11:09,550
this.

114
00:11:09,760 --> 00:11:17,260
So I'll say this not only Infinium was work I'll give it just in the name of the Kindle and the more

115
00:11:18,280 --> 00:11:22,790
we can get it off we don't need this.

116
00:11:22,960 --> 00:11:27,620
And here we know all the rights are that.

117
00:11:28,730 --> 00:11:34,560
Now the purpose of this tag is yet I'll give some cloth first.

118
00:11:34,870 --> 00:11:42,880
There is some alert clause which is for dental.

119
00:11:43,640 --> 00:11:47,550
No idea here is in this we are going to provide it.

120
00:11:47,580 --> 00:11:48,730
Don't miss it.

121
00:11:49,020 --> 00:11:51,090
Name is required.

122
00:11:53,210 --> 00:11:54,970
But right no name is required.

123
00:11:54,980 --> 00:11:56,870
It's going to be all displayed

124
00:12:00,270 --> 00:12:01,500
name is required is displayed.

125
00:12:01,560 --> 00:12:03,350
And they don't want it to be displayed.

126
00:12:03,780 --> 00:12:06,900
If the data is valid or even that it is invalid.

127
00:12:06,900 --> 00:12:07,880
It should be displayed.

128
00:12:07,920 --> 00:12:09,720
That is precisely the requirement.

129
00:12:12,340 --> 00:12:23,650
So what I do know is give your binding to who don't want your name because the name is not more than

130
00:12:23,980 --> 00:12:32,500
we can directly do binding ENP name dot Van let's run this.

131
00:12:36,140 --> 00:12:42,760
Buffer remove this value it comes up provided it's gone

132
00:12:49,100 --> 00:12:50,080
what.

133
00:12:50,720 --> 00:13:02,920
But my requirement also here is what if the employee is not having any initial value Lexi I go to the

134
00:13:02,920 --> 00:13:13,380
company and they give your empty life on this it is in line and immediately the name is required.

135
00:13:13,410 --> 00:13:18,810
I don't want this last I do not want name is required to be there.

136
00:13:19,530 --> 00:13:27,710
So I hope to do that he doesn't rule out putting two conditions when his name is valid and also say

137
00:13:27,740 --> 00:13:30,780
the MP name got pristine

138
00:13:33,380 --> 00:13:35,760
in these two conditions we are.

139
00:13:35,820 --> 00:13:36,780
Pristine.

140
00:13:36,850 --> 00:13:41,440
So first time it is pristine not on this

141
00:13:45,170 --> 00:13:50,550
perfect come your go out for the volume.

142
00:13:50,610 --> 00:13:53,710
It's not pristine though hide it.

143
00:13:53,850 --> 00:13:55,000
See we're getting it

144
00:13:59,500 --> 00:14:00,800
this is what we wanted.

145
00:14:02,240 --> 00:14:09,430
But what if I give you a Lexus.

146
00:14:09,440 --> 00:14:12,200
Stop stop stop stop stop.

147
00:14:12,200 --> 00:14:14,460
Problem is it is valid.

148
00:14:14,780 --> 00:14:21,520
I mean we are providing them but problem is it is not valid because of Boston but not because of required

149
00:14:21,560 --> 00:14:23,040
attribute.

150
00:14:23,180 --> 00:14:25,050
There are two reasons here.

151
00:14:25,610 --> 00:14:28,200
One is Parklane and the other is required.

152
00:14:28,370 --> 00:14:31,190
Right now this politician is succeeding.

153
00:14:31,400 --> 00:14:35,800
But this Validus and is feeling when they give any value here.

154
00:14:37,610 --> 00:14:45,050
All that's fine but not some specific articles if I use it is having that or that means required and

155
00:14:45,050 --> 00:14:47,460
succeeding back in this field.

156
00:14:47,890 --> 00:14:53,290
So I would like to have a different message for pattern and different message would require.

157
00:14:53,420 --> 00:14:57,570
So your input often leads them to say more.

158
00:14:58,030 --> 00:15:00,500
You'll be Neame door.

159
00:15:00,640 --> 00:15:05,260
There will be an array of errors.

160
00:15:05,320 --> 00:15:09,450
This is a list of all those which are dead in the name of that.

161
00:15:09,490 --> 00:15:11,200
I'm going to take this required

162
00:15:13,670 --> 00:15:17,130
Likewise we will not go for

163
00:15:20,120 --> 00:15:23,630
Meem cannot contain

164
00:15:27,030 --> 00:15:32,580
does.

165
00:15:32,840 --> 00:15:38,130
So this is not too far to the robot.

166
00:15:38,130 --> 00:15:41,710
It is not.

167
00:15:42,230 --> 00:15:43,060
More on this

168
00:15:47,210 --> 00:15:50,180
onger give an invalid name

169
00:15:58,790 --> 00:16:00,060
something is wrong.

170
00:16:01,150 --> 00:16:02,380
Name is required.

171
00:16:06,800 --> 00:16:18,150
Lucila classes as they are working.

172
00:16:18,210 --> 00:16:23,960
You might not get anything because we have given you indeed more than

173
00:16:28,390 --> 00:16:29,040
that.

174
00:16:29,300 --> 00:16:38,540
Christine is back to a low key domestic button to once again

175
00:16:42,390 --> 00:16:43,470
good value.

176
00:16:44,690 --> 00:16:45,710
No problem.

177
00:16:47,180 --> 00:16:47,890
Name is required.

178
00:16:47,890 --> 00:16:48,380
Good.

179
00:16:48,490 --> 00:16:50,090
Good start.

180
00:16:50,090 --> 00:16:51,920
Name cannot contain space characters

181
00:16:55,290 --> 00:16:57,940
so this is all we can show and hide.

182
00:17:00,820 --> 00:17:04,200
See you don't you can also use.

183
00:17:04,210 --> 00:17:09,400
Indeed if but the condition has to be reversed no with.

184
00:17:09,730 --> 00:17:22,560
Indeed if it's opposite of the so like this using Stine's NCSA Plus the all classes require pristine

185
00:17:22,740 --> 00:17:24,210
valid invalid.

186
00:17:24,690 --> 00:17:31,110
All these are different properties and they all become properties because we have given here for the

187
00:17:31,110 --> 00:17:39,100
template reference Dodik and Dimeola And with that template reference name we are directly able to access

188
00:17:39,100 --> 00:17:41,730
it.

189
00:17:41,760 --> 00:17:44,470
So this is very very very interesting.

190
00:17:44,470 --> 00:17:49,360
Aren't we just so easy to show and hide the error messages.

191
00:17:49,510 --> 00:17:52,420
We don't know the properties.

192
00:17:52,630 --> 00:17:53,720
That's all it is.

