1
00:00:16,590 --> 00:00:24,540
Using Formby basically using form builder we can avoid writing a lot of code specially to create each

2
00:00:24,540 --> 00:00:30,260
and every control inside the form and then create a form group out of it.

3
00:00:30,270 --> 00:00:36,660
We have that element utility function inside form builder with which we can make things much easier

4
00:00:36,720 --> 00:00:37,650
and quicker.

5
00:00:38,040 --> 00:00:44,780
So first for this we will have to add form builder class artist form builder.

6
00:00:45,280 --> 00:00:54,840
Now once we add form in our company plus we should I think construct of this construct is supposed to

7
00:00:54,840 --> 00:01:01,460
have a form builder injected into it right.

8
00:01:01,470 --> 00:01:04,830
No we did not loan dependency injection.

9
00:01:05,060 --> 00:01:13,340
But is this particular syntax is going to indeed form builder into the constructor of some phone company.

10
00:01:13,730 --> 00:01:16,480
So now I can make use of this as B anyway.

11
00:01:16,630 --> 00:01:18,300
In my project.

12
00:01:19,010 --> 00:01:27,380
So what I would want to do is I want to actually replace this and this is where we are explicitly creating

13
00:01:27,380 --> 00:01:28,800
form control.

14
00:01:29,080 --> 00:01:29,390
Right.

15
00:01:29,420 --> 00:01:38,020
All that I would like to replace but I'll actually come does not we don't need all this a socket socket

16
00:01:38,450 --> 00:01:39,750
is what I would like to write.

17
00:01:39,750 --> 00:01:42,630
No no what is that socket code.

18
00:01:42,770 --> 00:01:57,310
This dog or some form is equal to this dot for Embedded of dot group not in this group we are supposed

19
00:01:57,310 --> 00:01:59,010
to give a value bet.

20
00:01:59,380 --> 00:02:01,180
That means we are supposed to give an added

21
00:02:04,030 --> 00:02:06,360
Say not any I mean a dictionary.

22
00:02:06,590 --> 00:02:08,610
We would like to get a dictionary.

23
00:02:08,990 --> 00:02:13,600
So in dictionary we will give all the members of the form.

24
00:02:13,680 --> 00:02:22,010
So first name and its value or the value is supposed to be in any event the first member is supposed

25
00:02:22,010 --> 00:02:26,750
to be the initial value of Lexis and beeps only.

26
00:02:27,280 --> 00:02:33,240
And the second is supposed to be all the validations that is again it.

27
00:02:33,550 --> 00:02:46,930
So I'd say validate those dot require and come up validate those got done and what button we want we

28
00:02:46,930 --> 00:02:54,850
want either into the lower case and upper case or 0 9.

29
00:02:55,060 --> 00:02:59,750
At least one character so like this

30
00:03:03,120 --> 00:03:12,580
I would like to have no first name come up last name the same story I would like to give

31
00:03:17,230 --> 00:03:19,150
Does that we don't want

32
00:03:24,890 --> 00:03:33,290
and then the other two members we talk to and I'll just give me

33
00:03:37,210 --> 00:03:48,090
Cosmo qualification and just give that said Because that's it.

34
00:03:48,080 --> 00:03:51,200
This shortcut does the same thing.

35
00:03:51,230 --> 00:03:56,680
What we have done in detail earlier explicitly create the object and set it up.

36
00:03:56,690 --> 00:03:59,890
Here we are creating it in a little different sandbox.

37
00:04:00,080 --> 00:04:01,680
Basically we are creating a dictionary.

38
00:04:01,730 --> 00:04:04,030
And we should still be able to achieve the same

39
00:04:06,730 --> 00:04:08,250
Something should have gone wrong.

40
00:04:08,280 --> 00:04:09,420
Let's see what is it.

41
00:04:11,810 --> 00:04:16,150
Cannot read the property in inverted of undefined.

42
00:04:16,400 --> 00:04:16,790
Yes.

43
00:04:16,820 --> 00:04:22,790
One more thing we have to take care of it is when we are using form build up we cannot directly make

44
00:04:22,790 --> 00:04:29,710
use of first name dot first name dot in ballot and so on we are not supposed to use person named dot

45
00:04:29,720 --> 00:04:31,290
controls of first name.

46
00:04:31,340 --> 00:04:33,800
This extra precaution Toti.

47
00:04:33,940 --> 00:04:39,130
So whenever I had first name invalidations person that is here.

48
00:04:40,740 --> 00:04:48,310
First name I'll replace that with force and Dot control of first name dot in the same year.

49
00:04:48,440 --> 00:04:49,440
Same here.

50
00:04:51,810 --> 00:04:52,860
Same thing.

51
00:04:53,970 --> 00:04:55,500
Multiple places we have quoted

52
00:05:10,800 --> 00:05:13,010
likewise last name also.

53
00:05:32,540 --> 00:05:35,370
So let's run this once again control the fight.

54
00:05:36,080 --> 00:05:38,690
And this time we should get all the data

55
00:05:41,930 --> 00:05:45,120
first name last name gender

56
00:05:48,350 --> 00:05:49,760
qualification is not coming.

57
00:05:49,840 --> 00:05:50,780
Gender is not coming.

58
00:05:50,800 --> 00:05:51,780
Why.

59
00:05:52,070 --> 00:05:55,970
Maybe the initialization code is not actually called.

60
00:05:56,110 --> 00:06:00,840
So here we have some Ixtoc code which is in Palm controls.

61
00:06:11,200 --> 00:06:18,620
Nothing is wrong here so it's DMA and last name.

62
00:06:18,730 --> 00:06:21,380
Case same problem.

63
00:06:21,490 --> 00:06:27,880
I still do not have a first name dot rose with the last name at all.

64
00:06:27,910 --> 00:06:30,820
This line is to be the biggest

65
00:06:33,820 --> 00:06:34,630
note on this

66
00:06:40,860 --> 00:06:43,010
this mail is initialized.

67
00:06:43,160 --> 00:06:47,140
Default really will be because we got it and it's all functional.

68
00:06:47,570 --> 00:06:48,890
So what does that do with it.

69
00:06:48,890 --> 00:06:56,410
We just replaced all the old all create form controls and create Farmville.

70
00:06:57,070 --> 00:07:07,720
Now what we want to achieve this does not force game don't change for this we have to write Dizdar person

71
00:07:08,380 --> 00:07:12,560
form or get first not wanting us.

72
00:07:12,730 --> 00:07:19,100
So I just copy this as a piece and I dig it on top

73
00:07:21,810 --> 00:07:26,030
and in sort of writing this got first name when it does Daut

74
00:07:28,760 --> 00:07:35,540
person from DOT get off first thing

75
00:07:38,350 --> 00:07:41,060
and we can still continue to do this.

76
00:07:44,370 --> 00:07:45,160
Not on this

77
00:07:52,440 --> 00:07:59,900
type you had the Raid Raid and the raid.

78
00:08:00,430 --> 00:08:02,950
Okay perfect.

79
00:08:06,630 --> 00:08:16,380
So fun basically is a replacement do explicit creation of the form group on form control.

80
00:08:16,480 --> 00:08:23,720
Otherwise it provides the same impact or same outcome as those things that all of this.

81
00:08:23,860 --> 00:08:24,270
Thank you.

