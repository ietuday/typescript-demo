var Hello = /** @class */ (function () {
    function Hello() {
        this.sayHello = function () {
            return "Hello" + " " + this.firstName + " " + this.lastName;
        };
    }
    return Hello;
}());
// function sayHello(firstName:string, lastName:string): string{
//     var message: string = 'Hello World';
//     message += " " + firstName + " " + lastName;
//     return message;
// }
// alert(sayHello("Udayaditya", "Singh"));
var h = new Hello();
h.firstName = "Udayaditya";
h.lastName = "Singh";
alert(h.sayHello());
