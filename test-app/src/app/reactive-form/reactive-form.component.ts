import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';
import { PersonReactive } from '../Person-Reactive';
import { UpperCaseValidator } from '../upper.validator';


@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {
  person: PersonReactive
  firstName: FormControl;
  lastName: FormControl;
  gender: FormControl;
  qualification: FormControl;
  personForm: FormGroup;
  degrees: Array<string> = ['BCOM', 'BA', 'BE', 'MBBS'];
  message: string = '';
  changes: Array<string> = [];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.person = new PersonReactive();
    this.person.firstName = "ABCD";
    this.person.lastName = "XYZ";
    this.person.gender = "female";
    this.person.qualification = "MBBS";

    // this.createFormControls();
    // this.createForm();

    //SetValue : If all property wants to set 
    // this.personForm.setValue({
    //   firstName : this.person.firstName,
    //   lastName: this.person.lastName,
    //   gender: this.person.gender,
    //   qualification: this.person.qualification
    // })

    //patchValue: If Some property wants to set
    // this.personForm.patchValue({
    //   firstName : this.person.firstName,
    //   lastName: this.person.lastName,
    //   // gender: this.person.gender,
    //   qualification: this.person.qualification
    // })

    //with the help of formBuilder we did not need to write explicit intilise form control
    this.personForm = this.fb.group({
      firstName: ['Udayaditya', [Validators.required, Validators.pattern('[a-zA-z0-9 ]+')]],
      lastName: ['Singh', [Validators.required, Validators.pattern('[a-zA-z0-9 ]+')]],
      gender: ['male'],
      qualification: ['BCOM'],
      myname:["",[UpperCaseValidator]]
    })
    debugger;

    this.personForm.get('firstName').valueChanges.pipe(debounceTime(500), distinctUntilChanged()).subscribe((change) => {
      console.log("Changes", change);
      this.changes.push(change);
    })
  }

  // createFormControls(){
  //   this.firstName = new FormControl("Udayaditya",[Validators.required, Validators.pattern('[a-zA-z0-9 ]+')]);
  //   this.firstName.valueChanges.pipe(debounceTime(500),distinctUntilChanged()).subscribe((change) => {
  //     console.log("Changes",change);  
  //     this.changes.push(change);
  //   })
  //   this.lastName = new FormControl("Singh",[Validators.required, Validators.pattern('[a-zA-z0-9 ]+')]);
  //   this.gender = new FormControl("male");
  //   this.qualification = new FormControl("BE");
  // }

  // createForm(){
  //   this.personForm = new FormGroup({
  //     firstName: this.firstName,
  //     lastName: this.lastName,
  //     gender: this.gender,
  //     qualification: this.qualification
  //   });
  // }

  onSubmit(form: any) {
    console.log("44444444444");

    if (this.personForm.valid) {
      this.message = 'Form is Valid'
    } else {
      this.message = 'Form is Invalid'
    }
    console.log(form);

    //After passing data to the server we need to reset the form
    this.personForm.reset();

  }
}
