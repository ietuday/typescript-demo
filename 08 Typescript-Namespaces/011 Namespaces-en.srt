1
00:00:16,060 --> 00:00:24,210
Name spaces so in your chapter I've explained to you about what is immobile in this particular thing

2
00:00:24,210 --> 00:00:28,620
we are going to see something similar caller's namespace.

3
00:00:29,370 --> 00:00:34,530
So name spaces are previously termed as internal modules.

4
00:00:34,680 --> 00:00:41,310
So what we have covered all the way they were before the role models and namespace is really there for

5
00:00:41,310 --> 00:00:44,230
us in terms of modules.

6
00:00:44,660 --> 00:00:53,490
So now they are reforest namespace namespace is our primary named javascript object in global namespace

7
00:00:53,510 --> 00:00:55,380
that is important.

8
00:00:55,490 --> 00:01:03,880
That means namespace is not going to restrict the type to that particular namespace only unlike modules

9
00:01:03,880 --> 00:01:05,010
will do it.

10
00:01:05,330 --> 00:01:11,760
Model is going to always restrict a type to that particular namespace is not going to do that.

11
00:01:13,100 --> 00:01:19,660
Then what is namespace used for namespace is only used for avoiding name collisions.

12
00:01:19,670 --> 00:01:27,500
Suppose you have got two different types but if both of them have same name then we are going to make

13
00:01:27,500 --> 00:01:30,970
use of namespace to resolve the ambiguity.

14
00:01:31,020 --> 00:01:35,250
Obviously as we have seen earlier models can also do that.

15
00:01:35,690 --> 00:01:39,430
But the namespace is also one of the choice for doing it.

16
00:01:39,500 --> 00:01:48,440
It can span over multiple files unlike modules namespace can span or multiple Fynes namespace can be

17
00:01:48,440 --> 00:01:55,130
a good way to structure the code in which obligation with our dependencies included in this group in

18
00:01:55,130 --> 00:01:56,620
your head statement.

19
00:01:57,540 --> 00:02:05,150
We see that as an example all exported types are visible outside the namespace types used outside the

20
00:02:05,150 --> 00:02:08,450
namespace must be qualified by its namespace.

21
00:02:08,450 --> 00:02:10,220
Pretty obvious compulsory here.

22
00:02:10,450 --> 00:02:12,980
So if namespace is provided for a given type.

23
00:02:13,550 --> 00:02:18,800
And for you if you are using that type outside the namespace it is mandatory that you qualified by the

24
00:02:19,070 --> 00:02:27,680
namespace in the required namespace can be nested nested namespace also should be exported.

25
00:02:27,770 --> 00:02:30,310
So let us see this with the proper exam.

26
00:02:30,470 --> 00:02:32,630
So we create as usual a new project

27
00:02:42,070 --> 00:02:43,970
is be darknet of.

28
00:02:46,120 --> 00:02:46,950
Names is

29
00:02:50,410 --> 00:02:54,470
empty project only.

30
00:02:54,680 --> 00:03:01,810
And yet I'm going to like every other piece Steimle find first

31
00:03:06,980 --> 00:03:14,070
really call him the and then I'm going to add something called

32
00:03:18,190 --> 00:03:30,100
a typescript for the let's call it does I want I point but B is a little Z namespace

33
00:03:34,320 --> 00:03:37,220
followed by namespace maybe some name.

34
00:03:37,230 --> 00:03:39,540
Let's see my examples.

35
00:03:40,090 --> 00:03:50,420
And in that I'll say export in toughies I point a Before I point is made of both x

36
00:03:53,380 --> 00:03:57,370
and y which are not.

37
00:03:57,470 --> 00:04:02,160
And maybe you can also get this done wrong.

38
00:04:02,900 --> 00:04:03,490
All into

39
00:04:08,750 --> 00:04:10,520
good enough.

40
00:04:11,030 --> 00:04:15,260
So we have got a namespace under which we have got export interface.

41
00:04:15,320 --> 00:04:19,010
I don't know what I'm going to do next.

42
00:04:19,230 --> 00:04:20,280
I don't know right.

43
00:04:20,450 --> 00:04:22,010
Point dot B is

44
00:04:28,400 --> 00:04:35,130
no requirement it's pretty simple point B is should include the content for all I want.

45
00:04:35,330 --> 00:04:50,870
So you're like three slices Anglo bracket reference space but equal to the fine want to use I know we

46
00:04:50,870 --> 00:04:53,280
can write namespace

47
00:04:56,150 --> 00:04:58,770
same namespace I'm using my examples

48
00:05:01,280 --> 00:05:08,810
or double quotes and here I'm going to provide that gloss or

49
00:05:12,600 --> 00:05:16,040
no what what should I give here.

50
00:05:16,670 --> 00:05:17,770
You just given all

51
00:05:21,360 --> 00:05:22,180
like that.

52
00:05:23,450 --> 00:05:24,620
So what should I give you.

53
00:05:24,770 --> 00:05:34,730
So then you simply point yes it is fine to give a point here because board I point at point right now

54
00:05:34,740 --> 00:05:36,720
belong to the team namespace.

55
00:05:37,030 --> 00:05:43,070
But suppose I give you my examples one in such case what should I do.

56
00:05:43,320 --> 00:05:47,650
Then I would have to explicitly qualify my examples.

57
00:05:47,860 --> 00:05:50,420
One but I like that will have to come

58
00:05:54,320 --> 00:06:04,500
maybe I'll copy the implementation from the handle.

59
00:06:05,350 --> 00:06:09,520
So point is not taking care of Ex-White as well as distance running on it.

60
00:06:11,320 --> 00:06:19,960
Now I only use this board that is point and I point in is told to fight which is main Nazi is.

61
00:06:20,040 --> 00:06:22,850
So I'll add one more script.

62
00:06:23,580 --> 00:06:25,100
Let's call it mean.

63
00:06:25,260 --> 00:06:26,140
Yes.

64
00:06:26,580 --> 00:06:32,500
So what should I do in Main as source of reference.

65
00:06:32,970 --> 00:06:33,810
But does it work.

66
00:06:33,810 --> 00:06:35,170
Do I point

67
00:06:38,060 --> 00:06:39,010
Katia's

68
00:06:41,850 --> 00:06:47,700
like ways also are Point ideas and not start using it.

69
00:06:48,610 --> 00:06:52,540
Maybe this itself can be in a different namespace.

70
00:06:52,730 --> 00:06:54,300
So I'll give you a namespace

71
00:07:00,240 --> 00:07:02,240
my own.

72
00:07:03,450 --> 00:07:05,720
So what should I do know what.

73
00:07:06,940 --> 00:07:11,080
B-D what my examples.

74
00:07:11,510 --> 00:07:14,990
I point equal to new.

75
00:07:15,130 --> 00:07:17,470
My example is not quite

76
00:07:20,480 --> 00:07:26,930
what is important is it is not necessary that every file should have a different namespace namespace

77
00:07:26,930 --> 00:07:29,860
can span to multiple files so buoyant also.

78
00:07:29,970 --> 00:07:35,400
I'm putting my examples I point on so I'm putting my examples so here.

79
00:07:35,420 --> 00:07:45,080
I actually don't even need this because both belong to the same namespace and then in Main because we

80
00:07:45,080 --> 00:07:50,900
don't need this law we need it because the namespace is different

81
00:07:54,180 --> 00:08:05,240
and give you somebody songs because you are provided a constructor and in lot we can point that distance

82
00:08:05,240 --> 00:08:06,420
from it.

83
00:08:09,650 --> 00:08:13,940
So is the concept similar to that of monsoon.

84
00:08:13,970 --> 00:08:23,330
Yes but here everything is global namespace content is global and is accessible everywhere.

85
00:08:24,940 --> 00:08:30,760
Now the main question arises is in the market at Steimle bill which one should I include.

86
00:08:31,190 --> 00:08:38,600
Let's try to include only mean because first and you'll see that when exiguous this we are going to

87
00:08:38,600 --> 00:08:43,270
get an error rates as well and we will know the error.

88
00:08:43,710 --> 00:08:49,080
Preference my examples not defined what is a problem.

89
00:08:49,100 --> 00:08:55,280
Problem is we have only included mean Dardis that wins this fine but where is the declaration of my

90
00:08:55,280 --> 00:08:56,620
examples.

91
00:08:56,900 --> 00:09:00,780
My example set in is a namespace which is available in different files.

92
00:09:00,830 --> 00:09:08,480
So what we have to do is every script file we will have to include explicitly allowed to include point

93
00:09:08,480 --> 00:09:14,570
or tiers also include aiport good ideas in that order.

94
00:09:17,250 --> 00:09:23,610
Note to property fantastic regardless of lawyers.

95
00:09:24,210 --> 00:09:31,030
But this is in this process every time we want to include multiple files is a tedious process.

96
00:09:31,470 --> 00:09:32,880
So what is altered.

97
00:09:33,300 --> 00:09:33,530
Yes.

98
00:09:33,540 --> 00:09:38,840
All the three files can be combined into a single file.

99
00:09:38,850 --> 00:09:40,140
How can we do that.

100
00:09:41,540 --> 00:09:43,940
There are multiple ways of doing it.

101
00:09:43,940 --> 00:09:54,220
One is we can go to the command prompt and at the command prompt we can take the B C D C with the oppossum

102
00:09:55,360 --> 00:09:59,000
minus output file and give all the findings.

103
00:09:59,210 --> 00:10:04,720
It's going to generate one single design and then that simple does well we can include in the product

104
00:10:06,310 --> 00:10:15,370
or if you're using studio in studio you can if you have a proper project you can go to project properties

105
00:10:16,460 --> 00:10:24,990
typescript and here which should have an output fine combine Javascript can do a fine and give here

106
00:10:25,210 --> 00:10:30,370
maybe more or less when you do this.

107
00:10:30,640 --> 00:10:36,500
Here you have to only write like with a source code.

108
00:10:36,900 --> 00:10:38,880
The motor does.

109
00:10:39,550 --> 00:10:40,810
Let's see if this is running

110
00:10:44,600 --> 00:10:48,780
point is used before it's declaration.

111
00:10:49,040 --> 00:10:50,070
What would be OK.

112
00:10:50,070 --> 00:11:00,460
The rule is here we have to give the police last night to call the code that and we can also execute

113
00:11:00,460 --> 00:11:00,720
that

114
00:11:04,400 --> 00:11:06,770
that should not be OK.

115
00:11:06,880 --> 00:11:10,430
Last demoted to him and we got the

116
00:11:14,430 --> 00:11:17,170
so multiple files can be combined together.

117
00:11:18,890 --> 00:11:27,580
Rather than taking individual files and including all of them we just compiled all of them into a single

118
00:11:27,580 --> 00:11:30,790
file and single file can be included here.

119
00:11:31,030 --> 00:11:32,680
Of course there is one more option.

120
00:11:33,150 --> 00:11:38,400
You can always have a conflict or Jasen rather than using studio properties.

121
00:11:38,530 --> 00:11:44,890
And here you can write the same thing sample charges and in the extremal find you can only include sample

122
00:11:44,930 --> 00:11:49,960
charges or put file option you have to.

123
00:11:50,370 --> 00:11:53,960
So yes we can have a namespace.

124
00:11:54,120 --> 00:12:02,280
The only purpose of namespace be of use this is all the name of the time which is present within that

125
00:12:02,300 --> 00:12:03,210
namespace.

126
00:12:03,610 --> 00:12:09,150
But remember namespace are not going to restrict the school wherever we are using the file.

127
00:12:09,190 --> 00:12:17,420
We can make use of all the content within that namespace and of course namespace can span multiple files

128
00:12:17,480 --> 00:12:18,490
also.

129
00:12:19,130 --> 00:12:23,630
So it is recommended that we avoid name spaces in modern programming.

130
00:12:23,630 --> 00:12:30,440
It is recommended that we want names name places and preferably go for more details which is a better

131
00:12:30,440 --> 00:12:36,550
way of implementing because the whole project can be systematically broken into individual parts.

132
00:12:36,920 --> 00:12:37,430
Thank you.

