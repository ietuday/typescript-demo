1
00:00:16,800 --> 00:00:23,710
Working with classes in this particular module I'm going to discuss all object oriented features of

2
00:00:23,770 --> 00:00:35,130
typescript years from wasn't easy in its crib 6 javascript programmers were able to write classes using

3
00:00:35,130 --> 00:00:37,670
the object oriented features.

4
00:00:37,740 --> 00:00:43,830
How does typescript provide the same features which are compatible to all the browsers is what we are

5
00:00:43,830 --> 00:00:46,540
going to see in this particular chapter.

6
00:00:46,590 --> 00:00:50,030
We will first begin with writing a class and using a class.

7
00:00:50,430 --> 00:00:52,940
So I'll take a new project again now.

8
00:00:55,830 --> 00:00:57,360
New M-B website

9
00:01:00,380 --> 00:01:05,770
that's called colored dozen classes and objects Demel

10
00:01:10,860 --> 00:01:11,810
to it.

11
00:01:17,630 --> 00:01:32,520
A Daibes could find the daz do it page the moderated steamin include typescript in the moderated Steimle

12
00:01:32,520 --> 00:01:36,930
so that compiled version of typescript DEARDS file will be actually included.

13
00:01:37,110 --> 00:01:40,040
No we like what we won't do over here.

14
00:01:40,440 --> 00:01:46,830
So let's begin with writing a class so we have a class as the word person.

15
00:01:47,580 --> 00:01:52,040
I know we need two different properties properties of the Person class.

16
00:01:52,290 --> 00:01:53,880
So let's say first name.

17
00:01:55,550 --> 00:01:57,880
Colon string.

18
00:01:57,920 --> 00:02:01,150
Now what is important is we are not mentioning here.

19
00:02:01,170 --> 00:02:12,140
Let us not giving your lead we are just writing the first as one property last name maybe did onboard

20
00:02:20,030 --> 00:02:22,260
know these properties.

21
00:02:23,950 --> 00:02:27,760
These are three properties of the class that is for the for the time.

22
00:02:28,000 --> 00:02:31,490
I wouldn't want to actually create an object and use it.

23
00:02:31,780 --> 00:02:33,430
So how do we create the object.

24
00:02:33,520 --> 00:02:40,360
We can let the colon person is equal to new person.

25
00:02:40,360 --> 00:02:45,430
That's it not for to set the first name whatever name we want.

26
00:02:46,570 --> 00:02:47,570
Last name.

27
00:02:53,910 --> 00:02:54,910
Did about

28
00:03:00,130 --> 00:03:06,560
and we can get some ear Mundt

29
00:03:10,490 --> 00:03:11,360
date.

30
00:03:12,900 --> 00:03:13,440
That's it

31
00:03:19,310 --> 00:03:21,430
now and we can print the same thing.

32
00:03:21,460 --> 00:03:22,570
Or here again.

33
00:03:22,960 --> 00:03:28,710
But instead of printing like this what I would like to do is I would like to have amatory or class that

34
00:03:29,320 --> 00:03:36,700
show and so is supposed to show the date of the current object.

35
00:03:36,820 --> 00:03:42,240
So how do we access properties of the current object to access the properties of the current object

36
00:03:42,250 --> 00:03:46,550
we should use discovered and this is mandatory here.

37
00:03:46,900 --> 00:03:48,660
So does not force me.

38
00:03:48,920 --> 00:03:51,290
Now we can actually use string interpellation

39
00:03:53,670 --> 00:03:54,130
dollar

40
00:03:57,000 --> 00:03:57,670
space

41
00:04:07,330 --> 00:04:08,170
with every available

42
00:04:14,030 --> 00:04:19,670
and close the so no one is that one way of doing this.

43
00:04:20,000 --> 00:04:27,160
I can simply call it by using P-doc short.

44
00:04:27,620 --> 00:04:31,470
That's it not on this

45
00:04:40,370 --> 00:04:45,670
we have to meet that demand just start beach not on it.

46
00:04:47,750 --> 00:04:53,980
Sunday morning 17 May 1975 and that's it.

47
00:04:58,450 --> 00:04:59,270
First Name Last

48
00:05:02,460 --> 00:05:03,960
let us enhance this.

49
00:05:03,990 --> 00:05:09,020
What I wanted I want first name last name date of birth to be initialized to a constructor.

50
00:05:09,360 --> 00:05:15,180
So the key word constructor and here give the date of it you want to use put in just like this.

51
00:05:15,470 --> 00:05:17,010
So let's say first name.

52
00:05:19,700 --> 00:05:20,310
BLAUSTEIN

53
00:05:24,060 --> 00:05:26,170
they don't build

54
00:05:29,670 --> 00:05:30,280
this data.

55
00:05:30,300 --> 00:05:31,860
I should give it to the members.

56
00:05:32,040 --> 00:05:36,530
So again members should be doing this.

57
00:05:36,570 --> 00:05:39,620
Remember this is mandatory here.

58
00:05:57,400 --> 00:06:01,420
So not because it is a constructor with parameters available.

59
00:06:01,780 --> 00:06:05,450
Every time we create an object here we are supposed to provide the data.

60
00:06:05,920 --> 00:06:07,240
So I'll give first name

61
00:06:09,930 --> 00:06:13,770
last name and the date

62
00:06:17,030 --> 00:06:19,850
obviously we don't need no essential

63
00:06:22,870 --> 00:06:23,240
rather

64
00:06:26,550 --> 00:06:27,570
similes.

65
00:06:28,320 --> 00:06:35,840
So like this in a class we can provide a constructor for initializing the properties fields of that

66
00:06:35,840 --> 00:06:37,580
particular object.

67
00:06:37,820 --> 00:06:43,670
But remember in that class we can have only one form of constructor trying to write one more construct

68
00:06:43,670 --> 00:06:43,970
of

69
00:06:55,320 --> 00:06:56,510
may not be alone.

70
00:06:56,520 --> 00:06:56,930
Actually

71
00:06:59,710 --> 00:07:08,170
See this is not a multiple constructor implementations are not the only one constructor is allowed in

72
00:07:08,170 --> 00:07:15,830
a class that is restriction.

73
00:07:15,960 --> 00:07:22,740
So like this a class can freeze our properties we call it construct up our model.

74
00:07:22,840 --> 00:07:26,150
It's very important point.

75
00:07:26,180 --> 00:07:32,720
This is compulsory for every member when it is used or accessed inside a method or constructor.

76
00:07:32,720 --> 00:07:34,410
This is mandatory.

77
00:07:34,540 --> 00:07:35,950
It's not optional.

78
00:07:36,200 --> 00:07:39,370
If it does what is first name it's a no

79
00:07:45,030 --> 00:07:46,950
up this duck fuzzily

80
00:07:52,310 --> 00:07:52,880
for this.

81
00:07:52,880 --> 00:07:55,510
Also we can provide a shortcut.

82
00:07:55,900 --> 00:08:00,720
Actually in class we can have members as public we can have members as private.

83
00:08:00,770 --> 00:08:09,320
It's our choice so as an ultimate day of this class I can have more words of let's say person one I

84
00:08:09,340 --> 00:08:10,330
call it.

85
00:08:10,330 --> 00:08:12,920
I don't give this.

86
00:08:13,130 --> 00:08:20,730
I don't even need to give this an I'll just put up whatever I like for example that ziplining.

87
00:08:21,200 --> 00:08:24,340
So I'm not saying name is private.

88
00:08:24,440 --> 00:08:25,990
Last name is private.

89
00:08:26,210 --> 00:08:28,030
They don't vote is private.

90
00:08:28,340 --> 00:08:30,780
And because of this the word private there also.

91
00:08:30,820 --> 00:08:37,750
Now the members of the class so that the way we want it

92
00:08:42,260 --> 00:08:43,760
that's it.

93
00:08:44,600 --> 00:08:48,020
So this clause now has three properties.

94
00:08:48,020 --> 00:08:54,080
First Name Last Name date of birth which are by default private and they're also initialized to the

95
00:08:54,080 --> 00:08:58,400
constructor and everything was as it is not the change we have to make.

96
00:08:58,440 --> 00:09:05,540
So if I intended to person one and here also still everything works as it is.

97
00:09:05,570 --> 00:09:07,110
So this is the short form.

98
00:09:10,590 --> 00:09:13,860
So instead of writing precisely like this.

99
00:09:14,050 --> 00:09:15,600
A socket has been provided.

100
00:09:18,810 --> 00:09:25,930
Let's see now how can we implement inheritance so this in a person class and from person.

101
00:09:25,930 --> 00:09:28,600
I would like to inherit let's say Employee class

102
00:09:32,560 --> 00:09:34,660
and what are the syntax for inheritance.

103
00:09:34,660 --> 00:09:37,900
Your usual use extends parser

104
00:09:42,020 --> 00:09:44,760
so employee has everything that person has

105
00:09:49,480 --> 00:09:52,480
so he can have a new employee

106
00:09:58,220 --> 00:09:59,980
yes we can do that.

107
00:10:02,450 --> 00:10:03,190
Grandis

108
00:10:05,700 --> 00:10:06,730
everything you.

109
00:10:08,370 --> 00:10:12,600
But right no employee does not have anything Ixtoc.

110
00:10:12,900 --> 00:10:17,700
But we will surely want to have something more than person in employee.

111
00:10:17,930 --> 00:10:21,660
Let us say I would like to have department name

112
00:10:27,330 --> 00:10:31,480
I would like to help every employee a salary.

113
00:10:32,470 --> 00:10:34,370
But hold when it's like this.

114
00:10:34,480 --> 00:10:40,360
Obviously we would like to construct a water construct that we haven't paid in Plus we'll take at

115
00:10:43,680 --> 00:10:47,310
those extra values which is department name and

116
00:10:56,030 --> 00:10:57,020
I'm selling

117
00:11:00,050 --> 00:11:00,700
one.

118
00:11:00,730 --> 00:11:05,000
We're going to do is the full statement in the constructor.

119
00:11:05,250 --> 00:11:07,230
I think that's super.

120
00:11:07,940 --> 00:11:17,780
I'm part of the super constructor that is parent class constructor first name last name get on board

121
00:11:18,810 --> 00:11:21,870
and then we can help the rest of the deductions like that yet.

122
00:11:22,420 --> 00:11:29,210
So like a tight glove can have a constructive top with decks from the top of the tile plus.

123
00:11:29,600 --> 00:11:34,640
But that doesn't mean we are getting for the parent class as it is pass it to the parent class constructor

124
00:11:34,640 --> 00:11:38,080
using the forced statement super mind you.

125
00:11:38,090 --> 00:11:40,220
If I see what happens

126
00:11:43,420 --> 00:11:44,400
it said.

127
00:11:44,840 --> 00:11:50,100
So support is compulsory supposed to be just a statement of the constructor.

128
00:11:50,150 --> 00:11:52,250
If the last child.

129
00:11:52,270 --> 00:11:54,260
Plus this is my.

130
00:11:55,270 --> 00:11:58,240
So now obviously I should provide a department name.

131
00:12:06,000 --> 00:12:09,340
But problem is what Here we are creating B.

132
00:12:09,490 --> 00:12:17,120
So using the when I say show it is only going to show me details of the person department and so they

133
00:12:17,130 --> 00:12:18,690
will not be displayed.

134
00:12:18,750 --> 00:12:25,820
So I would like to have departmental salary also in the trial plus how are we going to do that.

135
00:12:27,540 --> 00:12:32,800
So Intel Plus we will have the same function.

136
00:12:33,170 --> 00:12:34,250
So again

137
00:12:40,260 --> 00:12:41,430
on the first

138
00:12:44,450 --> 00:12:48,240
line of this movie superdog so

139
00:12:53,860 --> 00:12:55,810
and then exporting to show you

140
00:12:59,250 --> 00:13:01,140
showing a lot.

141
00:13:01,160 --> 00:13:03,250
I want to replace it with something else.

142
00:13:03,330 --> 00:13:13,250
Let's say we don't like to give you a on to consider dumping So so is no more so in Dodik it is returning

143
00:13:13,250 --> 00:13:18,510
a string and that string I'm going to get most

144
00:13:26,050 --> 00:13:38,080
but once I get docstring can I make changes to that yes to docstring we can abandon the rest of those

145
00:13:38,290 --> 00:13:39,050
things.

146
00:13:42,720 --> 00:13:45,460
Does department name

147
00:13:48,170 --> 00:13:48,710
don't

148
00:13:53,580 --> 00:13:54,570
does not

149
00:14:07,950 --> 00:14:10,570
why it is not accessible again.

150
00:14:12,240 --> 00:14:22,190
Your intended book thing looks like some other is that word is not accessible to type string.

151
00:14:23,900 --> 00:14:33,850
Mordred so so don't play with string.

152
00:14:33,930 --> 00:14:35,280
What is wrong here.

153
00:14:40,300 --> 00:14:43,670
Must have a written word or get into it.

154
00:14:45,160 --> 00:14:47,790
But it's getting error in this place.

155
00:14:48,140 --> 00:15:00,180
I wanted is not assignable to types thing so it is saying that Supernaut so is written in one note does

156
00:15:00,180 --> 00:15:00,380
it.

157
00:15:00,420 --> 00:15:03,220
OK I got it wrong please.

158
00:15:04,000 --> 00:15:08,820
I should write code in some

159
00:15:14,700 --> 00:15:15,990
notes should be good enough.

160
00:15:18,520 --> 00:15:27,050
By mistake or someone which I'm not willing to I'm using person everything looks good.

161
00:15:27,180 --> 00:15:28,430
No this outputted

162
00:15:32,790 --> 00:15:35,430
selectees we can get lost in here.

163
00:15:35,960 --> 00:15:40,890
And when we are doing inheritance o undefined strange

164
00:15:45,100 --> 00:15:53,900
in the constructor of key in the tile class constructor I should only take care of initializing department

165
00:15:58,560 --> 00:15:59,970
and solid

166
00:16:03,680 --> 00:16:06,960
rest with any hope they can get off in debate in class.

167
00:16:06,970 --> 00:16:08,460
So that was copy paste problem

168
00:16:11,580 --> 00:16:13,190
name date.

169
00:16:14,250 --> 00:16:19,950
Department said.

170
00:16:20,190 --> 00:16:22,240
So this is all inheritance can be when.

171
00:16:22,430 --> 00:16:24,610
In the north we're told we have code.

172
00:16:25,220 --> 00:16:31,090
Felix constructable metters.

173
00:16:31,550 --> 00:16:32,430
I've got methods here.

174
00:16:32,450 --> 00:16:40,560
One is greed and the other is self-introduction which is good at telling the details.

175
00:16:40,700 --> 00:16:43,700
This is compulsory with object

176
00:16:46,470 --> 00:16:53,480
constructors we use and when we specify private to automatically become the properties so this and this

177
00:16:53,480 --> 00:17:00,650
are same basically extending the class we use the key word extents so bodies used to call the parent

178
00:17:00,650 --> 00:17:07,680
class matter in class constructor and if matter is dead it is going to call the parent class method.

179
00:17:10,220 --> 00:17:15,790
This is mandatory public Public-Private product that read only modifieds.

180
00:17:16,000 --> 00:17:16,300
Yes.

181
00:17:16,310 --> 00:17:23,000
A class can have been declared as private in such case the private member is accessible only to other

182
00:17:23,000 --> 00:17:32,750
members within the plus child plus and other classes is not a product in public is accessible even within

183
00:17:32,750 --> 00:17:33,090
the class.

184
00:17:33,090 --> 00:17:40,750
We use this alternative class we are supposed to make use of the reference to an object and predicted

185
00:17:40,770 --> 00:17:44,520
that is accessible within the class and child class but is not accessible.

186
00:17:44,530 --> 00:17:50,450
Also that class of course by default all the members of public very important point

187
00:17:56,330 --> 00:17:58,580
a member can be declared as read on.

188
00:17:58,670 --> 00:18:01,920
Sometimes we can dig declared members read.

189
00:18:02,300 --> 00:18:08,960
So when we declare a member as Read-Only the only place we can initialize that member is in the constructor.

190
00:18:09,080 --> 00:18:12,660
For example let's say I'll give you a read only.

191
00:18:13,090 --> 00:18:15,310
So department name is read only.

192
00:18:15,680 --> 00:18:22,550
Or trying to change the department name yet this dot department name is a little something else and

193
00:18:22,550 --> 00:18:26,620
we are supposed to get an editor for this.

194
00:18:26,680 --> 00:18:33,210
You're not alone in a department named because it's a constant or real only property so read only property

195
00:18:33,210 --> 00:18:37,690
of a class can be initialized only in that construct.

196
00:18:37,840 --> 00:18:38,770
Very important for

197
00:18:45,170 --> 00:18:49,060
the class can have get and set access specifier.

198
00:18:49,130 --> 00:18:50,600
What does this get into Texas.

199
00:18:50,600 --> 00:18:57,870
Because if you're right now we have got in-person Plus first name last name.

200
00:18:58,250 --> 00:19:06,000
But what if I don't fully own fully concatenation of first name and last name and I don't want to live

201
00:19:06,020 --> 00:19:07,920
to be a mentor.

202
00:19:08,510 --> 00:19:16,500
So do I declared them as a property whose value is dependent on other two properties of the same class.

203
00:19:16,640 --> 00:19:18,920
There is one in the class B candidate

204
00:19:22,310 --> 00:19:22,970
public.

205
00:19:23,030 --> 00:19:34,690
We want that matter to be accessible and say get get it over there fully not deeply committed that's

206
00:19:34,720 --> 00:19:38,150
the effect is all they in return.

207
00:19:39,030 --> 00:19:49,780
This got first name concatenated with his daughter blossomy so very important point.

208
00:19:49,990 --> 00:19:58,010
Not when I'm trying to use fullname let's say here I want to use putting in throve doing this.

209
00:19:58,040 --> 00:20:09,480
I can just break this door fully.

210
00:20:09,640 --> 00:20:10,930
It's giving it all.

211
00:20:11,510 --> 00:20:12,800
Same problem but someone

212
00:20:25,080 --> 00:20:29,410
you don't want let's minimize it.

213
00:20:30,890 --> 00:20:33,500
No important thing is Doel.

214
00:20:33,710 --> 00:20:38,380
If I remove this get it becomes a matter of when it becomes a matter we are supposed to provide pro-India

215
00:20:38,560 --> 00:20:43,640
brackets.

216
00:20:43,760 --> 00:20:50,880
Otherwise you see this problem will come with all brackets it becomes a reference to that.

217
00:20:51,380 --> 00:20:55,620
So back in April we required what Fullam we wanted to be treated as property.

218
00:20:55,820 --> 00:20:57,070
So I don't want to give black.

219
00:20:57,140 --> 00:21:00,080
So when they don't want to give bracket you give get

220
00:21:03,810 --> 00:21:14,220
over like that if required we can also have a mentor public set footling.

221
00:21:14,820 --> 00:21:17,770
But in this case why don't we do is supposed to be there.

222
00:21:24,090 --> 00:21:29,340
And of course they don't I will be white it does not threaten anything.

223
00:21:29,430 --> 00:21:33,480
But instead we should help them not this paramita

224
00:21:37,520 --> 00:21:47,370
it does not have it by default which is why Tony so this me does will do what does not force him and

225
00:21:47,650 --> 00:21:52,450
lastly him to think I should take from me.

226
00:21:52,660 --> 00:21:56,900
So I give you the name Daut.

227
00:21:57,150 --> 00:22:16,060
Let with space all this dog last name the are named or played split this thing get them Eddie and take

228
00:22:16,110 --> 00:22:16,830
the lead.

229
00:22:19,540 --> 00:22:21,250
So how do we use this.

230
00:22:21,690 --> 00:22:31,990
Now here I can beat your full name is according to ABC X Y Z literacy.

231
00:22:35,860 --> 00:22:39,110
We fight on this.

232
00:22:39,220 --> 00:22:40,850
You're going to get ABC.

233
00:22:43,670 --> 00:22:52,700
So we can have mentors we can help settle matters actually it is as well as having a mid-term only.

234
00:22:52,960 --> 00:22:57,190
But that does not get said makes the difference.

235
00:22:59,110 --> 00:23:06,560
Like this only it is ideally recommended to convert all the properties into these are actually not philosophies

236
00:23:07,460 --> 00:23:08,600
so this will be a feat.

237
00:23:08,620 --> 00:23:12,670
And for that we can have a property for example date of birth.

238
00:23:13,010 --> 00:23:18,050
I would like to convert that also to a property but the big question is why are we going to convert

239
00:23:18,050 --> 00:23:19,390
them to property.

240
00:23:19,880 --> 00:23:23,350
Yes simple we can do some kind of validations.

241
00:23:23,570 --> 00:23:32,600
For example you or I can write some validation code for validating them before we actually acceptable.

242
00:23:33,090 --> 00:23:36,270
And if the value is not valid we may probably throw an error.

243
00:23:37,280 --> 00:23:39,750
So something like that is the advantage.

244
00:23:39,980 --> 00:23:44,990
For example fullname requires both first name and last name if any one of them is missing.

245
00:23:44,990 --> 00:23:51,080
We would like to throw an error like that if you named NAME lead.

246
00:23:52,770 --> 00:23:55,790
I'm going to split name with space.

247
00:23:57,010 --> 00:23:59,410
And what we are going to get is Eddie.

248
00:23:59,680 --> 00:24:04,690
So Ill take the length of that and it is the length of the end is not equal to two.

249
00:24:04,990 --> 00:24:08,910
We can throw at it a throw

250
00:24:12,600 --> 00:24:16,850
invited me like this again.

251
00:24:18,590 --> 00:24:25,550
So this kind of validation is what is the basic advantage of having properties in a class and properties

252
00:24:25,610 --> 00:24:37,670
are encapsulating things this will call the math fields went back to the hand-wrote or some class construct.

253
00:24:37,740 --> 00:24:47,670
Great self-introduction employee extends perso increments out of the salary they feel a property or

254
00:24:48,960 --> 00:24:53,500
department name with a property we are sending the department name yet

255
00:24:59,470 --> 00:25:00,580
we are doing the validation

256
00:25:03,440 --> 00:25:08,730
then we are casting when we want to cast one day to the other.

257
00:25:08,990 --> 00:25:15,870
We can use casting operator and we have seen this actually in the debates chapter also we can either

258
00:25:15,870 --> 00:25:24,630
do casting using angle bracket or we can use as dog as is the recommended bit of casting the reason.

259
00:25:24,630 --> 00:25:26,350
In either case is seen

260
00:25:29,060 --> 00:25:39,930
we can also use something called an instance off for example here I want to check if either of the person

261
00:25:40,100 --> 00:25:46,010
or different object implied I would like to check if we use

262
00:25:48,790 --> 00:25:53,170
instance all employee say

263
00:26:00,700 --> 00:26:05,140
and we are supposed to get your BS instance of employee

264
00:26:09,430 --> 00:26:14,770
really obvious this feature is useful when we have multiple classes inherited from person.

265
00:26:15,290 --> 00:26:21,610
So we want to take with the variable of type percentages to object of class in play or some other class

266
00:26:22,210 --> 00:26:25,370
if it is implied it was written to as it was written.

267
00:26:25,370 --> 00:26:33,220
For example let's say I have another class some dummy some demo class which is also inherited from person

268
00:26:35,540 --> 00:26:37,450
and if I write

269
00:26:42,420 --> 00:26:42,560
them

270
00:26:46,830 --> 00:26:53,040
let be golden person is equal to them.

271
00:27:00,160 --> 00:27:05,320
So obviously you're going to get false because he's not going to employ them.

272
00:27:05,470 --> 00:27:09,740
Instead it is referring to oh no answer to what is wrong.

273
00:27:11,090 --> 00:27:12,740
Do not combine extends

274
00:27:23,330 --> 00:27:26,580
so he is called the new demo.

275
00:27:27,010 --> 00:27:27,620
OK.

276
00:27:27,690 --> 00:27:31,510
Because in part some of the constructor we have to give some good ideas.

277
00:27:31,590 --> 00:27:32,470
That's fine.

278
00:27:38,080 --> 00:27:40,660
False.

279
00:27:40,840 --> 00:27:49,400
So unless God is compiled We are not going to get the Newport Rhode Island.

280
00:27:49,500 --> 00:27:57,050
So that's what this instance all so is casting can be done using Anglo-Americans can be done using as

281
00:27:57,090 --> 00:27:58,300
operator.

282
00:27:58,720 --> 00:28:04,060
We can make use of instance of wherever needed.

283
00:28:04,140 --> 00:28:15,830
This is where we need custom personas of Babe Buchman MP for example if I have a variable of type employee

284
00:28:18,200 --> 00:28:20,240
and I know that it is employed.

285
00:28:20,330 --> 00:28:22,720
So I would like to write it is equal to be

286
00:28:27,650 --> 00:28:36,680
giving it a person is not as Anable to imply but employees are to some.

287
00:28:36,900 --> 00:28:37,600
This is what

288
00:28:41,450 --> 00:28:42,370
all

289
00:28:50,410 --> 00:28:52,100
this is absolutely no problem.

290
00:28:53,680 --> 00:28:54,300
Some

291
00:28:57,820 --> 00:29:02,280
employed so if needed I can be is it.

292
00:29:02,560 --> 00:29:07,560
That's not a problem but easy to beat is incorrect.

293
00:29:07,570 --> 00:29:11,580
But we know that in our case people are free to employ only.

294
00:29:11,680 --> 00:29:12,930
So we go for casting.

295
00:29:13,100 --> 00:29:24,090
So we like and and to be sure that it's really different to apply only we can use the instance of

296
00:29:26,860 --> 00:29:32,630
employee as easy it look like this thinking

297
00:29:38,990 --> 00:29:41,380
if you're coming from double or C-Sharp background.

298
00:29:41,660 --> 00:29:43,730
All this is pretty well known to you.

299
00:29:43,850 --> 00:29:45,670
The rules do not change.

300
00:29:45,960 --> 00:29:46,280
Right.

301
00:29:46,310 --> 00:29:46,990
Exactly.

302
00:29:46,990 --> 00:29:53,070
See Latino type of person or already customization.

303
00:29:53,100 --> 00:30:01,660
Customizing the types of typescript in any way you want to do is not as type as are some so basically

304
00:30:01,730 --> 00:30:02,920
customized.

305
00:30:03,280 --> 00:30:05,870
So here we have got user object which is empty.

306
00:30:06,170 --> 00:30:14,330
User first name got Hello Lowville give error obviously because it's not that in the type moslim it's

307
00:30:14,330 --> 00:30:15,120
not there.

308
00:30:15,470 --> 00:30:21,160
So what we can do now we can say we have the user simply costed to a user.

309
00:30:21,490 --> 00:30:25,850
When you cast into our user user that you really can be certain you are not using them.

310
00:30:25,860 --> 00:30:30,830
And isn't that what this is.

311
00:30:30,950 --> 00:30:31,420
As those

312
00:30:34,190 --> 00:30:38,870
your compiler will provide autocomplete for the object properties.

313
00:30:38,920 --> 00:30:49,690
That means basically even if you use the dot you're going to get the intellisense.

314
00:30:49,720 --> 00:30:56,510
And finally we have got static and what is static if a member in a class is declared as static.

315
00:30:56,510 --> 00:30:58,930
We are going to have only one copy of it.

316
00:30:59,140 --> 00:31:09,220
Even if we have multiple objects created precisely the same as in C++ or in jail or in C-sharp any object

317
00:31:09,220 --> 00:31:10,590
oriented language.

318
00:31:10,690 --> 00:31:20,550
So for every new object we are going to have a separate copy for the members of the class every new

319
00:31:20,550 --> 00:31:26,520
object or have three different copies of first name last name date of birth Creager windbreak reports

320
00:31:26,570 --> 00:31:27,690
adopting.

321
00:31:27,900 --> 00:31:34,440
But if I meet any one of them as static in such case we are going to have only one copy of that static

322
00:31:34,440 --> 00:31:39,140
member in memory irrespective of the number of objects created.

323
00:31:39,210 --> 00:31:40,780
That's what extatic.

324
00:31:41,270 --> 00:31:45,870
And yes static member should be always accessed using class name.

325
00:31:45,910 --> 00:31:51,510
Not this we should not use this like here we are using this for everything but for static member we

326
00:31:51,510 --> 00:31:52,560
should use this.

327
00:31:52,560 --> 00:31:56,720
So Maxell is static and we are using employees.

328
00:31:56,730 --> 00:32:01,390
The class name not the keyword.

329
00:32:01,420 --> 00:32:03,370
Finally we have got abstract.

330
00:32:03,410 --> 00:32:05,660
What is this abstract.

331
00:32:06,340 --> 00:32:07,340
Let me right now

332
00:32:12,020 --> 00:32:13,240
say we have a situation

333
00:32:17,900 --> 00:32:19,700
the situation is.

334
00:32:20,570 --> 00:32:27,570
I would like to have the object either a circle or as square.

335
00:32:27,590 --> 00:32:32,370
Any one of these two objects either it can be a circle object.

336
00:32:32,380 --> 00:32:34,170
It can be squared object.

337
00:32:34,300 --> 00:32:37,460
So how can I do this for this completely.

338
00:32:37,480 --> 00:32:42,150
I should have a common base class I'll call it that's bigger.

339
00:32:43,390 --> 00:32:53,890
And then we can have cloth squared which extends Feargal.

340
00:32:54,240 --> 00:32:59,410
I can also have a class circle that also extends.

341
00:33:00,430 --> 00:33:07,140
Now one of the advantage of doing something like this really obvious that one it is I can have only

342
00:33:07,140 --> 00:33:11,490
one variable figure which is of type figure.

343
00:33:11,730 --> 00:33:22,080
And yet I can put either circle or I can square any of my choice I can because figure is a common base

344
00:33:22,080 --> 00:33:26,800
class foot boat circle and square we're going to do that.

345
00:33:30,710 --> 00:33:34,030
Now if I have circle square good.

346
00:33:34,040 --> 00:33:35,080
All good.

347
00:33:35,270 --> 00:33:43,900
I would like to know something called that diamond every figure has some diamonds and I want the diamonds

348
00:33:44,050 --> 00:33:48,160
to be used in computational area and paramita.

349
00:33:48,150 --> 00:33:51,240
So I would have protected

350
00:33:53,790 --> 00:33:57,200
diamonds and let's say that of a number.

351
00:33:58,000 --> 00:34:00,430
I would like to initialize that to construct the

352
00:34:08,490 --> 00:34:09,640
this.

353
00:34:10,170 --> 00:34:12,510
Moreover you can actually go for a shortcut.

354
00:34:12,540 --> 00:34:14,880
Now that we are comfortable with index

355
00:34:17,510 --> 00:34:20,250
just put your protected

356
00:34:22,920 --> 00:34:27,040
diamond so diamonds are not a member.

357
00:34:27,280 --> 00:34:33,630
So if I'm writing here on I want to write a function call this area

358
00:34:36,520 --> 00:34:38,000
is a number

359
00:34:41,470 --> 00:34:45,500
LOL what is the area of squit site in the site.

360
00:34:45,920 --> 00:34:47,660
So I would say diamonds in

361
00:34:50,150 --> 00:34:57,010
discorporate mention in this dark diamonds

362
00:35:03,390 --> 00:35:04,730
like this I would like.

363
00:35:05,870 --> 00:35:12,480
Oh I just don't need to write Funk's just function like this better mean

364
00:35:18,760 --> 00:35:23,850
it does fall into the initial site of this

365
00:35:26,700 --> 00:35:28,940
so palmettos like this to two.

366
00:35:28,970 --> 00:35:37,190
I'm supposed to write in circle also what formulas are different by R-squared.

367
00:35:37,200 --> 00:35:43,840
So why is my dog bite that is relevant.

368
00:35:43,860 --> 00:35:51,040
Plus all the requirements you'll find in that math class by R-squared good.

369
00:35:51,220 --> 00:35:52,930
And here it is to pay up.

370
00:35:53,530 --> 00:35:56,990
So to start my daughter by

371
00:36:00,340 --> 00:36:08,110
all good now when I'm creating a circle or creating a trigger I'm supposed to provide

372
00:36:10,970 --> 00:36:12,360
this coming for diamonds in

373
00:36:26,650 --> 00:36:27,860
this dot

374
00:36:30,570 --> 00:36:32,450
distort.

375
00:36:32,950 --> 00:36:42,340
Not here I'm supposed to avoid them and then now I would like shows them in the area and perimeter of

376
00:36:42,670 --> 00:36:43,340
the circle.

377
00:36:43,360 --> 00:36:50,580
But the variable is I figure so can I alert figure not area.

378
00:36:50,880 --> 00:36:53,970
No I cannot break letting me.

379
00:36:56,670 --> 00:37:03,130
These are going to be compilation errors not a lot.

380
00:37:03,190 --> 00:37:03,880
Why.

381
00:37:04,060 --> 00:37:08,940
Because area and perimeter methods are not dead in figer.

382
00:37:10,650 --> 00:37:12,320
So should we let them in figure.

383
00:37:12,380 --> 00:37:14,220
Yeah we have to write them in figure.

384
00:37:14,580 --> 00:37:24,300
But when we write them in figure what will be the formula for implementing idio of Fiegel or implementing

385
00:37:24,420 --> 00:37:31,030
perimeter officier no nothing because bigger assets does not exist.

386
00:37:31,050 --> 00:37:35,910
So what we do we still need to look for them at 30 and we need to add them a third

387
00:37:39,810 --> 00:37:43,050
but we want to provide no implementation.

388
00:37:43,170 --> 00:37:47,080
We don't want to give that implementation.

389
00:37:48,200 --> 00:37:52,760
Right now if I give empty I'm going to get error because the return type is number.

390
00:37:52,840 --> 00:37:53,640
So what to do.

391
00:37:53,650 --> 00:37:59,110
Remove this implementation and use the keyword abstract.

392
00:37:59,840 --> 00:38:01,730
Same thing we will do even for

393
00:38:07,200 --> 00:38:13,830
public abstract me.

394
00:38:17,070 --> 00:38:27,600
Because of CLOS has at least one method which is abstract to actually the class also should be declared

395
00:38:27,630 --> 00:38:29,880
as abstract.

396
00:38:29,880 --> 00:38:34,470
The reader will know what is wanted of writing abstract keyword.

397
00:38:34,730 --> 00:38:41,310
Basically it means that this is not a concrete blocks and such a class cannot be instantiated.

398
00:38:41,310 --> 00:38:42,990
That means I cannot write no

399
00:38:46,160 --> 00:38:50,300
call equal to Newfie is not alone.

400
00:38:50,800 --> 00:38:51,850
This will not be an

401
00:38:58,930 --> 00:39:03,860
even if you give the data LAquila cannot create instance of an abstract figure.

402
00:39:04,210 --> 00:39:11,700
But yes we can have a variable of type figure and it can refer to the object of the classes any one

403
00:39:11,710 --> 00:39:12,710
of them.

404
00:39:13,270 --> 00:39:17,870
So that's all the very purpose of abstract keyword is to write.

405
00:39:17,870 --> 00:39:22,480
Immature in a class without implementation.

406
00:39:22,720 --> 00:39:29,980
But such a class also should be declared as abstract and the class inherited from abstract class must

407
00:39:30,040 --> 00:39:38,530
implement the abstract methods of the parent class so either permit or compulsory must be squared.

408
00:39:38,850 --> 00:39:45,020
Impedimenta compulsory must be implemented in circle because both Square and circle are inherited from

409
00:39:45,200 --> 00:39:45,950
Fig.

410
00:39:46,460 --> 00:39:53,480
So this is how we implement abstract given and of course yes if you are in power or if you are each

411
00:39:53,480 --> 00:39:54,380
of them.

412
00:39:54,680 --> 00:39:56,930
This is precisely known to you.

413
00:39:56,930 --> 00:39:58,590
This is nothing new.

414
00:39:59,660 --> 00:40:04,550
So with this we finished all the features of basic object orientation.

415
00:40:04,820 --> 00:40:09,160
We started with having a class with simply field members.

416
00:40:09,320 --> 00:40:15,970
Then we added a constructor then we added midterms then we added public and said methods which are nothing

417
00:40:15,980 --> 00:40:20,070
but properties to the class which are used for validations.

418
00:40:20,270 --> 00:40:24,070
We have seen how we can extend the class when we extend the class whole.

419
00:40:24,080 --> 00:40:29,560
The Tile class constructor should call the base class constructor of how we can have it's own class

420
00:40:29,560 --> 00:40:32,330
method calling the base class matter.

421
00:40:32,330 --> 00:40:41,300
We have seen how we can have a variable of one type and object of the time class and using instance

422
00:40:41,300 --> 00:40:44,110
of we can verify what is the type of object.

423
00:40:44,240 --> 00:40:53,160
And finally we have seen what exactly is abstract keyword with the example of a square circle on both

424
00:40:53,190 --> 00:40:54,950
inherited from Fig.

425
00:40:55,260 --> 00:40:55,740
Thank you.

