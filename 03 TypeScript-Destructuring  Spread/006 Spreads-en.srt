1
00:00:16,180 --> 00:00:24,550
Spread spread is precisely the opposite of destructuring it will break the area into individual components

2
00:00:25,000 --> 00:00:31,020
and to do that we are going to make use of the ellipses three dots as a prefix to an area.

3
00:00:31,480 --> 00:00:39,730
It allows you to spread it into another area or object into another object basically spreading it is

4
00:00:39,730 --> 00:00:41,050
spreading into another.

5
00:00:41,050 --> 00:00:43,830
That's how it is.

6
00:00:43,840 --> 00:00:45,620
So let's take an example.

7
00:00:47,110 --> 00:00:49,550
Let's say that we have got two at it.

8
00:00:49,550 --> 00:00:55,970
First I'll call it doesn't ef want or say it one is one.

9
00:00:59,120 --> 00:01:02,370
And I would have another at it too.

10
00:01:09,750 --> 00:01:11,580
So two areas of there.

11
00:01:11,670 --> 00:01:20,480
Now what I can do is I can combine both a 1 and 2 and then create one more at it that is 30 percent

12
00:01:21,700 --> 00:01:24,920
what what is this made up of.

13
00:01:25,110 --> 00:01:31,900
I want this to be made up of First I want zero then I want all the data off one.

14
00:01:31,930 --> 00:01:38,410
So I could come up and dot dot dot even Komarr I want all the details of it.

15
00:01:38,520 --> 00:01:40,990
So again God brought it.

16
00:01:41,520 --> 00:01:45,900
And finally I won't let a 6 7.

17
00:01:45,910 --> 00:01:47,610
So what is going to happen now.

18
00:01:47,650 --> 00:01:50,370
All the three elements of each one will be placed here.

19
00:01:50,560 --> 00:01:56,210
All the three elements of it will be placed here a new area will be constructed which will be as I go

20
00:01:57,040 --> 00:02:04,560
in and select the other one to me come.

21
00:02:04,600 --> 00:02:05,190
This

22
00:02:13,960 --> 00:02:17,440
one is one two 3 4 5 6 7.

23
00:02:18,080 --> 00:02:23,040
So basically we are spread the eddy under the

24
00:02:36,320 --> 00:02:43,700
so like that this is combined all the way up inducing so so shocked that syntax we don't have to keep

25
00:02:43,700 --> 00:02:45,850
writing for loops and stuff like that.

26
00:02:46,010 --> 00:02:52,750
If you want to simply join to a race but use ellipsis syntax that's square record and you're done.

27
00:02:53,030 --> 00:02:54,870
This is an Arab Spring.

28
00:02:55,130 --> 00:03:00,280
Likewise we also have objects pretty Let us if we have got one object or B.

29
00:03:01,650 --> 00:03:04,500
What will be one we get on top of it.

30
00:03:04,770 --> 00:03:17,930
So let's say OBSS want abs or probie US one is equal to and give some property B one with values that

31
00:03:17,940 --> 00:03:23,840
are c something like B one only kamo.

32
00:03:24,110 --> 00:03:28,560
Let's say I have another property P with the value.

33
00:03:28,850 --> 00:03:30,550
Maybe I have one more property.

34
00:03:30,610 --> 00:03:34,390
Three But some of that about

35
00:03:40,750 --> 00:03:45,230
I'll have one more object created now.

36
00:03:45,750 --> 00:03:53,880
But my requirement is the new object but I'm going to create should be in addition to what we have in

37
00:03:53,950 --> 00:03:55,530
obvious one.

38
00:03:55,670 --> 00:03:57,710
One extra property we want that.

39
00:03:57,840 --> 00:03:59,650
That means everything is there.

40
00:03:59,690 --> 00:04:03,500
Or because we want an extra property we want.

41
00:04:03,540 --> 00:04:05,220
How can we get that.

42
00:04:05,250 --> 00:04:13,810
So in that case simply write your dot dot dot or be as one Komarr and put one more extra property that

43
00:04:13,890 --> 00:04:15,480
you want let's say before

44
00:04:20,630 --> 00:04:21,870
that's it.

45
00:04:22,290 --> 00:04:29,520
This is an object writing you are spreading the object with extra property into a new object.

46
00:04:30,120 --> 00:04:36,550
It is spreading objects spreading.

47
00:04:37,190 --> 00:04:39,020
Oh yes one

48
00:04:41,800 --> 00:04:44,650
but that's an object that will only show me object.

49
00:04:45,180 --> 00:04:48,130
Let's all be asked do not even

50
00:04:52,100 --> 00:04:57,060
brand or be as to be

51
00:05:01,510 --> 00:05:06,420
me to B-tree And finally

52
00:05:09,440 --> 00:05:11,210
all be as told or

53
00:05:14,630 --> 00:05:15,560
modified on this

54
00:05:18,720 --> 00:05:23,830
be one and two hundred.

55
00:05:24,000 --> 00:05:26,800
So an existing object is expanded.

56
00:05:27,720 --> 00:05:30,670
Now here you have to be careful about one thing.

57
00:05:31,140 --> 00:05:31,730
No right.

58
00:05:31,730 --> 00:05:38,920
No I showed you all the properties of the obvious two.

59
00:05:39,020 --> 00:05:43,770
I don't like this but what if I had three here.

60
00:05:44,740 --> 00:05:49,960
Or do you.

61
00:05:50,120 --> 00:05:53,140
This property is no more than that again.

62
00:05:53,280 --> 00:05:56,030
So in the original object we have two.

63
00:05:56,330 --> 00:06:02,880
And here we are spreading obvious obvious one and two.

64
00:06:03,180 --> 00:06:05,080
So what will happen in this kind of case.

65
00:06:06,140 --> 00:06:09,040
What be the value of all be we do here.

66
00:06:09,500 --> 00:06:12,820
Will it take the be from here or this will be taken.

67
00:06:12,830 --> 00:06:15,100
That is the overeaten value.

68
00:06:15,110 --> 00:06:17,210
Let's take this.

69
00:06:17,310 --> 00:06:23,250
We are going to get 100 you know 100 because it does from right to left.

70
00:06:23,400 --> 00:06:31,140
Same statement teams and I'll post the two hundred Komal.

71
00:06:31,350 --> 00:06:33,210
The new object.

72
00:06:33,270 --> 00:06:34,300
So what is going to happen.

73
00:06:34,300 --> 00:06:38,700
No.

74
00:06:38,930 --> 00:06:44,680
Let's run this and you'll notice if it still only does not in 200.

75
00:06:44,770 --> 00:06:49,530
Why it is left left to right navigation.

76
00:06:49,880 --> 00:06:52,300
So in the object there is a property by name.

77
00:06:52,350 --> 00:06:53,680
Those values under.

78
00:06:53,960 --> 00:07:01,200
But when it finds this spread it takes all the properties of all beings one and if there is already

79
00:07:01,200 --> 00:07:02,150
existing property.

80
00:07:02,160 --> 00:07:06,890
All right the value that is 210 will be overridden or hit.

81
00:07:07,620 --> 00:07:10,440
So this is very very important point here.

82
00:07:10,860 --> 00:07:19,620
So yes we have got at it spreading where an array can be combined with another array original data so

83
00:07:19,620 --> 00:07:25,260
that we can get it combined that it objects printing an existing object with original properties can

84
00:07:25,260 --> 00:07:29,750
be created but the order is important.

85
00:07:29,970 --> 00:07:34,370
The object spreading is more complex than responding like it is spreading.

86
00:07:34,470 --> 00:07:42,330
It performs from left to right but the result is still an object like here and that it was created.

87
00:07:42,360 --> 00:07:46,520
This was the first element then these elements and these elements and then final element.

88
00:07:46,620 --> 00:07:51,480
Yep your daughter is left to right but the result is an object.

89
00:07:51,480 --> 00:08:00,540
That means if a big object comes with the property later than the real value the object is going to

90
00:08:00,540 --> 00:08:05,070
override the property so your underscore defaults.

91
00:08:05,060 --> 00:08:06,560
What do we have will.

92
00:08:06,610 --> 00:08:07,930
All right fool.

93
00:08:08,130 --> 00:08:12,310
It's so that's the problem.

94
00:08:13,390 --> 00:08:16,840
So why not say this is the practical usage of this example.

95
00:08:16,840 --> 00:08:19,560
I mean how would default values for all the properties.

96
00:08:19,960 --> 00:08:24,480
So if I want to preserve the default values I just put default.

97
00:08:24,730 --> 00:08:26,740
So the whole object stands here as it is.

98
00:08:27,010 --> 00:08:30,490
And for Forth I'm writing the value of i see.

99
00:08:30,490 --> 00:08:35,670
I'm thinking it's not the default Bollaert that lies even if one

100
00:08:38,810 --> 00:08:40,220
object has a limitation.

101
00:08:40,250 --> 00:08:41,920
And what is that limitation.

102
00:08:41,990 --> 00:08:48,260
If your object or object has mattered at the time of spreading the global object that is created does

103
00:08:48,260 --> 00:08:49,650
not have mattered.

104
00:08:49,700 --> 00:08:53,480
It has only properties so it is not exactly.

105
00:08:53,510 --> 00:08:56,300
It's only grown in context of beat up.

106
00:08:56,360 --> 00:08:58,540
It is not a clone otherwise.

107
00:08:58,700 --> 00:09:01,590
So yes with this we conclude the topic.

108
00:09:01,660 --> 00:09:04,010
Proctoring are spread.

109
00:09:04,370 --> 00:09:09,860
So remember we have the concept of a restructuring object which proctoring we can have mixed with proctoring

110
00:09:09,860 --> 00:09:15,670
that means from a single object we can get any as well as another object property renaming.

111
00:09:15,890 --> 00:09:20,620
And of course default values for the parameters along with optional parameters.

112
00:09:20,780 --> 00:09:23,220
And finally we have looked that spread.

113
00:09:23,690 --> 00:09:24,730
Very very important.

114
00:09:24,730 --> 00:09:30,530
The only purpose of this particular chapter or model I would say is to reduce the amount of code one

115
00:09:30,530 --> 00:09:32,260
would have to write in.

116
00:09:32,320 --> 00:09:38,990
Of writing long long all we can use shortcuts but initially for some time this is going to be confusing

117
00:09:38,990 --> 00:09:40,850
to you get export with it.

118
00:09:40,850 --> 00:09:44,710
So my advice would be to start using it automatically.

119
00:09:44,750 --> 00:09:49,540
Your skills on this will get sharpened and things will be smooth as you move forward.

120
00:09:49,790 --> 00:09:50,240
Thank you.

