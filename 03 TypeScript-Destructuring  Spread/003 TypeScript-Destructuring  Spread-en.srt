1
00:00:16,680 --> 00:00:25,040
Destructuring and spreads another very important topic which is based on databanks especially the areas

2
00:00:25,040 --> 00:00:27,480
that are tight and the object data type.

3
00:00:27,720 --> 00:00:34,080
So how exactly is typescript exploiting this feature so that the code can be reduced the gold written

4
00:00:34,080 --> 00:00:36,930
by the developer can be further reduced.

5
00:00:36,930 --> 00:00:39,620
So yes what is this proctoring and what is it.

6
00:00:40,140 --> 00:00:46,310
These proctoring is that a convenient way of extracting data are stored in an object.

7
00:00:46,350 --> 00:00:53,640
Or ANY see whether it is an object which is made up of properties or a rate which is made up of elements.

8
00:00:53,820 --> 00:01:00,510
Not always we will need all the data in them many times we need only partial lead up to be extracted

9
00:01:00,510 --> 00:01:04,420
from it so distracting is a feature we had begun.

10
00:01:04,470 --> 00:01:11,760
In simple we extract the data up from a right hand side expression or statement into a left turn side

11
00:01:11,910 --> 00:01:14,320
variable declarations.

12
00:01:14,610 --> 00:01:22,050
Of course it is a feature we call that in 2015 ECMA and spread is an exact opposite of this proctoring.

13
00:01:22,420 --> 00:01:26,040
It will break an entry into individual components.

14
00:01:26,230 --> 00:01:35,380
That means it is going to allow your energy to spread into multiple arrays and be spread and combined

15
00:01:35,410 --> 00:01:43,690
into a single A or multiple objects can be combined into a single object so one to many is destructuring

16
00:01:44,110 --> 00:01:44,990
many to one.

17
00:01:45,010 --> 00:01:46,640
I can say is spread.

18
00:01:47,080 --> 00:01:50,540
So let us see how practical how we can do this proctoring.

19
00:01:50,960 --> 00:01:52,740
Let me create a new project

20
00:01:56,310 --> 00:01:57,290
like before.

21
00:01:57,450 --> 00:02:00,140
I prefer using empty web site

22
00:02:13,820 --> 00:02:22,050
and in this particular project we would like to add a hit Steimle find and a B is by I fight solution

23
00:02:22,050 --> 00:02:22,630
explorer

24
00:02:26,250 --> 00:02:27,050
G.M.B.H..

25
00:02:27,120 --> 00:02:38,040
Let's call it a moderated daemon and add a typescript fine let's call it a bit more odious and of course

26
00:02:38,330 --> 00:02:41,560
in the water Digimon and drag and drop the motor does.

27
00:02:41,820 --> 00:02:44,690
But as I explained before it will show you.

28
00:02:44,970 --> 00:02:52,620
They were just beneath the combined output of the motor because I know I will obviously make the motor

29
00:02:52,630 --> 00:02:57,770
did start a bit more can close this.

30
00:02:57,810 --> 00:03:00,180
What do we want to do we will do it here now.

31
00:03:00,540 --> 00:03:09,390
So let us say we have got a variable called input on it sent Eddy Eddy off so the top two are enough

32
00:03:09,390 --> 00:03:10,310
for now.

33
00:03:10,680 --> 00:03:18,710
So I haven't any real numbers call in input but generally if I want to extract the numbers what I would

34
00:03:18,710 --> 00:03:30,470
do is allow the LED first call and number is equal to input 0 0.

35
00:03:30,910 --> 00:03:43,780
Likewise a second number in both 1 and yes I would probably want to display first and second in the

36
00:03:48,920 --> 00:03:55,380
Alturas controller 5 to execute open in the browser the motor daemon and that's how we are getting it.

37
00:03:55,380 --> 00:03:57,300
This is ordinary or not.

38
00:03:57,300 --> 00:03:59,970
This can be used in sort of writing like this.

39
00:04:00,210 --> 00:04:08,480
I can use the feature of destructuring So that's what I'll do is I simply say let that in square brackets.

40
00:04:08,570 --> 00:04:10,190
I'll say it first.

41
00:04:10,230 --> 00:04:15,850
Second is equal in that it and it's of the same meaning.

42
00:04:16,200 --> 00:04:24,120
So from an existing any We are restructuring and declaring two variables first and second and first

43
00:04:24,120 --> 00:04:26,220
is going to get the value of input of zero.

44
00:04:26,250 --> 00:04:31,540
Second is going to get the range of input on exactly like this.

45
00:04:31,540 --> 00:04:33,890
No run and we should still get the same output

46
00:04:36,210 --> 00:04:38,290
fantastic.

47
00:04:38,380 --> 00:04:40,820
Now this can be further extended.

48
00:04:40,860 --> 00:04:45,560
What we can do is introverting like this.

49
00:04:45,690 --> 00:04:52,710
Let's say Let f one or some and one column.

50
00:04:52,960 --> 00:04:57,290
I considered dot dot dot and say resti.

51
00:04:57,410 --> 00:04:59,210
I can simply write an essay.

52
00:04:59,480 --> 00:05:06,490
I can write an idea directly rather than another way they will like this nor does this mean.

53
00:05:06,540 --> 00:05:13,830
This means that value and one will be assigned with the value one and one will get the value one and

54
00:05:13,950 --> 00:05:16,990
the rest of the area will be assigned to this rest.

55
00:05:18,840 --> 00:05:28,010
So yes we can probably see a load and one and I would say a lot.

56
00:05:28,100 --> 00:05:32,180
The rest so they can come and did no

57
00:05:36,180 --> 00:05:36,800
one.

58
00:05:36,910 --> 00:05:48,750
And one that is two three four five is the best so we can have it at the where we have structure so

59
00:05:48,750 --> 00:05:51,610
that we can get one value and then the rest of it goes.

60
00:05:51,990 --> 00:05:52,940
Can I put it in.

61
00:05:52,980 --> 00:05:54,700
Yes I can do that also.

62
00:05:54,990 --> 00:06:00,750
In such case work will happen and what will get the value 1 and 2 will get the new tool and the rest

63
00:06:00,750 --> 00:06:01,710
will be 3 4 5

64
00:06:05,100 --> 00:06:16,260
4 5 Not only that if you don't want you can simply sit let in square brackets.

65
00:06:16,660 --> 00:06:27,550
Just give one variable Let's say I'm one and is equal to you given that you know what will happen.

66
00:06:28,550 --> 00:06:34,840
And when we get the value one and the rest of the day we get ignored that's what will happen.

67
00:06:35,990 --> 00:06:39,230
And when we get the rest of the data will be ignored.

68
00:06:41,870 --> 00:06:43,500
And how once one

69
00:06:51,120 --> 00:07:04,300
likewise we can also write lead and give more Conemaugh and to call more entry like this also we can

70
00:07:04,400 --> 00:07:05,920
do that.

71
00:07:06,060 --> 00:07:11,390
Now I give equal to let's say one more to come on three common folk.

72
00:07:11,410 --> 00:07:16,830
I look up to 40 so now what will happen.

73
00:07:17,000 --> 00:07:18,840
We are not taking the value want to.

74
00:07:18,890 --> 00:07:23,720
We are ignoring them but there will be a two and two and four will be as an entity.

75
00:07:23,910 --> 00:07:26,410
What can we have this as a larger area.

76
00:07:28,990 --> 00:07:34,370
We can in sense because what will happen the value 5 and 6 will get ignored.

77
00:07:34,410 --> 00:07:42,050
We are only taking three and four based on the chemical and assigning them to respective M2 m3.

78
00:07:42,070 --> 00:07:43,440
So I put a lot

79
00:07:49,040 --> 00:07:52,960
into m3.

80
00:07:53,440 --> 00:08:00,580
Basically I'm just trying to show you all the possible combinations all the possible indicators of B's

81
00:08:00,580 --> 00:08:11,290
proctoring destructuring and any so yes repeat once again I have an raid which can be displayed to create

82
00:08:11,290 --> 00:08:13,370
multiple variables.

83
00:08:13,480 --> 00:08:19,870
Remember first and second are all independent variables which can be used aspart are conveniently.

84
00:08:20,260 --> 00:08:26,140
Likewise we can have multiple data extracted multiple we are able to extract that and the rest of the

85
00:08:26,140 --> 00:08:29,510
data we can put it into a separate variable.

86
00:08:29,640 --> 00:08:34,740
We can extract only the required quantity the quantity of the data can be only the first element or

87
00:08:34,740 --> 00:08:40,760
it can be subsequent elements so like that we are able to destructor any.

88
00:08:40,830 --> 00:08:45,180
Also what is possible is because this proctored a regular expression.

89
00:08:45,180 --> 00:08:50,130
If you have a spring based on regular expressions we can distractor it.

90
00:08:50,310 --> 00:08:54,390
Let's say for example I didn't even know something like.

91
00:08:54,430 --> 00:08:57,840
Didn't notice it totally failed.

92
00:08:58,140 --> 00:08:59,920
Come on I want your part.

93
00:08:59,950 --> 00:09:03,600
I want money but I won't debunked.

94
00:09:03,820 --> 00:09:05,390
Where do I get this from.

95
00:09:05,550 --> 00:09:13,970
Let us assume that we have our regular expression all representing the need so that the syntax for giving

96
00:09:13,980 --> 00:09:17,510
regular expression a regular expression.

97
00:09:17,550 --> 00:09:22,410
I'm going to have a digit digit digit digit.

98
00:09:22,410 --> 00:09:25,430
That is what your hyphen.

99
00:09:25,560 --> 00:09:27,430
I'm putting brackets that is important

100
00:09:30,530 --> 00:09:31,180
again.

101
00:09:31,330 --> 00:09:32,410
And with it.

102
00:09:32,570 --> 00:09:34,820
That is what month.

103
00:09:35,300 --> 00:09:36,490
I would again.

104
00:09:36,510 --> 00:09:39,300
Or digit a digit.

105
00:09:39,320 --> 00:09:45,220
That is what you've.

106
00:09:45,590 --> 00:09:46,090
So that's it.

107
00:09:46,090 --> 00:09:47,740
That is my regular expression.

108
00:09:47,900 --> 00:09:51,930
There is a remote function called that's easy it's easy.

109
00:09:52,100 --> 00:09:54,010
I can specify the spring.

110
00:09:54,440 --> 00:09:57,570
So I'll give a spring on something like 20:17

111
00:10:00,100 --> 00:10:12,370
let's say 0 7 and some 8 0 1 know what is going to happen here let me just play the whole thing in a

112
00:10:12,370 --> 00:10:14,850
love I can get totally

113
00:10:17,990 --> 00:10:18,960
get in it with

114
00:10:21,840 --> 00:10:24,450
you can get in it with

115
00:10:27,320 --> 00:10:37,260
Mundt concatenated with let's say they know what is going to happen here in square brackets we are in

116
00:10:37,260 --> 00:10:42,240
on bracket we have got four digits so this 4 digits will be used here.

117
00:10:42,730 --> 00:10:48,000
This double digits will be used here 0 7 and 0 1 will be used here.

118
00:10:48,610 --> 00:10:53,050
And this whole regular expression will be then assigned to.

119
00:10:53,050 --> 00:10:57,650
This is going to result in some value that is for the destructor.

120
00:10:57,660 --> 00:11:00,090
Now we're in the total deal.

121
00:11:00,100 --> 00:11:06,540
The total value of this really wasn't a total debt not because we have put brackets here.

122
00:11:06,970 --> 00:11:13,200
This value is going to then get into your disvalue second bracket would be a month.

123
00:11:13,300 --> 00:11:15,130
And third value will be as the

124
00:11:21,930 --> 00:11:27,100
see this total year month day.

125
00:11:27,110 --> 00:11:33,340
Now what is important is also you know if I remove brackets what will happen let's evitable the brackets.

126
00:11:33,740 --> 00:11:41,840
So you will be assigned to this that's your value with this but money will be not taken from here.

127
00:11:46,990 --> 00:11:52,340
You see Montaigne's you don't know which is this part.

128
00:11:52,600 --> 00:11:57,450
And of course we don't have anything for day because her bracket is missing.

129
00:11:57,520 --> 00:12:04,210
So brackets are very important in the expression so complete value of irregular expression will be de-conflict

130
00:12:04,570 --> 00:12:10,930
the first element in the left and side first available on the left and take the rest of it will be decided

131
00:12:10,930 --> 00:12:19,690
based on number of brackets would you be happy at that so it's easy Wachs But what is very important

132
00:12:19,690 --> 00:12:25,140
is if the value is not as for this format what will happen.

133
00:12:25,600 --> 00:12:28,700
For example actually two digits are expected only.

134
00:12:29,000 --> 00:12:35,350
Let me put brackets here.

135
00:12:35,530 --> 00:12:36,360
Now this

136
00:12:42,310 --> 00:12:43,530
we are going to get an

137
00:12:48,160 --> 00:12:48,940
error that's gone

138
00:12:52,120 --> 00:12:58,730
self-dual.

139
00:12:59,310 --> 00:13:03,520
So what we should do in those kind of cases ideally in those kind of cases.

140
00:13:03,610 --> 00:13:04,970
Or with no

141
00:13:10,250 --> 00:13:11,840
or maybe some default value

142
00:13:17,920 --> 00:13:23,190
0 0 0 0.

143
00:13:23,220 --> 00:13:24,320
Let's run this not

144
00:13:27,550 --> 00:13:28,720
or 40 will that.

145
00:13:28,900 --> 00:13:29,520
Right.

146
00:13:29,620 --> 00:13:31,960
Because the expression is invalid.

147
00:13:32,050 --> 00:13:39,610
So when an expression is not granted this is going to give us an error to avoid that error or it or

148
00:13:39,610 --> 00:13:45,590
it with something that is meaningful to the context and then appropriately deal with the if condition.

149
00:13:46,000 --> 00:13:50,990
If total debt is not equal to zero like that we love to hate the good and so on.

150
00:13:51,670 --> 00:13:57,630
So this is how we are going to restructure an airway and a regular expressions.

151
00:13:57,910 --> 00:13:58,690
That's all it does.

152
00:13:58,750 --> 00:13:59,220
Thank you.

