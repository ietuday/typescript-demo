1
00:00:16,730 --> 00:00:22,320
More form controls in the previous example we have seen only text boxes.

2
00:00:22,710 --> 00:00:29,010
But what if I have multiple controls on or off different things like I would like to have a drop down

3
00:00:29,010 --> 00:00:29,370
list.

4
00:00:29,370 --> 00:00:37,810
And really what I'm also insets guess what Grubel first let us provide a facility to have them.

5
00:00:38,080 --> 00:00:46,460
So I would like to add to this but some form component to more properties I call it as an adult and

6
00:00:46,810 --> 00:00:49,530
that is also a form control.

7
00:00:50,320 --> 00:00:57,580
I would like to have a qualification of the person which is also a form control.

8
00:00:58,000 --> 00:01:06,970
So obviously this movie or any slates that people do here does not end up as people do new form of control.

9
00:01:09,670 --> 00:01:18,210
They mean as the default rather does dord qualification is it called the new form control.

10
00:01:19,040 --> 00:01:25,580
And will you same me as the default value for gender.

11
00:01:25,580 --> 00:01:29,080
I would like to use the radio buttons on for qualification.

12
00:01:29,090 --> 00:01:31,290
I would like to use dropdown list.

13
00:01:31,530 --> 00:01:36,640
OK that is a problem list we will need an area of values so I'll call it that.

14
00:01:36,660 --> 00:01:43,040
Degrees is equal to an empty square basket say something like we up

15
00:01:46,520 --> 00:01:54,530
B B and B with some bachelor's degree.

16
00:01:54,560 --> 00:02:01,100
So I would like to display in the dropdown list and from this read one of the values I would like to

17
00:02:01,100 --> 00:02:08,200
use for qualification current qualification of the parser graduation basically.

18
00:02:08,680 --> 00:02:10,390
So now what's next.

19
00:02:10,400 --> 00:02:15,120
Now go to the Steimle template template.

20
00:02:15,150 --> 00:02:21,270
We would like to add two more groups.

21
00:02:21,270 --> 00:02:31,190
This is full and up and this is for lipsticked qualification now.

22
00:02:31,260 --> 00:02:33,130
Gender is what really over them.

23
00:02:33,330 --> 00:02:43,280
So in way you can do without it is this I'll call it gender form control Neal also should be gender.

24
00:02:43,470 --> 00:02:50,450
Basically this is the name which we have used in the form component required is not applied but really

25
00:02:50,450 --> 00:02:56,390
over them and the ready made so obviously value should be provided.

26
00:02:56,630 --> 00:03:08,100
Sovaldi also that mean same thing one more copy form control everything the same just that this is female.

27
00:03:08,150 --> 00:03:18,160
And here also I give female to reduce male female for gender and qualification I would like to have

28
00:03:18,160 --> 00:03:19,570
a drop down list.

29
00:03:19,990 --> 00:03:26,560
So how to go for any of them just for that we use select or select that is supposed to help

30
00:03:29,510 --> 00:03:38,590
form control that is for bootstrapping and very important form of control name that activists should

31
00:03:38,590 --> 00:03:43,690
have the value qualification which is used in phone company.

32
00:03:43,990 --> 00:03:48,280
But what does this supposed to have options that are not an option.

33
00:03:48,320 --> 00:03:49,850
We want the reproduction back.

34
00:03:49,850 --> 00:04:03,940
So we lose and the full what is equal degree or degrees that the property have given.

35
00:04:04,040 --> 00:04:17,120
So for that we'll do the binding one way by adding value to the degree and of course the displayable

36
00:04:17,190 --> 00:04:18,900
is also degli.

37
00:04:19,540 --> 00:04:26,110
So drop down is going to pay for different values in their degrees and what will be the selected value

38
00:04:26,620 --> 00:04:28,990
that can be based on the value of qualification.

39
00:04:29,920 --> 00:04:31,560
So everything is sort of insane.

40
00:04:31,750 --> 00:04:34,770
I just added two new input elements here.

41
00:04:34,780 --> 00:04:39,900
One rebuttal and another popped up on programming then is also different

42
00:04:43,430 --> 00:04:44,650
but it's not coming good.

43
00:04:44,780 --> 00:04:48,330
So I think we can the modus flood control

44
00:04:51,820 --> 00:04:57,860
and just get the basic radio done then don't we.

45
00:04:57,920 --> 00:05:01,360
FREEMAN Oh they're not listed again in the autumn.

46
00:05:01,450 --> 00:05:12,390
So while is not listed in knobbed you have to be sure that in the object sort of agenda which is to

47
00:05:12,740 --> 00:05:14,630
distort the property

48
00:05:19,390 --> 00:05:26,540
and qualification is up to distort qualification.

49
00:05:29,690 --> 00:05:36,310
And as noted we are buffing Yes we have got full balance.

50
00:05:36,690 --> 00:05:37,610
Female.

51
00:05:38,130 --> 00:05:38,980
Male.

52
00:05:40,210 --> 00:05:44,910
It certainly can be jeans easy in job down.

53
00:05:45,080 --> 00:05:47,600
Oh something is wrong.

54
00:05:48,000 --> 00:05:52,160
All four of them have come as one single option.

55
00:05:56,980 --> 00:06:02,020
Template on bosomed something is wrong here.

56
00:06:02,740 --> 00:06:10,030
Yeah this should all be different than this it became a single thing.

57
00:06:10,080 --> 00:06:12,420
Now I've separated them into different strings

58
00:06:17,710 --> 00:06:20,820
of it extending the

59
00:06:24,630 --> 00:06:30,900
SO LIKE THIS any one element can be used for every Steimle fun element which would provide form control

60
00:06:30,900 --> 00:06:42,720
name binded to 0 value which is a key inform on each form group element is supposed to have a value

61
00:06:42,810 --> 00:06:51,900
as form control and form control can provide a default value in case we want and very important how

62
00:06:51,950 --> 00:06:53,670
are we going to get to the top.

63
00:06:53,720 --> 00:06:58,750
Does God want some form that first name this dog person form that last name.

64
00:06:58,820 --> 00:07:04,390
Is not some form that genders does that put some form of qualification like this we are going to get

65
00:07:04,420 --> 00:07:15,090
that done by the same example that in the existing form only some form competently or blended are these

66
00:07:15,180 --> 00:07:23,010
three new properties added them a third form control where we have our general qualification.

67
00:07:23,140 --> 00:07:24,430
And also we have to

68
00:07:27,370 --> 00:07:32,020
the normal to the next topic form control properties.

69
00:07:32,090 --> 00:07:37,430
Right now we are getting the form control that is an object.

70
00:07:37,430 --> 00:07:40,800
Here in the Component class.

71
00:07:41,330 --> 00:07:47,870
But what if we want to use this form control for some purpose within the template itself how can we

72
00:07:47,870 --> 00:07:49,290
get the value of it.

73
00:07:49,580 --> 00:08:04,520
So for example now I would like to use the I would like to get for some form dog get what we want to

74
00:08:04,520 --> 00:08:14,690
get a first name for that first name we want and so thought by likewise.

75
00:08:14,710 --> 00:08:15,940
This is what first name

76
00:08:18,890 --> 00:08:21,480
first name value.

77
00:08:21,500 --> 00:08:31,340
Likewise I would like to have first names of songs so we can give dog status.

78
00:08:31,580 --> 00:08:39,130
And this is for you this is all status

79
00:08:44,890 --> 00:08:46,290
life right on this

80
00:08:50,580 --> 00:08:52,850
here we are going to get the

81
00:08:56,030 --> 00:09:06,140
this is the status valid or it is valid because the river to remove first name it becomes invalid and

82
00:09:06,140 --> 00:09:12,590
there is no value for posting this validation is being performed because we have provided the required

83
00:09:12,680 --> 00:09:18,010
attribute here in the template for first name we have provided the requirements.

84
00:09:18,470 --> 00:09:22,950
So that's quite a tribute to his checking whether it is valid or invalid.

85
00:09:24,860 --> 00:09:31,760
Likewise in case we want we can also get to other properties like pristine which is true if the user

86
00:09:31,760 --> 00:09:41,350
has not changed the value in the UK or it can be untouched if control has not even entered.

87
00:09:41,360 --> 00:09:46,760
That means we have not focused into that control yet then it is untouched.

88
00:09:47,270 --> 00:09:57,700
Obviously there are other properties touched and dirty also pristine and dirty mind control body.

89
00:09:57,710 --> 00:10:05,500
Likewise here we have mind control not mind control not touch frequency.

90
00:10:06,440 --> 00:10:08,210
So these are different properties.

91
00:10:10,690 --> 00:10:13,150
Value state does pristine.

92
00:10:13,230 --> 00:10:17,620
Derby got untouched but what are they going to get of these.

93
00:10:17,620 --> 00:10:24,640
Based on these properties we can probably hide and show certain other controls especially the validation

94
00:10:24,640 --> 00:10:27,460
controls can be used here.

95
00:10:28,050 --> 00:10:35,380
Now something very important right now I don't have anything like a model object.

96
00:10:35,380 --> 00:10:37,850
What we have is a form model.

97
00:10:38,050 --> 00:10:45,160
But what we want if we have a data model data model is an object which I'm seeing we are going to get

98
00:10:45,160 --> 00:10:53,280
from sort of a for example in this case let's say I have an object called a person for the right the

99
00:10:53,300 --> 00:10:59,650
person class and what is that a person would have maybe a person plus perhaps

100
00:11:08,640 --> 00:11:19,000
something like first name string last name and begin to have gender.

101
00:11:19,490 --> 00:11:29,010
Actually you can go for them but for different purposes this is fine qualification so this is my parcelled

102
00:11:29,010 --> 00:11:35,040
plus an object of this class is what I'm going to get by making is called book.

103
00:11:35,080 --> 00:11:37,730
So word sight EPA that.

104
00:11:37,900 --> 00:11:45,080
We are making and that object that I see in the slides in the form component.

105
00:11:45,310 --> 00:11:52,850
So you know I'm going to have one more object with this person and that is of type person.

106
00:11:53,320 --> 00:11:56,590
But after includeonly import

107
00:11:59,740 --> 00:12:06,020
some from daunts last

108
00:12:09,910 --> 00:12:13,000
so this we are initialized now.

109
00:12:13,340 --> 00:12:18,940
Let's say I'm going to look initialises some of that in on in a maybe over here.

110
00:12:20,180 --> 00:12:22,880
Is equal to the new person.

111
00:12:24,830 --> 00:12:31,190
And we also put some data in that person object for the time being we put constant but assumption is

112
00:12:31,280 --> 00:12:38,550
we are going to get this data up from sort of this dog because he is a member of this dog.

113
00:12:39,200 --> 00:12:49,820
So this dog person dog was mean maybe something like ABC this dog person dog last name

114
00:12:52,530 --> 00:12:53,840
say x y z

115
00:12:57,200 --> 00:13:09,180
this dog or some dot gender female This dog or some dog qualification

116
00:13:12,730 --> 00:13:16,660
and maybe is so right.

117
00:13:16,670 --> 00:13:24,160
No I'm putting constructs but otherwise assumed that these values are all going to come from our But

118
00:13:24,160 --> 00:13:29,400
not the point is how am I going to set the value for this object.

119
00:13:29,620 --> 00:13:33,600
That is the moral optic we have here present group.

120
00:13:33,970 --> 00:13:36,730
How are we going to set the value for this person group.

121
00:13:36,730 --> 00:13:42,300
That's what is very important foretelling the value of person who may maybe here.

122
00:13:42,300 --> 00:13:44,130
I can't wait to go.

123
00:13:44,130 --> 00:13:47,520
We will make use of a set value method.

124
00:13:47,970 --> 00:13:59,730
So here we have person follow dot dot dot for some form Daut set value.

125
00:14:01,790 --> 00:14:07,340
Set values the method we are supposed to make you go but what we given in value.

126
00:14:07,630 --> 00:14:16,690
Instead we are supposed to provide the dictionary as you can see it so floor bracket What is that we

127
00:14:16,690 --> 00:14:25,840
want to set the form property first name which should it take the value from this dog person dog first

128
00:14:25,840 --> 00:14:28,070
name.

129
00:14:28,520 --> 00:14:40,770
Likewise last mean they should just take the value from the mortal object first and last name same gender.

130
00:14:43,110 --> 00:14:46,530
This door or some door gender

131
00:14:49,780 --> 00:14:57,440
finally qualification does not person door qualification.

132
00:15:00,440 --> 00:15:02,190
So set values the method.

133
00:15:02,540 --> 00:15:09,490
What is that really going to do for this person form that is far more than this is far more late for

134
00:15:09,660 --> 00:15:17,180
model is going to take data from the more than object test come from Sarber we can call it as a model.

135
00:15:17,420 --> 00:15:25,310
So that model is essential for model and form model is then binding to the form input elements.

136
00:15:25,340 --> 00:15:33,960
Very important data model is not binding the final in particular months unlike as in the template driven

137
00:15:33,970 --> 00:15:37,470
form we have to.

138
00:15:37,660 --> 00:15:44,640
So let us run and see if we are going to get these values EBC the x y z female and B B as are the values

139
00:15:44,640 --> 00:15:51,960
which we are supposed to get or some has not explored.

140
00:15:53,280 --> 00:15:56,950
Export Glaus be a case for some

141
00:16:05,210 --> 00:16:12,090
perfect you know but we see the x y z gender and I it this before.

142
00:16:13,490 --> 00:16:19,640
So this is all sometimes if but we have the object we would want to probably set the value in

143
00:16:25,310 --> 00:16:29,360
in is because right now are going on in it.

144
00:16:29,400 --> 00:16:33,420
But same thing sometimes can also be done on teenagers.

145
00:16:33,450 --> 00:16:41,580
Very important on teenagers is going to execute what ever the component data is going to teemed input

146
00:16:41,580 --> 00:16:47,500
property changes when the components input property changes on this method will execute.

147
00:16:47,700 --> 00:16:53,550
And maybe if this is input property imagine personally the input property code is not but in projects

148
00:16:53,550 --> 00:17:00,180
it might be maybe we have a parent component in which we have got the list of all parts and clicking

149
00:17:00,180 --> 00:17:01,710
on one of the person I would live.

150
00:17:01,830 --> 00:17:05,620
I would probably navigate to the child company in such kind of cases.

151
00:17:05,650 --> 00:17:09,840
Yes we would probably have them on tenders.

152
00:17:10,120 --> 00:17:16,320
Now something very important about set by you you should know is when you're trying to set the value

153
00:17:16,320 --> 00:17:23,790
of the form form go by making you upset the value you're supposed to set value for each and every property

154
00:17:23,860 --> 00:17:25,320
of all.

155
00:17:25,740 --> 00:17:30,640
If you miss any one of them let's say I missed this automatically.

156
00:17:30,660 --> 00:17:33,180
All the properties will become null.

157
00:17:33,440 --> 00:17:34,800
You'll see not everything is not

158
00:17:37,940 --> 00:17:40,660
all at least the default will is.

159
00:17:40,750 --> 00:17:44,520
That means the impact of set value has been ignored.

160
00:17:45,510 --> 00:17:50,270
Nothing has happened because of said the values are the way I have given here.

161
00:17:50,510 --> 00:17:54,370
When they created the contour so asset value did not work at all.

162
00:17:54,920 --> 00:18:01,280
And actually speaking you'll see that there is an exception thrown press afterward and you see areas

163
00:18:01,290 --> 00:18:06,220
that must supply value for the form controlled with name gender.

164
00:18:06,230 --> 00:18:14,700
We have not plate so what is that we generally do when you are using self-value you compulsory ensure

165
00:18:14,700 --> 00:18:16,590
that every property is.

166
00:18:17,130 --> 00:18:26,100
All you can't replace that value with that value it back to back to you at one point it does more than

167
00:18:26,110 --> 00:18:26,760
ever.

168
00:18:27,000 --> 00:18:34,990
And of course fuel properties can be set in strong all properties before Maylis said no see ABC the

169
00:18:34,990 --> 00:18:35,900
x y z.

170
00:18:36,130 --> 00:18:40,360
I actually get female but male was the original vallium it really.

171
00:18:42,770 --> 00:18:43,940
So be careful.

172
00:18:44,000 --> 00:18:48,560
You have to use either set or patch when you based on the requirement.

173
00:18:48,950 --> 00:18:56,170
So we can get the form control properties by using get and we have seen how we can get the value of

174
00:18:56,170 --> 00:18:58,490
a form control and status property.

175
00:18:58,730 --> 00:19:05,150
Of course other properties can also be produced and then we have seen set value by time to metalsmiths

176
00:19:05,150 --> 00:19:12,980
can be used for setting the state of form data by getting the data from the object which probably we

177
00:19:12,980 --> 00:19:14,900
would have got from the server.

178
00:19:15,260 --> 00:19:19,130
Basically the data model this I'll call it a data model.

179
00:19:19,370 --> 00:19:22,340
And this actually does form a model.

180
00:19:22,480 --> 00:19:30,410
So in the active forms what is binded the form that Stevel template is a form order and not the data

181
00:19:30,410 --> 00:19:34,920
model but the data model can be used for setting the form model.

182
00:19:35,480 --> 00:19:36,040
That's all.

183
00:19:36,050 --> 00:19:36,590
And this.

184
00:19:36,770 --> 00:19:37,160
Thank you.

