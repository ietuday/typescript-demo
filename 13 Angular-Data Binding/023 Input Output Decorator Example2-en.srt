1
00:00:17,040 --> 00:00:22,840
In the previous session I've explained to you how we can make use of the input and output decorator's

2
00:00:24,330 --> 00:00:30,860
and we have seen how we can parse the top from the time component to the parent component and then encumbering

3
00:00:30,870 --> 00:00:35,970
to the child component rather to attributes in the parent company.

4
00:00:36,600 --> 00:00:41,960
We're able to set the properties of the child component and from time component we are able to raise

5
00:00:41,970 --> 00:00:48,670
that event in the main component because this particular topic is a little complicated.

6
00:00:48,690 --> 00:00:52,320
I would like to take here one more example on the same lines.

7
00:00:53,250 --> 00:01:00,390
This time I would like to look inside their component not the size or component is going to do is increase

8
00:01:00,420 --> 00:01:05,120
or decrease the size of the font in the container component.

9
00:01:05,580 --> 00:01:11,990
So you're first in the project I would like to add Nahoum a new typescript document.

10
00:01:12,540 --> 00:01:21,450
Let's call it a sizer got component or B has to watch and with that it's either component or ideas.

11
00:01:21,630 --> 00:01:30,100
I would like to import as usual component then we would like to have input all the code

12
00:01:33,780 --> 00:01:35,380
underscores even a meta

13
00:01:38,430 --> 00:01:39,150
from

14
00:01:42,010 --> 00:01:45,490
I read last last call

15
00:01:49,850 --> 00:01:57,160
then I would like to build up a component component to the decorator of things that will help.

16
00:01:57,180 --> 00:01:59,010
First as usual the selector

17
00:02:11,150 --> 00:02:13,210
to what is the selector.

18
00:02:13,250 --> 00:02:15,540
Let's call it My size of

19
00:02:19,000 --> 00:02:20,420
Komarr.

20
00:02:20,470 --> 00:02:21,630
Then we use a template

21
00:02:27,410 --> 00:02:29,630
multi-line template so I'll use the symbol

22
00:02:33,810 --> 00:02:37,530
on finally exit of course.

23
00:02:37,530 --> 00:02:42,630
After this we are supposed to have a plus export clause.

24
00:02:42,710 --> 00:02:44,140
Let's call it just up.

25
00:02:45,260 --> 00:02:52,660
Component what we want to do using the template.

26
00:02:52,880 --> 00:02:54,680
I would like to first place a label

27
00:02:58,970 --> 00:03:00,480
for this label.

28
00:03:00,800 --> 00:03:03,180
We would like to put away oh Pharmac

29
00:03:10,120 --> 00:03:12,000
font size.

30
00:03:12,010 --> 00:03:14,630
What does font size does.

31
00:03:14,730 --> 00:03:20,890
We're going to have it as a property that does sort of fall into place.

32
00:03:21,710 --> 00:03:24,070
So size is going to be a property here.

33
00:03:29,730 --> 00:03:34,020
And maybe we can also set the default value for it in case you want that this looks say that

34
00:03:37,080 --> 00:03:39,020
now it is similar to this.

35
00:03:39,060 --> 00:03:41,070
I would like to put your two bottoms

36
00:03:47,280 --> 00:03:54,060
handle the equipment and make the I would like to resize the font by Plus one

37
00:03:58,200 --> 00:04:02,040
text will get let's say the same minus symbol.

38
00:04:02,120 --> 00:04:11,410
I mean the plus symbol increase the font size basically a copy of this resize matter only I would like

39
00:04:11,410 --> 00:04:13,490
to call but put negative value.

40
00:04:15,090 --> 00:04:17,490
So now I have to write resize Mr.

41
00:04:20,770 --> 00:04:24,360
on what are we doing in this what is the bottom.

42
00:04:24,450 --> 00:04:25,810
Let's say that is offset

43
00:04:31,280 --> 00:04:38,040
we are going to increase or decrease the size.

44
00:04:38,420 --> 00:04:39,010
That said

45
00:04:42,120 --> 00:04:44,970
what is wrong here this.

46
00:04:45,110 --> 00:04:46,680
Now I would like to also write some.

47
00:04:46,700 --> 00:04:48,850
It is not a politician.

48
00:04:49,110 --> 00:04:57,020
That means I would like to have at any point of time the minimum font size.

49
00:04:57,140 --> 00:04:59,900
So how do we get my main

50
00:05:04,740 --> 00:05:06,690
many more to use.

51
00:05:06,820 --> 00:05:08,240
Now what should be two values.

52
00:05:09,600 --> 00:05:12,950
Well if bless

53
00:05:16,090 --> 00:05:21,410
downsizing were to say I would like to take Cuomo 40.

54
00:05:21,490 --> 00:05:29,600
That means maximum fiancs we are going to take as per diem and address this so all of this is less

55
00:05:29,690 --> 00:05:31,950
I would like to take that as the minimum.

56
00:05:32,330 --> 00:05:37,080
So youre able to move this to a simple Likewise

57
00:05:43,730 --> 00:05:52,070
if something else does not say is less than 8 then I would like to also respect the size

58
00:05:55,970 --> 00:06:05,450
so actually speaking I try to make it simple less equal to offset.

59
00:06:05,930 --> 00:06:13,140
It does not say is that than 40 and is not to say this the

60
00:06:18,420 --> 00:06:29,000
if the site is less than it's strict in the range not this is a simple component based on these bottles

61
00:06:29,360 --> 00:06:32,690
were increasing or decreasing the value of this size.

62
00:06:32,990 --> 00:06:40,100
So my requirement is I should be able to sell this size from the parent company so let them for size

63
00:06:40,190 --> 00:06:41,120
I should wait here.

64
00:06:41,130 --> 00:06:51,370
I read in boys when they are the size teenagers the data sites should be communicated back to the parent

65
00:06:51,370 --> 00:06:55,670
component so that parent company can use the latest sites.

66
00:06:55,750 --> 00:06:56,550
How can I do that.

67
00:06:56,710 --> 00:06:59,100
How can I do that for this use.

68
00:06:59,160 --> 00:07:07,240
Even so I'll break your heart and give the even name which is Bageant property name only I it that size

69
00:07:07,710 --> 00:07:11,480
change which is equal to new event.

70
00:07:11,520 --> 00:07:20,050
And we talk and we want to work a number which is the new size does day to day as it is should be used

71
00:07:20,050 --> 00:07:20,460
here.

72
00:07:22,380 --> 00:07:30,000
So now obviously even the size changes I would like to raise that even that is yes I should like to

73
00:07:30,240 --> 00:07:32,810
know what should be the goal.

74
00:07:34,460 --> 00:07:40,620
Amityville so does dog size change dog.

75
00:07:40,670 --> 00:07:47,600
Mean what does dog size we would like to limit the size.

76
00:07:47,840 --> 00:07:48,940
What is wrong here.

77
00:07:49,140 --> 00:07:51,880
Is spelling mistake.

78
00:07:52,080 --> 00:07:57,950
So let's say this is a simple company which we have the oldest component I would like to use in the

79
00:07:59,130 --> 00:08:01,050
main main complaint.

80
00:08:01,430 --> 00:08:05,590
So I'll go to our dot com Yes.

81
00:08:06,700 --> 00:08:08,040
All that's fine.

82
00:08:11,980 --> 00:08:13,000
Your last.

83
00:08:13,030 --> 00:08:18,190
Can Make You Love this company and quadruple that.

84
00:08:18,250 --> 00:08:19,340
Simply put my size

85
00:08:25,640 --> 00:08:33,780
but before we can use my size it here we have to include size or component in Modu.

86
00:08:33,950 --> 00:08:41,840
So you're of the size of component and the size or component that's fine we'll have to import

87
00:08:44,640 --> 00:08:45,120
from

88
00:08:48,130 --> 00:08:50,670
last so that wouldn't

89
00:08:57,800 --> 00:09:08,010
know because we know size is the property and size and we are not winning.

90
00:09:08,630 --> 00:09:12,580
We can actually do two rebinding is equal to.

91
00:09:12,890 --> 00:09:22,160
We'll have a local property here called forum sites and say they supposed to be a property in this particular

92
00:09:22,160 --> 00:09:22,910
class.

93
00:09:24,200 --> 00:09:25,430
I get to the last

94
00:09:32,320 --> 00:09:34,640
full size which we can Google does.

95
00:09:34,720 --> 00:09:38,570
Maybe Ted.

96
00:09:38,690 --> 00:09:48,020
So what we are doing now when we teach on sites that tend to sites when we downsize that in just on

97
00:09:48,120 --> 00:09:49,450
sites.

98
00:09:49,520 --> 00:09:56,850
So what we can do because franaise is binded you're going to have some texta which is as per the that

99
00:09:56,870 --> 00:10:02,730
fansites before this we have to actually do the same thing.

100
00:10:03,030 --> 00:10:09,170
Well this topic I haven't covered yet that we are going to see later but styled by the font font size

101
00:10:09,570 --> 00:10:11,000
door in pixels.

102
00:10:11,000 --> 00:10:22,070
I would like to see it so I'll have to give Does the font size some sample

103
00:10:24,710 --> 00:10:25,340
text

104
00:10:34,690 --> 00:10:41,250
so not if it does you're going to have hopefully it first

105
00:10:46,250 --> 00:10:53,520
notice and you see it by default it's text minus plus I should actually

106
00:10:57,150 --> 00:10:59,670
there is wrong in my components either component.

107
00:10:59,810 --> 00:11:00,870
I've done them instead.

108
00:11:00,920 --> 00:11:08,640
This is my notes to doing with the font size degrees of font size of any control.

109
00:11:08,670 --> 00:11:10,490
We can simply use this.

110
00:11:10,770 --> 00:11:12,180
This is a palm print

111
00:11:15,820 --> 00:11:22,490
so this is all we are able to raise from the component and even through it we are getting the size here.

112
00:11:23,960 --> 00:11:28,650
On from here we are able to synthesize the way I think is possible.

113
00:11:30,720 --> 00:11:36,520
So this is all input and output are useful and probably with this example things sort of become much

114
00:11:36,770 --> 00:11:48,200
into the same examples here say the component emit we have to include in the app model and in the computer

115
00:11:48,290 --> 00:11:52,300
we have to put myself in the resizable text

116
00:11:57,730 --> 00:12:02,900
the final topic in this particular Mordieu attribute binding.

117
00:12:03,270 --> 00:12:09,990
So we have seen that whenever we are doing binding we are doing it on the basis of Dom property.

118
00:12:10,360 --> 00:12:15,580
There should be a dom property and that when we put it in the square bracket we can do one rebinding

119
00:12:17,210 --> 00:12:25,580
or we have seen in the immortal binding that it's binding but there are certain cases where we do not

120
00:12:25,580 --> 00:12:30,130
have the property for a given attribute.

121
00:12:30,800 --> 00:12:33,720
So in those kind of cases it is that we should do.

122
00:12:34,100 --> 00:12:43,270
We must use attribute binding when there is no element property to bind consider RIAA as we span span

123
00:12:43,310 --> 00:12:44,050
trivial.

124
00:12:44,130 --> 00:12:45,400
These are not there.

125
00:12:45,870 --> 00:12:51,960
So when we do not have a property for such kind of attributes of attack then compulsorily we have to

126
00:12:51,960 --> 00:12:54,810
go for attribute binding.

127
00:12:54,870 --> 00:13:00,570
For example we go for lower circles and we give like this.

128
00:13:00,570 --> 00:13:07,650
So this is going to be treated as an angle of expression being assigned to a property but we are going

129
00:13:07,650 --> 00:13:09,070
to get an error for this.

130
00:13:09,070 --> 00:13:17,310
We get parser error for this we call spired since it is not a known Native property we're going to get

131
00:13:17,310 --> 00:13:24,470
an error colspan is not ENBOM property for theory and hence the error.

132
00:13:24,830 --> 00:13:27,400
So in some kind of case what is the solution.

133
00:13:29,010 --> 00:13:36,270
As a Mathis's deliverymen does not have colspan property it just calls but attribute attribute is there

134
00:13:36,330 --> 00:13:45,560
for DD but for the object representing this doc colspan is not a property that's property binding one

135
00:13:45,560 --> 00:13:50,760
way binding of an expression to that attribute is not valid.

136
00:13:51,290 --> 00:13:52,300
So what is the solution.

137
00:13:52,310 --> 00:13:53,740
You have to write like this.

138
00:13:53,900 --> 00:14:00,320
Just put any spare bracket the OP got the name of that to be boot into the order.

139
00:14:00,530 --> 00:14:05,690
The name of that would be nothing said what do you like to have to do this so automatically that we

140
00:14:05,840 --> 00:14:07,940
can get all.

141
00:14:07,940 --> 00:14:11,060
So this is called an attribute binding.

142
00:14:11,150 --> 00:14:16,040
So whenever there are attributes for which there is no property.

143
00:14:16,040 --> 00:14:25,690
The best way to do is attribute the binding that a binding expression can be assigned to the attribute

144
00:14:26,540 --> 00:14:32,870
pre-qualifying that bit it up got in square brackets that is important square brackets is important.

145
00:14:32,870 --> 00:14:35,490
Otherwise it is not going to be binding.

146
00:14:36,080 --> 00:14:42,050
So that is the only thing you have to take care of about the attribute binding on because the clause

147
00:14:42,050 --> 00:14:47,970
binding installed style binding are two major topics I would like to cover them as separate more do

148
00:14:48,710 --> 00:14:51,490
so in this particular chapter in this particular model.

149
00:14:51,500 --> 00:14:58,580
We have seen how they can have binding properties and interpellation basically hold the property binding

150
00:14:58,590 --> 00:15:02,940
or one way binding is implemented using square brackets syntax.

151
00:15:03,230 --> 00:15:06,630
How binding is implemented using grown brackets syntax.

152
00:15:07,000 --> 00:15:14,360
How we can make use of the event object and then we have seen how we can have the rebinding square brackets

153
00:15:14,360 --> 00:15:18,630
on bracket combination with the help of the model.

154
00:15:18,690 --> 00:15:22,760
Finally just don't recover aggregate by adding a very small topic though.

155
00:15:23,270 --> 00:15:29,360
Basically for only those special cases where we do not have an attribute mapped to any property of the

156
00:15:29,390 --> 00:15:34,590
DOM object and of course the state and class binding will take it in the next party.

157
00:15:34,820 --> 00:15:35,330
Thank you.

