1
00:00:16,610 --> 00:00:24,950
Destructuring and spreads another very important topic which is based on data types especially the areas

2
00:00:24,950 --> 00:00:27,230
that are tight and the object the debate.

3
00:00:27,650 --> 00:00:33,980
So how exactly is typescript exploiting this feature so that the code can be reduced the goal written

4
00:00:33,980 --> 00:00:36,800
by the developer can be further reduced.

5
00:00:36,830 --> 00:00:43,850
So yes what is this proctoring and what is bread these proctoring is that a convenient way of extracting

6
00:00:43,850 --> 00:00:47,420
data are stored in an object or an attic.

7
00:00:47,710 --> 00:00:53,990
So whether it is an object which is made up of properties or an array which is made up of elements not

8
00:00:54,050 --> 00:01:00,680
always we would need all the data in the many times we need only partially lead up to be extracted from

9
00:01:00,680 --> 00:01:01,510
it.

10
00:01:01,520 --> 00:01:09,200
So restructuring is a feature we had begun in simple we extract the data up from a right hand side expression

11
00:01:09,290 --> 00:01:14,340
or statement into a live Tensei variable declarations.

12
00:01:14,540 --> 00:01:21,970
Of course it is a feature which was added in 2015 ECMA and spread is an exact opposite of this proctoring.

13
00:01:22,340 --> 00:01:26,030
It will break and break into individual components.

14
00:01:26,150 --> 00:01:35,300
That means it is going to allow your Arey to spread into multiple arrays and be spread and combined

15
00:01:35,330 --> 00:01:40,980
into a single area or multiple objects can be combined into a single object.

16
00:01:41,270 --> 00:01:44,930
So one in too many is destructuring many to one.

17
00:01:44,930 --> 00:01:50,430
I can say is spread so let's see know practically how we can do this practice.

18
00:01:51,020 --> 00:01:52,660
Let me create a new project

19
00:01:56,230 --> 00:01:57,210
like before.

20
00:01:57,370 --> 00:02:00,010
I prefer using the Web site

21
00:02:13,810 --> 00:02:21,980
on this particular project we would like to add a hit Steimle find on a be is by next to define solution

22
00:02:21,980 --> 00:02:22,550
explorer

23
00:02:26,310 --> 00:02:33,630
G.M.B.H. let's call it Digimon and add Arbeitskraft.

24
00:02:33,690 --> 00:02:35,700
Fine let's call it an order das.

25
00:02:37,270 --> 00:02:41,420
And of course in the model Digimon are drag and drop them all to be as.

26
00:02:41,740 --> 00:02:48,270
But as I explained before it will show you they were just beneath the compiler or part of the model

27
00:02:48,470 --> 00:02:56,780
because I not I will obviously make the motor did stimulate this thought a bit more to get close to

28
00:02:56,780 --> 00:02:57,700
this.

29
00:02:57,740 --> 00:02:59,940
What do we want to do we will do it here.

30
00:03:00,470 --> 00:03:06,390
So let's say we have got a variable called input and it isn't any at all.

31
00:03:06,400 --> 00:03:10,280
So the two are enough for now.

32
00:03:10,610 --> 00:03:15,600
So I haven't any real numbers call that in.

33
00:03:16,070 --> 00:03:22,550
But generally if I want to extract the numbers what I would do is allow the LED first call that number

34
00:03:24,440 --> 00:03:30,410
is equal to input 0 0.

35
00:03:30,830 --> 00:03:36,760
Likewise I'll say second number in one.

36
00:03:37,120 --> 00:03:44,070
And yes I would probably want to display first and second in the other

37
00:03:48,860 --> 00:03:53,140
Alturas controlling 5 to execute open in the browser the moderates.

38
00:03:53,500 --> 00:03:55,300
And that's how we're getting it.

39
00:03:55,300 --> 00:03:57,090
This is all or not.

40
00:03:57,220 --> 00:03:59,920
This can be used in sort of writing like this.

41
00:04:00,130 --> 00:04:08,500
I can use the feature of destructuring So that's where Waterloo and implicit let that in square bracket.

42
00:04:08,500 --> 00:04:12,170
I'll say first second is equal in.

43
00:04:12,420 --> 00:04:15,760
That's it and it's of the same meaning.

44
00:04:16,120 --> 00:04:24,040
So from an existing any We are restructuring and declaring two variables first and second and first

45
00:04:24,040 --> 00:04:26,160
is going to get the value of input of zero.

46
00:04:26,200 --> 00:04:31,410
Second is going to get the value of input exactly like this.

47
00:04:31,490 --> 00:04:33,800
Run and we should still get the same output

48
00:04:36,130 --> 00:04:38,360
fantastic.

49
00:04:38,370 --> 00:04:40,550
Now this can be further extended.

50
00:04:40,770 --> 00:04:45,460
What we can do is introverting like this.

51
00:04:45,630 --> 00:04:52,560
Let's say Let f one or sum and one column.

52
00:04:52,890 --> 00:04:59,210
I can see dot dot dot and say restively and I can simply write them at it.

53
00:04:59,420 --> 00:05:06,390
I can get an area directly rather than another variable like this nor does this mean.

54
00:05:06,450 --> 00:05:13,890
This means that value and one will be assigned with the value one and one will get the value one and

55
00:05:14,010 --> 00:05:18,760
rest of the rate will be assigned to this rest.

56
00:05:18,780 --> 00:05:27,240
So yes we can probably see a lot and one I would say a lot.

57
00:05:28,010 --> 00:05:32,090
The rest so they can come and did no

58
00:05:36,120 --> 00:05:41,300
one and one that is two three four five is the best.

59
00:05:41,310 --> 00:05:50,840
So we can have it at the where we have distracter so that we can get one value and then the rest of

60
00:05:50,840 --> 00:05:51,540
it goes up.

61
00:05:51,690 --> 00:05:52,870
Now can I put it in.

62
00:05:52,910 --> 00:05:54,610
Yes I can do that also.

63
00:05:54,920 --> 00:06:00,830
In such case work will happen and what will get the value 1 and 2 will get the value and the rest will

64
00:06:00,830 --> 00:06:01,640
be 3 4 5

65
00:06:05,030 --> 00:06:06,550
4 5.

66
00:06:06,840 --> 00:06:16,330
Not only that if you don't want you can simply say let in square brackets.

67
00:06:16,600 --> 00:06:18,300
Just give one variable.

68
00:06:18,310 --> 00:06:29,840
Let's say I'm one and is equal to you given that you know what will happen no one will get the value

69
00:06:29,840 --> 00:06:32,710
of one and the rest of the data will get ignored.

70
00:06:32,890 --> 00:06:34,060
That's what will happen now.

71
00:06:35,910 --> 00:06:39,130
And when we get the rest of the data will be ignored.

72
00:06:41,810 --> 00:06:43,110
And much of

73
00:06:51,030 --> 00:07:04,230
Likewise we can also write Let give call more call more and to call more anything like this so we can

74
00:07:04,230 --> 00:07:05,880
do it in.

75
00:07:05,980 --> 00:07:14,440
Now I give equal let's say one more to call my three common folk five to four on the.

76
00:07:14,670 --> 00:07:16,820
So now what will happen.

77
00:07:16,900 --> 00:07:18,760
We are not taking the value one.

78
00:07:18,790 --> 00:07:23,820
We are ignoring them but there will be a two and two and four will be as an entity.

79
00:07:23,930 --> 00:07:25,330
What can we have this as a larger

80
00:07:28,780 --> 00:07:31,210
we in instead because what will happen.

81
00:07:31,590 --> 00:07:34,010
The value 5 and 6 will get ignored.

82
00:07:34,190 --> 00:07:42,150
We are only taking the value 3 and 4 based on the chemical and assigning them to respective and to empty.

83
00:07:42,290 --> 00:07:43,370
I looked

84
00:07:49,040 --> 00:07:53,340
into m3.

85
00:07:53,380 --> 00:08:00,500
Basically I'm just trying to show you all the possible combinations all the possible indicators of beast

86
00:08:00,500 --> 00:08:05,230
proctoring destructuring in any for years.

87
00:08:05,310 --> 00:08:06,670
Repeat once again.

88
00:08:06,800 --> 00:08:13,300
I have an air raid which can be destructo to create multiple variables.

89
00:08:13,510 --> 00:08:19,690
Remember first and second are two independent variables which can be used as part or conveniently.

90
00:08:20,180 --> 00:08:26,310
Likewise we can have multiple data extracted multiple are able to extract that and the rest of the data

91
00:08:26,420 --> 00:08:29,470
and put it into a separate variable.

92
00:08:29,560 --> 00:08:34,960
We can extract only the required quantity quantity of the data can be only the first element or it can

93
00:08:34,960 --> 00:08:40,660
be subsequent elements so like that we are able to display any.

94
00:08:40,730 --> 00:08:45,100
Also what is possible is this proctored regular expression.

95
00:08:45,100 --> 00:08:50,040
If you have a spring based on regular expressions we can block it.

96
00:08:50,230 --> 00:08:54,990
Let's say for example I don't even know something like date.

97
00:08:55,550 --> 00:08:57,790
Makes it totally failed.

98
00:08:58,090 --> 00:08:59,830
Come on I want your part.

99
00:08:59,860 --> 00:09:03,280
I want to but I won't depart.

100
00:09:03,500 --> 00:09:05,470
Or where do I get this from.

101
00:09:05,470 --> 00:09:13,360
Let us assume that we have our regular expression or two representing the need so that the syntax for

102
00:09:13,600 --> 00:09:17,470
giving it a clear expression in the regular expression.

103
00:09:17,470 --> 00:09:22,250
I'm going to have a digit digit digit digit.

104
00:09:22,330 --> 00:09:25,400
That is what your hyphen.

105
00:09:25,480 --> 00:09:27,350
I'm putting brackets that is important

106
00:09:30,460 --> 00:09:31,180
again.

107
00:09:31,270 --> 00:09:39,210
And with it that is what month I want again or did a digit.

108
00:09:39,240 --> 00:09:44,960
That is what you.

109
00:09:45,510 --> 00:09:46,010
So that's it.

110
00:09:46,010 --> 00:09:47,660
That is my regular expression.

111
00:09:47,820 --> 00:09:51,130
There is a limit function call that is easy.

112
00:09:51,720 --> 00:09:54,040
Then I can specify the spring.

113
00:09:54,360 --> 00:09:57,480
So I'll give a spring not something like 20:17

114
00:10:00,030 --> 00:10:11,510
let's say 0 7 and some point 0 1 know what is going to happen here let me just play the whole thing

115
00:10:11,510 --> 00:10:15,190
in a love I can get totally hit

116
00:10:18,070 --> 00:10:18,910
get in it with

117
00:10:21,780 --> 00:10:29,910
you can't get in it with Mont concatenates.

118
00:10:29,930 --> 00:10:36,300
With that said they know what is going to happen here in square brackets.

119
00:10:36,310 --> 00:10:37,820
We are in on bracket.

120
00:10:37,820 --> 00:10:45,500
We have got four digits so this 4 digits will be used here this double digits will be used here 0 7

121
00:10:45,900 --> 00:10:47,900
and 0 1 will be used here.

122
00:10:48,530 --> 00:10:55,880
And this whole regular expression will be then assigned to this is going to result in some value that

123
00:10:55,880 --> 00:10:57,650
is for that structure.

124
00:10:57,670 --> 00:11:04,190
No where in that order did the total value of this will be assigned to total debt.

125
00:11:04,250 --> 00:11:06,570
Not because we have put brackets here.

126
00:11:06,890 --> 00:11:13,120
This value is going to get us into your disvalue second bracket will be a month.

127
00:11:13,220 --> 00:11:15,070
And third value will be to the

128
00:11:21,880 --> 00:11:26,770
see this total Mundt day.

129
00:11:27,040 --> 00:11:33,580
Now what is important is also you know if I remove brackets what will happen less available to my kids.

130
00:11:33,670 --> 00:11:37,160
So you will be assigned to this.

131
00:11:37,250 --> 00:11:41,750
You're right you will with this but money will be not taken from here.

132
00:11:46,920 --> 00:11:52,240
You see Montaigne's you don't know which is just part.

133
00:11:52,550 --> 00:11:57,390
And of course we don't have anything put the third bracket is missing.

134
00:11:57,440 --> 00:12:03,650
So brackets are very important in the expression so compete value of a regular expression will be can

135
00:12:03,670 --> 00:12:10,310
put the first element in the left hand side first variable on the left and take the rest of it will

136
00:12:10,310 --> 00:12:18,490
be decided based on number of brackets which should be happy and so there that so it's easy works but

137
00:12:18,490 --> 00:12:25,130
what is very important is if the value is not as per this format what will happen.

138
00:12:25,540 --> 00:12:30,340
For example actually two digits are expected given you know the triple brackets and

139
00:12:35,160 --> 00:12:36,300
now on this

140
00:12:42,230 --> 00:12:43,210
we are going to get an

141
00:12:47,960 --> 00:12:48,840
error has gone

142
00:12:52,040 --> 00:12:58,610
self-dual.

143
00:12:59,240 --> 00:13:04,930
So what we should do in those kind of cases in those kind of cases you are with no

144
00:13:10,170 --> 00:13:11,760
or maybe some default value

145
00:13:17,960 --> 00:13:18,460
0.

146
00:13:18,470 --> 00:13:19,930
C'mon c'mon c'mon

147
00:13:23,120 --> 00:13:23,630
let's run.

148
00:13:23,630 --> 00:13:24,230
This is not

149
00:13:27,480 --> 00:13:28,670
our $40.

150
00:13:28,820 --> 00:13:29,530
Why.

151
00:13:29,540 --> 00:13:31,820
Because the expression is invalid.

152
00:13:31,970 --> 00:13:38,000
So when an expression is not granted this is going to give us an error to avoid that error.

153
00:13:38,240 --> 00:13:44,710
Or it or it with something that is meaningful to the context and then appropriately deal with the if

154
00:13:44,720 --> 00:13:45,510
condition.

155
00:13:45,920 --> 00:13:50,920
If total debt is not equal to zero like that we have the right to go and so on.

156
00:13:51,590 --> 00:13:57,520
So this is what we are going to restructure and aere and a regular expressions.

157
00:13:57,830 --> 00:13:58,600
That's all in this.

158
00:13:58,670 --> 00:13:59,150
Thank you.

