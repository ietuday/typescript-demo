1
00:00:16,590 --> 00:00:24,780
Dynamic components using component outlet and component outlet is a new directive that provides a declarative

2
00:00:24,780 --> 00:00:27,270
approach for creating dynamic components.

3
00:00:27,270 --> 00:00:32,750
That means we can create components and put it into another container dynamically.

4
00:00:33,420 --> 00:00:37,340
And for that we are going to make use of the index container data.

5
00:00:38,110 --> 00:00:45,780
It does a local container logical container that can be used to group morbus but is not rendered in

6
00:00:45,780 --> 00:00:46,630
the dome.

7
00:00:47,010 --> 00:00:52,920
The member in the container is just used for grouping of nodes by itself it is not.

8
00:00:52,920 --> 00:00:59,760
Rather we can say that the has come and a container is mostly used when we want to group elements on

9
00:00:59,760 --> 00:01:01,900
a consistent basis.

10
00:01:01,920 --> 00:01:09,090
That means if we want to do this or otherwise do these kind of cases then we want to make use of components

11
00:01:09,620 --> 00:01:14,130
either component or that component then we can make use of in the container.

12
00:01:14,550 --> 00:01:19,240
This is the basic syntax of it but let's do it with a proper example.

13
00:01:19,590 --> 00:01:22,250
So I have the same project open as before.

14
00:01:22,260 --> 00:01:26,780
What we have created for lifecycle hooks the same project here.

15
00:01:26,790 --> 00:01:30,300
I would like to add no multiple components.

16
00:01:30,330 --> 00:01:34,760
So rather than creating one more file as of now I'm adding in the same file.

17
00:01:35,130 --> 00:01:36,740
Let's say I have a component called

18
00:01:47,000 --> 00:01:47,570
Boehland

19
00:01:54,710 --> 00:01:56,420
and this component will call it as

20
00:01:59,750 --> 00:02:05,980
a lot Sukses component.

21
00:02:06,560 --> 00:02:06,870
That's

22
00:02:10,060 --> 00:02:17,930
in the alert status component of course I'll push the selector let it be something like

23
00:02:21,260 --> 00:02:27,090
Sukses and we will have template

24
00:02:29,980 --> 00:02:31,590
what do we want to do.

25
00:02:31,590 --> 00:02:31,920
Or the

26
00:02:35,860 --> 00:02:37,580
next to be Dagr.

27
00:02:38,070 --> 00:02:42,280
This is sukses.

28
00:02:42,710 --> 00:02:46,720
Likewise I also have another component.

29
00:02:46,820 --> 00:02:48,570
Simple example only.

30
00:02:49,020 --> 00:02:50,930
It does not failure.

31
00:03:01,610 --> 00:03:07,410
Feed So when we are calling it a success either we call it doesn't Filiale

32
00:03:15,180 --> 00:03:19,800
assignable barometer of type component spelling mistake

33
00:03:22,900 --> 00:03:24,740
done right.

34
00:03:26,760 --> 00:03:35,160
Now my requirement is I would like to use this component in the room but it should be dynamically decided

35
00:03:35,160 --> 00:03:36,650
which component.

36
00:03:37,260 --> 00:03:43,070
So I'll say good job.

37
00:03:43,800 --> 00:03:44,430
Dynamic

38
00:03:46,960 --> 00:03:49,030
component example.

39
00:03:54,080 --> 00:03:56,220
Now here we are going to get.

40
00:03:56,990 --> 00:03:57,450
Dana

41
00:04:00,330 --> 00:04:06,500
an indie container has this attribute in the Cumberland outlet

42
00:04:16,320 --> 00:04:22,400
let's call it a success or failure.

43
00:04:28,970 --> 00:04:30,810
No what.

44
00:04:30,850 --> 00:04:40,520
Alert alert is not supposed to be so property in this class.

45
00:04:40,830 --> 00:04:44,480
Here I will have a look.

46
00:04:44,780 --> 00:04:45,820
Well worth a look.

47
00:04:45,840 --> 00:04:51,490
Let us give you the name of the component class that is alert.

48
00:04:51,540 --> 00:04:53,370
Success can work for the time

49
00:04:57,250 --> 00:04:58,680
that's it.

50
00:04:58,730 --> 00:05:05,030
So right now what we are going to see is at the place where we have in the container we are going to

51
00:05:05,030 --> 00:05:06,350
get the output from

52
00:05:09,690 --> 00:05:11,230
something went wrong.

53
00:05:16,790 --> 00:05:19,550
Alerts are going to not add to the model.

54
00:05:19,610 --> 00:05:28,010
Again what is important is these two components we will have to add it to the entry component.

55
00:05:29,280 --> 00:05:40,010
Off either the comp or the mode where we are going to make use of.

56
00:05:40,030 --> 00:05:50,800
So here we don't always go to more and end up more do we have to know one more property class and three

57
00:05:50,800 --> 00:05:51,820
components

58
00:05:54,650 --> 00:05:58,000
in three components we are going to provide the

59
00:06:01,000 --> 00:06:03,630
sexist component.

60
00:06:04,750 --> 00:06:06,390
And I learned

61
00:06:08,980 --> 00:06:09,450
failing

62
00:06:17,880 --> 00:06:21,000
to learn success let failure complete

63
00:06:26,940 --> 00:06:32,220
and these two are in component so just in the same fight.

64
00:06:32,340 --> 00:06:37,160
So we can put up and included.

65
00:06:37,250 --> 00:06:38,700
That's it.

66
00:06:39,660 --> 00:06:43,040
So this is also important in three components.

67
00:06:43,630 --> 00:06:44,690
No that is right.

68
00:06:51,380 --> 00:06:53,080
Again something went wrong.

69
00:06:56,830 --> 00:06:59,320
A lot a company is not part of anymore.

70
00:06:59,460 --> 00:07:00,660
Ok ok.

71
00:07:00,720 --> 00:07:02,280
Small correction.

72
00:07:02,280 --> 00:07:08,640
These two classes have also included as a part of declarations

73
00:07:12,150 --> 00:07:20,380
you would have to include it.

74
00:07:20,450 --> 00:07:24,490
This is success like Norm good but not what I'm going to do.

75
00:07:24,500 --> 00:07:29,440
Is your.

76
00:07:29,520 --> 00:07:36,840
I'm going to provide about the let's call it US teams document

77
00:07:40,340 --> 00:07:46,170
or teens or.

78
00:07:46,360 --> 00:07:58,380
And when I click on these button we're going to have event based which all not my to one function let's

79
00:07:58,410 --> 00:07:59,690
call it doesn't change.

80
00:08:01,380 --> 00:08:01,730
And

81
00:08:05,890 --> 00:08:09,470
Undine's component I should note wrote in the implementation

82
00:08:17,920 --> 00:08:24,450
or I and simply say this is not other is equal to a failure.

83
00:08:26,880 --> 00:08:38,070
I'm changing it actually begin if condition if the dotted line is equal to this.

84
00:08:38,140 --> 00:08:42,900
There's not a lot is equal to success compering

85
00:08:49,110 --> 00:08:51,900
else there's not a lot.

86
00:08:51,940 --> 00:08:55,680
Is it going to lot really.

87
00:08:56,340 --> 00:09:05,720
So I'm swapping them all out every time I click on the bottom.

88
00:09:05,900 --> 00:09:08,320
So I say click does this fail.

89
00:09:08,460 --> 00:09:09,860
This is success.

90
00:09:09,900 --> 00:09:11,000
This is success.

91
00:09:11,400 --> 00:09:18,600
So dynamically we are swapping between the two components in the parent company and that we are able

92
00:09:18,600 --> 00:09:21,380
to achieve using indeed container.

93
00:09:21,840 --> 00:09:27,580
So this is also some feature which we would like to use at different times.

94
00:09:27,630 --> 00:09:37,070
So once again indicum is mostly used when we want to group elements on a condition basis.

95
00:09:37,100 --> 00:09:38,640
That's what I did right.

96
00:09:39,000 --> 00:09:42,330
If Or else then we have to use them.

97
00:09:45,200 --> 00:09:47,990
And the component in the model is important.

98
00:09:48,140 --> 00:09:55,180
Here we are using in the container with Indic component outlet that takes as input reference to the

99
00:09:55,180 --> 00:10:03,320
component and you need to add your dynamic input into the entry components exit of the more complex

100
00:10:03,380 --> 00:10:10,400
which are added to in the model bootstrap or added automatically and the components automatically and

101
00:10:10,400 --> 00:10:14,560
components which are referencing the configuration are also added automatically.

102
00:10:14,840 --> 00:10:19,130
Of course these are the topics which we have not covered how we can make use of the routing configuration

103
00:10:19,130 --> 00:10:20,370
and stuff like that.

104
00:10:20,420 --> 00:10:24,300
Once we do that maybe if you come back and read this paragraph later you will understand.

105
00:10:24,800 --> 00:10:32,060
But yes at this point of time we are very clear with how we can make changes to show how we can decide

106
00:10:32,370 --> 00:10:37,940
on what component our component be in a particular location independent compiler.

107
00:10:38,300 --> 00:10:38,720
Thank you.

