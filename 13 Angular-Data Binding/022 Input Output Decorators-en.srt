1
00:00:16,440 --> 00:00:21,590
Using input and output decoding those a very interesting topic.

2
00:00:21,980 --> 00:00:32,490
See right now when I included up component I used the selector so I used to select the person in app

3
00:00:32,500 --> 00:00:33,240
component

4
00:00:36,760 --> 00:00:46,240
know what my requirement is from here I should be able to set the value of forced me and lost me so

5
00:00:46,250 --> 00:00:47,530
how can we do that.

6
00:00:49,080 --> 00:00:54,260
Like we don't have anything we don't have anything for setting the value of first name and last name.

7
00:00:54,930 --> 00:01:01,590
But through this I should be able to say that means I should be able to write your first name is equal

8
00:01:01,590 --> 00:01:06,680
to let's say my name and the last name is it will say my name.

9
00:01:06,810 --> 00:01:07,110
The

10
00:01:11,500 --> 00:01:16,340
how are we going to provide this input to the person component.

11
00:01:17,100 --> 00:01:22,120
That is where we have to write an important component properties for us.

12
00:01:22,190 --> 00:01:34,220
We love to write two properties by same name first name and last name and very important for these two

13
00:01:34,220 --> 00:01:35,050
properties.

14
00:01:35,170 --> 00:01:37,960
We are not supposed to put away the attribute.

15
00:01:38,420 --> 00:01:41,390
What is the name of that to be an input.

16
00:01:41,420 --> 00:01:44,650
So here I get added input.

17
00:01:44,840 --> 00:01:49,090
But right now within that region that we declare any of it.

18
00:01:49,180 --> 00:01:49,850
No.

19
00:01:49,970 --> 00:01:58,840
So first we will have to use from the only address and then this will look to we have for first name

20
00:01:58,840 --> 00:01:59,930
of the rating.

21
00:02:00,120 --> 00:02:04,370
And likewise for that last name also I'll get it in.

22
00:02:04,830 --> 00:02:10,630
And this first name and last name is what I would want to use it actually for the person.

23
00:02:12,820 --> 00:02:14,180
So I can log in

24
00:02:17,060 --> 00:02:22,250
this Dopp first name does not last me.

25
00:02:22,370 --> 00:02:25,640
Let's see.

26
00:02:25,660 --> 00:02:35,010
So instead of giving constant x y z and z see this problem come back you're not supposed to do something

27
00:02:35,010 --> 00:02:36,070
like this.

28
00:02:36,690 --> 00:02:38,140
What we will do is instead.

29
00:02:38,250 --> 00:02:40,150
This is not the right thing to do.

30
00:02:41,620 --> 00:02:47,570
Write constructor but even a constructor will not work and he was

31
00:02:51,720 --> 00:02:52,600
I didn't think he would

32
00:03:03,060 --> 00:03:11,160
Dizdar.

33
00:03:11,370 --> 00:03:12,890
This will also not work.

34
00:03:14,700 --> 00:03:21,400
We have to actually handle certain component events for this or that topic we haven't covered yet so

35
00:03:21,400 --> 00:03:27,450
we're going to do it for the time being directed by his first name and last name of the person component

36
00:03:27,960 --> 00:03:36,840
to the input elements so I'll have one more form and not pass this up one more step altogether.

37
00:03:38,900 --> 00:03:44,810
And here I will say no or some not just a first name and last name

38
00:03:48,590 --> 00:03:51,100
on here so you can actually use the same syntax.

39
00:03:51,100 --> 00:03:52,750
We don't want to separate syntax.

40
00:03:52,990 --> 00:03:57,970
What is all this.

41
00:03:58,000 --> 00:04:00,270
So what are we trying to do know.

42
00:04:00,340 --> 00:04:02,430
Let me explain the whole thing.

43
00:04:05,560 --> 00:04:16,770
Faucett we will use this component using a person bag and have done that in the app component in app

44
00:04:16,770 --> 00:04:20,760
component I'm setting the value for the properties first name and last name.

45
00:04:21,270 --> 00:04:25,820
But these values will go as input only.

46
00:04:26,430 --> 00:04:34,400
Here we are going to write and direct input in case we do not write it in the value set in the tactful

47
00:04:34,500 --> 00:04:36,720
person will not be assigned here.

48
00:04:37,260 --> 00:04:38,860
That is very very important.

49
00:04:40,840 --> 00:04:49,150
Now if I run the program we'll see here see Sundeep sone as calm as the value.

50
00:04:49,290 --> 00:05:00,120
So this is precisely the purpose of input the word is 0 0 input is too big to be from back into the

51
00:05:00,120 --> 00:05:01,260
component.

52
00:05:01,630 --> 00:05:11,710
So obviously from component to the stack but where is that used that is used in the other component.

53
00:05:12,160 --> 00:05:20,670
So yes open decorator is used whenever we want to make an event and pull some component we are going

54
00:05:20,670 --> 00:05:22,020
to have an event.

55
00:05:22,530 --> 00:05:27,400
And this even when raised we would like to handle it in the up component.

56
00:05:27,540 --> 00:05:29,560
So understand this point very clearly.

57
00:05:30,920 --> 00:05:32,370
Very very important.

58
00:05:32,430 --> 00:05:42,890
We have got a room component which is up component and we have got under icon on the dock or this person

59
00:05:42,970 --> 00:05:47,860
most of the time person or some component is no.

60
00:05:47,880 --> 00:05:53,300
I would say contained inside room component that is important.

61
00:05:53,480 --> 00:06:01,340
So when we want to have a property from that component in incompetent class for the property we specify

62
00:06:01,540 --> 00:06:05,610
input but normally requirement is reverse.

63
00:06:05,610 --> 00:06:11,370
I would like to raise an event from the present component and for that I would like to handle it in

64
00:06:11,370 --> 00:06:15,020
the up component with this person.

65
00:06:15,020 --> 00:06:20,620
Actually it is your right from your coming here but no details will go from here to here.

66
00:06:21,260 --> 00:06:26,750
But it's not that they don't just it is the event which is actually raised from that child component

67
00:06:26,750 --> 00:06:28,750
to the parent component.

68
00:06:28,940 --> 00:06:36,060
So first for this we will have to do is on top right something called as even a meter.

69
00:06:36,410 --> 00:06:37,870
That's one clock we really need.

70
00:06:37,970 --> 00:06:41,610
And of course to me it also means the open plus two plus is beginning.

71
00:06:42,200 --> 00:06:49,750
And then in my trial component person I'm going to provide a name let us see that in the event.

72
00:06:49,820 --> 00:06:51,290
I would like to call it that.

73
00:06:51,600 --> 00:06:52,840
Oh name changed.

74
00:06:52,870 --> 00:06:58,790
Not to say there is something called name changed name change event.

75
00:06:59,240 --> 00:07:04,790
The hotel initialises that is even Amandus object.

76
00:07:04,880 --> 00:07:07,960
Now what's up with the data.

77
00:07:08,390 --> 00:07:11,500
So that is where we use a generic type here.

78
00:07:11,870 --> 00:07:15,310
Let's say the new name is what we would like to pass there.

79
00:07:15,770 --> 00:07:23,510
So what I'll do is I'll have one more property here called the full name which I want to get it from

80
00:07:23,510 --> 00:07:29,770
the input and let's say full name is what we would like to get

81
00:07:38,840 --> 00:07:40,670
or else I'll just keep it simple.

82
00:07:40,700 --> 00:07:47,370
That is not our new property I'll have forced name change.

83
00:07:47,520 --> 00:07:53,410
Remember earlier we have seen this index of property X and even ex-teammate.

84
00:07:53,700 --> 00:07:55,700
So we can probably existe first name.

85
00:07:55,950 --> 00:08:00,490
So exit changed and then I'm going to write that.

86
00:08:00,560 --> 00:08:02,820
Oh what a a.

87
00:08:05,570 --> 00:08:09,700
Likewise I need one more first name last name

88
00:08:12,730 --> 00:08:19,170
first name and last name change of type string and hence the type string.

89
00:08:19,550 --> 00:08:26,500
Now these events are going to be handled in the container but how are we going to handle it in the container

90
00:08:26,820 --> 00:08:31,030
basically in container we would like to do some kind of data binding.

91
00:08:31,230 --> 00:08:35,350
So here is the container component.

92
00:08:35,640 --> 00:08:38,340
And like we do any kind of binding here.

93
00:08:38,540 --> 00:08:40,470
No we haven't done any binding here.

94
00:08:40,860 --> 00:08:50,470
So what I would want to do all this bind is I'm going to be by the first name is a property first name

95
00:08:50,620 --> 00:08:51,750
teemed is that event.

96
00:08:51,760 --> 00:08:53,980
Hence we are able to use the syntax.

97
00:08:53,980 --> 00:08:55,360
Same thing here.

98
00:08:55,360 --> 00:08:57,020
First name is a property or a last name.

99
00:08:57,020 --> 00:09:06,170
The property last name is Doug but this is sort of giving constant I would like to bind it to something

100
00:09:09,870 --> 00:09:16,660
or here or call it that's tomorrow more two more things.

101
00:09:16,760 --> 00:09:18,800
So what is that I should point this out.

102
00:09:19,190 --> 00:09:22,970
So in component also notice that F name

103
00:09:28,040 --> 00:09:29,480
under that name

104
00:09:33,090 --> 00:09:34,260
so here I'll give

105
00:09:37,590 --> 00:09:51,160
f maybe and here I'll give you an F. name is binded to first name and a name is my last name and it

106
00:09:51,160 --> 00:09:52,760
is a blue ribbon.

107
00:09:53,140 --> 00:09:56,850
So I would like to play Hello

108
00:09:59,520 --> 00:10:00,360
f named

109
00:10:04,930 --> 00:10:12,690
addling and there is this hello from hello from component.

110
00:10:15,440 --> 00:10:18,150
I know from component of first name.

111
00:10:18,610 --> 00:10:25,830
So in our component we have got two properties which we have minded person first name last name we and

112
00:10:26,020 --> 00:10:31,810
we are able to do this right because in person component we have got first name as input property and

113
00:10:31,810 --> 00:10:37,560
first name changes or property but the whole point is notices.

114
00:10:37,690 --> 00:10:38,670
So in this post

115
00:10:43,810 --> 00:10:51,340
Sunday morning and that's coming here now I give you a busy distending within the company.

116
00:10:51,610 --> 00:10:58,080
But anything in the container no X Y Z is it in the container.

117
00:10:58,810 --> 00:11:01,880
Do we have that binding in the container.

118
00:11:01,880 --> 00:11:08,060
It is not changing it in the container the component to display it is not ending because no there we

119
00:11:08,060 --> 00:11:10,900
have written code for emitting that event.

120
00:11:11,210 --> 00:11:14,450
We have not written or for raising that event.

121
00:11:14,480 --> 00:11:17,290
So where do we write code for raising the event.

122
00:11:17,840 --> 00:11:23,450
We have to actually raise the event from the present component to the component.

123
00:11:23,590 --> 00:11:27,740
That means here we have to separate these you know.

124
00:11:27,910 --> 00:11:30,200
So I'll take all brackets.

125
00:11:31,930 --> 00:11:37,460
And we're in bracket and the it will

126
00:11:42,570 --> 00:11:54,510
Mexicali does change for me and do that I'll post all of you likewise something similar I'm going to

127
00:11:54,510 --> 00:11:55,080
go for

128
00:11:57,790 --> 00:11:59,110
Tende last name

129
00:12:03,130 --> 00:12:05,240
and you're also in the book backwards.

130
00:12:05,610 --> 00:12:08,490
But what is this change first name change last name.

131
00:12:08,490 --> 00:12:13,290
These two are not supposed to be the functions in the autism component.

132
00:12:13,590 --> 00:12:19,100
So you're allowed no change for us.

133
00:12:19,150 --> 00:12:22,840
I mean we'll get it possibly.

134
00:12:23,810 --> 00:12:25,540
And like what.

135
00:12:25,830 --> 00:12:29,630
The last thing it will get lost me.

136
00:12:30,090 --> 00:12:31,680
Now what is that we should do.

137
00:12:32,000 --> 00:12:36,590
Well whenever we say change first name first thing obviously we will have to do is we love to say it

138
00:12:36,620 --> 00:12:37,620
does talk.

139
00:12:37,790 --> 00:12:48,810
Post them in a couple of and you should call them string and string.

140
00:12:49,050 --> 00:12:53,600
And likewise here I should write this your last name.

141
00:12:53,710 --> 00:12:56,770
And but this is not sufficient.

142
00:12:56,790 --> 00:12:57,990
Why did we actually do this.

143
00:12:57,990 --> 00:13:02,670
This is actually the default behavior when we don't believe in Cancian we will still be able to do the

144
00:13:02,670 --> 00:13:09,490
same thing when we did this actually because we wanted to raise the event.

145
00:13:10,110 --> 00:13:17,260
We wanted to raise the human with the help of a readymade utility function that is M-ID.

146
00:13:17,310 --> 00:13:18,910
Now what does that even mean here.

147
00:13:19,080 --> 00:13:21,690
Does dot force name change.

148
00:13:21,690 --> 00:13:26,480
Is that event new event meter door Emmett.

149
00:13:26,890 --> 00:13:32,210
Emmett what we would like him in the latest volume but just trust me.

150
00:13:33,810 --> 00:13:41,580
Likewise your eyes would admit that even this dog last name change that even if you were actually raising

151
00:13:42,040 --> 00:13:49,520
with as well as raising the event it is at this point of time because the event is raised in the container.

152
00:13:49,600 --> 00:13:50,800
Reverse action will happen.

153
00:13:50,800 --> 00:13:57,090
That means first name value will be the to name which is going to reflect your Knoller's on this

154
00:14:04,230 --> 00:14:05,140
trendier.

155
00:14:05,270 --> 00:14:09,260
Let's say a b c not tending

156
00:14:14,300 --> 00:14:15,010
did we get any

157
00:14:18,600 --> 00:14:20,160
noise.

158
00:14:20,840 --> 00:14:25,970
We need to debug a little bit more to understand why this value is not in.

159
00:14:25,980 --> 00:14:28,250
That means if name is not tending

160
00:14:35,070 --> 00:14:41,650
your first name last name we did the rebinding first name last name.

161
00:14:41,650 --> 00:14:48,740
All good first name last name are both in properties in Clauss.

162
00:14:48,740 --> 00:14:51,280
First Name Last Name or two input properties.

163
00:14:51,710 --> 00:14:55,090
First name change last name change or two properties.

164
00:14:55,240 --> 00:15:00,080
Also good on both.

165
00:15:00,220 --> 00:15:09,950
Even if we did when we emit the event we have to give the string first name to get the string last name.

166
00:15:09,950 --> 00:15:22,210
All good but is everything right here in the indeed more than even if you want all looks good.

167
00:15:22,640 --> 00:15:23,050
Oh yes.

168
00:15:23,080 --> 00:15:24,720
The editor is here actually.

169
00:15:25,030 --> 00:15:35,450
This is supposed to be indeed more than teen teen and same thing here.

170
00:15:36,130 --> 00:15:44,120
Let's on the program.

171
00:15:44,220 --> 00:15:45,740
I tend to.

172
00:15:45,890 --> 00:15:48,240
Yes it is trending here also it is changing.

173
00:15:48,240 --> 00:15:52,640
This is coming from the person component and this is coming from app component.

174
00:15:53,100 --> 00:16:01,630
So team that text box is reflecting in some component parts and component is admitting that even though

175
00:16:01,700 --> 00:16:05,290
app component and white is able to do that.

176
00:16:07,760 --> 00:16:13,550
Only because it has a podium looks at all but just for different purposes the more.

177
00:16:14,540 --> 00:16:15,570
Now what will happen.

178
00:16:20,920 --> 00:16:25,860
Pierre think this will work.

179
00:16:27,650 --> 00:16:33,560
So is replicating within the component but it is not cascading to the parent component.

180
00:16:33,620 --> 00:16:40,100
So when they were teens in the child component has to be cascaded into the parent component the child

181
00:16:40,100 --> 00:16:46,610
component will have to admit even to the parent component very importantly the child components are

182
00:16:46,610 --> 00:16:49,950
declared that even as old.

183
00:16:51,050 --> 00:16:56,830
Now remember all your input we can give a name as we can.

184
00:16:57,140 --> 00:17:03,650
Let's see if I can give you a good what is that we have to do in parent component.

185
00:17:03,800 --> 00:17:13,810
The tribunal is supposed to be effen this is supposed to be what we are using here that is supposed

186
00:17:13,810 --> 00:17:16,480
to be used here not on this

187
00:17:25,600 --> 00:17:27,600
law something went wrong.

188
00:17:29,050 --> 00:17:33,180
Obviously this also should be sort of like give your

189
00:17:36,880 --> 00:17:42,470
first change would have to be in sync with each other.

190
00:17:48,480 --> 00:17:50,570
No see it's blocking.

191
00:17:50,640 --> 00:17:57,350
So you have the provision of providing a Parminter to the input decorator as well as a decorator.

192
00:17:57,870 --> 00:18:04,230
And that property is nothing but that there is nothing but by the property name that is the name of

193
00:18:04,230 --> 00:18:06,540
the property as used in the container.

194
00:18:06,810 --> 00:18:11,910
So in component we can have a separate property name and in the container the parent component also

195
00:18:11,910 --> 00:18:14,230
can have different properties.

196
00:18:14,910 --> 00:18:21,920
So I repeat once again whatever we have done in this particular example forced We have ridden on top

197
00:18:22,250 --> 00:18:31,270
a metre in a now what is this for this particular topic what we have covered up is ideally used in case

198
00:18:31,330 --> 00:18:38,320
we want to have a child component communicate with the parent component and there are again two forms

199
00:18:38,320 --> 00:18:41,340
of communication from parent component.

200
00:18:41,350 --> 00:18:49,730
Along with that back we would like to get the tool from the child component we would like to raise that

201
00:18:50,250 --> 00:18:55,230
event to the parent company of the school to the parent company.

202
00:18:55,820 --> 00:19:03,140
So those kind of cases when we want to get input from parent component we use in put it up when we want

203
00:19:03,140 --> 00:19:05,980
to produce or put the parent component.

204
00:19:05,990 --> 00:19:15,380
We use our own decorator is supposed to be associated with an event because the child is always going

205
00:19:15,380 --> 00:19:25,170
to make use of even for exhibiting some accent in the parent and always even dressed within the component

206
00:19:25,620 --> 00:19:27,560
of gone into more than whining.

207
00:19:27,810 --> 00:19:34,320
So first name off person component will be assigned to the text box text box will be changing the first

208
00:19:34,320 --> 00:19:35,560
name.

209
00:19:35,730 --> 00:19:37,760
That is what we have done here.

210
00:19:38,070 --> 00:19:44,310
When the value of the text box changes there is going to call a function change me and change first

211
00:19:44,310 --> 00:19:52,310
name will cascade first it will turn the property of the component first name and then immediately it

212
00:19:52,310 --> 00:19:58,760
will emit an event which we are handling in the parent component because we are going to be binding

213
00:19:58,760 --> 00:20:02,400
here in the parent company and we are binding it.

214
00:20:03,350 --> 00:20:09,940
So if name a name or two properties in the app component of course little complex topic you'll have

215
00:20:09,950 --> 00:20:12,450
to walk look at it couple of times.

216
00:20:13,240 --> 00:20:20,550
And understand and do it step by step because this is going to become a foundation for our subsequent

217
00:20:20,550 --> 00:20:21,300
topics.

218
00:20:21,320 --> 00:20:28,200
Mind you here only I have introduced to you hold the parent component and the child components are able

219
00:20:28,200 --> 00:20:29,840
to communicate with each other.

220
00:20:30,030 --> 00:20:34,070
And this is pretty common to see that in projects.

221
00:20:34,080 --> 00:20:39,930
So again I am requesting you to practice this topic at least a couple of times or at least what couple

222
00:20:39,930 --> 00:20:44,290
of times before you practice once at least and proceed to the next topic.

223
00:20:44,580 --> 00:20:45,210
Thank you.

