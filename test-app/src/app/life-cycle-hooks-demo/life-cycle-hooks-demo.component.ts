import { Component,
         OnChanges, 
         OnInit,
         DoCheck,
         AfterContentInit,
         AfterContentChecked,
         AfterViewInit,
         AfterViewChecked,
         OnDestroy
} from '@angular/core';

@Component({
  selector: 'app-life-cycle-hooks-demo',
  templateUrl: './life-cycle-hooks-demo.component.html',
  styleUrls: ['./life-cycle-hooks-demo.component.scss']
})
export class LifeCycleHooksDemoComponent implements OnInit, OnDestroy, OnChanges, DoCheck, AfterContentChecked,
AfterContentInit, AfterViewInit, AfterViewChecked  {
  name:string = 'Angular';
  
  constructor() { }



  ngOnChanges(){
    console.log("ngOnChanges fired");
  }

  ngOnInit() {
    console.log("ngOnInit fired");
    
  }

  ngDoCheck(){
    console.log("ngDoCheck fired");

  }

  ngAfterContentInit(){
    console.log("ngAfterContentInit fired");

  }

  ngAfterContentChecked(){
    console.log("ngAfterContentChecked fired");

  }

  ngAfterViewInit(){
    console.log("ngAfterViewInit fired");

  }

  ngAfterViewChecked(){
    console.log("ngAfterViewChecked fired");

  }

  ngOnDestroy(){
    console.log("ngOnChanges fired");

  }

}
