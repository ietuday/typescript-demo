abstract class Figure {
    protected Dimension: number
    public abstract Area(): number
}
class Circle extends Figure {
    constructor(radius: number) {
        super();
        this.Dimension = radius;
    }
    public Area(): number {
        return Math.PI * this.Dimension * this.Dimension;
    }
}
class Square extends Figure {
    constructor(side: number) {
        super();
        this.Dimension = side;
    }
    public Area(): number {
        return this.Dimension * this.Dimension;
    }
}
var fig: Figure = new Circle(10); //new Square(10);
alert(fig.Area());