class Hello{
    firstName:string;
    lastName:string;

    sayHello = function(): string{
        return "Hello" + " " + this.firstName + " " + this.lastName;
    }
}

// function sayHello(firstName:string, lastName:string): string{
//     var message: string = 'Hello World';
//     message += " " + firstName + " " + lastName;
//     return message;
// }

// alert(sayHello("Udayaditya", "Singh"));

var h:Hello = new Hello();

h.firstName = "Udayaditya";
h.lastName  = "Singh";
alert(h.sayHello())