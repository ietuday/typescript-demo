1
00:00:16,680 --> 00:00:23,640
Validating form elements we already know that in extremal 5 we have got certain attributes which are

2
00:00:23,700 --> 00:00:27,570
useful for providing validation to different form elements.

3
00:00:27,570 --> 00:00:32,580
For example white men learn McGlenn pattern and many more are there.

4
00:00:33,060 --> 00:00:38,180
These are all head Steimle five building attributes for Validus.

5
00:00:38,490 --> 00:00:45,420
And also you should note that I will provide builtin attributes to know the state of the form and its

6
00:00:45,630 --> 00:00:46,960
controls.

7
00:00:47,340 --> 00:00:53,550
So we have got indeed that indeed there are two different classes actually and we have seen this in

8
00:00:53,790 --> 00:01:02,060
template driven for the same thing all the orphant control actually has all loaded parameters.

9
00:01:02,240 --> 00:01:08,420
When we create the object of form control the constructor can take as follows paramita up the initial

10
00:01:08,420 --> 00:01:09,640
value of the control.

11
00:01:09,740 --> 00:01:17,720
Yes we have done that in our example we have seen how we could give the first value first name last

12
00:01:17,720 --> 00:01:23,390
name gender qualification the Default Value not the first value the default.

13
00:01:23,640 --> 00:01:27,460
Be the second part of me to notice the second parameter.

14
00:01:27,650 --> 00:01:30,900
It can contain all the validators.

15
00:01:31,100 --> 00:01:35,440
It can be only one or it can be a list.

16
00:01:35,480 --> 00:01:41,680
So what we have to do now is we have to see how we can provide validation rules for first name.

17
00:01:41,750 --> 00:01:48,100
All these elements if don't actually think about the Steimle corresponding to it.

18
00:01:48,800 --> 00:01:50,540
So what should we do.

19
00:01:50,570 --> 00:01:55,750
I'm going to make changes not of first name but something like this.

20
00:01:57,030 --> 00:02:01,570
The second part of.

21
00:02:02,350 --> 00:02:06,630
So validate those is what we need to include now.

22
00:02:06,960 --> 00:02:12,280
So on top I go on here of the form control we include validators.

23
00:02:12,330 --> 00:02:13,870
That's also the box.

24
00:02:14,400 --> 00:02:16,440
So we have Brandee does not require

25
00:02:21,390 --> 00:02:28,840
validators but we have to specify the pattern which is going to be used but that is same thing maybe

26
00:02:28,840 --> 00:02:29,970
we can do it for you.

27
00:02:29,970 --> 00:02:30,710
And lastly

28
00:02:40,540 --> 00:02:42,350
first name last name.

29
00:02:42,430 --> 00:02:46,090
The first is the default value which will go away.

30
00:02:46,270 --> 00:02:49,450
Second is the validators validators.

31
00:02:49,570 --> 00:02:56,990
We can actually put only one on in case you want to build we can put multiple combusts operators list

32
00:02:57,470 --> 00:02:59,590
and Edie's what we are supposed to provide is

33
00:03:03,960 --> 00:03:12,150
validate those dot e-mail Leonard Marineland Marsland.

34
00:03:12,480 --> 00:03:22,190
But so many others too based on the context we can provide the appropriate validator.

35
00:03:22,860 --> 00:03:29,670
Now obviously when the validation fails we would like to display the validation method that we are going

36
00:03:29,670 --> 00:03:31,790
to provide in the form data.

37
00:03:32,190 --> 00:03:33,370
So maybe here.

38
00:03:37,200 --> 00:03:44,770
I say do you some gloss form control

39
00:03:47,970 --> 00:03:48,840
feedback

40
00:03:54,980 --> 00:04:06,910
which would give your if I would like to hide or so this section and see if if there are arrows What

41
00:04:06,920 --> 00:04:07,720
is the error.

42
00:04:07,800 --> 00:04:15,480
We have got Force name dot arrows.

43
00:04:19,600 --> 00:04:34,620
I put the condition first name dot the on dot.

44
00:04:35,460 --> 00:04:45,620
So if it is either that or if you just know the and if there are errors then I would like to display

45
00:04:45,620 --> 00:04:47,290
this section.

46
00:04:47,300 --> 00:04:48,080
It's the same thing.

47
00:04:48,080 --> 00:04:55,720
Actually we should provide full Lastly most just that this will change from first name to last.

48
00:05:01,040 --> 00:05:02,080
That's it.

49
00:05:02,760 --> 00:05:08,870
But what is there in that in that again there are two controls one I would like to have is the required

50
00:05:10,400 --> 00:05:13,190
but when required is that it should not perform.

51
00:05:13,290 --> 00:05:23,980
But invalidism So how do you control that we start indeed if when the pattern validation fails we can

52
00:05:23,980 --> 00:05:27,370
say that the first required validation fails.

53
00:05:27,730 --> 00:05:31,340
We can say errors thought required that we do.

54
00:05:31,830 --> 00:05:34,340
So here I'll get lost.

55
00:05:36,340 --> 00:05:38,480
Name is required.

56
00:05:39,770 --> 00:05:40,460
Likewise

57
00:05:43,840 --> 00:05:44,730
with what you know.

58
00:05:44,870 --> 00:05:48,160
The second one is back to first name

59
00:05:50,880 --> 00:05:51,430
not

60
00:05:54,440 --> 00:06:01,060
contain special Ghatak tools something similar.

61
00:06:01,140 --> 00:06:03,530
We do it for last name to

62
00:06:16,860 --> 00:06:17,650
that's it.

63
00:06:24,390 --> 00:06:28,670
It is very important here isn't it.

64
00:06:28,820 --> 00:06:30,620
No next round is

65
00:06:36,160 --> 00:06:45,440
no remove first name and you'll see first name is required for this go the last name.

66
00:06:45,450 --> 00:06:52,090
Last name is required with the focus but why divide it is gone.

67
00:06:59,640 --> 00:07:01,560
Last name there is something wrong.

68
00:07:01,710 --> 00:07:02,760
This is supposed to be

69
00:07:08,900 --> 00:07:10,430
rather we don't need this later

70
00:07:18,810 --> 00:07:19,950
that's it.

71
00:07:19,950 --> 00:07:24,960
So like this vulcanism condones can we provide enough code for this validation control that we can put

72
00:07:24,960 --> 00:07:35,270
some state to make it red color or say something like Clauss against a flood maybe dental.

73
00:07:35,620 --> 00:07:38,320
Same thing we can do for this

74
00:07:43,920 --> 00:07:46,570
and nothing more but to you I said

75
00:07:51,580 --> 00:07:59,920
firstly what was required the last name last name with a so like this ventilations form

76
00:08:02,810 --> 00:08:08,810
so these things are all that and I know you have to create the form control provide the right validation

77
00:08:10,100 --> 00:08:11,960
not required or mainland.

78
00:08:12,210 --> 00:08:13,470
Max Linton's or

79
00:08:17,830 --> 00:08:21,830
your have to qualify to use the word misled.

80
00:08:24,950 --> 00:08:33,480
Validating Stine's one group indeed gloss dandled or success.

81
00:08:33,500 --> 00:08:37,540
OK this can also be done first named are invalid.

82
00:08:37,540 --> 00:08:39,920
You can also have this as the form

83
00:08:44,920 --> 00:08:48,830
miscued is for adding new water or the greenboard.

84
00:08:49,120 --> 00:09:07,460
So we can actually do this and just copy it for the parent doc.

85
00:09:07,480 --> 00:09:08,660
Same thing here also

86
00:09:17,630 --> 00:09:18,560
if it is valid

87
00:09:24,980 --> 00:09:25,350
see

88
00:09:29,820 --> 00:09:30,500
small.

89
00:09:30,550 --> 00:09:36,550
Here I did copy paste does soon be lost to me.

90
00:09:43,970 --> 00:09:44,750
Not on this

91
00:09:51,070 --> 00:09:52,300
green color border.

92
00:09:54,390 --> 00:10:02,150
But it's not giving red color border green though it is showing Violet

93
00:10:08,000 --> 00:10:19,490
has the danger is not working the check clause is the will to as good as not.

94
00:10:19,540 --> 00:10:21,420
Ugh let's change it.

95
00:10:22,010 --> 00:10:27,740
So this is supposed to be an error and same thing here.

96
00:10:28,280 --> 00:10:29,940
So when we give that one the

97
00:10:33,380 --> 00:10:38,450
supposed there is an error like this.

98
00:10:38,570 --> 00:10:40,040
It is showing the red border.

99
00:10:40,340 --> 00:10:49,690
But if there is no error it is going to show a green border where the green border but of course Red

100
00:10:49,750 --> 00:10:58,260
or Green is also decided on the basis of whether we have already visited and if it is valid or invalid.

101
00:10:58,340 --> 00:11:01,800
That is also the condition which we are getting.

102
00:11:01,900 --> 00:11:05,300
So let's see what all we have seen in this example.

103
00:11:05,420 --> 00:11:13,050
We have first seen that these are the builtin attributes mainland and monomorphic to remain and so on

104
00:11:14,000 --> 00:11:20,570
and we add multiple validations to the form element without touching the template.

105
00:11:20,780 --> 00:11:28,010
But to form a control object and once we provide the form control object we need to display the error

106
00:11:28,010 --> 00:11:36,910
messages for displaying the error messages because the corresponding divs and here we can also make

107
00:11:36,910 --> 00:11:43,060
use of the style and it's the more beautification you want the more you can achieve by putting all these

108
00:11:43,060 --> 00:11:44,210
things.

109
00:11:44,260 --> 00:11:48,270
So this is all about fun validations of form elements.

110
00:11:48,550 --> 00:11:48,910
Thank you.

